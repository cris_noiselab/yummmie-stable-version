//
//  NLMentionInteractionCell.h
//  Yummmie
//
//  Created by bot on 9/23/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>

@protocol NLMentionInteractionDelegate;

@interface NLMentionInteractionCell : UITableViewCell<TTTAttributedLabelDelegate>

@property (weak, nonatomic) id<NLMentionInteractionDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIButton *userImageButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *dishButton;


- (void)setOwner:(NSString *)owner comment:(NSString *)comment;
@end

@protocol NLMentionInteractionDelegate <NSObject>

@required

- (void)openMentionOwnerWithTag:(NSInteger)tag;
- (void)openMentionYumWithTag:(NSInteger)tag;
- (void)openMentionUserWithString:(NSString *)username;

@end