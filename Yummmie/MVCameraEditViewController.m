//
//  MVCameraEditViewController.m
//  Yummmie
//
//  Created by Mauricio Ventura on 21/07/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import "MVCameraEditViewController.h"
#import "MVCameraViewController.h"

@interface MVCameraEditViewController ()

@end

@implementation MVCameraEditViewController
{
	UIImage *originalImage;
	UIImage *previewImage;
}
-(instancetype)initWithImage:(UIImage *)img
{
	previewImage = img;
	originalImage = img;
	return [super init];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationController.navigationBar.hidden = true;
	self.imageView.image = previewImage;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.navigationController.navigationBar.hidden = true;
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender
{
	[self.navigationController popViewControllerAnimated:true];
	
}

- (IBAction)imageDidPicked:(id)sender
{
	[(MVCameraViewController *)self.navigationController.viewControllers.firstObject imageDidPicked:previewImage];
	
}

- (IBAction)pinch:(UIPinchGestureRecognizer *)sender
{
	switch (sender.state)
	{
		case UIGestureRecognizerStateBegan:
		{
			
		}
			break;
		case UIGestureRecognizerStateFailed:
		case UIGestureRecognizerStateCancelled:

			break;
		case UIGestureRecognizerStateChanged:
		{
			dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
			dispatch_async(myQueue, ^
			{
    // Perform long running process
				CGAffineTransform transform = CGAffineTransformMakeScale(sender.scale, sender.scale);
				
				UIGraphicsBeginImageContext(previewImage.size);
				CGContextRef context = UIGraphicsGetCurrentContext();
				
				[previewImage drawInRect:CGRectMake(0, 0, previewImage.size.width, previewImage.size.height)];
				
				//    [newImage drawAtPoint:point];  draw without applying the transformation
				
				CGRect originalRect = CGRectMake(0, 0, previewImage.size.width * sender.scale, previewImage.size.height * sender.scale);
				
				
				CGContextConcatCTM(context, transform);
				CGContextDrawImage(context, originalRect, previewImage.CGImage);

				UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();

				dispatch_async(dispatch_get_main_queue(), ^
				{
					// Update the UI
					self.imageView.image = result;
				});
			});
			
		
		}
			break;
		default:
			break;
	}
}

- (IBAction)rotate:(UIRotationGestureRecognizer *)sender
{
	switch (sender.state)
	{
		case UIGestureRecognizerStateBegan:
			
			break;
		case UIGestureRecognizerStateFailed:
		case UIGestureRecognizerStateCancelled:
			
			break;
		case UIGestureRecognizerStateChanged:
		{
			NSLog(@"%f", sender.rotation);
		}
			break;
		default:
			break;
	}
}

@end
