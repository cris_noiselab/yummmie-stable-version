
//  NLInteractionViewContoroller.m
//  Yummmie
//
//  Created by bot on 8/12/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SSToolkit/SSToolkit.h>

#import "UIColor+NLColor.h"
#import "NLInteractionViewContoroller.h"
#import "NLInteraction.h"
#import "NLLikeInteractionViewCell.h"
#import "NLFollowerInteractionViewCell.h"
#import "NLCommentInteractionViewCell.h"
#import "NLProfileViewController.h"
#import "NLPhotoDetailViewController.h"
#import "NLFollowingPeopleInteractionViewCell.h"
#import "NLMentionInteractionCell.h"
#import "NLConstants.h"
#import "NLNotifications.h"
#import "NLSession.h"

static UIFont *cellInteractionFont;

static NSString * const kNLLikeInteractionCellIdentfier = @"kNLLikeInteractionCellIdentfier";
static NSString * const kNLFollowerInteractionViewCellIdentifier = @"kNLFollowerInteractionViewCellIdentifier";
static NSString * const kNLCommentinteractionViewCellIdentifier = @"kNLCommentinteractionViewCellIdentifier";
static NSString * const kNLFollowingPeopleInteractionViewCellIdenfier = @"kNLFollowingPeopleInteractionViewCellIdenfier";
static NSString * const kNLMentionInteractionViewCellIdentifier = @"kNLMentionInteractionViewCellIdentifier";

@interface NLInteractionViewContoroller ()<NLLikeInteractionViewCellDelegate,NLFollowerInteractionViewCellDelegate,NLCommentInteractionViewCellDelegate,NLFollowingPeopleInteractionViewCellDelegate,NLMentionInteractionDelegate>

@property (nonatomic, strong) UISegmentedControl *control;
@property (nonatomic, strong) NSMutableArray *me;
@property (nonatomic, strong) NSMutableArray *people;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UITableView *interactionTable;
@property (strong, nonatomic) NSMutableArray *meSizes;
@property (strong, nonatomic) NSMutableArray *peopleSizes;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) BOOL loggedIn;
@property (nonatomic) NSTimer *reloadTimer;
@property (weak, nonatomic) NSURLSessionDataTask *resumeTask;


- (void)segmentChange:(UISegmentedControl *)control;
- (void)updateData:(id)sender;
- (void)loadUserDataWithInteractionId:(NSInteger)interactionId;
- (void)loadPeopleDataWithInteractionId:(NSInteger)interactionId;
- (void)resignActive:(NSNotification *)notification;

- (void)userLogedIn:(NSNotification *)notification;
- (void)userLogedOut:(NSNotification *)notification;

- (CGSize)sizeForMe:(NLInteraction *)interaction;
- (CGSize)sizeForPeople:(NLInteraction *)interaction;

- (void)showActivityFeed:(NSNotification *)notification;
- (void)resumeReceived:(NSNotification *)notification;
- (void)receiveResumeNotification:(NSNotification *)notification;

@end

@implementation NLInteractionViewContoroller

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _me = [NSMutableArray array];
        _people = [NSMutableArray array];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogedIn:) name:kNLLoginNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogedOut:) name:kNLLogoutNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showActivityFeed:) name:kNLShowActivityFeed object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeReceived:) name:kNLShowInteractionPopUpNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeReceived:) name:kNLLoadLatestInteractions object:nil];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.refreshControl = [[UIRefreshControl alloc] init];
    //    self.refreshControl.tintColor = [UIColor colorWithRed:205.0/255.0 green:36.0f/255.0f blue:49.0/255.0 alpha:1.0];
    self.refreshControl.tintColor = [UIColor nl_applicationRedColor];
    [[self refreshControl] addTarget:self action:@selector(updateData:) forControlEvents:UIControlEventValueChanged];
    [[self interactionTable] insertSubview:[self refreshControl] atIndex:0];
    
    self.control = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"people_segment_title", nil),NSLocalizedString(@"me_segment_title", nil)]];
    self.control.tintColor = [UIColor whiteColor];
    CGRect controlRect= self.control.frame;
    controlRect.size.width = 300.0f;
    [self.control setFrame:controlRect];
    [self.control addTarget:self action:@selector(segmentChange:) forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = self.control;
    

    
    [[self interactionTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLLikeInteractionViewCell class]) bundle:nil] forCellReuseIdentifier:kNLLikeInteractionCellIdentfier];
    [[self interactionTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLFollowerInteractionViewCell class]) bundle:nil] forCellReuseIdentifier:kNLFollowerInteractionViewCellIdentifier];
    [[self interactionTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLCommentInteractionViewCell class]) bundle:nil] forCellReuseIdentifier:kNLCommentinteractionViewCellIdentifier];
    [[self interactionTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLFollowingPeopleInteractionViewCell class]) bundle:nil] forCellReuseIdentifier:kNLFollowingPeopleInteractionViewCellIdenfier];
    [[self interactionTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLMentionInteractionCell class]) bundle:nil] forCellReuseIdentifier:kNLMentionInteractionViewCellIdentifier];
    
    [[self interactionTable] setDelegate:self];
    [[self interactionTable] setDataSource:self];

    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    [footerView setBackgroundColor:[UIColor greenColor]];
    [self.interactionTable setTableFooterView:footerView];
    [[self interactionTable] setContentInset:UIEdgeInsetsMake(0, 0, 44, 0)];

    [[self control] setSelectedSegmentIndex:1];
    [self loadUserDataWithInteractionId:0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self interactionTable] reloadData];
    if (self.loggedIn)
    {
        self.loggedIn = NO;
        [self segmentChange:self.control];
    }
    
    if (!self.reloadTimer.isValid)
    {
        self.reloadTimer = [NSTimer scheduledTimerWithTimeInterval:80 target:self selector:@selector(updateData:) userInfo:nil repeats:YES];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLHidePendingInteractionsNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLHideInteractionPopUpNotification object:nil];
    
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"Interaction View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - receive Resume Notification

- (void)receiveResumeNotification:(NSNotification *)notification
{
    [self.resumeTask cancel];
    /*self.resumeTask =  [NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes) {
        
        NSDictionary *userInfo  = @{
                                    @"newComments":comments,
                                    @"newFollowers":followers,
                                    @"newLikes":likes
                                    };
        [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification
                                                            object:nil
                                                          userInfo:userInfo];
    }];*/
}

#pragma mark - Activity Feed Notification

- (void)showActivityFeed:(NSNotification *)notification
{
    [[self control] setSelectedSegmentIndex:1];
    [self loadUserDataWithInteractionId:[[[self me] firstObject] interactionId]];
}

#pragma mark - Reload User Interactions
- (void)resumeReceived:(NSNotification *)notification;
{
    [self loadUserDataWithInteractionId:[[[self me] firstObject] interactionId]];
}

#pragma mark - Resign Active
- (void)resignActive:(NSNotification *)notification
{
    if (self.reloadTimer.isValid)
    {
        [self.reloadTimer invalidate];
    }
}

#pragma mark - Reload Data helpers

- (void)loadUserDataWithInteractionId:(NSInteger)interactionId
{
    __weak NLInteractionViewContoroller *weakSelf = self;
    
    [NLInteraction getUserInteractionsAfter:interactionId withCompletionBlock:^(NSError *error, NSArray *interactions) {
        
        if (!error)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                for (NSInteger index = interactions.count-1; index >= 0; index--)
                {
                    NLInteraction *currentInteraction = interactions[index];
                    [[weakSelf me] insertObject:currentInteraction atIndex:0];
                    //calculate the size and add it to!
                    CGSize currentInteractionSize = [weakSelf sizeForMe:currentInteraction];
                    [[weakSelf meSizes] insertObject:[NSValue valueWithCGSize:currentInteractionSize] atIndex:0];
                }
                dispatch_async(dispatch_get_main_queue(), ^
							{
                    [[weakSelf spinner] stopAnimating];
                    [[weakSelf interactionTable] reloadData];
                    [[weakSelf interactionTable] reloadData];                    
                    [[weakSelf interactionTable] setAlpha:1.0f];
                    if ([[weakSelf refreshControl] isRefreshing])
                    {
                        [[weakSelf refreshControl] endRefreshing];
                    }
                });
            });
        }else
        {
            [[weakSelf spinner] stopAnimating];
            [weakSelf showErrorMessage:NSLocalizedString(@"cant_load_interactions", nil) withDuration:2.0f];
        }
        
    }];
}

- (void)loadPeopleDataWithInteractionId:(NSInteger)interactionId
{
    __weak NLInteractionViewContoroller *weakSelf = self;
    
    [NLInteraction getFollowingInteractionsAfter:interactionId withCompletionBlock:^(NSError *error, NSArray *interactions) {
        if (!error)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                
                //add the interactions to the people array
                for (NSInteger index = interactions.count-1; index >= 0; index --)
                {
                    NLInteraction *currentInteraction = interactions[index];
                    [[weakSelf people] insertObject:currentInteraction atIndex:0];
                    CGSize currentInteractionSize = [weakSelf sizeForPeople:currentInteraction];
                    [[weakSelf peopleSizes] insertObject:[NSValue valueWithCGSize:currentInteractionSize] atIndex:0];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[weakSelf spinner] stopAnimating];
                    [[weakSelf interactionTable] reloadData];
                    [[weakSelf interactionTable] setAlpha:1.0f];
                    
                    if ([[weakSelf refreshControl] isRefreshing])
                    {
                        [[weakSelf refreshControl] endRefreshing];
                    }
                    
                });
            });
        }else
        {
            [[weakSelf spinner] stopAnimating];
            [weakSelf showErrorMessage:NSLocalizedString(@"cant_load_interactions", nil) withDuration:2.0f];
            [[weakSelf refreshControl] endRefreshing];
        }
    }];
}

-(void)viewDidLayoutSubviews
{
    if ([self.interactionTable respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.interactionTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.interactionTable respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.interactionTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Load Data

- (void)updateData:(id)sender
{
    if (self.control.selectedSegmentIndex == 0)
    {
        //check if the array is empty to load everythin from start or from the latest one
        NSInteger interactionId = (self.people.count == 0)?0:[[[self people] firstObject] interactionId];
        [self loadPeopleDataWithInteractionId:interactionId];
        
    }else if(self.control.selectedSegmentIndex == 1)
    {
        //check if the array is empty to load everthing from start or from the latest one
        NSInteger interactionId = (self.me.count == 0)?0:[[[self me] firstObject] interactionId];
        [self loadUserDataWithInteractionId:interactionId];
    }
}

#pragma mark - get Sizes

- (CGSize)sizeForMe:(NLInteraction *)interaction
{
    __weak NLInteractionViewContoroller *weakSelf = self;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        weakSelf.meSizes = [NSMutableArray array];
    });
    
    NSString *contentString;
    CGFloat contentWidth;
    
    switch (interaction.type)
    {
        case NLLikeInteraction:
        {
            contentString  = [NSString stringWithFormat:@"@%@ %@",interaction.owner,NSLocalizedString(@"likes_user_yum_text", nil)];
            contentWidth = kInteractionCellWithRightPhotoWidth;
            break;
        }
			case NLRepostInteraction:
			{
				contentString  = [NSString stringWithFormat:@"@%@ %@",interaction.owner,NSLocalizedString(@"likes_user_yum_text", nil)];
				contentWidth = kInteractionCellWithRightPhotoWidth;
				break;
			}
        case NLCommentInteraction:
        {
            contentString = [NSString stringWithFormat:@"@%@ %@ %@",interaction.owner,NSLocalizedString(@"comment_on_you", nil),interaction.comment];
            contentWidth = kInteractionCellWithRightPhotoWidth;
            break;
        }
        case NLFollowInteraction:
        {
            contentString = [NSString stringWithFormat:@"@%@ %@",interaction.owner,NSLocalizedString(@"follow_user_yum_text", nil)];
            contentWidth = kOtherInteractionCellWidth;
            break;
        }
        case NLMentionInteraction:
        {
            contentString = [NSString stringWithFormat:@"@%@ %@: %@",interaction.owner,NSLocalizedString(@"mention_user_yum_text", nil),interaction.comment];
            contentWidth = kInteractionCellWithRightPhotoWidth;
            break;
        }
        default:
            break;
    }
    
    static dispatch_once_t anotherOnceToken;
    dispatch_once(&anotherOnceToken, ^{
        cellInteractionFont = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
    });
    
    CGRect newframe = [contentString boundingRectWithSize:CGSizeMake(contentWidth, 0.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellInteractionFont} context:nil];
    newframe.size.width = contentWidth;
    newframe.size.height+=(kTimeLabelHeight+kTimeLabelVerticalMargin+kTimeLabelYOrigin);
    
    return newframe.size;

}

- (CGSize)sizeForPeople:(NLInteraction *)interaction
{
    __weak NLInteractionViewContoroller *weakSelf = self;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        weakSelf.peopleSizes = [NSMutableArray array];
    });
    
    //Calculate the size fo the interaction
    NSString *contentString;
    CGFloat contentWidth = 0;
    switch (interaction.type)
    {
        case NLPeopleCommentInteraction:
        {
            contentString = [NSString stringWithFormat:@"@%@ %@ @%@: %@",interaction.owner,NSLocalizedString(@"comment_on_other", nil),interaction.recipient, interaction.comment];
            contentWidth = kInteractionCellWithRightPhotoWidth;
            break;
        }
        case NLPeopleFollowInteraction:
        {
            contentString = [NSString stringWithFormat:@"@%@ %@ @%@",interaction.owner,NSLocalizedString(@"follow_other_yum_text", nil),interaction.recipient];

            contentWidth = kOtherInteractionCellWidth;
            break;
        }
        case NLPeopleLikeInteraction:
        {
            contentString = [NSString stringWithFormat:@"@%@ %@",interaction.owner,NSLocalizedString(@"likes_a_user_yum_text", nil)];
            contentWidth = kInteractionCellWithRightPhotoWidth;
            break;
        }
			case NLPeopleFavInteraction:
			{
				contentString = [NSString stringWithFormat:@"@%@ %@",interaction.owner,NSLocalizedString(@"likes_a_user_yum_text", nil)];
				contentWidth = kInteractionCellWithRightPhotoWidth;
				break;
			}
			case NLPeopleRepostInteraction:
			{
				contentString = [NSString stringWithFormat:@"@%@ %@",interaction.owner,NSLocalizedString(@"likes_a_user_yum_text", nil)];
				contentWidth = kInteractionCellWithRightPhotoWidth;
				break;
			}
        case NLMentionInteraction:
        {
            contentString = @"";
            break;
        }
        default:
            break;
    }
    
    static dispatch_once_t anotherOnceToken;
    dispatch_once(&anotherOnceToken, ^{
        cellInteractionFont = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
    });
    
    CGRect newframe = [contentString boundingRectWithSize:CGSizeMake(contentWidth, 320.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellInteractionFont} context:nil];
    newframe.size.width = contentWidth;
    newframe.size.height+=(kTimeLabelHeight+kTimeLabelVerticalMargin+kTimeLabelYOrigin);
    return newframe.size;
}

#pragma mark - Value changed

- (void)segmentChange:(UISegmentedControl *)control
{
    [self.refreshControl endRefreshing];
    
    if (control.selectedSegmentIndex == 0)
    {
        if ([self.people count] == 0)
        {
            [[self spinner] startAnimating];
            [[self interactionTable] setAlpha:0.0f];
            [self loadPeopleDataWithInteractionId:0];

        }else
        {
            [[self interactionTable] setAlpha:1.0f];
            [[self interactionTable] reloadData];
        }
    }else if (control.selectedSegmentIndex == 1)
    {
        if ([self.me count] == 0)
        {
            [self.spinner startAnimating];
            [[self interactionTable] setAlpha:0.0f];
            [self loadUserDataWithInteractionId:0];
            
        }else
        {
            [[self interactionTable] setAlpha:1.0f];
            [[self interactionTable] reloadData];
        }
        
    }
}
#pragma mark - User login/logout 
- (void)userLogedIn:(NSNotification *)notification
{
    self.loggedIn = YES;
}
- (void)userLogedOut:(NSNotification *)notification
{
    [[self people] removeAllObjects];
    [[self me] removeAllObjects];
    [[self meSizes] removeAllObjects];
    [[self peopleSizes] removeAllObjects];
    
    [[self interactionTable] reloadData];
    self.loggedIn = NO;
    [[self reloadTimer] invalidate];
}

#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.control.selectedSegmentIndex == 0)
    {
        return [self.people count];
    }else
    {
        return [self.me count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLInteraction *interaction;
    BOOL isMe = (self.control.selectedSegmentIndex == 1)?YES:NO;
    
    if ([[self control] selectedSegmentIndex] == 1)
    {
        interaction = (indexPath.row<[[self me] count])?[[self me] objectAtIndex:indexPath.row]:[[self me] objectAtIndex:indexPath.row-1];
    }else
    {
        interaction = (indexPath.row  < [[self people] count])?[[self people] objectAtIndex:indexPath.row]:[[self people] objectAtIndex:indexPath.row-1];
    }
    
    switch (interaction.type)
    {
        case NLFollowInteraction:
        {
            NLFollowerInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLFollowerInteractionViewCellIdentifier forIndexPath:indexPath];
            [[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [cell setTag:indexPath.row];
            [cell setUser:interaction.owner];
            [cell setDelegate:self];
            [[cell timeLabel] setText:interaction.created];
            return cell;
        }
        case NLLikeInteraction:
        {
            NLLikeInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLLikeInteractionCellIdentfier forIndexPath:indexPath];
            
            [[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            
            [[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
            [cell setTag:indexPath.row];
            [cell setDelegate:self];
            [cell setUser:interaction.owner isMe:isMe];
            [[cell timeLabel] setTextColor:[UIColor nl_colorWith255Red:180 green:180 blue:180 andAlpha:255]];
					[[cell timeLabel] setText:interaction.created];
            return cell;
        }
        case NLCommentInteraction:
        {
            NLCommentInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLCommentinteractionViewCellIdentifier forIndexPath:indexPath];
            [cell setTag:indexPath.row];
            [[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
            [cell setOwner:interaction.owner comment:interaction.comment];
            [cell setDelegate:self];
					[[cell timeLabel] setText:interaction.created];
            [[cell timeLabel] setTextColor:[UIColor nl_colorWith255Red:180 green:180 blue:180 andAlpha:255]];
            return cell;
        }
        case NLPeopleFollowInteraction:
        {
            NLFollowingPeopleInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLFollowingPeopleInteractionViewCellIdenfier forIndexPath:indexPath];
            
            [[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [cell setTag:indexPath.row];
					[[cell timeLabel] setText:interaction.created];

					[cell setOwner:@{@"userName": interaction.owner, @"userId": [NSNumber numberWithInteger:interaction.ownerId]} andTarget: @{@"userName": interaction.recipient, @"userId": [NSNumber numberWithInteger:interaction.recipientId]}];
            [cell setDelegate:self];
            [[cell timeLabel] setTextColor:[UIColor nl_applicationRedColor]];
            return cell;
        }
        case NLPeopleLikeInteraction:
        {
            NLLikeInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLLikeInteractionCellIdentfier forIndexPath:indexPath];
            
            [[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            
            [[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
            [cell setTag:indexPath.row];
            [cell setDelegate:self];
            [cell setUser:interaction.owner isMe:isMe];
					[[cell timeLabel] setText:interaction.created];
            [[cell timeLabel] setTextColor:[UIColor nl_applicationRedColor]];
            return cell;
        }
			case NLPeopleFavInteraction:
			{
				NLLikeInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLLikeInteractionCellIdentfier forIndexPath:indexPath];
				
				[[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
				
				[[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
				[cell setTag:indexPath.row];
				[cell setDelegate:self];
				[cell setUser:interaction.owner isMe:isMe];
				[[cell timeLabel] setText:interaction.created];
				[[cell timeLabel] setTextColor:[UIColor nl_applicationRedColor]];
				return cell;
			}
			case NLPeopleRepostInteraction:
			{
				NLLikeInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLLikeInteractionCellIdentfier forIndexPath:indexPath];
				
				[[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
				
				[[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
				[cell setTag:indexPath.row];
				[cell setDelegate:self];
				[cell setUser:interaction.owner isMe:isMe];
				[[cell timeLabel] setText:interaction.created];
				[[cell timeLabel] setTextColor:[UIColor nl_applicationRedColor]];
				return cell;
			}
        case NLPeopleCommentInteraction:
        {
            NLCommentInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLCommentinteractionViewCellIdentifier forIndexPath:indexPath];
            [cell setTag:indexPath.row];
            [[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
            [cell setOwner:interaction.owner comment:interaction.comment andTarget:interaction.recipient];
            [cell setDelegate:self];
					[[cell timeLabel] setText:interaction.created];
            [[cell timeLabel] setTextColor:[UIColor nl_applicationRedColor]];
            return cell;
        }
        case NLMentionInteraction:
        {
            NLMentionInteractionCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLMentionInteractionViewCellIdentifier forIndexPath:indexPath];
            [cell setTag:indexPath.row];
            [[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
					[[cell timeLabel] setText:interaction.created];
            [cell setOwner:interaction.owner comment:interaction.comment];
            [cell setDelegate:self];
					[[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];

            return cell;
        }
			case NLRepostInteraction:
			{
				
				
				
				
				NLLikeInteractionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLLikeInteractionCellIdentfier forIndexPath:indexPath];
				cell.repost = true;
				[[cell userImageButton] sd_setImageWithURL:[NSURL URLWithString:interaction.ownerAvatar] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
				
				[[cell dishButton] sd_setImageWithURL:[NSURL URLWithString:interaction.imageURL] forState:UIControlStateNormal placeholderImage:nil];
				[cell setTag:indexPath.row];
				[cell setDelegate:self];
				[cell setUser:interaction.owner isMe:isMe];
				[[cell timeLabel] setTextColor:[UIColor nl_colorWith255Red:180 green:180 blue:180 andAlpha:255]];
				[[cell timeLabel] setText:interaction.created];
				return cell;
			}

        default:
            break;
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (self.control.selectedSegmentIndex)
    {
        case 0:
            {
                return [[self.peopleSizes objectAtIndex:indexPath.row] CGSizeValue].height;
            }
        case 1:
            {
							
                return [[self.meSizes objectAtIndex:indexPath.row] CGSizeValue].height;
            }
        default:
            return 0;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - NLLikeInteractionCellViewDelegate

- (void)openOwnerWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
	NSLog(@"%ld", (long)interaction.ownerId);
	
    NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUserId:[interaction ownerId] andCloseButton:false];
    [profile setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:profile animated:YES];
}

- (void)openYumWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
    NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:interaction.yumId];
    [detail setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:detail animated:YES];
    
}

#pragma mark - NLFollowingInteractionViewCellDelegate

- (void)openFollowOwnerWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
	NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUserId:[interaction ownerId] andCloseButton:false];
    [profile setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:profile animated:YES];
}


#pragma mark - NLCommentInteractionViewCellDelegate
- (void)openCommentOwnerWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
	//NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUserId:[interaction ] andCloseButton:false];

		NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUserId:[interaction ownerId] andCloseButton:false];
    [profile setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:profile animated:YES];
}

- (void)openUserWithName:(NSString *)username
{
		NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUsername:username];
	[profile setHidesBottomBarWhenPushed:YES];
	[self.navigationController pushViewController:profile animated:YES];
}
- (void)openCommentYumWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
    NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:interaction.yumId];
    [detail setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)openCommentUserWithString:(NSString *)username
{
	NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUsername:username];
	[profile setHidesBottomBarWhenPushed:YES];
	[self.navigationController pushViewController:profile animated:YES];
    
}

#pragma mark - NLMentionCellDelegate 

- (void)openMentionOwnerWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
	NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUserId:[interaction ownerId] andCloseButton:false];
    [profile setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:profile animated:YES];
}

- (void)openMentionYumWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
    NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:interaction.yumId];
    [detail setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)openMentionUserWithString:(NSString *)username
{
    NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUsername:username];
    [profile setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:profile animated:YES];
}
#pragma mark - NLFollowingPeopleInteractionViewCellDelegate

- (void)openFollowingPeopleUserId:(NSString *)username
{
    NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUsername:username];
    [profile setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:profile animated:YES];
		 
}

- (void)openFollowingPeopleOwnerWithTag:(NSInteger)tag
{
    NLInteraction *interaction = (self.control.selectedSegmentIndex == 1)?[[self me] objectAtIndex:tag]:[[self people] objectAtIndex:tag];
    NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUserId:[interaction ownerId] andCloseButton:false];
    [profile setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:profile animated:YES];
}
- (void)openUserWithId:(NSInteger)userId
{
	NLProfileViewController *profile = [[NLProfileViewController alloc] initWithUserId:userId andCloseButton:false];
	[profile setHidesBottomBarWhenPushed:YES];
	[self.navigationController pushViewController:profile animated:YES];
}

#pragma mark - override

- (void)scrollToTop
{
    [self.interactionTable setContentOffset:CGPointZero animated:YES];
}

@end
