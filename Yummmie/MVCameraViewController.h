//
//  MVCameraViewController.h
//  Yummmie
//
//  Created by Mauricio Ventura on 20/07/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "DBCameraViewController.h"
#import <CoreLocation/CoreLocation.h>

@protocol MVCameraViewControllerDelegate <NSObject>

@required

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata;
-(void)closeCamera;
//-(void)mv

@end

@interface MVCameraViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, DBCameraViewControllerDelegate, CLLocationManagerDelegate>

@property double precision;
@property NSUInteger countdown;
@property CGSize size;

@property bool withGallery;
@property bool canEdit;
@property bool showControls;

@property (weak, nonatomic) IBOutlet UIView *area;
@property (weak, nonatomic) IBOutlet UIView *ball;
@property (strong) UILabel *counter;
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (strong) UIColor *areaOkColor;
@property (strong) UIColor *areaWrongColor;
@property (weak, nonatomic) IBOutlet UIView *topControls;
@property (weak, nonatomic) IBOutlet UIView *bottonControls;
@property (weak, nonatomic) IBOutlet UIView *dotsView;
@property (weak, nonatomic) IBOutlet UIButton *shotBtn;

@property (weak, nonatomic) IBOutlet UIImageView *grid;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *galleryBtn;

@property (weak, nonatomic) IBOutlet UIView *topIndicator;
@property (weak, nonatomic) IBOutlet UIView *bottomIndicator;


@property (weak, nonatomic) id<MVCameraViewControllerDelegate> delegate;

- (IBAction)close:(id)sender;
- (IBAction)gallery:(id)sender;
- (IBAction)takePhoto:(id)sender;
- (IBAction)focusCenter:(UITapGestureRecognizer *)sender;

-(void)imageDidPicked:(UIImage *)image;

-(void)showEditorWithImage:(UIImage *)img;
@end

