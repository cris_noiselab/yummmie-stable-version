//
//  NLShareSettingsCellView.m
//  Yummmie
//
//  Created by bot on 1/8/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLShareSettingsCellView.h"
#import "UIColor+NLColor.h"

@interface NLShareSettingsCellView ()

@property (nonatomic,readwrite) NLShareSettingCellViewType type;
@property (nonatomic, getter=isLinked) BOOL linked;

@property (weak, nonatomic) IBOutlet UIButton *linkButton;
@property (weak, nonatomic) IBOutlet UILabel *socialNetworkLabel;


- (IBAction)linkAction:(id)sender;

@end

@implementation NLShareSettingsCellView

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellType:(NLShareSettingCellViewType)type withLinkedValue:(BOOL)linkedFlag
{
    
//    "social_network_facebook_title"="Facebook";
//    "social_network_twitter_title"="Twitter";
//    "social_network_instagram_title"="Instagram";
//    "social_network_foursquare"="Foursquare";
    
    [self setLinked:linkedFlag];
    [self setType:type];
    
    NSString *linkTitle = ([self isLinked])?NSLocalizedString(@"authorization_unlink_title", nil):NSLocalizedString(@"authorization_link_title", nil);
    [[self linkButton] setTitle:linkTitle forState:UIControlStateNormal];
    UIColor *color = ([self isLinked])?[UIColor grayColor]:[UIColor nl_applicationRedColor];
    [[self linkButton] setTitleColor:color forState:UIControlStateNormal];
    
    switch (type)
    {
        case NLShareSettingCellViewTypeFacebook:
            {
                [[self socialNetworkLabel] setText:NSLocalizedString(@"social_network_facebook_title", nil)];
                UIImage *socialImage = ([self isLinked])?[UIImage imageNamed:@"bot_link_fb_a"]:[UIImage imageNamed:@"bot_link_fb_b"];
                [[self imageView] setImage:socialImage];
                break;
            }
        case NLShareSettingCellViewTypeTwitter:
            {
                [[self socialNetworkLabel] setText:NSLocalizedString(@"social_network_twitter_title", nil)];
                UIImage *socialImage = ([self isLinked])?[UIImage imageNamed:@"bot_link_tw_a"]:[UIImage imageNamed:@"bot_link_tw_b"];
                [[self imageView] setImage:socialImage];
                break;
            }
        default:
            break;
    }
}

#pragma mark - Link Action

- (void)linkAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(updateLinkStatusWithValue:withCellType:)])
    {
        [[self delegate] updateLinkStatusWithValue:[self isLinked] withCellType:[self type]];
    }
}
@end
