//
//  NLLoadingReusableView.h
//  Yummmie
//
//  Created by bot on 5/19/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLLoadingReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
