//
//  NLShareSettingsCellView.h
//  Yummmie
//
//  Created by bot on 1/8/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, NLShareSettingCellViewType)
{
    NLShareSettingCellViewTypeFacebook,
    NLShareSettingCellViewTypeTwitter
};

@protocol NLShareSettingCellViewDelegate;

@interface NLShareSettingsCellView : UITableViewCell

@property (nonatomic,readonly) NLShareSettingCellViewType type;
@property (nonatomic, weak) id<NLShareSettingCellViewDelegate>delegate;

- (void)setCellType:(NLShareSettingCellViewType)type withLinkedValue:(BOOL)linkedFlag;


@end

@protocol NLShareSettingCellViewDelegate <NSObject>

@required

- (void)updateLinkStatusWithValue:(BOOL)isLinked withCellType:(NLShareSettingCellViewType)type;

@end