//
//  NLHashtagFooter.h
//  Yummmie
//
//  Created by bot on 6/24/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLHashtagFooter : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
