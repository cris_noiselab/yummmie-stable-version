//
//  NLInteraction.h
//  Yummmie
//
//  Created by bot on 8/20/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"
#import <SSToolkit/SSToolkit.h>

typedef NS_ENUM(NSInteger, NLInteractionType)
{
    NLCommentInteraction,
    NLLikeInteraction,
		NLRepostInteraction,
		NLFavInteraction,
    NLFollowInteraction,
    NLMentionInteraction,
    NLPeopleLikeInteraction,
    NLPeopleFollowInteraction,
    NLPeopleCommentInteraction,
		NLPeopleRepostInteraction,
		NLPeopleFavInteraction,
    NLErrorInteraction
};


@interface NLInteraction : NSObject

@property (nonatomic) NSInteger interactionId;
@property (nonatomic,assign) NLInteractionType type;
@property (nonatomic,copy) NSString *owner;
@property (nonatomic) NSInteger ownerId;
@property (nonatomic,copy) NSString *recipient;
@property (nonatomic) NSInteger recipientId;

@property (nonatomic, copy) NSString *imageURL;
@property (nonatomic, copy) NSString *ownerAvatar;
@property (nonatomic, copy) NSString *recipientAvatar;
@property (nonatomic, strong) NSString *created;
@property (nonatomic) NSInteger yumId;
@property (nonatomic, copy) NSString * comment;

- (instancetype)initWithDictionary:(NSDictionary *)interactionDict;
- (instancetype)initWithDictionaryForPeopleInteraction:(NSDictionary *)interactionDict;

+ (NSURLSessionDataTask *)getUserInteractionsAfter:(NSInteger)interactionId withCompletionBlock:(void (^)(NSError *error, NSArray *interactions))block;

+ (NSURLSessionDataTask *)getFollowingInteractionsAfter:(NSInteger)interactionId withCompletionBlock:(void (^)(NSError *error, NSArray *interactions))block;

+ (NSURLSessionDataTask *)getResumeWithCompletionBlock:(void (^)(NSError *error,NSNumber *followers, NSNumber *comments, NSNumber *likes))block;

@end
