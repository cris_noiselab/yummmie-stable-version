//
//  NLCustomSearchViewController.m
//  Yummmie
//
//  Created by bot on 8/5/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLCustomSearchViewController.h"
#import "Foursquare2.h"
#import "FSConverter.h"
#import "UIColor+NLColor.h"
#import "NLVenue.h"

@interface NLCustomSearchViewController ()

@property (nonatomic, copy) NSString *text;
@property (nonatomic, strong) NSArray *db_places;
@property (nonatomic, strong) NSArray *fb_places;

@property (nonatomic, weak) NSOperation *lastSearchOperation;
@property (nonatomic, strong) CLLocation *location;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) NSURLSessionDataTask *task;

@end

@implementation NLCustomSearchViewController

- (id)initWithSearchText:(NSString *)searchText andLocation:(id)location
{
	if (self = [super initWithNibName:NSStringFromClass([NLCustomSearchViewController class]) bundle:nil])
	{
		_text = searchText;
		self.edgesForExtendedLayout = UIRectEdgeNone;
		self.title = NSLocalizedString(@"custom_location_search_title", nil);
		_location = location;
		
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	[[self tableView] setDelegate:self];
	[[self tableView] setDataSource:self];
	[self startSearchWithString:[self text]];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
	//Google Analytics
	[super viewWillAppear:animated];
	NSString *name = [NSString stringWithFormat:@"Buscar más lugares cercanos View"];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)startSearchWithString:(NSString *)string
{
	[self showLoadingSpinner];
	[self.lastSearchOperation cancel];
	[self.task cancel];
	
	
	
	__weak NLCustomSearchViewController *weakSelf = self;
	
	self.task = [NLVenue nearestVenuesWithQuery:string latitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude andCompletionBlock:^(NSError *error, NSDictionary *venues) {
		self.db_places = [venues objectForKey:@"db_venues"];
		self.fb_places = [venues objectForKey:@"fb_venues"];
		[weakSelf.tableView reloadData];

		/*
		self.lastSearchOperation = [Foursquare2
																venueSearchNearByLatitude:@(self.location.coordinate.latitude)
																longitude:@(self.location.coordinate.longitude)
																query:string
																limit:nil
																intent:intentBrowse
																radius:@(1000)
																categoryId:@"4d4b7105d754a06374d81259"
																callback:^(BOOL success, id result){
																	if (success)
																	{
																		NSDictionary *dic = result;
																		NSArray *venues = [dic valueForKeyPath:@"response.venues"];
																		FSConverter *converter = [[FSConverter alloc] init];
																		foursquareVenues =  [converter convertToVenues:venues];
																		if (!foursquareVenues)
																		{
																			[self setPlaces:(yummmieVenues)?yummmieVenues:[NSArray array]];
																		}else
																		{
																			//merge this and return
																			if (yummmieVenues)
																			{
																				NSMutableArray *mergedVenues = [NSMutableArray arrayWithArray:yummmieVenues];
																				for (NLVenue *venue in foursquareVenues)
																				{
																					if (![mergedVenues containsObject:venue])
																					{
																						[mergedVenues addObject:venue];
																					}
																				}
																				
																				[self setPlaces:mergedVenues];
																			}else
																			{
																				[self setPlaces:foursquareVenues];
																			}
																		}
																		
																		[weakSelf.tableView reloadData];
																	} else
																	{
																		[[weakSelf tableView] reloadData];
																	}
																	dispatch_async(dispatch_get_main_queue(), ^{
																		[weakSelf hideLoadingSpinner];
																	});
																	
																}];*/
		
	}];
		 
	
}

#pragma mark - UITableVieWDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	if (self.fb_places.count > 0)
	{
		return 2;
	}
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0) {
		if ([[self db_places] count]>0)
		{
			return [[self db_places] count];
		}else
		{
			return 1;
		}
	}
	else {
		if ([[self fb_places] count]>0)
		{
			return [[self fb_places] count];
		}else
		{
			return 1;
		}
	}

	
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (section == 1) {
		return NSLocalizedString(@"otros_lugares", nil);
	}
	return @"";
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	if (indexPath.section == 0)
	{
		if (![self db_places])
		{
			//We are searching stuff
			static NSString *searchingCellIdentifier = @"searchingCellIdentifier";
			cell  = [tableView dequeueReusableCellWithIdentifier:searchingCellIdentifier];
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchingCellIdentifier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
			}
			[[cell textLabel] setText:NSLocalizedString(@"loading_venues_string", nil)];
		}else if([[self db_places] count] == 0)
		{
			//we found nothing!
			static NSString *errorCellIdentIdenfier = @"errorCellIdentIdenfier";
			cell = [tableView dequeueReusableCellWithIdentifier:errorCellIdentIdenfier];
			
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:errorCellIdentIdenfier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
			}
			
			[[cell textLabel] setText:NSLocalizedString(@"error_loading_venues_string", nil)];
		}else
		{
			//show the results
			static NSString *resultsFoundCellIdentifier = @"ResultsFoundIdentifier";
			cell = [tableView dequeueReusableCellWithIdentifier:resultsFoundCellIdentifier];
			
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:resultsFoundCellIdentifier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
				[[cell detailTextLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
				[[cell detailTextLabel] setTextColor:[UIColor nl_colorWith255Red:166 green:166 blue:166 andAlpha:255]];
			}
			
			NLVenue *venue = [[self db_places] objectAtIndex:[indexPath row]];
			// FSVenue *venue = [[self places] objectAtIndex:[indexPath row]];
			[[cell textLabel] setText:venue.name];
			[[cell detailTextLabel] setText:venue.street];
			
		}

	}
	else
	{
		if (![self fb_places])
		{
			//We are searching stuff
			static NSString *searchingCellIdentifier = @"searchingCellIdentifier";
			cell  = [tableView dequeueReusableCellWithIdentifier:searchingCellIdentifier];
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchingCellIdentifier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
			}
			[[cell textLabel] setText:NSLocalizedString(@"loading_venues_string", nil)];
		}else if([[self fb_places] count] == 0)
		{
			//we found nothing!
			static NSString *errorCellIdentIdenfier = @"errorCellIdentIdenfier";
			cell = [tableView dequeueReusableCellWithIdentifier:errorCellIdentIdenfier];
			
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:errorCellIdentIdenfier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
			}
			
			[[cell textLabel] setText:NSLocalizedString(@"error_loading_venues_string", nil)];
		}else
		{
			//show the results
			static NSString *resultsFoundCellIdentifier = @"ResultsFoundIdentifier";
			cell = [tableView dequeueReusableCellWithIdentifier:resultsFoundCellIdentifier];
			
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:resultsFoundCellIdentifier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
				[[cell detailTextLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
				[[cell detailTextLabel] setTextColor:[UIColor nl_colorWith255Red:166 green:166 blue:166 andAlpha:255]];
			}
			
			NLVenue *venue = [[self fb_places] objectAtIndex:[indexPath row]];
			// FSVenue *venue = [[self places] objectAtIndex:[indexPath row]];
			[[cell textLabel] setText:venue.name];
			[[cell detailTextLabel] setText:venue.street];
			
		}
		
	}

	return cell;
}

#pragma mark - UITableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	__weak NLCustomSearchViewController *weakSelf = self;
	
	if ([[self db_places] count] > 0 || [[self fb_places] count] > 0)
	{
		NLVenue *venue;
		BOOL fb = false;
		if (indexPath.section == 0)
		{
			venue = [[self db_places] objectAtIndex:[indexPath row]];
			[[self delegate] didSelectVenue:venue andIndexPath:indexPath];

		}
		if (indexPath.section == 1)
		{
			venue = [[self fb_places] objectAtIndex:[indexPath row]];
			NSLog(@"%@", venue);
			[NLVenue createVenuewithId:venue.facebook_place_id venueName:venue.name andCompletionBlock:^(NSError *error,NLVenue *nvenue) {
				
				if (!error)
				{
					if ([self delegate] && [[self delegate] respondsToSelector:@selector(didSelectVenue:andIndexPath:)])
					{
						[[self delegate] didSelectVenue:nvenue andIndexPath:indexPath];
					}
				}else
				{
					dispatch_async(dispatch_get_main_queue(), ^{
						[weakSelf hideLoadingSpinner];
						[weakSelf showErrorMessage:NSLocalizedString(@"error_creating_venue", nil) withDuration:3.0f];
						[[weakSelf tableView] deselectRowAtIndexPath:indexPath animated:YES];
					});
				}
			}];
			fb = true;
		}
		/*
		[self showLoadingSpinner];
		if ([venue isFromFoursquare])
		{
			[NLVenue createVenuewithId:venue.facebook_place_id venueName:venue.name andCompletionBlock:^(NSError *error,NLVenue *venue) {
				
				if (!error)
				{
					if ([self delegate] && [[self delegate] respondsToSelector:@selector(didSelectVenue:andIndexPath:)])
					{
						[[self delegate] didSelectVenue:venue andIndexPath:indexPath];
					}
				}else
				{
					dispatch_async(dispatch_get_main_queue(), ^{
						[weakSelf hideLoadingSpinner];
						[weakSelf showErrorMessage:NSLocalizedString(@"error_creating_venue", nil) withDuration:3.0f];
						[[weakSelf tableView] deselectRowAtIndexPath:indexPath animated:YES];
					});
				}
			}];
		}
		else
		{
			if ([self delegate] && [[self delegate] respondsToSelector:@selector(didSelectVenue:andIndexPath:)])
			{
				[[self delegate] didSelectVenue:venue andIndexPath:indexPath];
			}
		}
		 */
		
	}
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([[self db_places] count] > 0 || [[self fb_places] count] > 0)
	{
		return indexPath;
	}
	return nil;
}
@end
