//
//  NLConstants.h
//  Yummmie
//
//  Created by bot on 6/5/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kTermsOfUseURL;
extern NSString * const kPrivacyPoliceURL;

extern const CGFloat errorMessageViewHeight;

extern const CGFloat kHudWidth;
extern const CGFloat kHudHeight;

extern const CGFloat kHudAnimationFrameWidth;
extern const CGFloat kHudAnimationFrameHeight;
extern const CGFloat kHudAnimationDuration;

extern const CGFloat kTimeLineViewCellHeight;
extern const CGFloat kTimeLineViewCellHeightBottomOffset;
extern const CGFloat kTimeLineViewCellBottomBarHeight;
extern const CGFloat kTimeLineViewCellBottomBarTopOffset;

extern const CGFloat kCommentViewCellHeight;
extern const CGFloat kCommentViewCellBottomOffset;

extern NSString * const kUsernameScheme;
extern NSString * const kHashtagScheme;

extern const CGFloat kTabBarHeight;
extern const CGFloat kDishCollectionViewBottomInset;
extern const CGFloat kDishDistanceToStartLoading;

extern NSString * const kPNGMimeType;
extern NSString * const kJPGMimeType;

extern NSString * const kJSONUserEmailKey;
extern NSString * const kJSONUserUsernameKey;
extern const CGFloat kKeyboardHeight;

extern NSString * const kYummmieGalleryName;

extern const CGFloat kTimeLabelYOrigin;
extern const CGFloat kTimeLabelXPadding;

extern const CGFloat kInteractionCellWithRightPhotoWidth;
extern const CGFloat kInteractionFollowingWidth;
extern const CGFloat kOtherInteractionCellWidth;
extern const CGFloat kTimeLabelHeight;
extern const CGFloat kTimeLabelVerticalMargin;

extern NSString * const kReloadTimelineKey;

extern const CGFloat kTimeLineCellCommentSeparation;
extern const CGFloat kTimeLineCellMoreButtonTopOffset;
extern const CGFloat kTImeLineCellCommentHeight;
extern const CGFloat kTImeLineCellMinimumCommentHeight;
extern const CGFloat kTimeLineCellMoreCommentsButtonHeight;


extern const CGFloat kTimeLineCellMinCommentLabelHeight;
extern const CGFloat kTimeLineCellMaxCommentLabelHeight;

extern const CGFloat kTimelineCellBigCommentOffsetY;

typedef NS_ENUM(NSInteger, UserButtonType)
{
    FOLLOWING,
    NOTFOLLOWING,
    PENDING
};

typedef NS_ENUM(NSInteger, NLSearchScope)
{
    NLSearchScopeUser,
    NLSearchScopeHashtag,
    NLSearchScopeVenue,
    NLSearchScopeDish
};

typedef NS_ENUM(NSInteger, NLMessageMode)
{
    NLMessageModeNone,
    NLMessageModeMention,
    NLMessageModeHashtag
};
