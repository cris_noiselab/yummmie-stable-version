//
//  NLPushNotificationConstants.m
//  Yummmie
//
//  Created by bot on 1/22/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLPushNotificationConstants.h"



NSString * const kNLPushNotificationActionIdKey = @"action_id";
NSString * const kNLPushNotificationEntityKey = @"entity_id";