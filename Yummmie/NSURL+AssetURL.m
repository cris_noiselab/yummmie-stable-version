//
//  NSURL+AssetURL.m
//  Yummmie
//
//  Created by bot on 7/14/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NSURL+AssetURL.h"

static NSString * const kAppPathURL = @"http://yummmie-test.herokuapp.com";

@implementation NSURL (AssetURL)


+ (NSURL *)urlForAssetPath:(NSString *)pathToAsset
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kAppPathURL,pathToAsset]];
}

@end
