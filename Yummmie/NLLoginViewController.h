//
//  NLLoginViewController.h
//  Yummmie
//
//  Created by bot on 6/9/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import "TTTAttributedLabel.h"
#import "NLYummieApiClient.h"

@interface NLLoginViewController : NLBaseViewController<UITextFieldDelegate,TTTAttributedLabelDelegate>

@end
