//
//  NLCommentViewCell.m
//  Yummmie
//
//  Created by bot on 6/25/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLCommentViewCell.h"
#import "UIColor+NLColor.h"
#import "NLConstants.h"

static const CGFloat kCommentYOffset = 10.0f;

@interface NLCommentViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *usernameButton;
@property (strong, nonatomic) TTTAttributedLabel *commentLabel;


- (IBAction)usernameAction:(id)sender;
- (IBAction)profileThumbAction:(id)sender;
- (void)updateUsername:(NSString *)username;

@end

@implementation NLCommentViewCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self addObserver:self forKeyPath:@"username" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    return  self;
}

- (void)awakeFromNib
{

    self.profileThumbButton.layer.cornerRadius =  self.profileThumbButton.frame.size.height/2.0f;
    self.profileThumbButton.layer.masksToBounds = YES;
    
    //setup comment label
    CGRect usernameButtonFrame = self.usernameButton.frame;
    
    self.commentLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(usernameButtonFrame.origin.x,usernameButtonFrame.origin.y+usernameButtonFrame.size.height+kCommentYOffset, 0, 35.0f)];
    UIColor *commentColor = [UIColor nl_colorWith255Red:89.0f green:92.0f blue:105.0f andAlpha:255.0f];
    
    [[self commentLabel] setDelegate:self];    
    [self.commentLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.commentLabel setMinimumScaleFactor:0.5f];
    [self.commentLabel setNumberOfLines:0];
    [self.commentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.commentLabel setTextColor:commentColor];
    [self.commentLabel adjustsFontSizeToFitWidth];
    [self.commentLabel setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    
    NSDictionary *linkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self commentLabel] setLinkAttributes:linkAttributes];
    
    NSDictionary *activeLinkAttributes = @{(id)kCTForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    [[self commentLabel] setActiveLinkAttributes:activeLinkAttributes];
    NSDictionary *inactiveLinkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self commentLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    
    [self.contentView addSubview:self.commentLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"username"];
}

#pragma mark - Actions

- (IBAction)usernameAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openUser:)])
    {
        NSString *username = [[[self usernameButton] titleLabel] text];
        NSString *cleanUsername = [[username componentsSeparatedByString:@"@"] objectAtIndex:1];
        [[self delegate] openUser:cleanUsername];
    }
}

- (IBAction)profileThumbAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openUser:)])
    {
        NSString *username = [[[self usernameButton] titleLabel] text];
        NSString *cleanUsername = [[username componentsSeparatedByString:@"@"] objectAtIndex:1];
        [[self delegate] openUser:cleanUsername];
    }
}

#pragma  mark - KVO 

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"username"])
    {
        NSString *newUsername = [change objectForKey:@"new"];
        if ([newUsername length]>0)
        {
            [self updateUsername:newUsername];
        }
    }
}

#pragma mark - Update UI

- (void)updateUsername:(NSString *)username
{
    [[self usernameButton] setTitle:username forState:UIControlStateNormal];
}

#pragma mark - set comment

- (void)setComment:(NSString *)comment withSize:(CGSize)size
{
    self.commentLabel.text = comment;
    
    NSRegularExpression *mentionExpression = [NSRegularExpression regularExpressionWithPattern:@"@{1}([-A-Za-z0-9_]{2,})" options:NO error:nil];
    NSArray *matches = [mentionExpression matchesInString:comment
                                                  options:0
                                                    range:NSMakeRange(0, [comment length])];
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [comment substringWithRange:matchRange];
        NSString* user = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
        [self.commentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    

    NSRegularExpression *hashtagExpression = [NSRegularExpression regularExpressionWithPattern:@"[##]+([A-Za-z0-9-_ñNáÁÉéíÍóÓúÓüÜ]+)" options:NO error:nil];
    matches = [hashtagExpression matchesInString:comment
                                         options:0
                                           range:NSMakeRange(0, [comment length])];
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [comment substringWithRange:matchRange];
        NSString* hashtag = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self.commentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    
    CGRect newCommentFrame = CGRectZero;
    newCommentFrame.origin.x = self.usernameButton.frame.origin.x;
    newCommentFrame.origin.y = self.usernameButton.frame.origin.y+self.usernameButton.frame.size.height+kCommentYOffset;
    newCommentFrame.size = size;
    [self.commentLabel setFrame:newCommentFrame];
}

#pragma mark - TTTAtributtedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result
{

}
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:kUsernameScheme])
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(openUser:)])
        {
            [[self delegate] openUser:[url resourceSpecifier]];
        }
    }else if ([[url scheme] isEqualToString:kHashtagScheme])
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(openHashtag:andTag:)])
        {
					NSLog(@"Tags %@", [[url resourceSpecifier] stringByRemovingPercentEncoding]);
            [[self delegate] openHashtag:[[url resourceSpecifier] stringByRemovingPercentEncoding] andTag:[self tag]];
        }
    }
}
@end