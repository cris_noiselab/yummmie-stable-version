//
//  NLUserListViewController.m
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "NLUserListViewController.h"
#import "NLProfileViewController.h"
#import "NLUser.h"
#import "NSURL+AssetURL.h"
#import "NLNotifications.h"
#import "NLSession.h"
#import "UIColor+NLColor.h"

static const CGFloat kUserCellHeight = 57.0f;
static NSString * const kUserListViewCellIdentifier = @"kUSerListViewCellIdentifier";

@interface NLUserListViewController ()

- (void)cellButtonAction:(UIButton *)sender;

@property (nonatomic) NLUserListType userListType;
@property (strong, nonatomic) NSMutableArray *users;
@property (nonatomic) NSInteger userId;
@property (nonatomic,getter=isLoadingPrevious) BOOL loadingPrevious;

- (void)loadData:(NLUserListType)aType;
- (void)fetchNewUsers:(id)control;
- (void)fetchOldUsers;

- (void)updateFollowStatus:(NSNotification *)notification;

@end

@implementation NLUserListViewController
{
    NSString *gaiTrackName;
	NSInteger currentUsersPage;
	NSInteger totalUserPages;
}
- (id)initWithUserListType:(NLUserListType)userListType andUserId:(NSUInteger)userId
{
    if (self = [super init])
    {
        _userListType = userListType;
        _userId = userId;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollowStatus:) name:kNLUnfollowUserNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollowStatus:) name:kNLFollowUserNotification object:nil];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self tableView] registerNib:[UINib nibWithNibName:NSStringFromClass([NLUserListViewCell class]) bundle:nil] forCellReuseIdentifier:kUserListViewCellIdentifier];
    self.navigationItem.titleView = nil;
    [self showLoadingSpinner];
	currentUsersPage = 1;
	totalUserPages = 10;
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"%@ View", gaiTrackName];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
	
	//load users
	[self loadData:[self userListType]];
	self.refreshControl = [[UIRefreshControl alloc] init];
	self.refreshControl.tintColor = [UIColor nl_applicationRedColor];
	[self.refreshControl addTarget:self action:@selector(fetchNewUsers:) forControlEvents:UIControlEventValueChanged];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Unfollow/Follow Notifications

- (void)updateFollowStatus:(NSNotification *)notification
{
    NLUser *unfollowed = [[notification userInfo] objectForKey:@"user"];
    if (notification.object != self)
    {
        for (NLUser *user in self.users)
        {
            if ([unfollowed isEqual:user])
            {
                user.follow = !user.follow;
            }
        }
    }
    __weak NLUserListViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[weakSelf tableView] reloadData];
    });
}

#pragma mark - Fetch Users

- (void)fetchNewUsers:(id)control
{
    __weak NLUserListViewController *weakSelf = self;
	currentUsersPage = 1;
    void (^responseBlock)(NSError *error, NSArray*users, NSInteger currentPage, NSInteger totalPages) = ^(NSError *error, NSArray *users, NSInteger currentPage, NSInteger totalPages){
        [[weakSelf refreshControl] endRefreshing];
        if ([users count]>0)
        {
					weakSelf.users = [users mutableCopy];
					//[[weakSelf users] insertObjects:users atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [users count])]];
            [[weakSelf tableView] reloadData];
        }
    };
    
    if (NLUserListFollowers == self.userListType)
    {
			//[NLUser getFollowersFromUser:[self userId] beforeUserId:[firstUser userID] withCompletionBlock:responseBlock];

			[NLUser getFollowersFromUser:[self userId] page:currentUsersPage withCompletionBlock:responseBlock];
    }else if (NLUserListFollowing == self.userListType)
    {
			//[NLUser getFollowingsFromUser:[self userId] beforeUserId:[firstUser userID] withCompletionBlock:responseBlock];

			[NLUser getFollowingsFromUser:[self userId] page:currentUsersPage withCompletionBlock:responseBlock];
    }
}

- (void)fetchOldUsers
{
    __weak NLUserListViewController *weakSelf = self;
    
    void (^responseBlock)(NSError *, NSArray*, NSInteger currentPage, NSInteger totalPages) = ^(NSError *error, NSArray *users, NSInteger currentPage, NSInteger totalPages)
	{
        [weakSelf setLoadingPrevious:NO];
        if ([users count]>0)
        {
            NSInteger lastIndex = [[weakSelf users] count];
            NSIndexSet *index = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(lastIndex, [users count])];
            [[weakSelf users] insertObjects:users atIndexes:index];
            [[weakSelf tableView] reloadData];
        }
    };
	currentUsersPage++;
    if (NLUserListFollowers == self.userListType)
    {
			[NLUser getFollowersFromUser:[self userId] page:currentUsersPage withCompletionBlock:responseBlock];
    }else if (NLUserListFollowing == self.userListType)
    {
			[NLUser getFollowingsFromUser:[self userId] page:currentUsersPage withCompletionBlock:responseBlock];
    }
}

- (void)loadData:(NLUserListType)aType
{
    __weak NLUserListViewController *weakSelf = self;
    [self showLoadingSpinner];
    switch ([self userListType])
    {
        case NLUserListFollowers:
        {
            self.title = NSLocalizedString(@"followers_text", nil);
            gaiTrackName = @"seguidores";
					currentUsersPage = 1;
					[NLUser getFollowersFromUser:[self userId] page:currentUsersPage withCompletionBlock:^(NSError *error, NSArray *users, NSInteger currentPage, NSInteger totalPages) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf hideLoadingSpinner];
                });
                
                if (!error)
                {
									currentUsersPage = currentPage;
									totalUserPages = totalPages;
                    [weakSelf setUsers:[NSMutableArray arrayWithArray:users]];
                    [[weakSelf tableView] reloadData];
                }else
                {
                    [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_following", nil) withDuration:2.0f];
                }
            }];
//            [NLUser getFollowersFromUser:[self userId] withCompletionBlock:^(NSError *error, NSArray *followers) {
//                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [weakSelf hideLoadingSpinner];
//                });
//                
//                if (!error)
//                {
//                    [weakSelf setUsers:[NSMutableArray arrayWithArray:followers]];
//                    [[weakSelf tableView] reloadData];
//                }else
//                {
//                    [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_following", nil) withDuration:2.0f];
//                }
//            }];
            break;
        }
        case NLUserListFollowing:
        {
            self.title = NSLocalizedString(@"following_text", nil);
            gaiTrackName = @"siguiendo";
					currentUsersPage = 1;
					[NLUser getFollowingsFromUser:[self userId] page:currentUsersPage withCompletionBlock:^(NSError *error, NSArray *users, NSInteger currentPage, NSInteger totalPages) {
                if (!error)
                {
									currentUsersPage = currentPage;
									totalUserPages = totalPages;
                    [weakSelf setUsers:[NSMutableArray arrayWithArray:users]];
                    [[weakSelf tableView] reloadData];
                }else
                {
                    [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_followers", nil) withDuration:2.0f];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf hideLoadingSpinner];
                });
            }];
//            [NLUser getFollowings:[self userId] withCompletionBlock:^(NSError *error, NSArray *followings) {
//                if (!error)
//                {
//                    [weakSelf setUsers:[NSMutableArray arrayWithArray:followings]];
//                    [[weakSelf tableView] reloadData];
//                }else
//                {
//                    [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_followers", nil) withDuration:2.0f];
//                }
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [weakSelf hideLoadingSpinner];
//                });
//            }];
//            
            break;
        }
        case NLUserListSuggestions:
            break;
        default:
            break;
    }


}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions

- (void)cellButtonAction:(UIButton *)sender
{
    
}

#pragma mark - UITableDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self users] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLUserListViewCell *cell = (NLUserListViewCell *)[tableView dequeueReusableCellWithIdentifier:kUserListViewCellIdentifier];
    NLUser *user = [[self users] objectAtIndex:[indexPath row]];
    [cell setName:[user name] andUsername:[NSString stringWithFormat:@"@%@",[user username]]];
    [[cell userPhoto] sd_setImageWithURL:[NSURL URLWithString:[user thumbAvatar]] placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
	[[cell userPhoto] setContentMode:UIViewContentModeScaleAspectFill];

    if (![NLSession isCurrentUser:[user userID]])
    {
        if ([user follow])
        {
            [cell setButtonType:FOLLOWING];
        }else
        {
            [cell setButtonType:NOTFOLLOWING];
        }
        [cell setDelegate:self];
    }else
    {
        [[cell actionButton] setHidden:YES];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.row == ([self.users count]-1)))
    {
        if (![self isLoadingPrevious])
        {
            [self setLoadingPrevious:YES];
            [self fetchOldUsers];
        }
    }
}
#pragma mark - UITableVewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kUserCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLUser *selectedUser = [[self users] objectAtIndex:[indexPath row]];
    NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:selectedUser isProfileUser:NO];
    [[self navigationController] pushViewController:profileVC animated:YES];
}

#pragma mark - NLUserListViewCellDelegate

- (void)userCell:(NLUserListViewCell *)cell actionWithType:(UserButtonType)type
{
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:cell];
    NLUser *selectedUser = [[self users] objectAtIndex:[indexPath row]];
    __weak NLUserListViewController *weakSelf = self;
    
    if (type == FOLLOWING)
    {        
        [cell setButtonType:NOTFOLLOWING];
        [NLUser unfollowUser:[selectedUser userID] withCompletionBlock:^(NSError *error) {
            if (error)
            {
                [weakSelf showErrorMessage:@"There was an error while updating" withDuration:2.0f];
                [cell setButtonType:FOLLOWING];
            }else
            {
                if ([weakSelf userListType] == NLUserListFollowing)
                {
                    [[weakSelf users] removeObject:selectedUser];
                }else
                {
                    [selectedUser setFollow:NO];
                }
                
                [[weakSelf tableView] reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLReloadDishesNotification object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLUnfollowUserNotification object:self userInfo:@{@"user":selectedUser}];
            }
        }];
        
    }else if (type == NOTFOLLOWING)
    {
        [cell setButtonType:FOLLOWING];
        [NLUser followUser:[selectedUser userID] withCompletionBlock:^(NSError *error) {
            if (error)
            {
                [weakSelf showErrorMessage:@"There was an error while updating" withDuration:2.0f];
                [cell setButtonType:NOTFOLLOWING];
            }else
            {
                [selectedUser setFollow:YES];
                [[weakSelf tableView] reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLReloadDishesNotification object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLFollowUserNotification object:self userInfo:@{@"user":selectedUser}];
            }
        }];
    }
}
@end
