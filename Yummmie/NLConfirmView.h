//
//  NLConfirmRecommendView.h
//  Yummmie
//
//  Created by Mauricio Ventura on 15/08/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol NLConfirmViewDelegate;

@interface NLConfirmView : UIView
@property (weak, nonatomic) id<NLConfirmViewDelegate> delegate;
@property (nonatomic) NSInteger yumId;
@property (nonatomic) NSInteger originalYumId;


-(instancetype)initWithDictionary:(NSDictionary *)dict andFrame:(CGRect)frame;

@end

@protocol NLConfirmViewDelegate <NSObject>

@required
-(void)confirm:(BOOL)confirmed yumId:(NSInteger)yumId andOriginalYumId:(NSInteger)originalYumId;


@end