//
//  NLSharePhotoViewController.m
//  Yummmie
//
//  Created by on 7/15/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLSharePhotoViewController.h"
#import <ASImageResize/UIImage+Resize.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MGInstagram/MGInstagram.h>
#import <MessageUI/MessageUI.h>

#import "NLConstants.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "NLSession.h"
#import "NLYum.h"
#import "NLRegion.h"
#import "NLFoodType.h"
#import "NLVenue.h"
#import "NLNotifications.h"
#import "NLNavigationViewController.h"
#import "NLUser.h"
#import "NLSession.h"
#import "NLAuthentication.h"

#import "NLShareSettingsViewController.h"

#import "NLYummieApiClient.h"
#import "UIColor+NLColor.h"

#import "NLSuggestedUsersBar.h"


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <YKTwitterHelper/YATTwitterHelper.h>
#import <TwitterKit/TwitterKit.h>

@import Social;
@interface NLSharePhotoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *shareLabel;

@property (nonatomic, strong) UIBarButtonItem *loadingRightButton;
@property (nonatomic, strong) UIBarButtonItem *shareRightButton;
@property (weak, nonatomic) IBOutlet UIButton *changeVenueButton;
@property (strong, nonatomic) NLVenue *venue;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *instagramButton;
@property (weak, nonatomic) NSURLSessionDataTask *photoRequest;
@property (strong, nonatomic) NSProgress *progress;

@property (strong, nonatomic) IBOutlet NLSuggestedUsersBar *suggestedBar;

@property (nonatomic) NLMessageMode currentMessageMode;
@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) NSArray *filteredUsers;

@property (copy, nonatomic) NSURLSessionDataTask *queryUsersTask;


@property (strong, nonatomic) NSMutableArray *hashtags;
@property (strong, nonatomic) NSArray *filteredHashtags;

@property (strong, nonatomic) YATTwitterHelper *twitterHelper;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *subRegionHeightConstraint;
@property (strong, nonatomic) IBOutlet UIPickerView *optionsPicker;
@property (strong, nonatomic) IBOutlet UINavigationBar *actionbar;
@property (strong, nonatomic) IBOutlet UITextField *regionLabel;
@property (strong, nonatomic) IBOutlet UITextField *subRegionLabel;
@property (strong, nonatomic) IBOutlet UITextField *foodTypeLabel;

@property(nonatomic, assign) int code;
@property(nonatomic, assign) int pickedItemIndex;

@property(nonatomic, assign) int pickedRegionIndex;
@property(nonatomic, assign) int pickedSubRegionIndex;
@property(nonatomic, assign) int pickedFoodTypeIndex;

@property(nonatomic, assign) bool isTwitterSelected;
@property(nonatomic, assign) CGFloat offsetView;
@property (nonatomic, strong) NSMutableArray *regions;
@property (nonatomic, strong) NSMutableArray *subRegions;
@property (nonatomic, strong) NSMutableArray *foodTypes;
@property (strong, nonatomic) IBOutlet UIButton *regionButton;
@property (strong, nonatomic) IBOutlet UIButton *subRegionButton;
@property (strong, nonatomic) IBOutlet UIButton *foodTypeButton;
@property (strong, nonatomic) IBOutlet UIImageView *subregionIcon;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;



@property (strong, nonatomic) UILabel *progressLabel;

- (IBAction)changeVenue:(id)sender;

- (void)share:(id)sender;

- (void)blockInterface;
- (void)unblockInterface;

- (BOOL)errorInFields:(NSError **)error;

- (IBAction)shareFacebook:(id)sender;
- (IBAction)shareTwitter:(id)sender;
- (IBAction)instagramAction:(id)sender;

- (IBAction)itemPicked:(id)sender;


@end

@implementation NLSharePhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		self.edgesForExtendedLayout = UIRectEdgeBottom;
	}
    _regions = [NSMutableArray array];
    _subRegions = [NSMutableArray array];
    _foodTypes = [NSMutableArray array];
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	__weak NLSharePhotoViewController *weakSelf = self;
    
    //[self fillArrays];

    self.isTwitterSelected=false;
	_currentMessageMode = NLMessageModeNone;
	[[self suggestedBar] setDelegate:self];
    
    
	
	// Do any additional setup after loading the view from its nib.
	[self.messageTextView becomeFirstResponder];
	self.shareRightButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"share_button_title", nil) style:UIBarButtonItemStylePlain target:self action:@selector(share:)];
	
	[self.navigationItem setRightBarButtonItem:self.shareRightButton];
	
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	[spinner startAnimating];
	self.loadingRightButton = [[UIBarButtonItem alloc] initWithCustomView:spinner];
	
	UIImage *sharedPhoto = [NLSession getSharedPhoto];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		UIImage *thumbImage = [sharedPhoto resizedImageToSize:CGSizeMake(190, 190)];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			self.dishPhoto.image = thumbImage;
		});
	});
	
	NLNavigationViewController *navController = (NLNavigationViewController *)[self navigationController];
	[navController setupProgressView];
	
	self.shareLabel.text = NSLocalizedString(@"share_photo_text_label_value", nil);
	
	if ([NLSession getSharedVenue]) {
		self.venue = [NLSession getSharedVenue];
		[self.changeVenueButton setTitle:self.venue.name forState:UIControlStateNormal];
	}
	
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		
		NSMutableSet *usersSet = [NSMutableSet set];
		
		
		//            NSLog(@"Los usuarios : %@",usersSet);
		
		NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:YES];
		NSArray *commentOwners = [usersSet sortedArrayUsingDescriptors:@[descriptor]];
		
		[NLUser getFollowings:[[NLSession currentUser] userID] page:0 withCompletionBlock:^(NSError *error, NSArray *followings, NSInteger currentPage, NSInteger totalPages)
		 {
			 //        [weakSelf.suggestedBar updateDatasource:followings];
			 NSArray *all = [commentOwners arrayByAddingObjectsFromArray:followings];
			 [weakSelf setUsers:all];
			 //[[weakSelf suggestedBar] updateDatasource:[weakSelf users]];
		 }];
		
		
	});
	
	/*
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	
	if ([def objectForKey:@"venue"])
	{
		self.venue = [[NLVenue alloc] initWithDictionary:[def objectForKey:@"venue"]];
		
		[[self changeVenueButton] setTitleColor:[UIColor nl_colorWith255Red:203 green:39 blue:54 andAlpha:255] forState:UIControlStateNormal];
		[[self changeVenueButton] setTitle:[[self venue] name] forState:UIControlStateNormal];
	}*/
	
	/*UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
	 logoView.contentMode = UIViewContentModeScaleAspectFit;
	 self.navigationItem.titleView = logoView;
	 //[self.navigationItem.titleView setOpaque:true];
	 [self.navigationItem.titleView setAlpha:1];
	 
	 NSLog(@"%@ see", self.navigationItem.titleView);*/
    
   
    [NLRegion getRegions:^(NSError *error, NSArray *regions) {
        
        if (!error)
        {
            [weakSelf setRegions:regions];
            //[[self yums] addObjectsFromArray:yums];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([[weakSelf regions] count] > 0 )
                {
                    self.regionLabel.delegate = self;
                    self.subRegionLabel.delegate = self;
                    self.foodTypeLabel.delegate = self;
                    self.optionsPicker.dataSource = self;
                    self.optionsPicker.delegate = self;
                    //[self dissapearSubRegion];
                    self.code=1;
                    
                    self.pickedRegionIndex=0;
                    self.pickedSubRegionIndex=0;
                    
                    [self.regionButton setTitle: NSLocalizedString(@"select_region_title", nil) forState:UIControlStateNormal ] ;
                    [self.subRegionButton setTitle: NSLocalizedString(@"select_subregion_title", nil) forState:UIControlStateNormal ] ;
                    //[self.foodTypeButton setTitle: NSLocalizedString(@"select_foodtype_title", nil) forState:UIControlStateNormal ] ;

                    
                }
            });
        }else
        {
            [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_likes", nil) withDuration:2.0f];
        }
    }];
    
    
    [NLFoodType getFoodTypes:^(NSError *error, NSArray *types) {
        
        if (!error)
        {
            [weakSelf setFoodTypes:types];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([[weakSelf regions] count] > 0 )
                {
                    self.pickedFoodTypeIndex=0;
                    [self.foodTypeButton setTitle: NSLocalizedString(@"select_foodtype_title", nil) forState:UIControlStateNormal ] ;
                    
                    
                }
            });
        }else
        {
            [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_likes", nil) withDuration:2.0f];
        }
    }];

    
    [self disapearSubRegion];
    
    
    
	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.navigationController.navigationBar.hidden = false;

	[self.navigationController setNavigationBarHidden:NO];
	CGRect frame = self.navigationController.navigationBar.frame;
	frame.origin.y = 20.0f;
	[self.navigationController.navigationBar setFrame:frame];
	
	self.navigationItem.titleView.alpha = 1.0;
	
	self.progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 10)];
	[[self progressLabel] setTextAlignment:NSTextAlignmentCenter];
	self.progressLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
	[self.progressLabel setTextColor:[UIColor whiteColor]];
	[self.navigationItem setTitleView:[self progressLabel]];
	
	UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
	logoView.contentMode = UIViewContentModeScaleAspectFit;
	logoView.center = CGPointMake(self.progressLabel.center.x - 8, self.progressLabel.center.y);
	//self.navigationItem.titleView = logoView;
	[self.progressLabel addSubview:logoView];
	//[self.navigationItem.titleView setOpaque:true];
	
	
	NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
	
	[defaultCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[defaultCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
	[defaultCenter addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
	
	//Google Analytics
	NSString *name = [NSString stringWithFormat:@"Share Photo View"];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	//    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)changeVenue:(id)sender
{
	NLFoursquareSearchViewController *fsVC = [[NLFoursquareSearchViewController alloc] init];
	[fsVC setDelegate:self];
	NLNavigationViewController *navController = [[NLNavigationViewController alloc] initWithRootViewController:fsVC];
	[self presentViewController:navController animated:YES
									 completion:nil];
	
}

#pragma mark - Share

- (void)share:(id)sender
{
	[[self photoRequest] cancel];
	
	NSError *error;
	if (![self errorInFields:&error])
	{
		[self.navigationItem setRightBarButtonItem:nil];
		[self blockInterface];
		
		
		[[self.progressLabel.subviews lastObject] removeFromSuperview];
		self.progressLabel.text = NSLocalizedString(@"upload_text", nil);
		[self.progressLabel sizeToFit];
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *newImage = [NLSession getSharedPhoto];

            if ([NLSession getSharedPhoto].size.width > 1280)
            {
                newImage = [[NLSession getSharedPhoto] resizedImageToSize:CGSizeMake(1280, 1280)];
            }
			
			__weak NLSharePhotoViewController *weakSelf = self;
			
			NSMutableString *providers = [[NSMutableString alloc] initWithString:@""];
			
			if (self.facebookButton.selected)
			{
				[providers appendString:@"facebook"];
			}
			if (self.twitterButton.selected)
			{
				if (providers.length > 0)
				{
					[providers appendString:@",twitter"];
				}else
				{
					[providers appendString:@"twitter"];
				}
			}
			NSProgress *tmpProg;
			dispatch_async(dispatch_get_main_queue(), ^{
				[[self navigationItem] setHidesBackButton:YES];
			});
			NSUserDefaults *def = [NSUserDefaults standardUserDefaults];

		
			[def setObject:[self.venue getDictionaryFromVenue] forKey:@"venue"];
			[def synchronize];
			self.photoRequest = [NLYum postYum:newImage caption:self.messageTextView.text name:[[self dishNameField] text] andVenueId:[[self venue] venueId] providers:providers progress:&tmpProg withCompletionBlock:^(NSError *error, NLYum *yum)
			{
				
				[[weakSelf progressLabel] setText:@""];
				
				if (!error)
				{
					//notifiy we are done
					if (self.facebookButton.isSelected)
					{
						FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
						photo.image = newImage;
						photo.userGenerated = YES;
						FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
						content.photos = @[photo];
						[FBSDKShareAPI shareWithContent:content delegate:nil];
						
					}
					if (self.twitterButton.isSelected)
					{
                        //---
                        if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
                            //TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
                            NSString *initialText = [NSString stringWithFormat:@"%@ %@",[[self dishNameField] text], [[self messageTextView] text]];
                            //TWTRComposerViewController *composer = [[TWTRComposerViewController alloc] initWithInitialText:initialText image:newImage videoURL:nil];
                            TWTRComposer *composer = [[TWTRComposer alloc] init];
                            [composer setText: initialText];
                            [composer setImage: [[self dishPhoto] image]];
                            //[composer setImage: newImage];
                            [composer setURL: nil ];
                            
                            // Called from a UIViewController
                            [composer showFromViewController:self completion:^(TWTRComposerResult result) {
                                if (result == TWTRComposerResultCancelled) {
                                    [self hasPosted:yum andImage:newImage];
                                }
                                else {
                                    [self hasPosted:yum andImage:newImage];
                                }
                            }];
                            
                        } else {
                            [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
                                if (session) {
                                    TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
                                    [self presentViewController:composer animated:YES completion:nil];
                                } else {
                                    [self showErrorMessage:NSLocalizedString(@"twitter_not_setup", nil) withDuration:2.0f];
                                }
                            }];
                        }
                        
                        
                        //---

					}
                    if (self.isTwitterSelected==false){
                        [self hasPosted:yum andImage:newImage];
                    }
                    
                    
				}else
				{
					dispatch_async(dispatch_get_main_queue(), ^{
						[weakSelf showErrorMessage:NSLocalizedString(@"upload_yummmie_error",nil) withDuration:3.0f];
						[weakSelf hideLoadingSpinner];
						[weakSelf unblockInterface];
						[[weakSelf navigationItem] setRightBarButtonItem:[weakSelf shareRightButton]];
						[[[[weakSelf navigationItem] rightBarButtonItem] customView] setAlpha:1.0f];
						[[weakSelf navigationItem] setHidesBackButton:NO animated:YES];
						
						__weak NLNavigationViewController *nav = (NLNavigationViewController *)[self navigationController];
						[nav updateProgress:0.0f];
						[nav hideProgressView];
					});
				}
				[NLSession setSharedVenue:nil];
			}];
			self.progress = tmpProg;
			
			[[self progress] addObserver:self forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:NULL];
		});
	}else
	{
		[self showErrorMessage:[error localizedDescription] withDuration:3.0f];
		[[self progressLabel] setText:@""];
	}
	
}
-(void)hasPosted:(NLYum *) yum andImage: (UIImage *) newImage{
    __weak NLSharePhotoViewController *weakSelf = self;
    __weak NLNavigationViewController *nav = (NLNavigationViewController *)[self navigationController];
    [nav completeProgress];
    
    [NLSession clearLocation];
    [NLSession setYum:yum];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLYumPostReadyNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLReloadDishesNotification object:nil];
    
    if ([[weakSelf instagramButton] isSelected])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInstagramOption object:nil userInfo:nil];
    }
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                       [library saveImage:[NLSession getSharedPhoto] toAlbum:kYummmieGalleryName completion:^(NSURL *assetURL, NSError *error)
                        {
                            [NLSession setSharedPhoto:newImage];
                            
                        } failure:^(NSError *error) {
                            [NLSession setSharedPhoto:newImage];
                        }];
                   });

}

#pragma mark - Input Validation

- (BOOL)errorInFields:(NSError *__autoreleasing *)error
{
	if ([[[self dishNameField] text] length] == 0 || [[[[self dishNameField] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
		
		NSDictionary *userInfo = @{
															 NSLocalizedDescriptionKey:@"you must enter a dish name"
															 };
		*error = [[NSError alloc] initWithDomain:@"com.yummmie.fieldValidations" code:0 userInfo:userInfo];
		return YES;
	}
	
	if ([[[[self changeVenueButton] titleLabel] text] isEqualToString:@"Select a Venue"]) {
		NSDictionary *userInfo = @{
															 NSLocalizedDescriptionKey:@"you must select a Venue"
															 };
		*error = [[NSError alloc] initWithDomain:@"com.yummmie.fieldValidations" code:0 userInfo:userInfo];
		return YES;
	}
	return NO;
}

- (IBAction)shareFacebook:(id)sender
{
	if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"])
	{
		NSString *facebookButtonImage = (self.facebookButton.isSelected)?@"social_bot_b_facebook":@"social_bot_facebook";
		self.facebookButton.selected = !self.facebookButton.selected;
		[self.facebookButton setImage:[UIImage imageNamed:facebookButtonImage] forState:UIControlStateNormal];
	}
	else
	{
		FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
		[loginManager logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
		 {
			 if (error)
			 {
				 NSLog(@"Process error %@", error);
			 }
			 else if (result.isCancelled)
			 {
				 
			 }
			 else
			 {
				 NSString *facebookButtonImage = (self.facebookButton.isSelected)?@"social_bot_b_facebook":@"social_bot_facebook";
				 self.facebookButton.selected = !self.facebookButton.selected;
				 [self.facebookButton setImage:[UIImage imageNamed:facebookButtonImage] forState:UIControlStateNormal];
				 
			 }
			 
		 }];
	}
}

- (IBAction)shareTwitter:(id)sender
{
    /*
	if ([NLSession containsAuthentication:NLAuthenticationTypeTwitter])
	{
		NSString *twitterButtonImage = (self.twitterButton.isSelected)?@"social_bot_b_twitter":@"social_bot_twitter";
		self.twitterButton.selected = !self.twitterButton.selected;
		[self.twitterButton setImage:[UIImage imageNamed:twitterButtonImage] forState:UIControlStateNormal];
        self.isTwitterSelected=(self.twitterButton.isSelected)?true:false;
	}
	else
	{
		self.twitterHelper  = [[YATTwitterHelper alloc] initWithKey:@"Aa1eoNrMZhOpALcwz1nOEND3d" andSecret:@"60dQjthbgmN3Zav85v424VovMIh0FTFXB9ugH6PzjalGtBpw9p"];
		[self.twitterHelper reverseAuthWithSuccess:^(NSDictionary *data) {
		[NLAuthentication addAuthenticationWithType:NLAuthenticationTypeTwitter userID:[NLSession getUserID] token:[data valueForKey:@"oauth_token"] tokenSecret:[data valueForKey:@"oauth_token_secret"] withCompletionBlock:^(NSError *error, NLAuthentication *authentication)
			 {				 
				 if (!error)
				 {
					 [[[NLSession currentUser] authentications] addObject:authentication];
					 NSString *twitterButtonImage = (self.twitterButton.isSelected)?@"social_bot_b_twitter":@"social_bot_twitter";
					 self.twitterButton.selected = !self.twitterButton.selected;
					 [self.twitterButton setImage:[UIImage imageNamed:twitterButtonImage] forState:UIControlStateNormal];
                     
				 }
				 
			 }];
            self.isTwitterSelected=(self.twitterButton.isSelected)?true:false;
		} failure:^(NSError *error)
		{
			NSString *twitterButtonImage = (self.twitterButton.isSelected)?@"social_bot_b_twitter":@"social_bot_twitter";
			self.twitterButton.selected = !self.twitterButton.selected;
			[self.twitterButton setImage:[UIImage imageNamed:twitterButtonImage] forState:UIControlStateNormal];
            self.isTwitterSelected=(self.twitterButton.isSelected)?true:false;
		}];
	}*/
    
    NSString *twitterButtonImage = (self.twitterButton.isSelected)?@"social_bot_b_twitter":@"social_bot_twitter";
    self.isTwitterSelected = !self.isTwitterSelected;
    self.twitterButton.selected = !self.twitterButton.selected;
    [self.twitterButton setImage:[UIImage imageNamed:twitterButtonImage] forState:UIControlStateNormal];
    
    
}

- (IBAction)instagramAction:(id)sender

{
	if ([MGInstagram isAppInstalled])
	{
		//toggle instagram button
		NSString *instagramButtonImage = (self.instagramButton.isSelected)?@"social_bot_b_instagram":@"social_bot_instagram";
		self.instagramButton.selected = !self.instagramButton.selected;
		[self.instagramButton setImage:[UIImage imageNamed:instagramButtonImage] forState:UIControlStateNormal];
		
		/*if (self.instagramButton.isSelected)
		{
			self.twitterHelper  = [[YATTwitterHelper alloc] initWithKey:@"Aa1eoNrMZhOpALcwz1nOEND3d"
																												andSecret:@"60dQjthbgmN3Zav85v424VovMIh0FTFXB9ugH6PzjalGtBpw9p"];
			[self.twitterHelper reverseAuthWithSuccess:^(NSDictionary *data)
			 {
				 NSLog(@"%@", data);
				 
			 } failure:^(NSError *error)
			 {
				 [self showErrorMessage:NSLocalizedString(@"error_message_twitter_permission", nil) withDuration:3.0f];
			 }];
		}
		else
		{
			
		}*/
	}
	else
	{
		[self showErrorMessage:NSLocalizedString(@"instagram_installed_error", nil) withDuration:2.0f];
	}
}

- (IBAction)itemPicked:(id)sender {
    //always
    [self.actionbar setHidden:true];
    [self.optionsPicker setHidden:true];
    //self.pickedItemIndex = [self.optionsPicker selectedRowInComponent:(0)];
    if(self.code==1){
        self.pickedRegionIndex = [self.optionsPicker selectedRowInComponent:(0)];
        NLRegion *region = self.regions[self.pickedRegionIndex];
        [self.regionButton setTitle:region.name forState:UIControlStateNormal];
        [self.regionButton setTitleColor:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
        
        self.subRegions = region.subRegions;
        if(self.subRegions.count>0){
            [self appearSubRegion];
        }
        else{
            [self disapearSubRegion];
        }
        [self.subRegionButton setTitle: NSLocalizedString(@"select_subregion_title", nil) forState:UIControlStateNormal ] ;

    }
    if(self.code==2){
        self.pickedSubRegionIndex = [self.optionsPicker selectedRowInComponent:(0)];
        NLSubRegion *subRegion = self.subRegions[self.pickedSubRegionIndex];
        [self.subRegionButton setTitle:subRegion.name forState:UIControlStateNormal];
        [self.subRegionButton setTitleColor:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
        



    }
    if(self.code==3){
        self.pickedFoodTypeIndex = [self.optionsPicker selectedRowInComponent:(0)];
        NLFoodType *foodType = self.foodTypes[self.pickedFoodTypeIndex];
        [self.foodTypeButton setTitle:foodType.food_type_name forState:UIControlStateNormal];
        [self.foodTypeButton setTitleColor:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
        CGPoint newOffset = CGPointMake(0, 0);
        [self.scrollView setContentOffset: newOffset animated: YES];
        
        
        [self.regionButton setEnabled:true];
        [self.subRegionButton setEnabled:true];
        [self.changeVenueButton setEnabled:true];
        [self.dishNameField setEnabled:true];
        [self.messageTextView setEditable:true];
    }
    
    
    
    self.code = 0;
    //self.pickedItemIndex=0;
    [self.optionsPicker selectRow:0 inComponent:0 animated:YES];
    
}
- (void)blockInterface
{
	[self.messageTextView setEditable:NO];
}

- (void)unblockInterface
{
	[self.messageTextView setEditable:YES];
}

#pragma mark - NLFoursquareSearchDelegate

- (void)didFinishSelectingWithVenue:(NLVenue *)venue
{
	self.venue = venue;
	
	[[self changeVenueButton] setTitleColor:[UIColor nl_colorWith255Red:203 green:39 blue:54 andAlpha:255] forState:UIControlStateNormal];
	[[self changeVenueButton] setTitle:[[self venue] name] forState:UIControlStateNormal];
	
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	NLNavigationViewController *nav = (NLNavigationViewController *)[self navigationController];
	[nav updateProgress:[object fractionCompleted]];
}
#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	//    NSLog(@"La ubicación del caracter ingresado : %d",range.location);
	//    NSLog(@"El texto : %@",text);
	
	
	NSArray *words = [[textView text] componentsSeparatedByString:@" "];
	//    NSLog(@"Palabras %@",words);
	
	NSString *lastWord = [words lastObject];
	
	NSRange previousRange = NSMakeRange(range.location-1, 1);
	
	
	if ([text isEqualToString:@"@"])
	{
		if (range.location == 0)
		{
			[self startMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeMention)
		{
			[self endMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeHashtag)
		{
			[self endMentionMode];
		}
		else
		{
			NSString *previousCharacter = [[textView text] substringWithRange:previousRange];
			if ([previousCharacter isEqual:@" "])
			{
				[self startMentionMode];
			}
		}
	}
	else if ([text isEqualToString:@"#"])
	{
		if (range.location == 0)
		{
			[self startHashtagMode];
		}
		else if (self.currentMessageMode == NLMessageModeMention)
		{
			[self endMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeHashtag)
		{
			[self endMentionMode];
		}
		else
		{
			NSString *previousCharacter = [[textView text] substringWithRange:previousRange];
			if ([previousCharacter isEqual:@" "])
			{
				[self startHashtagMode];
			}
		}
	}
	else if ([text isEqualToString:@" "])
	{
		if (self.currentMessageMode == NLMessageModeMention)
		{
			[self endMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeHashtag)
		{
			NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
			self.hashtags = [[def objectForKey:@"hashtags"] mutableCopy];
			if (self.hashtags == nil)
			{
				self.hashtags = [[NSMutableArray array] mutableCopy];
			}
			if (![self.hashtags containsObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]])
			{
				[self.hashtags addObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]];
				[def setObject:self.hashtags forKey:@"hashtags"];
				[def synchronize];
			}
			[self endMentionMode];
		}
	}
	else if (text.length == 0)
	{
		if ([lastWord containsString:@"@"])
		{
			NSRange atRange = [lastWord rangeOfString:@"@"];
			
			
			if (atRange.location == 0 && lastWord.length > 1) {
				[self startMentionMode];
				//Get the word and ask for suggestions
			}
			else{
				[self endMentionMode];
			}
		}
		else if ([lastWord containsString:@"#"])
		{
			NSRange atRange = [lastWord rangeOfString:@"#"];
			
			
			if (atRange.location == 0 && lastWord.length > 1) {
				[self startHashtagMode];
				//Get the word and ask for suggestions
			}
			else{
				[self endMentionMode];
			}
		}
	}
	
	
	if (self.currentMessageMode == NLMessageModeMention) {
		//        NSLog(@"Seguimos en modo mention!");
		//get the word and ask for sugesionts
	}else
	{
		//        NSLog(@"Estamos ecribendo normal!");
	}
	return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
	NSArray *words = [[textView text] componentsSeparatedByString:@" "];
	NSString *lastWord = [words lastObject];
	
	if (self.currentMessageMode == NLMessageModeMention) {
		//        NSLog(@"Buscar info sobre : %@",lastWord);
		NSString *cleanUser = [lastWord stringByReplacingOccurrencesOfString:@"@" withString:@""];
		//        NSLog(@"El clean user : %@ y su tamaño : %d",cleanUser,cleanUser.length);
		
		__weak NLSharePhotoViewController *weakSelf = self;
		
		if (cleanUser.length > 0) {
			[self.queryUsersTask cancel];
			self.queryUsersTask = [NLUser getSuggestedFollowersFromUser:[[NLSession currentUser] userID] andQuery:cleanUser withCompletionBlock:^(NSError *error, NSArray *followings)
														 {
															 if (!error) {
																 [weakSelf setFilteredUsers:followings];
																 [[weakSelf suggestedBar] updateDatasource:[weakSelf filteredUsers]];
															 }
														 }];
		}else
		{
			[[self suggestedBar] updateDatasource:[self users]];
		}
	}
	else if (self.currentMessageMode == NLMessageModeHashtag)
	{
		//        NSLog(@"Buscar info sobre : %@",lastWord);
		NSString *cleanHashtag = [lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""];
		//        NSLog(@"El clean user : %@ y su tamaño : %d",cleanUser,cleanUser.length);
		
		if (cleanHashtag.length > 0)
		{
			self.filteredHashtags = [self filterHashtagsQuery:cleanHashtag];
			[[self suggestedBar] updateDatasource:[self filteredHashtags]];
			//[[weakSelf suggestedBar] updateDatasource:[weakSelf filteredUsers]];
		}
		else
		{
			NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
			self.hashtags = [def objectForKey:@"hashtags"];
			[[self suggestedBar] updateDatasource:[self hashtags]];
		}
	}
}
-(NSArray *)filterHashtagsQuery:(NSString *)query
{
	NSMutableArray *coincidences = [NSMutableArray array];
	for (NSString *hashtag in self.hashtags)
	{
		if (hashtag.length >= query.length)
		{
			NSString *nStr = [hashtag substringWithRange:NSMakeRange(0, query.length)];
			if ([nStr containsString: query])
			{
				[coincidences addObject:hashtag];
			}
		}
	}
	return [NSArray arrayWithArray:coincidences];
}
#pragma mark - NLSuggestedBar Delegate

- (void)suggestedBar:(NLSuggestedUsersBar *)suggestedBar didSelectUser:(id)user atIndex:(NSInteger)index
{
	NSArray *words = [[[self messageTextView] text] componentsSeparatedByString:@" "];
	NSString *lastWord = [words lastObject];
	
	NSRange rangeOfLastWord = [[[self messageTextView] text] rangeOfString:lastWord options:NSBackwardsSearch];
	/*rangeOfLastWord = [self.messageTextView selectedRange];
	 NSString *subToWord = [self.messageTextView.text substringToIndex:rangeOfLastWord.location];
	 
	 */
	if (self.currentMessageMode == NLMessageModeMention)
	{
		//NSRange iniPosition = [subToWord rangeOfString:@"@" options:NSBackwardsSearch];
		//NSString *actualWord = [self.messageTextView.text substringWithRange:NSMakeRange(iniPosition.location, rangeOfLastWord.location)];
		//rangeOfLastWord = NSMakeRange(iniPosition.location, rangeOfLastWord.location);
		self.messageTextView.text = [self.messageTextView.text stringByReplacingCharactersInRange:rangeOfLastWord withString:[NSString stringWithFormat:@"@%@ ",[user username]]];
	}
	else
	{
		//NSRange iniPosition = [subToWord rangeOfString:@"#" options:NSBackwardsSearch];
		//rangeOfLastWord = NSMakeRange(iniPosition.location, rangeOfLastWord.location);
		self.messageTextView.text = [self.messageTextView.text stringByReplacingCharactersInRange:rangeOfLastWord withString:[NSString stringWithFormat:@"#%@ ",user]];
		
	}
	[self endMentionMode];
	
}
#pragma  mark - Start Mention Mode

- (void)startMentionMode
{
	self.currentMessageMode = NLMessageModeMention;
	[[self suggestedBar] setHidden:NO];
	[self suggestedBar].hashtagging = false;
	
}
- (void)startHashtagMode
{
	self.currentMessageMode = NLMessageModeHashtag;
	[[self suggestedBar] setHidden:NO];
	[self suggestedBar].hashtagging = YES;
}
#pragma  mark - End Mention Mode

- (void)endMentionMode
{
	self.currentMessageMode = NLMessageModeNone;
	[[self suggestedBar] setHidden:YES];
}
#pragma mark - Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
	
	NSDictionary *userInfo = [notification userInfo];
	CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	
	CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	//self.InputViewVerticalSpaceConstraint.constant = keyboardRect.size.height;
	
	__weak NLSharePhotoViewController *weakSelf = self;
	
	dispatch_async(dispatch_get_main_queue(), ^{
		
		[UIView animateWithDuration:animationDuration delay:0.0f options:0 animations:^{
			[UIView setAnimationCurve:animationCurve];
			if ([self.messageTextView isFirstResponder])
			{
				self.bottomSuggestedBar.constant = keyboardRect.size.height;
				self.suggestedBar.alpha = 1;
			}
			else
			{
				
				self.suggestedBar.alpha = 0;
				
			}
			
			[[weakSelf view] layoutIfNeeded];
		} completion:^(BOOL finished) {
			
		}];
		
	});
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
	NSDictionary *userInfo = [notification userInfo];
	
	CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
	UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	
	//self.InputViewVerticalSpaceConstraint.constant = 0;
	__weak NLSharePhotoViewController *weakSelf = self;
	dispatch_async(dispatch_get_main_queue(), ^{
		[UIView animateWithDuration:animationDuration delay:0.0f options:0 animations:^{
			[UIView setAnimationCurve:animationCurve];
			
			self.bottomSuggestedBar.constant = 0;
			self.suggestedBar.alpha = 0;
			
			[[weakSelf view] layoutIfNeeded];
			
		} completion:^(BOOL finished) {
			
		}];
	});
	
}

- (void)keyboardDidHide:(NSNotification *)notification
{
	
	[self.messageTextView resignFirstResponder];
}


- (IBAction)selectOption:(id)sender {
    [self.regionButton setTitleColor:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.subRegionButton setTitleColor:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.foodTypeButton setTitleColor:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];

    [self.view endEditing:YES];
    if ([sender tag] == 1){
        self.code=1;
        [self.regionButton setTitleColor:[UIColor colorWithRed:223/255.0 green:37/255.0 blue:40 /255.0 alpha:1.0] forState:UIControlStateNormal];
        [self.optionsPicker selectRow:self.pickedRegionIndex inComponent:0 animated:NO];
    }
    if ([sender tag] == 2){
        self.code=2;
        [self.subRegionButton setTitleColor:[UIColor colorWithRed:223/255.0 green:37/255.0 blue:40 /255.0 alpha:1.0] forState:UIControlStateNormal];
        [self.optionsPicker selectRow:self.pickedSubRegionIndex inComponent:0 animated:NO];
    }
    if ([sender tag] == 3){
        self.code=3;
        [self.foodTypeButton setTitleColor:[UIColor colorWithRed:223/255.0 green:37/255.0 blue:40 /255.0 alpha:1.0] forState:UIControlStateNormal];
        
        UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
        CGFloat height=rootView.frame.size.height;
        [self.optionsPicker selectRow:self.pickedFoodTypeIndex inComponent:0 animated:NO];
        
       if(height>568){
            self.offsetView=0.0;
        }
        else{
            self.offsetView=self.foodTypeButton.frame.origin.y*6;
            [self.regionButton setEnabled:false];
            [self.subRegionButton setEnabled:false];
            [self.changeVenueButton setEnabled:false];
            [self.dishNameField setEnabled:false];
            [self.messageTextView setEditable:false];
            
        }
        
        CGPoint newOffset = CGPointMake(0, self.offsetView);
        [self.scrollView setContentOffset: newOffset animated: YES];
        
        
        

    }
    
    [self.optionsPicker reloadAllComponents];
    [self.optionsPicker setHidden:false];
    [self.actionbar setHidden:false];

    NSLog(@"Tapped tag %i",[sender tag]);
    
    /*if ([sender tag] == 1){
        [self.optionsPicker selectRow:self.pickedRegionIndex inComponent:0 animated:YES];
    }
    if ([sender tag] == 2){
        [self.optionsPicker selectRow:self.pickedSubRegionIndex inComponent:0 animated:YES];
    }
    if ([sender tag] == 3){
        [self.optionsPicker selectRow:self.pickedFoodTypeIndex inComponent:0 animated:YES];
    }*/
}


// for pickerView
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (self.code==1){
        return self.regions.count;
    }
    if (self.code==2){
        return self.subRegions.count;
    }
    if (self.code==3){
        return self.foodTypes.count;
    }
    
    return 1;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (self.code==1){
        NLRegion *region = self.regions[row];
        return region.name;
        
    }
    if (self.code==2){
        NLSubRegion *subRegion = self.subRegions[row];
        return subRegion.name;
    }
    if (self.code==3){
        NLFoodType *foodType = self.foodTypes[row];
        return foodType.food_type_name;
    }
    
    return @"";
}
-(void) appearSubRegion {
    self.subRegionHeightConstraint.constant=44.0f;
    [self.subRegionButton setHidden:false];
    [self.subregionIcon setHidden:false];
}
-(void) disapearSubRegion{
    self.subRegionHeightConstraint.constant = 0.0f;
    [self.subRegionButton setHidden:true];
    [self.subregionIcon setHidden:true];
}



@end
