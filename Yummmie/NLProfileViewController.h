//
//  NLProfileViewController.h
//  Yummmie
//
//  Created by bot on 7/1/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import "NLProfileHeaderView.h"

@class NLUser;


@interface NLProfileViewController : NLBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,NLProfileHeaderViewDelegate,UIAlertViewDelegate,UIViewControllerTransitioningDelegate>

@property (nonatomic,strong) NLUser *user;
@property (weak, nonatomic) IBOutlet UICollectionView *profileCollection;

- (id)initWithUser:(NLUser *)aUser;
- (id)initWithUser:(NLUser *)aUser isProfileUser:(BOOL)flag;
- (id)initWithUsername:(NSString *)aUsername;
- (id)initWithUserId:(NSInteger)userId andCloseButton:(BOOL)flag;

@end
