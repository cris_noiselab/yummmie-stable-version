//
//  NLComment.m
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLComment.h"
#import "NLUser.h"
#import "NLSession.h"
#import "NLTag.h"
#import "NLYummieApiClient.h"

@implementation NLComment

- (id)initWithDictionary:(NSDictionary *)dict
{
	if (self = [super init] )
	{
		_content = [dict valueForKey:@"content"];
		_commentId = [[dict valueForKey:@"id"] integerValue];
		if ([dict valueForKey:@"user"])
		{
			_user = [[NLUser alloc] initWithDictionary:[dict valueForKey:@"user"]];
		}else
		{
			_user = nil;
		}
		
		NSArray *rawTags = [dict valueForKey:@"tags_info"];
		if ([rawTags count])
		{
			NSMutableArray *tmpTags = [NSMutableArray array];
			
			for (NSDictionary *tagDict in rawTags)
			{
				NLTag *tag = [[NLTag alloc] initWithDictionary:@{@"id":[tagDict objectForKey:@"id"], @"name":[tagDict objectForKey:@"content"]}];
				[tmpTags addObject:tag];
			}
			
			_tags = [NSArray arrayWithArray:tmpTags];
		}
		
	}
	
	return self;
}


+ (NSURLSessionDataTask *)getCommentsForYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error, NSArray *comments))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSLog(@"%@", [NLSession accessToken]);
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/comments",(long)yumId];
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				NSMutableArray *newComments = [NSMutableArray array];
				
				if ([[responseObject objectForKey:@"comments"] count] > 0)
				{
					for (NSDictionary *commentDict in [responseObject objectForKey:@"comments"])
					{
						NLComment *comment = [[NLComment alloc] initWithDictionary:commentDict];
						[newComments addObject:comment];
					}
				}
				block(nil, newComments);
			}
			else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);
				block([NSError new],nil);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block([NSError new],nil);
		}
	}];
}

+ (NSURLSessionDataTask *)getPreviousCommentsForYum:(NSInteger)yumId commentId:(NSInteger)commentId withCompletionBlock:(void (^)(NSError *error, NSArray *comments))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/comments",(long)yumId];
	
	NSDictionary *params = @{@"before":[NSString stringWithFormat:@"%ld",(long)commentId]};
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:(commentId>0)?params:nil success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([responseObject isKindOfClass:[NSArray class]])
							{
								NSMutableArray *newComments = [NSMutableArray array];
								
								if ([responseObject count] > 0)
								{
									for (NSDictionary *commentDict in responseObject)
									{
										NLComment *comment = [[NLComment alloc] initWithDictionary:commentDict];
										[newComments addObject:comment];
									}
								}
								block(nil, newComments);
							}else
							{
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						
						if (block)
						{
							block([NSError new],nil);
						}
					}];
}
@end