//
//  NLSeparatorLineView.h
//  Yummmie
//
//  Created by Mauricio Ventura on 05/07/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLSeparatorLineView : UIView

@end
