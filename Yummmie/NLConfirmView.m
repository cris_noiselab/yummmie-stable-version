//
//  NLConfirmRecommendView.m
//  Yummmie
//
//  Created by Mauricio Ventura on 15/08/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import "NLConfirmView.h"

@implementation NLConfirmView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithDictionary:(NSDictionary *)dict andFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];

	if (self)
	{
		self.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.7];
		UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, frame.size.width, 200)];
		[iconView setBackgroundColor:[UIColor clearColor]];
		[iconView setImage:[UIImage imageNamed:[dict objectForKey:@"image"]]];
		[iconView setContentMode:UIViewContentModeCenter];
		[self addSubview:iconView];
		
		UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, frame.size.height / 1.6, frame.size.width - 40, 20)];
		[title setText:[dict objectForKey:@"title"]];
		[title setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
		[title setTextColor:[UIColor whiteColor]];
		title.textAlignment = NSTextAlignmentCenter;

		[self addSubview:title];
		/*
		UILabel *texto = [[UILabel alloc] initWithFrame:CGRectMake(20, frame.size.height / 2, frame.size.width - 40, 60)];
		[texto setText:[dict objectForKey:@"text"]];
		[texto setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
		[texto setTextColor:[UIColor whiteColor]];
		texto.numberOfLines = 3;
		texto.textAlignment = NSTextAlignmentCenter;
		[self addSubview:texto];
		*/
		UIButton *aceptar = [[UIButton alloc] initWithFrame:CGRectMake(0, frame.size.height - 60, frame.size.width, 60)];
		[aceptar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[aceptar setBackgroundColor:[UIColor colorWithRed:205.0/255.0 green:36.0f/255.0f blue:49.0/255.0 alpha:1.0]];
		[aceptar setTitle:[dict objectForKey:@"botTitle"] forState:UIControlStateNormal];
		[aceptar addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:aceptar];
		
		
		UIButton *cancelar = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - 60)];
		[cancelar setTitle:@"" forState:UIControlStateNormal];
		[cancelar addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
		[cancelar setBackgroundColor:[UIColor clearColor]];
		[self addSubview:cancelar];

	}
	return self;
}
-(void)confirm
{
	if ([self delegate])
	{
		[self.delegate confirm:true yumId:self.yumId andOriginalYumId:self.originalYumId];
		
	}
	[UIView animateWithDuration:0.3 animations:^{
		self.alpha = 0;
		
	} completion:^(BOOL finished) {
		[self removeFromSuperview];
	}];

}
-(void)cancel
{
	if ([self delegate])
	{
		[self.delegate confirm:false yumId:self.yumId andOriginalYumId:self.originalYumId];
		
	}
	[UIView animateWithDuration:0.3 animations:^{
		self.alpha = 0;
		
	} completion:^(BOOL finished) {
		[self removeFromSuperview];
	}];
}

@end
