//
//  NLProgressIndicator.m
//  ProgressView
//
//  Created by bot on 8/19/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLProgressIndicator.h"

@interface NLProgressIndicator()

@property (nonatomic, assign) CGFloat strokeWidth;
@property (nonatomic, strong) UIBezierPath *circlePath;
@property (nonatomic, strong) CAShapeLayer *currentLayer;


@end

@implementation NLProgressIndicator



- (id)initWithFrame:(CGRect)frame
{
    return [self init];
}

- (id)init
{
    if (self = [super initWithFrame:CGRectMake(0, 0, 105, 105)])
    {
        _progress = 0.0f;
        _strokeWidth = 8.0f;
        CGPoint arcCenter = CGPointMake(CGRectGetMidX([self bounds]), CGRectGetMidY([self bounds]));
        CGFloat radius = CGRectGetMidX([self bounds]);
        _circlePath = [UIBezierPath bezierPathWithArcCenter:arcCenter radius:radius startAngle:M_PI endAngle:4*M_PI clockwise:YES];
        
        [self addLayer];
    }
    return self;
}

- (void)addLayer
{
    CAShapeLayer *progressLayer = [CAShapeLayer layer];
    progressLayer.path = self.circlePath.CGPath;
    progressLayer.strokeColor = [[UIColor grayColor] CGColor];
    progressLayer.fillColor = [[UIColor clearColor] CGColor];
    progressLayer.lineWidth = self.strokeWidth;
    
    [self.layer addSublayer:progressLayer];
    self.currentLayer = progressLayer;
}


- (void)updateAnimations
{
    self.currentLayer.strokeEnd = self.progress;
    [self.currentLayer didChangeValueForKey:@"endValue"];
}

- (void)setProgress:(CGFloat)progress
{
    _progress = progress;
    [self updateAnimations];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
