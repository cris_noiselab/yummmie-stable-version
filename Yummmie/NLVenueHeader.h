//
//  NLVenueHeader.h
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLVenueHeader : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIImageView *venueImage;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueDetailAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueStateLabel;
@property (weak, nonatomic) IBOutlet UIButton *sharePhoto;

@end
