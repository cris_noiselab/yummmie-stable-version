//
//  NLYummieApiClient.m
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLYummieApiClient.h"


static NLYummieApiClient *_sharedClient = nil;

@implementation NLYummieApiClient

+ (instancetype)sharedClient
{
    NSLog(@"Base url  %@", AFAppDotNetAPIBaseURLString);

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[NLYummieApiClient alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _sharedClient.responseSerializer = [NLJSONResponseSerializerWithData serializer];
        _sharedClient.requestSerializer = [AFHTTPRequestSerializer serializer];
        [_sharedClient.requestSerializer setTimeoutInterval:90.0f];
			_sharedClient.responseSerializer = [NLJSONResponseSerializerWithData serializer];
    });
    
    return _sharedClient;
}

+ (void)setValue:(NSString *)value forHeaderField:(NSString *)field
{
    [_sharedClient.requestSerializer setValue:value forHTTPHeaderField:field];
    [_sharedClient.requestSerializer setTimeoutInterval:15.0f];
    [_sharedClient.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
}

- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler
{
    NSMutableURLRequest *modifiedRequest = request.mutableCopy;
    AFNetworkReachabilityManager *reachability = self.reachabilityManager;
    if (!reachability.isReachable)
    {
        NSLog(@"Not reachable");
        NSLog(@"Request : %@", modifiedRequest);
        NSLog(@"**********\n");
        if ([request.HTTPMethod isEqualToString:@"GET"])
        {
            modifiedRequest.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
        }
        else
        {
            modifiedRequest.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        }
    }
    return [super dataTaskWithRequest:modifiedRequest
                    completionHandler:completionHandler];
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
 willCacheResponse:(NSCachedURLResponse *)proposedResponse
 completionHandler:(void (^)(NSCachedURLResponse *cachedResponse))completionHandler
{
    NSURLResponse *response = proposedResponse.response;
    NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse*)response;
    NSDictionary *headers = HTTPResponse.allHeaderFields;
    
    if (headers[@"Cache-Control"])
    {
        NSMutableDictionary *modifiedHeaders = headers.mutableCopy;
        modifiedHeaders[@"Cache-Control"] = @"max-age=60";
        NSHTTPURLResponse *modifiedHTTPResponse = [[NSHTTPURLResponse alloc]
                                                   initWithURL:HTTPResponse.URL
                                                   statusCode:HTTPResponse.statusCode
                                                   HTTPVersion:@"HTTP/1.1"
                                                   headerFields:modifiedHeaders];
        
        proposedResponse = [[NSCachedURLResponse alloc] initWithResponse:modifiedHTTPResponse
                                                                    data:proposedResponse.data
                                                                userInfo:proposedResponse.userInfo
                                                           storagePolicy:proposedResponse.storagePolicy];
    }
    
    [super URLSession:session dataTask:dataTask willCacheResponse:proposedResponse completionHandler:completionHandler];
}
@end
