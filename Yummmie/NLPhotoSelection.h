//
//  NLPhotoSelection.h
//  Yummmie
//
//  Created by bot on 11/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#ifndef Yummmie_NLPhotoSelection_h
#define Yummmie_NLPhotoSelection_h

typedef NS_ENUM(NSInteger, PhotoSelectionMode)
{
    PhotoSelectionModeNone,
    PhotoSelectionModeProfile,
    PhotoSelectionModeCover
};

#endif
