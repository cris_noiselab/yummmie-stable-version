//
//  NLBrowserViewController.m
//  Yummmie
//
//  Created by bot on 9/2/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBrowserViewController.h"

@interface NLBrowserViewController ()<UIWebViewDelegate>

@property (nonatomic, strong) NSURL *url;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)backAction:(id)sender;
- (IBAction)refreshAction:(id)sender;

- (void)dismiss:(id)sender;

@end

@implementation NLBrowserViewController


- (id)initWithURL:(NSURL *)url
{
    self = [super initWithNibName:NSStringFromClass([NLBrowserViewController class]) bundle:nil];
    if (self)
    {
        _url = url;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self.navigationController navigationBar] setTranslucent:NO];
    [[self webView] setDelegate:self];
    [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:[self url]]];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"close_title", nil) style:UIBarButtonItemStylePlain target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender
{
    [self.webView goBack];
}

- (IBAction)refreshAction:(id)sender
{
    [self.webView reload];
}

- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideLoadingSpinner];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showLoadingSpinner];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self hideLoadingSpinner];
}
@end
