//
//  NLTemporalViewController.m
//  Yummmie
//
//  Created by bot on 7/4/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLTemporalViewController.h"
#import "NLDiscoverUserSwipeCellView.h"

@interface NLTemporalViewController ()<UITableViewDataSource, UITableViewDelegate,NLDiscoverUserSwipeCellViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation NLTemporalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.table registerNib:[UINib nibWithNibName:NSStringFromClass([NLDiscoverUserSwipeCellView class]) bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.table setDelegate:self];
    [self.table setDataSource:self];
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLDiscoverUserSwipeCellView *cell = (NLDiscoverUserSwipeCellView *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.tag = indexPath.row;
    [cell setDelegate:self];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}

#pragma mark - NLDiscoverUserSwipeCellViewDelegate

- (void)openDetailWithTag:(NSInteger)tag
{
    NLTemporalViewController *temporal = [[NLTemporalViewController alloc] init];
    [temporal setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:temporal animated:YES];
}

- (void)openDishWithTag:(NSInteger)tag
{
    NLTemporalViewController *temporal = [[NLTemporalViewController alloc] init];
    [temporal setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:temporal animated:YES];
}

@end