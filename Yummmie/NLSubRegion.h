//
//  NLSubRegion.h
//  Yummmie
//
//  Created by bot on 7/27/17.
//  Copyright © 2017 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"



@interface NLSubRegion : NSObject

@property (nonatomic, copy) NSString *subRegion_id;
@property (nonatomic, copy) NSString *name;

- (instancetype)initWithDictionary:(NSDictionary *)authenticationDict;




@end
