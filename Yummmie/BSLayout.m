//
//  BSLayout.m
//  Yummmie
//
//  Created by bot on 2/12/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "BSLayout.h"

@interface BSLayout ()

@property (nonatomic, strong) NSMutableArray *itemsAttributes;

@end

@implementation BSLayout

- (void)prepareLayout
{
    self.minimumInteritemSpacing = 1;
    self.minimumLineSpacing = 1;
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.itemsAttributes = [NSMutableArray array];
    
    NSUInteger numberOfItems = [self.collectionView numberOfItemsInSection:0];
    
    
    CGFloat width = self.collectionView.frame.size.width;
    
    CGFloat xOffset = 0.0f;
    CGFloat yOffset = 0.0f;
    
    NSUInteger numberOfSections = numberOfItems/6;
    
    BOOL left = YES;
    
    NSInteger elementIndex = 0;
    
    BOOL bigWassAdded = NO;    
    for (NSInteger section = 0; section < numberOfSections; section++)
    {
        for (NSInteger i = 0; i < 6; i++)
        {
            CGSize itemSize;

            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:elementIndex inSection:0];
            UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            
            if (left)
            {
                if (i == 0)
                {
                    itemSize = CGSizeMake(213, 213);
                }else
                {
                    itemSize = CGSizeMake(106, 106);
                }
                
                attributes.frame = CGRectMake(xOffset, yOffset, itemSize.width, itemSize.height);
                
                if ((xOffset+itemSize.width) < (width-2))
                {
                    xOffset +=(itemSize.width+1);
                
                
                    if (itemSize.width == 213)
                    {
                        bigWassAdded = YES;
                    }
                }else
                {
                    if (bigWassAdded)
                    {
                        bigWassAdded = NO;
                        yOffset += (itemSize.height+1);
                    }else
                    {
                        xOffset = 0;
                        yOffset += (itemSize.height+1);
                    }
        
                }
                
            }else
            {
                if (i == 1)
                {
                    itemSize = CGSizeMake(213, 213);
                }else
                {
                    itemSize = CGSizeMake(106, 106);
                }
                
                attributes.frame = CGRectMake(xOffset, yOffset, itemSize.width, itemSize.height);
                
                
                if (xOffset+itemSize.width < width)
                {
                    xOffset +=(itemSize.width+1);
                    

                    
                }else
                {
                    yOffset+=107;
                    xOffset = 0;
                    
                }
                
                if (i == 2)
                {
                    yOffset+=107;
                    xOffset = 0;
                }
                
            }
            
            if (elementIndex < numberOfItems)
            {
                elementIndex++;
            }
            
            [[self itemsAttributes] addObject:attributes];
        }
        left = !left;
    }
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return NO;
}


- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    return [[self itemsAttributes] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes *evaluatedObject, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, [evaluatedObject frame]);
    }]];
}

- (CGSize)collectionViewContentSize
{
    return CGSizeMake(self.collectionView.frame.size.width, ([self.collectionView numberOfItemsInSection:0]/6)*325+(([UIScreen mainScreen].bounds.size.height==480)?70:0));
}
@end
