//
//  NLVenueDetailHeaderView.m
//  Yummmie
//
//  Created by bot on 5/15/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLVenueDetailHeaderView.h"

@implementation NLVenueDetailHeaderView

- (void)awakeFromNib {
    // Initialization code
    self.venuePicture.layer.cornerRadius = CGRectGetWidth(self.venuePicture.bounds)/2.0f;
    [self.venuePicture setClipsToBounds:YES];
}

- (IBAction)shareAction:(id)sender {
    if ([[self shareButton] isEnabled] && [[self delegate] respondsToSelector:@selector(shareRestaurantInfo)]) {
        [[self delegate] shareRestaurantInfo];
    }
}

- (IBAction)openLocation:(id)sender {
    if ([[self locationButton] isEnabled] && [[self delegate] respondsToSelector:@selector(openRestarantLocation)]) {
        [[self delegate] openRestarantLocation];
    }
}

- (IBAction)callAction:(id)sender {
    if ([[self phoneButton] isEnabled] && [[self delegate] respondsToSelector:@selector(callRestaurant)]) {
        [[self delegate] callRestaurant];
    }
}

- (IBAction)openVenueSite:(id)sender {
    if ([[self delegate] respondsToSelector:@selector(openVenueSite)]) {
        [[self delegate] openVenueSite];
    }
}

- (IBAction)openCamera:(id)sender {
    if ([[self delegate] respondsToSelector:@selector(openCamera)]) {
        [[self delegate] openCamera];
    }
}
@end
