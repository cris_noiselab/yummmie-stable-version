//
//  NLUser.h
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"

@interface NLUser : NSObject <NSCoding>

@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSInteger userID;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *bio;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *thumbAvatar;
@property (nonatomic, copy) NSString *bigAvatar;
@property (nonatomic, copy) NSString *cover;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *authToken;
@property (nonatomic) NSInteger followers;
@property (nonatomic) NSInteger following;
@property (nonatomic) NSInteger yummmies;
@property (nonatomic) BOOL follow;
@property (nonatomic) BOOL followBack;

@property (nonatomic) BOOL verified;
@property (nonatomic, strong) NSMutableArray *authentications;
@property (nonatomic) BOOL newRegister;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

+ (NSURLSessionDataTask *)registerUserWithEmail:(NSString *)email name:(NSString *)name username:(NSString *)username activity:(BOOL)activity password:(NSString *)password coverImage:(UIImage *)coverImage profileImage:(UIImage *)profileImage withCompletionBlock:(void (^)(NSError * error, NLUser *user))block;

+ (NSURLSessionDataTask *)registerUserWithFacebookToken:(NSString *)token email:(NSString *)email activity:(BOOL)activity withCompletionBlock:(void (^)(NSError * error, NLUser *user))block;
+ (NSURLSessionDataTask *)loginWithFacebookToken:(NSString *)facebookToken withCompletionBlock:(void (^)(NSError *error,NLUser *user))block;

+ (NSURLSessionDataTask *)loginWithEmail:(NSString *)email password:(NSString *)password withCompletionBlock:(void (^)(NSError *error, NLUser *user))block;

+ (NSURLSessionDataTask *)getUser:(NSInteger)userID withCompletionBlock:(void (^)(NSError *error, NLUser *user))block;

+ (NSURLSessionDataTask *)getUserWithUsername:(NSString *)username withCompletionBlock:(void (^)(NSError *error,NLUser *user))block;



+ (NSURLSessionDataTask *)getFollowersFromUser:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *followers, NSInteger currentPage, NSInteger totalPages))block;

+ (NSURLSessionDataTask *)getFollowingsFromUser:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *users, NSInteger currentPage, NSInteger totalPages))block;

+ (NSURLSessionDataTask *)getFollowings:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *followings, NSInteger currentPage, NSInteger totalPages))block;
/*
+ (NSURLSessionDataTask *)getFollowersFromUser:(NSInteger)userId beforeUserId:(NSInteger)followerId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;

+ (NSURLSessionDataTask *)getFollowersFromUser:(NSInteger)userId afterUserId:(NSInteger)followerId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;

+ (NSURLSessionDataTask *)getFollowingsFromUser:(NSInteger)userId beforeUserId:(NSInteger)followingId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;
+ (NSURLSessionDataTask *)getFollowingsFromUser:(NSInteger)userId afterUserId:(NSInteger)followingId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;
*/



+ (NSURLSessionDataTask *)getFavsBeforeId:(NSInteger)beforeId withCompletionBlock:(void (^)(NSError *error, NSArray *yums))block;
+ (NSURLSessionDataTask *)getFavsAfterId:(NSInteger)beforeId withCompletionBlock:(void (^)(NSError *error, NSArray *yums))block;

+ (NSURLSessionDataTask *)unfollowUser:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error))block;

+ (NSURLSessionDataTask *)followUser:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error))block;

+ (NSURLSessionDataTask *)updateUserWithUserId:(NSInteger)userId name:(NSString *)name website:(NSString *)website bio:(NSString *)bio coverPhoto:(UIImage *)coverImage avatarPhoto:(UIImage *)avatarImage email:(NSString *)email username:(NSString *)username withCompletionBlock:(void (^)(NSError *error, NLUser *user))block;

+ (NSURLSessionDataTask *)searchUserWithString:(NSString *)string afterUserId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;

+ (NSURLSessionDataTask *)resetPasswordWithEmail:(NSString *)email andCompletionBlock:(void (^)(NSError *error, BOOL emailFound))block;

+ (NSURLSessionDataTask *)randomUsersWithCompletionBlock:(void (^)(NSError * error, NSArray *users))block;

+ (NSURLSessionDataTask *)getSuggestedFollowersFromUser:(NSInteger)userId andQuery:(NSString *)query withCompletionBlock:(void (^)(NSError *error, NSArray *followings))block;

+ (NSURLSessionDataTask *)findFriends:(NSString *)provider withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;

- (NSURLSessionDataTask *)destroyWithCompletionBlock:(void (^)(NSError *))block;

@end
