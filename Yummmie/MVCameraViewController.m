//
//  MVCameraViewController.m
//  Yummmie
//
//  Created by Mauricio Ventura on 20/07/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import "MVCameraViewController.h"
#import "MVCameraEditViewController.h"

#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>


#import "DBCameraSegueViewController.h"
#import "DBCameraBaseCropViewController+Private.h"
#import "DBCameraCropView.h"
#import "DBCameraFiltersView.h"
#import "DBCameraFilterCell.h"
#import "DBCameraLoadingView.h"
#import "UIImage+TintColor.h"
#import "UIImage+Crop.h"

#import <GPUImage/GPUImage.h>
@import ImageIO;

@interface MVCameraViewController ()


typedef NS_ENUM(NSInteger, MVCameraOrientation)
{
	MVCameraOrientationPortraitVertical = 1,
	MVCameraOrientationPortraitHorizontal = 2,
	
	MVCameraOrientationLandscapeVertical = 1,
	MVCameraOrientationLandscapeHorizontal = 2,
};

@end

@implementation MVCameraViewController
{
	bool enfoque;
	AVCaptureSession *capture;
	CMMotionManager *motionManager;
	NSUInteger triggercam;
	AVCaptureStillImageOutput *imagecapture;
	
	AVCaptureDevice *cameraDevice;
	AVCaptureVideoPreviewLayer *previewLayer;
	
	NSOperationQueue *theQueue;
	NSTimer *enfoqueTimer;

	
	NSDictionary *gps_dict;
	CLLocationManager *locationManager;
	CLLocation *currentLocation;
}
/*-(instancetype)init
{
	
	return [super init];
}*/
- (void)viewDidLoad
{
	[super viewDidLoad];
	if(!self.areaOkColor)
	{
		self.areaOkColor = self.area.backgroundColor;
		
	}
	if(!self.areaWrongColor)
	{
		self.areaWrongColor = self.area.backgroundColor;
		
	}
	if (self.countdown == 0)
	{
		self.countdown = 3;
	}
	theQueue = [[NSOperationQueue alloc] init];
	motionManager = [[CMMotionManager alloc] init];
	motionManager.accelerometerUpdateInterval = 0.3;
	
	locationManager = [[CLLocationManager alloc] init];
	locationManager.delegate = self;
	locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	

}
/*- (BOOL) prefersStatusBarHidden
{
	return YES;
}*/
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.view.userInteractionEnabled = true;
	self.navigationController.navigationBar.hidden = true;
	[locationManager startUpdatingLocation];

}
-(void)viewDidAppear:(BOOL)animated
{
	
	[super viewDidAppear:animated];
	[self begin];
	triggercam = 0;
	enfoque = false;
	[capture startRunning];
	[self startAccelerationUpdates];
	
}
-(void)viewWillDisappear:(BOOL)animated
{
	[locationManager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"didFailWithError: %@", error);
	UIAlertView *errorAlert = [[UIAlertView alloc]
														 initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	NSLog(@"didUpdateToLocation: %@", newLocation);
	currentLocation = newLocation;
	
}
-(void)startAccelerationUpdates
{
	
	[motionManager startAccelerometerUpdatesToQueue:theQueue withHandler:^(CMAccelerometerData * _Nullable accelerometerData, NSError * _Nullable error)
	 {
		 if (!error)
		 {
			 CMAcceleration acceleration = accelerometerData.acceleration;
			 [self motionHandlerWithAcceleration:acceleration];
		 }
		 
		 
	 }];
}
-(void)motionHandlerWithAcceleration:(CMAcceleration)acceleration
{
	if(acceleration.z < -0.98 && fabs(acceleration.x) < 0.3 && fabs(acceleration.y) < 0.3)
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[UIView animateWithDuration:0.5 animations:^{
				self.topIndicator.alpha = 1;
				self.bottomIndicator.alpha = 1;
				[self.shotBtn setImage:[UIImage imageNamed:@"bot-shutter-red"] forState:UIControlStateNormal];

			}];
		});
		enfoque = true;
	}
	else if (acceleration.z > -0.97 && (fabs(acceleration.x) > 0.3 || fabs(acceleration.y) > 0.3))
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[UIView animateWithDuration:0.5 animations:^{
				self.topIndicator.alpha = 0;
				self.bottomIndicator.alpha = 0;
				[self.shotBtn setImage:[UIImage imageNamed:@"bot-shutter-white"] forState:UIControlStateNormal];

			}];

		});
		enfoque = false;
	}
	/*if (triggercam < (self.countdown * 10))
	{
		if (acceleration.z < -0.80 && enfoque == false)
		{
			if (!enfoqueTimer)
			{
				enfoqueTimer = [NSTimer scheduledTimerWithTimeInterval:20000 target:self selector:@selector(enfoque) userInfo:nil repeats:false];
				[enfoqueTimer fire];
			}
			dispatch_async(dispatch_get_main_queue(), ^
										 {
											 self.grid.alpha = 1;
											 self.dotsView.alpha = 1;
											 self.ball.alpha = 1;
											 for (UIView *dot in self.dotsView.subviews)
											 {
												 [dot setBackgroundColor:[UIColor whiteColor]];
											 }
										 });
			
		}
		else if(acceleration.z > -0.80)
		{
			enfoque = false;
			dispatch_async(dispatch_get_main_queue(), ^
										 {
											 self.grid.alpha = 0;
											 self.dotsView.alpha = 0;
											 self.ball.alpha = 0;
											 for (UIView *dot in self.dotsView.subviews)
											 {
												 [dot setBackgroundColor:[UIColor whiteColor]];
											 }
										 });
		}
		if (acceleration.z < -0.94 && enfoque == true)
		{
			dispatch_async(dispatch_get_main_queue(), ^
										 {
											 self.ball.center = CGPointMake(self.area.frame.size.width * (acceleration.x * -1) + (self.area.frame.size.width / 2) , self.area.frame.size.height * acceleration.y + (self.area.frame.size.height / 2));
											 int dot = floor(triggercam / 10);
											 int i = 0;
											 while (i <= dot)
											 {
												 if (dot < 3)
												 {
													 [[self.dotsView.subviews objectAtIndex:i] setBackgroundColor:[UIColor blackColor]];
												 }
												 i++;
											 }
											 
										 });
			if (fabs(acceleration.x) < 0.3 && fabs(acceleration.y) < 0.3)
			{
				if( triggercam == (self.countdown * 2))
				{
					NSError *error;
					[cameraDevice lockForConfiguration:&error];
					[cameraDevice setFocusPointOfInterest:CGPointMake(0.5, 0.5)];
					[cameraDevice unlockForConfiguration];
				}
				triggercam++;
				NSLog(@"%ld", triggercam);
				self.area.backgroundColor = self.areaOkColor;
				self.counter.text = [NSString stringWithFormat:@"%d", (int)floor(triggercam / 6)];
				
			}
			else
			{
				self.area.backgroundColor = self.areaWrongColor;
				dispatch_async(dispatch_get_main_queue(), ^
											 {
												 for (UIView *dot in self.dotsView.subviews)
												 {
													 [dot setBackgroundColor:[UIColor whiteColor]];
												 }
											 });
			}
		}
		else
		{
			enfoque = false;
			triggercam = 0;
			self.area.backgroundColor = self.areaWrongColor;
			self.counter.text = @"0";
			dispatch_async(dispatch_get_main_queue(), ^{
				for (UIView *dot in self.dotsView.subviews)
				{
					[dot setBackgroundColor:[UIColor whiteColor]];
				}
			});
			
		}
	}
	else
	{
		//triggercam = 0;
		
		[self takePhoto];
		
	}*/
}
-(void)begin
{
	cameraDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

	NSError *error;
	/*[cameraDevice lockForConfiguration:&error];
	//cameraDevice.videoZoomFactor = 1;
	[cameraDevice unlockForConfiguration];
	 */
	
	
	capture = [[AVCaptureSession alloc] init];
	capture.sessionPreset = AVCaptureSessionPresetPhoto;
	AVCaptureDeviceInput *input = [[AVCaptureDeviceInput alloc] initWithDevice:cameraDevice error:&error];
	
	if (error == nil && [capture canAddInput:input])
	{
		[capture addInput:input];
	}
	else
	{
		NSLog(@"error %@", error);
		NSLog(@"can %d", [capture canAddInput:input]);

	}
	previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:capture];
	//previewLayer.frame = CGRectMake(0, self.view.center.y - (self.view.frame.size.width / 2), self.view.frame.size.width, self.view.frame.size.width);
	
	
	previewLayer.frame = CGRectMake(0, 0, self.cameraView.frame.size.width, self.cameraView.frame.size.width);
	previewLayer.masksToBounds = true;
	previewLayer.anchorPoint = CGPointMake(0.5, 0.5);
	previewLayer.position = CGPointMake((self.cameraView.frame.size.width / 2), (self.cameraView.frame.size.width / 2));
	previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
	//previewLayer.videoGravity = AVLayerVideoGravityResize;

	previewLayer.backgroundColor = [UIColor redColor].CGColor;
	
	[self.cameraView.layer addSublayer:previewLayer];
	
	imagecapture = [[AVCaptureStillImageOutput alloc] init];
	[imagecapture setOutputSettings:@{AVVideoCodecKey: AVVideoCodecJPEG}];
	[capture addOutput:imagecapture];
	[capture startRunning];
}
-(void)enfoque
{
	[enfoqueTimer invalidate];
	enfoqueTimer = nil;
	enfoque = true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)takePhoto
{
	[locationManager stopUpdatingLocation];

	self.view.userInteractionEnabled = false;
	self.view.backgroundColor = self.areaOkColor;
	[motionManager stopAccelerometerUpdates];
	AVCaptureConnection *videoConnection = [imagecapture connectionWithMediaType:AVMediaTypeVideo];
	[imagecapture captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error)
	 {
		 if(imageDataSampleBuffer != nil)
		 {
			 NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
			 UIImage *image = [[UIImage alloc] initWithData:imageData];
			 
			 CGRect outputRect = [previewLayer metadataOutputRectOfInterestForRect:previewLayer.bounds];
			 CGImageRef takenCGImage = image.CGImage;
			 size_t width = CGImageGetWidth(takenCGImage);
			 size_t height = CGImageGetHeight(takenCGImage);
			 // NSLog(@)
			 CGRect cropRect = CGRectMake(((width / 2) - (height / 2)), 0, outputRect.size.width * width, outputRect.size.width * width);
			 
			 CGImageRef cropCGImage = CGImageCreateWithImageInRect(takenCGImage, cropRect);
			 image = [UIImage imageWithCGImage:cropCGImage scale:1 orientation:image.imageOrientation];
			 CGImageRelease(cropCGImage);
			 
			 CGFloat newW = 256.0;
			 CGFloat newH = 340.0;
			 
			 if ( image.size.width > image.size.height ) {
				 newW = 340.0;
				 newH = ( newW * image.size.height ) / image.size.width;
			 }
			 NSDictionary *meta;
			 if (currentLocation)
			 {
				 meta = @{@"gps": [NSNumber numberWithBool:true], @"latitude": [NSNumber numberWithFloat:currentLocation.coordinate.latitude], @"longitude": [NSNumber numberWithFloat:currentLocation.coordinate.longitude]};
			 }
			 [self showEditorWithImage:image  andMeta:meta];

			 
			 
		 }
	 }];
}
-(void)viewDidDisappear:(BOOL)animated
{
	[capture stopRunning];
	[motionManager stopAccelerometerUpdates];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)imageDidPicked:(UIImage *)image
{
	[self.delegate camera:self didFinishWithImage:image withMetadata:gps_dict];
	
}
- (IBAction)close:(UIButton *)sender
{
	[[self delegate] closeCamera];
}

- (IBAction)gallery:(id)sender
{
	//Permisos
	UIImagePickerController *pickerVC = [[UIImagePickerController alloc] init];
	pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	pickerVC.mediaTypes = @[(NSString *)kUTTypeImage];
	pickerVC.navigationBar.tintColor = [UIColor redColor];
	//pickerVC.allowsEditing = true;
	pickerVC.delegate = self;
	[self presentViewController:pickerVC animated:true completion:nil];
}
- (IBAction)takePhoto:(id)sender
{
	if ([sender isKindOfClass:[UIButton class]] == 1)
	{
		[self takePhoto];
	}
	else
	{
		if (enfoque == true)
		{
			[self takePhoto];
		}
	}
}

- (IBAction)focusCenter:(UITapGestureRecognizer *)sender
{
	self.area.hidden = false;
	[UIView animateWithDuration:0.5 animations:^
	{
		
		self.area.alpha = 1;
	} completion:^(BOOL finished)
	{
		NSError *error;
		[cameraDevice lockForConfiguration:&error];
		[cameraDevice setFocusPointOfInterest:CGPointMake(0.5, 0.5)];
		[cameraDevice unlockForConfiguration];

		[UIView animateWithDuration:0.5 animations:^{
			self.area.alpha = 0;
		} completion:^(BOOL finished) {
			self.area.hidden = true;
		}];
	}];
	

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[capture startRunning];
	[self startAccelerationUpdates];
	[picker dismissViewControllerAnimated:true completion:nil];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
	NSLog(@"%@", info);
	
	
	
	ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
	[library assetForURL:[info objectForKey:UIImagePickerControllerReferenceURL]
					 resultBlock:^(ALAsset *asset)
	 {

         ALAssetRepresentation *image_representation = [asset defaultRepresentation];
		 
		 // create a buffer to hold image data
		 uint8_t *buffer = (Byte*)malloc([image_representation size]);
		 NSUInteger length = [image_representation getBytes:buffer fromOffset: 0.0  length:image_representation.size error:nil];
		 
		 if (length != 0)  {
			 
			 // buffer -> NSData object; free buffer afterwards
			 NSData *adata = [[NSData alloc] initWithBytesNoCopy:buffer length:image_representation.size freeWhenDone:YES];
			 
			 // identify image type (jpeg, png, RAW file, ...) using UTI hint
			 NSDictionary* sourceOptionsDict = [NSDictionary dictionaryWithObjectsAndKeys:(id)[image_representation UTI] ,kCGImageSourceTypeIdentifierHint,nil];
			 
			 // create CGImageSource with NSData
			 CGImageSourceRef sourceRef = CGImageSourceCreateWithData((__bridge CFDataRef) adata,  (__bridge CFDictionaryRef) sourceOptionsDict);
			 
			 // get imagePropertiesDictionary
			 CFDictionaryRef imagePropertiesDictionary;
			 imagePropertiesDictionary = CGImageSourceCopyPropertiesAtIndex(sourceRef,0, NULL);
			 
			 // get exif data
			 CFDictionaryRef exif = (CFDictionaryRef)CFDictionaryGetValue(imagePropertiesDictionary, kCGImagePropertyExifDictionary);
			 NSDictionary *exif_dict = (__bridge NSDictionary*)exif;
			 NSLog(@"exif_dict: %@",exif_dict);
			 
			 CFDictionaryRef gps = (CFDictionaryRef)CFDictionaryGetValue(imagePropertiesDictionary, kCGImagePropertyGPSDictionary);
			 gps_dict = (__bridge NSDictionary*)gps;
			 NSLog(@"gps_dict: %@",gps_dict);
			 // save image WITH meta data
			 NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
			 NSURL *fileURL = nil;
			 CGImageRef imageRef = CGImageSourceCreateImageAtIndex(sourceRef, 0, imagePropertiesDictionary);
			 
			 if (![[sourceOptionsDict objectForKey:@"kCGImageSourceTypeIdentifierHint"] isEqualToString:@"public.tiff"])
			 {
				 fileURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.%@",
																					 documentsDirectory,
																					 @"myimage",
																					[[[sourceOptionsDict objectForKey:@"kCGImageSourceTypeIdentifierHint"] componentsSeparatedByString:@"."] objectAtIndex:1]
																					 ]];
				 
				 CGImageDestinationRef dr = CGImageDestinationCreateWithURL ((__bridge CFURLRef)fileURL,
																																		 (__bridge CFStringRef)[sourceOptionsDict objectForKey:@"kCGImageSourceTypeIdentifierHint"],
																																		 1,
																																		 NULL
																																		 );
				 CGImageDestinationAddImage(dr, imageRef, imagePropertiesDictionary);
				 CGImageDestinationFinalize(dr);
				 CFRelease(dr);
			 }
			 else
			 {
				 NSLog(@"no valid kCGImageSourceTypeIdentifierHint found …");
			 }
			 
			 // clean up
			 CFRelease(imageRef);
			 CFRelease(imagePropertiesDictionary);
			 CFRelease(sourceRef);
		 }
		 else {
			 NSLog(@"image_representation buffer length == 0");
		 }
	 }
					failureBlock:^(NSError *error) {
						NSLog(@"couldn't get asset: %@", error);
					}
	 ];
	
	
	
	UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
	if(!img) img = [info objectForKey:UIImagePickerControllerOriginalImage];
	;
	CIImage *fullImage = [CIImage imageWithContentsOfURL:info[UIImagePickerControllerReferenceURL]];
	
	NSLog(@"%@", fullImage.properties.description);
	[picker dismissViewControllerAnimated:true completion:^{
		[self showEditorWithImage:img andMeta:gps_dict];
	}];

	
}

-(void)showEditorWithImage:(UIImage *)image andMeta:(NSDictionary *)meta
{
	if (image)
	{
        UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
		//CGFloat newW = 256.0;
		//CGFloat newH = 340.0;
        CGFloat newW = rootView.frame.size.width*0.8;
        CGFloat newH = rootView.frame.size.width*1.062;
        
		if ( image.size.width > image.size.height ) {
			newW = rootView.frame.size.width*1.062;
			newH = ( newW * image.size.height ) / image.size.width;
		}
		
		DBCameraSegueViewController *segue = [[DBCameraSegueViewController alloc] initWithImage:image thumb:[UIImage returnImage:image withSize:(CGSize){ newW, newH }]];
		[segue setTintColor:[UIColor whiteColor]];
		[segue setSelectedTintColor:[UIColor whiteColor]];
		[segue setForceQuadCrop:true];
		[segue enableGestures:true];
		[segue setDelegate:self];
		[segue setCapturedImageMetadata:meta];
		[segue setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
		 {
			 segue.cropMode = YES;
			 segue.cropRect = CGRectMake(0, 0, rootView.frame.size.width, rootView.frame.size.width);
		 }];
		
		[self.navigationController pushViewController:segue animated:YES];
	}
}
-(void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
	NSLog(@"%@", metadata);
	NSDictionary *nMeta = metadata;
	if (![metadata objectForKey:@"gps"])
	{
		CGFloat latitude = (([[metadata valueForKey:@"LatitudeRef"] isEqualToString:@"N"])?(1):(-1))*[[metadata valueForKey:@"Latitude"] floatValue];
		CGFloat longitude = (([[metadata valueForKey:@"LongitudeRef"] isEqualToString:@"N"])?(1):(-1))*[[metadata valueForKey:@"Longitude"] floatValue];
		nMeta = @{@"gps": [NSNumber numberWithBool:true], @"latitude": [NSNumber numberWithFloat:latitude], @"longitude": [NSNumber numberWithFloat:longitude]};
	}
	
	[self.delegate camera:self didFinishWithImage:image withMetadata:nMeta];

}
@end
