//
//  NLMentionInteractionCell.m
//  Yummmie
//
//  Created by bot on 9/23/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLMentionInteractionCell.h"
#import "NLConstants.h"
#import "UIColor+NLColor.h"

static NSRegularExpression *userMentionExpression;

@interface NLMentionInteractionCell ()

@property (strong, nonatomic) TTTAttributedLabel *contentLabel;

- (IBAction)openYum:(id)sender;
- (IBAction)openUser:(id)sender;

@end

@implementation NLMentionInteractionCell

- (void)awakeFromNib
{
    // Initialization code
    
    self.contentLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.bounds)+kTimeLabelXPadding,kTimeLabelYOrigin, 200.0f, 90.0f)];
    UIColor *commentColor = [UIColor nl_colorWith255Red:89.0f green:92.0f blue:105.0f andAlpha:255.0f];
    
    [[self contentLabel] setDelegate:self];
    [self.contentLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.contentLabel setMinimumScaleFactor:0.5f];
    [self.contentLabel setNumberOfLines:0];
    [self.contentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.contentLabel setTextColor:commentColor];
    [self.contentLabel adjustsFontSizeToFitWidth];
    [self.contentLabel setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    
    NSDictionary *linkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self contentLabel] setLinkAttributes:linkAttributes];
    
    NSDictionary *activeLinkAttributes = @{(id)kCTForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    [[self contentLabel] setActiveLinkAttributes:activeLinkAttributes];
    NSDictionary *inactiveLinkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self contentLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    [self.contentView addSubview:self.contentLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)openYum:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openMentionYumWithTag:)])
    {
        [[self delegate] openMentionYumWithTag:[self tag]];
    }
}

- (IBAction)openUser:(id)sender
{

    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openMentionOwnerWithTag:)])
    {
        [[self delegate] openMentionOwnerWithTag:[self tag]];
    }
}

- (void)setOwner:(NSString *)owner comment:(NSString *)comment
{
    NSString *content = [NSString stringWithFormat:@"@%@ %@ %@",owner,NSLocalizedString(@"mention_user_yum_text", nil),comment];
    self.contentLabel.text = content;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        userMentionExpression = [NSRegularExpression regularExpressionWithPattern:@"@{1}([-A-Za-z0-9_]{2,})" options:NO error:nil];
    });
    
    NSArray *matches;
    NSRange range;
    
    @try
    {
        range = NSMakeRange(0, [content length]);
        matches = [userMentionExpression matchesInString:content
                                          options:0
                                            range:range];
    }
    @catch (NSException *exception)
    {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",content,NSStringFromRange(range));
    }
    @finally {
        
    }
    
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [content substringWithRange:matchRange];
        NSString* user = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
        [self.contentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    
    [self.contentLabel sizeToFit];
    
    __weak NLMentionInteractionCell *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect timeLabelFrame = weakSelf.timeLabel.frame;
        timeLabelFrame.origin.y = CGRectGetMaxY(weakSelf.contentLabel.bounds)+25.0f;
    });
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [[self contentLabel] setText:@""];
    
    [[self contentLabel] setFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.bounds)+kTimeLabelXPadding,kTimeLabelYOrigin, 200.0f, 90.0f)];
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:kUsernameScheme])
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(openMentionOwnerWithTag:)])
        {
					[[self delegate] openMentionUserWithString:[url resourceSpecifier]];

					//[[self delegate] openMentionOwnerWithTag:[self tag]];
        }
    }
}
@end
