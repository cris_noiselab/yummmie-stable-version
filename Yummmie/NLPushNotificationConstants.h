//
//  NLPushNotificationConstants.h
//  Yummmie
//
//  Created by bot on 1/22/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, NLPushNotificationActionType)
{
    NLPushNotificationActionNone = -1,
    NLPushNotificationMentionType = 1,
    NLPushNotificationLikeType = 2,
    NLPushNotificationFollowType = 3,
    NLPushNotificationCommentType = 4,
		NLPushNotificationFavType = 5,
		NLPushNotificationRepostType = 6


};


extern NSString * const kNLPushNotificationActionIdKey;
extern NSString * const kNLPushNotificationEntityKey;

