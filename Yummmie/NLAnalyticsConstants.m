//
//  NLAnalyticsConstants.m
//  Yummmie
//
//  Created by bot on 4/27/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLAnalyticsConstants.h"

NSString * const kNLAnalyticsSectionViewed = @"section viewed";
NSString * const kNLAnalyticsSectionNameKey = @"section name";
NSString * const kNLAnalyticsTimelineSection = @"Timeline";
NSString * const kNLAnalyticsDiscoverSection = @"Discover";
NSString * const kNLAnalyticsInteractionSection = @"Interactions";
NSString * const kNLAnalyticsProfileSection = @"Profile";
NSString * const kNLAnalyticsExternalProfileSection = @"Othe Profile";
NSString * const kNLAnalyticsSettingsSection = @"Settings";
NSString * const kNLAnalyticsLikesSection = @"Likes";
NSString * const kNLAnalyticsCommentsSection = @"Comments";
NSString * const kNLAnalyticsUsernameKey = @"username";
NSString * const kNLAnalyticsYumId = @"yummmie id";
NSString * const kNLAnalyticsYumOwnerKey = @"owner";
