//
//  NLEditViewController.m
//  Yummmie
//
//  Created by Mauricio Ventura on 23/06/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import "NLEditViewController.h"
#import "NLVenue.h"
#import "NLUser.h"
#import "NLSession.h"
#import "NLNavigationViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIColor+NLColor.h"
#import "NLConstants.h"
@interface NLEditViewController ()
@property (strong, nonatomic) NLYum *yum;
@property (strong, nonatomic) NLVenue *venue;
@property (nonatomic, strong) UIBarButtonItem *editRightButton;
@property (nonatomic, strong) UIBarButtonItem *loadingRightButton;
@property (strong, nonatomic) UILabel *progressLabel;
@property (nonatomic) NLMessageMode currentMessageMode;

@property (strong, nonatomic) NSMutableArray *hashtags;
@property (strong, nonatomic) NSArray *filteredHashtags;


@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) NSArray *filteredUsers;

@property (copy, nonatomic) NSURLSessionDataTask *queryUsersTask;
@property (weak, nonatomic) NSURLSessionDataTask *editRequest;

@end

@implementation NLEditViewController
{
	BOOL modifiedVenue;
}
@synthesize dishName, venueName, image, caption, yum;
- (id)initWithYum:(NLYum *)aYum
{
	if (self = [super init])
	{
		yum = aYum;
		self.edgesForExtendedLayout = UIRectEdgeBottom;
		self.title = NSLocalizedString(@"edit_yum_view_title", nil);

	}
	return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_currentMessageMode = NLMessageModeNone;

	self.editRightButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save_button_title", nil) style:UIBarButtonItemStylePlain target:self action:@selector(edit:)];
	[self.navigationItem setRightBarButtonItem:self.editRightButton];
	/*
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	[spinner startAnimating];
	self.loadingRightButton = [[UIBarButtonItem alloc] initWithCustomView:spinner];

	NLNavigationViewController *navController = (NLNavigationViewController *)[self navigationController];
	[navController setupProgressView];
*/
	
	
	[image sd_setImageWithURL:[NSURL URLWithString:[yum image]] placeholderImage:[NLSession getSharedPhoto]];

	dishName.text = yum.name;
	[venueName setTitle:[(NLVenue *)yum.venue name] forState:UIControlStateNormal];
	caption.text = yum.caption;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
	/*[super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:NO];
	[self.navigationItem.leftBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
		item.customView.alpha = 1;
	}];
	[self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
		item.customView.alpha = 1;
	}];
	self.navigationItem.titleView.alpha = 1;
	self.navigationController.navigationBar.tintColor = [self.navigationController.navigationBar.tintColor colorWithAlphaComponent:1];*/
	
	[[self navigationController] setNavigationBarHidden:NO];
	
	CGRect frame = self.navigationController.navigationBar.frame;
	
	if (frame.origin.y < 20.0f)
	{
		frame.origin.y = 20.0f;
		[self.navigationController.navigationBar setFrame:frame];
	}
	
	self.navigationItem.titleView.alpha  = 1.0;
	
	
	//[self navigationItem]
	/*CGRect frame = self.navigationController.navigationBar.frame;
	frame.origin.y = 20.0f;
	[self.navigationController.navigationBar setFrame:frame];
	
	//self.navigationItem.titleView.alpha = 1.0;
	self.progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 10)];
	[[self progressLabel] setTextAlignment:NSTextAlignmentCenter];
	self.progressLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
	[self.progressLabel setTextColor:[UIColor whiteColor]];
	[self.navigationItem setTitleView:[self progressLabel]];
	
	UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
	logoView.contentMode = UIViewContentModeScaleAspectFit;
	logoView.center = CGPointMake(self.progressLabel.center.x - 8, self.progressLabel.center.y);*/
	//self.navigationItem.titleView = logoView;
	
	//[self.progressLabel addSubview:logoView];
	
	//[self.navigationItem.titleView setOpaque:true];
	
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
	
	[defaultCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[defaultCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
	[defaultCenter addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
	
	//Google Analytics
	NSString *name = [NSString stringWithFormat:@"Edit Photo View"];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
	

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)changeVenue:(id)sender
{
	NLFoursquareSearchViewController *fsVC = [[NLFoursquareSearchViewController alloc] init];
	[fsVC setDelegate:self];
	NLNavigationViewController *navController = [[NLNavigationViewController alloc] initWithRootViewController:fsVC];
	[self presentViewController:navController animated:YES
									 completion:nil];
	
}
- (void)edit:(id)sender
{
	[[self editRequest] cancel];

	NSError *error;
	if (![self errorInFields:&error])
	{
		[self.navigationItem setRightBarButtonItem:nil];
		[self blockInterface];
		
		
		NSString *venueId = [(NLVenue *)yum.venue venueId];
		if (modifiedVenue)
		{
			venueId = self.venue.venueId;
		}
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			
			
			
			
			
			dispatch_async(dispatch_get_main_queue(), ^{
				[[self navigationItem] setHidesBackButton:YES];
			});
			NSDictionary *params = @{
															 @"yum[caption]":caption.text,
															 @"yum[venue_id]": venueId,
															 @"yum[name]":dishName.text
															 };

			self.editRequest = [NLYum editYumId:yum.yumId dict:params withCompletionBlock:^(NSError *error, NLYum *nYum)
			{
				if (!error)
				{
					yum = nYum;
				}
				else
				{
					
				}
				[self.navigationController popViewControllerAnimated:true];
				[self unblockInterface];
			}];
			
		});
	}
	else
	{
		//[self showErrorMessage:[error localizedDescription] withDuration:3.0f];
	}
	
}

- (BOOL)errorInFields:(NSError *__autoreleasing *)error
{
	if ([[dishName text] length] == 0 || [[[dishName text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
		
		NSDictionary *userInfo = @{
															 NSLocalizedDescriptionKey:@"you must enter a dish name"
															 };
		*error = [[NSError alloc] initWithDomain:@"com.yummmie.fieldValidations" code:0 userInfo:userInfo];
		return YES;
	}
	
	if ([[[venueName titleLabel] text] isEqualToString:@"Select a Venue"]) {
		NSDictionary *userInfo = @{
															 NSLocalizedDescriptionKey:@"you must select a Venue"
															 };
		*error = [[NSError alloc] initWithDomain:@"com.yummmie.fieldValidations" code:0 userInfo:userInfo];
		return YES;
	}
	return NO;
}
- (void)blockInterface
{
	[caption setEditable:NO];
}

- (void)unblockInterface
{
	[caption setEditable:YES];
}
- (void)didFinishSelectingWithVenue:(NLVenue *)venue
{
	modifiedVenue = 1;
	self.venue = venue;
	//update the button title and color
	
	[venueName setTitleColor:[UIColor nl_colorWith255Red:203 green:39 blue:54 andAlpha:255] forState:UIControlStateNormal];
	[venueName setTitle:[[self venue] name] forState:UIControlStateNormal];
	
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	NLNavigationViewController *nav = (NLNavigationViewController *)[self navigationController];
	[nav updateProgress:[object fractionCompleted]];
}
#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	//    NSLog(@"La ubicación del caracter ingresado : %d",range.location);
	//    NSLog(@"El texto : %@",text);
	
	
	NSArray *words = [[textView text] componentsSeparatedByString:@" "];
	//    NSLog(@"Palabras %@",words);
	
	NSString *lastWord = [words lastObject];
	
	NSRange previousRange = NSMakeRange(range.location-1, 1);
	
	
	if ([text isEqualToString:@"@"])
	{
		if (range.location == 0)
		{
			[self startMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeMention)
		{
			[self endMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeHashtag)
		{
			[self endMentionMode];
		}
		else
		{
			NSString *previousCharacter = [[textView text] substringWithRange:previousRange];
			if ([previousCharacter isEqual:@" "])
			{
				[self startMentionMode];
			}
		}
	}
	else if ([text isEqualToString:@"#"])
	{
		if (range.location == 0)
		{
			[self startHashtagMode];
		}
		else if (self.currentMessageMode == NLMessageModeMention)
		{
			[self endMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeHashtag)
		{
			[self endMentionMode];
		}
		else
		{
			NSString *previousCharacter = [[textView text] substringWithRange:previousRange];
			if ([previousCharacter isEqual:@" "])
			{
				[self startHashtagMode];
			}
		}
	}
	else if ([text isEqualToString:@" "])
	{
		if (self.currentMessageMode == NLMessageModeMention)
		{
			[self endMentionMode];
		}
		else if (self.currentMessageMode == NLMessageModeHashtag)
		{
			NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
			self.hashtags = [[def objectForKey:@"hashtags"] mutableCopy];
			if (self.hashtags == nil)
			{
				self.hashtags = [[NSMutableArray array] mutableCopy];
			}
			if (![self.hashtags containsObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]])
			{
				[self.hashtags addObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]];
				[def setObject:self.hashtags forKey:@"hashtags"];
				[def synchronize];
			}
			[self endMentionMode];
		}
	}
	else if (text.length == 0)
	{
		if ([lastWord containsString:@"@"])
		{
			NSRange atRange = [lastWord rangeOfString:@"@"];
			
			
			if (atRange.location == 0 && lastWord.length > 1) {
				[self startMentionMode];
				//Get the word and ask for suggestions
			}
			else{
				[self endMentionMode];
			}
		}
		else if ([lastWord containsString:@"#"])
		{
			NSRange atRange = [lastWord rangeOfString:@"#"];
			
			
			if (atRange.location == 0 && lastWord.length > 1) {
				[self startHashtagMode];
				//Get the word and ask for suggestions
			}
			else{
				[self endMentionMode];
			}
		}
	}
	
	
	if (self.currentMessageMode == NLMessageModeMention) {
		//        NSLog(@"Seguimos en modo mention!");
		//get the word and ask for sugesionts
	}else
	{
		//        NSLog(@"Estamos ecribendo normal!");
	}
	return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
	NSArray *words = [[textView text] componentsSeparatedByString:@" "];
	NSString *lastWord = [words lastObject];
	
	if (self.currentMessageMode == NLMessageModeMention) {
		//        NSLog(@"Buscar info sobre : %@",lastWord);
		NSString *cleanUser = [lastWord stringByReplacingOccurrencesOfString:@"@" withString:@""];
		//        NSLog(@"El clean user : %@ y su tamaño : %d",cleanUser,cleanUser.length);
		
		__weak NLEditViewController *weakSelf = self;
		
		if (cleanUser.length > 0) {
			[self.queryUsersTask cancel];
			self.queryUsersTask = [NLUser getSuggestedFollowersFromUser:[[NLSession currentUser] userID] andQuery:cleanUser withCompletionBlock:^(NSError *error, NSArray *followings)
														 {
															 if (!error) {
																 [weakSelf setFilteredUsers:followings];
																 [[weakSelf suggestedBar] updateDatasource:[weakSelf filteredUsers]];
															 }
														 }];
		}else
		{
			[[self suggestedBar] updateDatasource:[self users]];
		}
	}
	else if (self.currentMessageMode == NLMessageModeHashtag)
	{
		//        NSLog(@"Buscar info sobre : %@",lastWord);
		NSString *cleanHashtag = [lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""];
		//        NSLog(@"El clean user : %@ y su tamaño : %d",cleanUser,cleanUser.length);
		
		if (cleanHashtag.length > 0)
		{
			self.filteredHashtags = [self filterHashtagsQuery:cleanHashtag];
			[[self suggestedBar] updateDatasource:[self filteredHashtags]];
			//[[weakSelf suggestedBar] updateDatasource:[weakSelf filteredUsers]];
		}
		else
		{
			NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
			self.hashtags = [def objectForKey:@"hashtags"];
			[[self suggestedBar] updateDatasource:[self hashtags]];
		}
	}
}
-(NSArray *)filterHashtagsQuery:(NSString *)query
{
	NSMutableArray *coincidences = [NSMutableArray array];
	for (NSString *hashtag in self.hashtags)
	{
		if (hashtag.length >= query.length)
		{
			NSString *nStr = [hashtag substringWithRange:NSMakeRange(0, query.length)];
			if ([nStr containsString: query])
			{
				[coincidences addObject:hashtag];
			}
		}
	}
	return [NSArray arrayWithArray:coincidences];
}
#pragma mark - NLSuggestedBar Delegate

- (void)suggestedBar:(NLSuggestedUsersBar *)suggestedBar didSelectUser:(id)user atIndex:(NSInteger)index
{
	NSArray *words = [[caption text] componentsSeparatedByString:@" "];
	//    NSLog(@"Palabras %@",words);
	NSString *lastWord = [words lastObject];
	
	NSRange rangeOfLastWord = [[caption text] rangeOfString:lastWord options:NSBackwardsSearch];
	/*rangeOfLastWord = [self.messageTextView selectedRange];
	 NSString *subToWord = [self.messageTextView.text substringToIndex:rangeOfLastWord.location];
	 
	 */
	if (self.currentMessageMode == NLMessageModeMention)
	{
		//NSRange iniPosition = [subToWord rangeOfString:@"@" options:NSBackwardsSearch];
		//NSString *actualWord = [self.messageTextView.text substringWithRange:NSMakeRange(iniPosition.location, rangeOfLastWord.location)];
		//rangeOfLastWord = NSMakeRange(iniPosition.location, rangeOfLastWord.location);
		caption.text = [caption.text stringByReplacingCharactersInRange:rangeOfLastWord withString:[NSString stringWithFormat:@"@%@ ",[user username]]];
	}
	else
	{
		//NSRange iniPosition = [subToWord rangeOfString:@"#" options:NSBackwardsSearch];
		//rangeOfLastWord = NSMakeRange(iniPosition.location, rangeOfLastWord.location);
		caption.text = [caption.text stringByReplacingCharactersInRange:rangeOfLastWord withString:[NSString stringWithFormat:@"#%@ ",user]];
		
	}
	[self endMentionMode];
	
}
#pragma  mark - Start Mention Mode

- (void)startMentionMode
{
	self.currentMessageMode = NLMessageModeMention;
	[[self suggestedBar] setHidden:NO];
	[self suggestedBar].hashtagging = false;
	
}
- (void)startHashtagMode
{
	self.currentMessageMode = NLMessageModeHashtag;
	[[self suggestedBar] setHidden:NO];
	[self suggestedBar].hashtagging = YES;
}
#pragma  mark - End Mention Mode

- (void)endMentionMode
{
	self.currentMessageMode = NLMessageModeNone;
	[[self suggestedBar] setHidden:YES];
}
#pragma mark - Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
	
	NSDictionary *userInfo = [notification userInfo];
	CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	
	CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	//self.InputViewVerticalSpaceConstraint.constant = keyboardRect.size.height;
	
	__weak NLEditViewController *weakSelf = self;
	NSLog(@"%f", keyboardRect.size.height);
	dispatch_async(dispatch_get_main_queue(), ^{
		
		[UIView animateWithDuration:animationDuration delay:0.0f options:0 animations:^{
			[UIView setAnimationCurve:animationCurve];
			if ([caption isFirstResponder])
			{
				self.bottomSuggestedBar.constant = keyboardRect.size.height;
				self.suggestedBar.alpha = 1;
			}
			if ([dishName isFirstResponder])
			{
				self.bottomSuggestedBar.constant = keyboardRect.size.height  - caption.frame.size.height;
			}
			else
			{
				
				self.suggestedBar.alpha = 0;
				
			}
			
			[[weakSelf view] layoutIfNeeded];
		} completion:^(BOOL finished) {
			
		}];
		
	});
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
	NSDictionary *userInfo = [notification userInfo];
	
	CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
	UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	
	//self.InputViewVerticalSpaceConstraint.constant = 0;
	__weak NLEditViewController *weakSelf = self;
	dispatch_async(dispatch_get_main_queue(), ^{
		[UIView animateWithDuration:animationDuration delay:0.0f options:0 animations:^{
			[UIView setAnimationCurve:animationCurve];
			
			self.bottomSuggestedBar.constant = 0;
			self.suggestedBar.alpha = 0;
			
			[[weakSelf view] layoutIfNeeded];
			
		} completion:^(BOOL finished) {
			
		}];
	});
	
}

- (void)keyboardDidHide:(NSNotification *)notification
{
	
	[caption resignFirstResponder];
	
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
