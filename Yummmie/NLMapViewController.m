//
//  NLMapViewController.m
//  Yummmie
//
//  Created by bot on 5/20/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLMapViewController.h"

@interface NLMapViewController ()
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property CLLocationCoordinate2D coordinate;

@end

@implementation NLMapViewController

- (instancetype)initWithLocactionCoordinate:(CLLocationCoordinate2D)locationCoordinate
{
    if (self = [super init]) {
        _coordinate = locationCoordinate;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.coordinate.latitude longitude:self.coordinate.longitude zoom:12.0f];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = self.coordinate;
    marker.icon = [UIImage imageNamed:@"pin_map"];
    marker.map = [self mapView];
    [[self mapView] setCamera:camera];
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
