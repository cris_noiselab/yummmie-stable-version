//
//  NLSubRegion.m
//  Yummmie
//
//  Created by bot on 7/27/17.
//  Copyright © 2017 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLSubRegion.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLConstants.h"


static NSString * const kNLAuthenticationUserIdKey = @"authentication[uid]";
static NSString * const kNLAuthenticationProviderKey  = @"provider";
static NSString * const kNLAuthenticationTokenKey = @"provider_token";
static NSString * const kNLAuthenticationTokenSecretKey = @"provider_token_secret";

static NSString * const kNLAuthenticationResponseProviderKey = @"provider";
static NSString * const kNLAuthenticationResponseTokenKey = @"token";
static NSString * const kNLAuthenticationResponseTokenSecretKey = @"token_secret";
static NSString * const kNLAuthenticationResponseUserIdKey = @"uid";
static NSString * const kNLAuthenticationResponseIdKey = @"id";

static NSString * const kNLAuthenticationResponseAuthenticationKey = @"authentication";

@interface NLSubRegion ()

@property (nonatomic, copy, readwrite) NSString * token;
@property (nonatomic, copy, readwrite) NSString * tokenSecret;
@property (nonatomic, copy, readwrite) NSString * userId;
@property (nonatomic, copy, readwrite) NSString *authenticationID;

@end

@implementation NLSubRegion


- (instancetype)initWithDictionary:(NSDictionary *)subRegionDict
{
    if (self = [super init])
    {
        _subRegion_id = [subRegionDict objectForKey:@"id" ];
        _name = [subRegionDict objectForKey:@"name"];
        
    }
    
    return self;
}

@end
