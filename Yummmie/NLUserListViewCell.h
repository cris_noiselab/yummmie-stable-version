//
//  NLUserListViewCell.h
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NLConstants.h"

@protocol NLUserListViewCellDelegate;


@interface NLUserListViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (nonatomic) UserButtonType buttonType;
@property (weak, nonatomic) id<NLUserListViewCellDelegate> delegate;

- (void)setName:(NSString *)aName andUsername:(NSString *)aUsername;

@end


@protocol NLUserListViewCellDelegate <NSObject>

@required

- (void)userCell:(NLUserListViewCell *)cell actionWithType:(UserButtonType)type;

@end