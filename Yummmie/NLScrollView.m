//
//  NLScrollView.m
//  Yummmie
//
//  Created by bot on 6/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLScrollView.h"

@implementation NLScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setDelaysContentTouches:NO];
    }
    return self;
}


- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    if ([view isKindOfClass:[UIButton class]])
    {
        return YES;
    }
    return NO;
}

//- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated
//{
//
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
