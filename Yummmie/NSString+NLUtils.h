//
//  NSString+NLUtils.h
//  Yummmie
//
//  Created by bot on 12/4/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NLUtils)

+ (NSString *)integerToFollowText:(NSInteger)quantitiy;


@end
