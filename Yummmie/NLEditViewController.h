//
//  NLEditViewController.h
//  Yummmie
//
//  Created by Mauricio Ventura on 23/06/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NLYum.h"
#import "NLFoursquareSearchViewController.h"
#import "NLSuggestedUsersBar.h"
#import "NLBaseViewController.h"

@interface NLEditViewController : NLBaseViewController<NLFoursquareSearchDelegate, UITextViewDelegate, NLSuggestedUsersBarDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UITextField *dishName;
@property (weak, nonatomic) IBOutlet UIButton *venueName;
@property (weak, nonatomic) IBOutlet UITextView *caption;
@property (weak, nonatomic) IBOutlet UIButton *choosePhoto;
@property (weak, nonatomic) IBOutlet NLSuggestedUsersBar *suggestedBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSuggestedBar;
- (id)initWithYum:(NLYum *)aYum;

@end
