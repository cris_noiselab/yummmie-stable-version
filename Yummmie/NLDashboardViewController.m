//
//  NLDashboardViewController.m
//  Yummmie
//
//  Created by bot on 9/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import "NLDashboardViewController.h"
#import "NLUserLikesViewController.h"
#import "UIColor+NLColor.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLDashboardUserCellView.h"
#import "NLProfileViewController.h"
#import "NLNotifications.h"
#import "NLSettingsViewController.h"
#import "NLTermsPolicyViewController.h"
#import "NLConstants.h"
#import "NLShareAppViewController.h"
#import "NLSearchFriendsViewController.h"
#import "NLShareSettingsViewController.h"


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
static NSString * const kNLDashboardUserCellViewIdentifier = @"kNLDashboardUserCellViewIdentifier";

@interface NLDashboardViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *settingsTable;
@property (strong, nonatomic) UIButton *logoutButton;
@property (strong, nonatomic)  UILabel *versionLabel;

- (void)askForLogout:(id)sender;
- (void)logout;

- (void)settingsUpdated:(NSNotification *)notification;
- (void)dismiss:(id)sender;

@end

@implementation NLDashboardViewController
{
	UIPageControl *pageControl;
	UIScrollView *scroll;
	UIButton *closeTutorial;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)    {
        // Custom initialization
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.title = @"dashboard";
        self.tabBarItem.title = nil;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsUpdated:) name:kNLUpdateSettingsNotification object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    // Do any additional setup after loading the view from its nib.
    [self.settingsTable setDelegate:self];
    [self.settingsTable setDataSource:self];
    [self.settingsTable setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.settingsTable.bounds.size.width, 0.01f)]];
    
    [self.settingsTable registerNib:[UINib nibWithNibName:NSStringFromClass([NLDashboardUserCellView class]) bundle:nil] forCellReuseIdentifier:kNLDashboardUserCellViewIdentifier];
    
    CGRect buttonRect = CGRectMake(10, 0, rootView.frame.size.width*0.9375, rootView.frame.size.height*0.07746);
    
    self.logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.logoutButton setFrame:buttonRect];
    [self.logoutButton setBackgroundImage:[UIImage imageNamed:@"bot_red"] forState:UIControlStateNormal];
    [[self.logoutButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0f]];
    [self.logoutButton setTitle:NSLocalizedString(@"logout_title", nil) forState:UIControlStateNormal];
    [self.logoutButton addTarget:self action:@selector(askForLogout:) forControlEvents:UIControlEventTouchUpInside];
    [self.logoutButton setCenter:CGPointMake(rootView.center.x, self.logoutButton.center.y)];
    
    
    //---
    
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * versionBuildString = [NSString stringWithFormat:@"Yummmie* Version: %@ (%@)", appVersionString, appBuildString];
    
    self.versionLabel=[[UILabel alloc]initWithFrame:CGRectMake(10.0f, 80.0f, 300.0f, 20.0f)];
    [self.versionLabel setText:versionBuildString];
    [self.versionLabel setTextColor:[UIColor nl_applicationRedColor]];
    [self.versionLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:11.0f]];
    [self.versionLabel setTextAlignment:NSTextAlignmentCenter];
    [self.versionLabel setCenter:CGPointMake(rootView.center.x, self.versionLabel.center.y)];
    
    //---
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frame.size.width, 100)];
     [footerView addSubview:self.logoutButton];
    [footerView addSubview:self.versionLabel];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    [self.settingsTable setTableFooterView:footerView];
    [[self settingsTable] setSeparatorInset:UIEdgeInsetsZero];
    
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if ([self.settingsTable respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.settingsTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.settingsTable respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.settingsTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self settingsTable] deselectRowAtIndexPath:[[self settingsTable] indexPathForSelectedRow] animated:YES];
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"close_title", nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem = closeButton;
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self settingsTable] reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Dismiss Method

- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - handle settings update

- (void)settingsUpdated:(NSNotification *)notification
{
    [[self settingsTable] reloadData];
}

#pragma mark - handle logout

- (void)askForLogout:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"logout_alert_title", nil) message:NSLocalizedString(@"logout_alert_message", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"logout_alert_cancel_title", nil) otherButtonTitles:NSLocalizedString(@"logout_alert_accept_title", nil), nil] show];
}

- (void)logout
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLLogoutNotification object:nil];
	
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return 3;
        case 1:
            return 2;
        case 2:
            return 0;
        case 3:
            return 2;
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *regularIdentifier = @"regularIdentifier";
    if (indexPath.row == 0 && indexPath.section == 0)
    {
        NLDashboardUserCellView *cell = [tableView dequeueReusableCellWithIdentifier:kNLDashboardUserCellViewIdentifier forIndexPath:indexPath];
        NLUser *user = [NLSession currentUser];
        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"pic_userdefault"]];
        [cell.usernameLabel setTextColor:[UIColor nl_applicationRedColor]];
        cell.usernameLabel.text = [NSString stringWithFormat:@"@%@",user.username];
        cell.nameLabel.text = user.name;
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
    }else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:regularIdentifier];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:regularIdentifier];
            cell.textLabel.textColor = [UIColor grayColor];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        if (indexPath.section == 0)
        {
            if (indexPath.row == 1)
            {
                
                [[cell imageView] setImage:[UIImage imageNamed:@"icon_editprofile"]];
                [cell.textLabel setText:NSLocalizedString(@"edit_profile_title", nil)];
            }else if (indexPath.row == 2)
            {
                [[cell imageView] setImage:[UIImage imageNamed:@"icon_myyummmie"]];
                [cell.textLabel setText:NSLocalizedString(@"my_yummmies_title", nil)];
            }
        }else if (indexPath.section == 1)
        {
            if (indexPath.row == 0)
            {
                [[cell textLabel] setText:NSLocalizedString(@"invite_users_title", nil)];
            }else
            {
                [[cell textLabel] setText:NSLocalizedString(@"search_users_title", nil)];
            }
        }else if (indexPath.section == 2)
        {
            [[cell textLabel] setText:NSLocalizedString(@"sharing_account_title", nil)];
        }else if (indexPath.section == 3)
        {
					if (indexPath.row == 0)
					{
						[[cell textLabel] setText:NSLocalizedString(@"tutorial", nil)];
					}
					else
					{
						[[cell textLabel] setText:NSLocalizedString(@"terms_and_conditions_title", nil)];
					}
        }
        return cell;
    }
}


#pragma mark - UITableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    switch (section)
    {
        case 2:
            return 0.0f;
        case 1:
        case 3:
            return 30.0f;
        default:
            return 10.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 1:
            return 30.0f;
        case 2:
        default:
            return 0.0f;
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSString *title;
    switch (section) {
        case 1:
            title = NSLocalizedString(@"title_dashboard_title", nil);
            break;
        case 2:
            title = NSLocalizedString(@"dashboard_account_title", nil);
            break;
        default:
            break;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor whiteColor]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15,5, 280, 20)];
    [label setText:title];
    [label setTextColor:[UIColor nl_applicationRedColor]];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0f]];
    [view addSubview:label];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
        if (indexPath.section == 2)
    {
        return 0;
    }
    if (indexPath.row == 0 && indexPath.section == 0)
    {
        return rootView.frame.size.height*0.13;
        //return 74.0f;
    }
    return 43.0f;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:[NLSession currentUser] isProfileUser:YES];
            [profileVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:profileVC animated:YES];
        }else if (indexPath.row == 1)
        {
            NLSettingsViewController *settingsVC = [[NLSettingsViewController alloc] init];
            [self.navigationController pushViewController:settingsVC animated:YES];
        }else if(indexPath.row == 2)
        {
            NLUserLikesViewController *likesVC = [[NLUserLikesViewController alloc] initWithNibName:NSStringFromClass([NLUserLikesViewController class]) bundle:nil];
            [self.navigationController pushViewController:likesVC animated:YES];
        }
        
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            NLShareAppViewController *shareAppVC = [[NLShareAppViewController alloc] initWithNibName:NSStringFromClass([NLShareAppViewController class]) bundle:nil];
            [[self navigationController] pushViewController:shareAppVC animated:YES];
        }else if (indexPath.row == 1)
        {
            NLSearchFriendsViewController *searchFriends = [[NLSearchFriendsViewController alloc] initWithNibName:NSStringFromClass([NLSearchFriendsViewController class]) bundle:nil];
            [[self navigationController] pushViewController:searchFriends animated:YES];
        }
    }else if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            NLShareSettingsViewController *shareSettingsVC = [[NLShareSettingsViewController alloc] initWithNibName:NSStringFromClass([NLShareSettingsViewController class]) bundle:nil];
            [[self navigationController] pushViewController:shareSettingsVC animated:YES];
        }
    }else if (indexPath.section == 3)
    {
			if (indexPath.row == 0)
			{
				[self createCarrousel];
			}
        if (indexPath.row == 1)
        {
            NLTermsPolicyViewController *termsVC = [[NLTermsPolicyViewController alloc] initWithNibName:NSStringFromClass([NLTermsPolicyViewController class]) bundle:nil andURL:[NSURL URLWithString:kTermsOfUseURL]];
            [self presentViewController:termsVC animated:YES completion:nil];
        }
    }
}
-(void)createCarrousel
{
	scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + 65)];
	scroll.tag = 3;
	[scroll setBackgroundColor:[UIColor clearColor]];
	//[scroll setScrollEnabled:false];
	[scroll setBounces:false];
	[scroll setPagingEnabled:true];
	[scroll setContentSize:CGSizeMake(scroll.frame.size.width * 3, scroll.frame.size.height)];
	scroll.delegate = self;
	[scroll setShowsHorizontalScrollIndicator:false];
	[self.navigationController.view addSubview:scroll];
	UIView *content = [[UIView alloc] initWithFrame:CGRectMake(0, 0, scroll.frame.size.width * 3, scroll.frame.size.height)];
	[content setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
	[scroll addSubview:content];
	pageControl = [[UIPageControl alloc] init];
	pageControl.numberOfPages = 3;
	pageControl.currentPage = 0;
	//[pageControl setTintColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	[pageControl setPageIndicatorTintColor:[UIColor whiteColor]];
	[pageControl setCurrentPageIndicatorTintColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	UIView *v1 = [[UIImageView alloc] initWithFrame:CGRectMake(20, 0, scroll.frame.size.width - 40, scroll.frame.size.height - 40)];
	[v1 setBackgroundColor:[UIColor clearColor]];
	[content addSubview:v1];
	pageControl.center = CGPointMake(scroll.center.x, v1.frame.size.height / 1.6 + 120);
	[self.navigationController.view addSubview:pageControl];
	
	UILabel *titleV1 = [[UILabel alloc] initWithFrame:CGRectMake(0, v1.frame.size.height / 1.6, v1.frame.size.width, 20)];
	[titleV1 setText:@"WISHLIST"];
	[titleV1 setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];
	titleV1.textAlignment = NSTextAlignmentLeft;
	titleV1.textColor = [UIColor whiteColor];
	[v1 addSubview:titleV1];
	
	UILabel *contentV1 = [[UILabel alloc] initWithFrame:CGRectMake(0, v1.frame.size.height / 1.6 + 20, v1.frame.size.width, 70)];
	[contentV1 setText:NSLocalizedString(@"v1-content", nil)];
	[contentV1 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
	contentV1.textAlignment = NSTextAlignmentLeft;
	contentV1.numberOfLines = 3;
	contentV1.textColor = [UIColor whiteColor];
	[v1 addSubview:contentV1];
	
	UIImageView *iv1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, scroll.frame.size.width - 40, scroll.frame.size.height / 2.1)];
	[iv1 setContentMode:UIViewContentModeScaleAspectFit];
	[iv1 setBackgroundColor:[UIColor clearColor]];
	iv1.image = [UIImage imageNamed:@"v1"];
	[v1 addSubview:iv1];
	
	UIView *v2 = [[UIImageView alloc] initWithFrame:CGRectMake(scroll.frame.size.width + 20, 0, scroll.frame.size.width - 40, scroll.frame.size.height - 40)];
	[v2 setBackgroundColor:[UIColor clearColor]];
	[content addSubview:v2];
	
	UILabel *titlev2 = [[UILabel alloc] initWithFrame:CGRectMake(0, v2.frame.size.height / 1.6, v2.frame.size.width, 20)];
	[titlev2 setText:NSLocalizedString(@"v2-title", nil)];
	[titlev2 setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];
	titlev2.textAlignment = NSTextAlignmentLeft;
	titlev2.textColor = [UIColor whiteColor];
	[v2 addSubview:titlev2];
	
	UILabel *contentv2 = [[UILabel alloc] initWithFrame:CGRectMake(0, v2.frame.size.height / 1.6 + 20, v2.frame.size.width, 70)];
	[contentv2 setText:NSLocalizedString(@"v2-content", nil)];
	[contentv2 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
	contentv2.textAlignment = NSTextAlignmentLeft;
	contentv2.numberOfLines = 3;
	contentv2.textColor = [UIColor whiteColor];
	[v2 addSubview:contentv2];
	
	UIImageView *iv2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, scroll.frame.size.width - 40, scroll.frame.size.height / 2.1)];
	[iv2 setContentMode:UIViewContentModeScaleAspectFit];
	[iv2 setBackgroundColor:[UIColor clearColor]];
	iv2.image = [UIImage imageNamed:@"v2"];
	
	[v2 addSubview:iv2];
	
	
	UIView *v3 = [[UIImageView alloc] initWithFrame:CGRectMake(scroll.frame.size.width * 2 + 20, 0, scroll.frame.size.width - 40, scroll.frame.size.height)];
	[v3 setBackgroundColor:[UIColor clearColor]];
	[v3 setClipsToBounds:false];
	[content addSubview:v3];
	
	UILabel *titlev3 = [[UILabel alloc] initWithFrame:CGRectMake(0, v3.frame.size.height / 1.65, v3.frame.size.width, 20)];
	[titlev3 setText:NSLocalizedString(@"v3-title", nil)];
	[titlev3 setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];
	titlev3.textAlignment = NSTextAlignmentCenter;
	titlev3.textColor = [UIColor whiteColor];
	[v3 addSubview:titlev3];
	
	UILabel *contentv3 = [[UILabel alloc] initWithFrame:CGRectMake(0, v3.frame.size.height / 1.65 + 20, v3.frame.size.width, 20)];
	[contentv3 setText:NSLocalizedString(@"v3-content", nil)];
	[contentv3 setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
	contentv3.textAlignment = NSTextAlignmentCenter;
	contentv3.numberOfLines = 3;
	contentv3.textColor = [UIColor whiteColor];
	[v3 addSubview:contentv3];
	
	closeTutorial = [[UIButton alloc] initWithFrame:CGRectMake(0, v3.frame.size.height - 60, scroll.frame.size.width, 60)];
	[closeTutorial setBackgroundColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	[closeTutorial setTitle:@"OK" forState:UIControlStateNormal];
	[closeTutorial setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[closeTutorial addTarget:self action:@selector(quitTutorial) forControlEvents:UIControlEventTouchDown];
	closeTutorial.alpha = 0;
	[self.navigationController.view addSubview:closeTutorial];
	UIImageView *iv3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, scroll.frame.size.width - 40, scroll.frame.size.height / 2.1)];
	[iv3 setContentMode:UIViewContentModeScaleAspectFit];
	[iv3 setBackgroundColor:[UIColor clearColor]];
	iv3.image = [UIImage imageNamed:@"v3"];
	[v3 addSubview:iv3];
	
	
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && indexPath.section == 0)
    {
        return nil;
    }
    return indexPath;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.tag == 3)
	{
		static NSInteger previousPage = 0;
		CGFloat pageWidth = scrollView.frame.size.width;
		float fractionalPage = scrollView.contentOffset.x / pageWidth;
		NSInteger page = lround(fractionalPage);
		if (previousPage != page)
		{
			[pageControl setCurrentPage:page];
			
			// Page has changed, do your thing!
			// ...
			// Finally, update previous page
			previousPage = page;
			if (page == 2)
			{
				[UIView animateWithDuration:0.3 animations:^{
					closeTutorial.alpha = 1;
				}];
			}
			else
			{
				closeTutorial.alpha = 0;
				
			}
		}
	}
	
}
-(void)quitTutorial
{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	[def setBool:true forKey:@"tutorialShowed"];
	[def synchronize];
	[UIView animateWithDuration:0.5 animations:^{
		scroll.alpha = 0;
		pageControl.alpha = 0;
		closeTutorial.alpha = 0;
	} completion:^(BOOL finished) {
		[scroll removeFromSuperview];
		[pageControl removeFromSuperview];
		[closeTutorial removeFromSuperview];
	}];
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        __weak NLDashboardViewController *weakSelf = self;
        
        [self dismissViewControllerAnimated:YES completion:^{
            [weakSelf logout];
        }];
    }
}
@end
