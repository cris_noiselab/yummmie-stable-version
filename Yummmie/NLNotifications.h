//
//  NLNotifications.h
//  Yummmie
//
//  Created by bot on 6/12/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kNLLoginNotification;
extern NSString * const kNLLoadTimeLineNotification;
extern NSString * const kNLLoadTimeLineErrorNotification;
extern NSString * const kNLReloadUserDataNotification;
extern NSString * const kNLYumPostReadyNotification;
extern NSString * const kNLCommentAddedNotification;
extern NSString * const kNLReloadDishesNotification;
extern NSString * const kNLFollowUserNotification;
extern NSString * const kNLUnfollowUserNotification;
extern NSString * const kNLShowSettingsNotification;
extern NSString * const kNLDismissSettingsNotification;
extern NSString * const kNLUpdateSettingsNotification;
extern NSString * const kNLShowInstagramOption;
extern NSString * const kNLLogoutNotification;
extern NSString * const kNLShowActivityFeed;
extern NSString * const kNLShowInteractionPopUpNotification;
extern NSString * const kNLShowPendingInteractionsNotification;
extern NSString * const kNLHidePendingInteractionsNotification;
extern NSString * const kNLHideInteractionPopUpNotification;
extern NSString * const kNLLoadLatestInteractions;
extern NSString * const kNLOpenVenueNotification;
extern NSString * const kNLOpenCameraNotification;

extern NSString * const kNLLikeNotification;
extern NSString * const kNLFavNotification;
