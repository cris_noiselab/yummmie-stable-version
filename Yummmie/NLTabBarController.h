//
//  NLTabBarController.h
//  Yummmie
//
//  Created by bot on 7/9/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVCameraViewController.h"
#import "NLRepostFeedbackView.h"
@interface NLTabBarController : UITabBarController<UITabBarControllerDelegate, MVCameraViewControllerDelegate>
@property (nonatomic, strong, readwrite) NLRepostFeedbackView *repostFeedback;

- (void)setupCamera;

@end
