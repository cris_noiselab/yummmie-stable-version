//
//  NLSettingsViewController.h
//  Yummmie
//
//  Created by bot on 7/29/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLSettingsViewController : NLBaseViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>

- (id)initWithCloseButton;

@end
