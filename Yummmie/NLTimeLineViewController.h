//
//  NLTimeLineViewController.h
//  Yummmie
//
//  Created by bot on 6/16/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLTableViewController.h"
#import "NLTimeLineViewCell.h"
#import "iRate.h"
#import <MGInstagram/MGInstagram.h>

@interface NLTimeLineViewController : NLTableViewController<NLTimeLineViewCellDelegate,UIActionSheetDelegate,UIViewControllerTransitioningDelegate, UIDocumentInteractionControllerDelegate,UITextFieldDelegate>

@property (nonatomic) BOOL userLoggedIn;
@property (nonatomic, retain) UIDocumentInteractionController *documentController;


@end
