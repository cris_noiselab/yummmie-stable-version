//
//  NLInteractionViewContoroller.h
//  Yummmie
//
//  Created by bot on 8/12/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLInteractionViewContoroller : NLBaseViewController<UITableViewDelegate,UITableViewDataSource>

@end
