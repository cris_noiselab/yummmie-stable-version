//
//  NLConstants.m
//  Yummmie
//
//  Created by bot on 6/5/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLConstants.h"

NSString * const kTermsOfUseURL = @"http://yummmie.com/terms";
NSString * const kPrivacyPoliceURL = @"http://yummmie.com/terms";

const CGFloat errorMessageViewHeight = 40.0f;

const CGFloat kHudWidth = 75.0f;
const CGFloat kHudHeight = 75.0f;

const CGFloat kHudAnimationFrameWidth = 100.0f;
const CGFloat kHudAnimationFrameHeight = 100.0f;
const CGFloat kHudAnimationDuration = 0.5f;

const CGFloat kTimeLineViewCellHeightBottomOffset = 20.0f;
const CGFloat kTimeLineViewCellHeight = 410.0f;
const CGFloat kTimeLineViewCellBottomBarHeight = 32.0f;
const CGFloat kTimeLineViewCellBottomBarTopOffset = 12.0f;

const CGFloat kCommentViewCellHeight = 45.0f;
const CGFloat kCommentViewCellBottomOffset = 10.0f;


NSString * const kUsernameScheme = @"user";
NSString * const kHashtagScheme = @"hashtag";

const CGFloat kTabBarHeight = 44.0f;
const CGFloat kDishCollectionViewBottomInset = 5.0f;
const CGFloat kDishDistanceToStartLoading = 200.0f;

NSString * const kPNGMimeType = @"image/png";
NSString * const kJPGMimeType = @"image/jpeg";
NSString * const kJSONUserEmailKey = @"email";
NSString * const kJSONUserUsernameKey = @"username";

const CGFloat kKeyboardHeight = 216.0f;

NSString * const kYummmieGalleryName = @"Yummmie*";

const CGFloat kTimeLabelYOrigin = 15.0f;
const CGFloat kTimeLabelXPadding = 15.0f;
const CGFloat kInteractionCellWithRightPhotoWidth = 200.0f;
const CGFloat kInteractionFollowingWidth = 220.0f;
const CGFloat kOtherInteractionCellWidth = 260.0f;
const CGFloat kTimeLabelHeight = 17.0f;
const CGFloat kTimeLabelVerticalMargin = 20.0f;

NSString  * const kReloadTimelineKey = @"kReloadTimelineKey";

const CGFloat kTimeLineCellCommentSeparation = 0.0f;
const CGFloat kTimeLineCellMoreButtonTopOffset = 8.0f;
const CGFloat kTImeLineCellCommentHeight = 60.0f;
const CGFloat kTImeLineCellMinimumCommentHeight = 25;//was 52
//const CGFloat kTImeLineCellCommentHeight = 56.0f;
const CGFloat kTimeLineCellMoreCommentsButtonHeight = 30.0f;
const CGFloat kTimeLineCellMinCommentLabelHeight = 20;
const CGFloat kTimeLineCellMaxCommentLabelHeight = 32;

const CGFloat kTimelineCellBigCommentOffsetY = 4;
