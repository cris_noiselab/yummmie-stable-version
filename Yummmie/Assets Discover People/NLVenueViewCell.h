//
//  NLVenueViewCell.h
//  Yummmie
//
//  Created by bot on 9/1/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLVenueViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *venueTitleLabel;
@end
