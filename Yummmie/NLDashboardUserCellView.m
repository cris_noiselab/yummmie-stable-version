//
//  NLDashboardUserCellView.m
//  Yummmie
//
//  Created by bot on 9/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLDashboardUserCellView.h"

@implementation NLDashboardUserCellView

- (void)awakeFromNib
{
    
    
    // Initialization code
    
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    
    CGFloat imageHeight= rootView.frame.size.height*0.0903;
    if (rootView.frame.size.height==667){
        imageHeight= rootView.frame.size.height*0.096;
    }
    if (rootView.frame.size.height==736){
        imageHeight= rootView.frame.size.height*0.0996;
    }
    
    [self.userImageView setFrame:CGRectMake(self.userImageView.frame.origin.x, self.userImageView.frame.origin.y, imageHeight,  imageHeight)];

    self.userImageView.layer.cornerRadius = imageHeight/2.0f;
    self.userImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
