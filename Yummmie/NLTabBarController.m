//
//  NLTabBarController.m
//  Yummmie
//
//  Created by bot on 7/9/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <ImageIO/ImageIO.h>
#import "NLTableViewController.h"
#import "NLTabBarController.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "NLZoomAnimationController.h"
#import "DBCameraSegueViewController.h"
#import "DBCameraView.h"
#import "NLSharePhotoViewController.h"
#import "NLSession.h"
#import "NLNotifications.h"
#import "NLSettingsViewController.h"
#import "NLNavigationViewController.h"
#import "NLInteractionsPopUpView.h"

#import "NLProfileViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "NLInteraction.h"
@import Photos;
@import AssetsLibrary;

static UIViewController *previousController = nil;

@interface NLTabBarController ()<DBCameraViewControllerDelegate,UIViewControllerTransitioningDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) NLZoomAnimationController *animationController;
@property (nonatomic, strong) DBCameraContainerViewController *cameraContainer;
@property (nonatomic, strong) MVCameraViewController *mvCam;

@property (nonatomic, strong) NLInteractionsPopUpView *popUpView;
@property (nonatomic, strong) UIView *pendingNotificationsView;

- (void)yumReady:(NSNotification *)aNotification;
- (void)showSettings:(NSNotification *)aNotification;
- (void)dismissSettings:(NSNotification *)aNotification;
- (void)receivePopUpNotification:(NSNotification *)aNotification;
- (void)showPendingInteractions:(NSNotification *)aNotification;
- (void)hidePendingInteractions:(NSNotification *)aNotification;
- (void)setupCamera;
- (void)showPopUpWithNumberOfLikes:(NSInteger)likes numberOfComments:(NSInteger)comments andFollowers:(NSInteger)followers;
- (void)openCamera:(NSNotification *)notification;

@end

@implementation NLTabBarController
{
	UIImagePickerController *pvc;
	CLLocationManager *locationManager;
	CLLocation *deviceLocation;
	UIButton *photosBtn;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    if (self = [super init])
    {
        [self setDelegate:self];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	    // Do any additional setup after loading the view.
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(yumReady:) name:kNLYumPostReadyNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(showSettings:) name:kNLShowSettingsNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(dismissSettings:) name:kNLDismissSettingsNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(receivePopUpNotification:) name:kNLShowInteractionPopUpNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(showPendingInteractions:) name:kNLShowPendingInteractionsNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(hidePendingInteractions:) name:kNLHidePendingInteractionsNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(hidePopUpInteraction:) name:kNLHideInteractionPopUpNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(openCamera:) name:kNLOpenCameraNotification object:nil];
    self.pendingNotificationsView = [[UIView alloc] initWithFrame:CGRectMake(self.tabBar.bounds.size.width*0.73, self.tabBar.bounds.size.height*0.1, 8, 8)];
    [[self pendingNotificationsView] setBackgroundColor:[UIColor colorWithRed:0.918 green:0.329 blue:0.384 alpha:1]];
    self.pendingNotificationsView.layer.cornerRadius = 4;
    [[self tabBar] addSubview:[self pendingNotificationsView]];
    [self.pendingNotificationsView setAlpha:0.0f];
	
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
    [super setSelectedIndex:selectedIndex];
	
    //we need to set the first previous controller just one time
    
    static dispatch_once_t onceToken;
    __weak NLTabBarController *weakSelf = self;
    dispatch_once(&onceToken, ^{
        previousController = [[weakSelf viewControllers] objectAtIndex:selectedIndex];
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
	
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
	
	return false;
}

- (UIViewController *)childViewControllerForStatusBarHidden
{
    return [self.childViewControllers objectAtIndex:self.selectedIndex];
}

- (UIViewController *)childViewControllerForStatusBarStyle
{
	NSLog(@" tab index%ld", (unsigned long)self.selectedIndex);
	
    return [self.childViewControllers objectAtIndex:self.selectedIndex];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup CAmera


-(void)navigationController:(UINavigationController *)navigationController
		 willShowViewController:(UIViewController *)viewController
									 animated:(BOOL)animated
{
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
}


- (void)setupCamera
{
	self.mvCam = [[MVCameraViewController alloc] init];
	self.mvCam.delegate = self;
	[self.mvCam setWasFullScreenLayout:false];

	NLNavigationViewController *nav = [[NLNavigationViewController alloc] initWithRootViewController:self.mvCam];
	//nav.navigationBar.hidden = true;
	self.animationController = [[NLZoomAnimationController alloc] init];
	[nav setNavigationBarHidden:YES];
	[nav setTransitioningDelegate:self];
	
	
	[self presentViewController:nav animated:YES completion:^{
		[self setSelectedIndex:0];
	}];
	//Brincar
	
	/*
    DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self cameraSettingsBlock:^(DBCameraView *cameraView, id container) {
        cameraView.flashButton.hidden = YES;
        cameraView.gridButton.hidden = YES;
        cameraView.cameraButton.hidden = YES;
    }];
    DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
    [cameraController setUseCameraSegue:YES];
    [cameraController setCameraSegueConfigureBlock:^(DBCameraSegueViewController *segue) {
        
    }];
    
    [cameraController setForceQuadCrop:YES];
    [cameraController setWasFullScreenLayout:NO];
    [container setCameraViewController:cameraController];
    
    NLNavigationViewController *nav = [[NLNavigationViewController alloc] initWithRootViewController:container];
    self.animationController = [[NLZoomAnimationController alloc] init];
    [nav setNavigationBarHidden:YES];
    [nav setTransitioningDelegate:self];
    
    
    [self presentViewController:nav animated:YES completion:^{
        [self setSelectedIndex:0];
    }];
	 */
	

	
}

#pragma mark - PopUp

- (void)showPopUpWithNumberOfLikes:(NSInteger)likes numberOfComments:(NSInteger)comments andFollowers:(NSInteger)followers
{
    __weak NLTabBarController *weakSelf = self;

    
    CGFloat animationDuration = 1.0f;
    CGFloat animationDelay = 5.0f;
	
	if (!weakSelf.popUpView)
	{
		weakSelf.popUpView = [[NLInteractionsPopUpView alloc] initWithNumberOfLikes:likes  numberOfComments:comments andNumberOfFollowers:followers];
		CGFloat xCenter = weakSelf.tabBar.bounds.size.width*0.7;
		weakSelf.popUpView.center = CGPointMake(xCenter, -CGRectGetHeight(weakSelf.popUpView.bounds)/2);
		[[weakSelf tabBar] addSubview:[weakSelf popUpView]];
		[[weakSelf popUpView] setAlpha:0.0];
		
		
		if ([[weakSelf popUpView] alpha] == 0)
		{
			dispatch_async(dispatch_get_main_queue(), ^{
				[UIView animateWithDuration:animationDuration delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
					[[weakSelf popUpView] setAlpha:1.0f];
				} completion:^(BOOL finished) {
					dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animationDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
						[UIView animateWithDuration:animationDuration delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
							[[weakSelf popUpView] setAlpha:0.0f];
						} completion:^(BOOL finished) {
							if (finished) {
								[[weakSelf popUpView] removeFromSuperview];
								[weakSelf setPopUpView:nil];
							}
						}];
					});
				}];
			});
		}

	}
	else
	{
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			[weakSelf showPopUpWithNumberOfLikes:likes numberOfComments:comments andFollowers:followers];
		});


	}
	
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSUInteger index = [tabBarController.viewControllers indexOfObject:viewController];
    if (index == 2)
    {
        [self setupCamera];
        return NO;
        
    }
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    NSUInteger index = [tabBarController.viewControllers indexOfObject:viewController];
    
    if (index != 2)
    {
        if (previousController == viewController)
        {
					NLNavigationViewController *nav = (NLNavigationViewController *)viewController;
					[nav scrollToTop];
        }
    }
    if (index == 4)
    {
        NLNavigationViewController *nav = (NLNavigationViewController *)viewController;
        NLProfileViewController *profileVC = [[nav viewControllers] objectAtIndex:0];
			//[profileVC.profileCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:true];
        [profileVC scrollToTop];
    }
	if (index == 3)
	{
			[UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
				[[self popUpView] setAlpha:0.0f];
			} completion:^(BOOL finished) {
				if (finished) {
					[[self popUpView] removeFromSuperview];
					[self setPopUpView:nil];
				}
			}];
	}
    previousController = viewController;
}
#pragma mark - MVCAm

- (void)closeCamera
{
	
	//[self.mvCam restoreFullScreenMode];
	[self.mvCam.navigationController dismissViewControllerAnimated:YES completion:^{
		[[UIApplication sharedApplication] setStatusBarHidden:false];

	}];
	[NLSession clearLocation];
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    [NLSession setSharedPhoto:image];
    NLSharePhotoViewController *share = [[NLSharePhotoViewController alloc] init];
    [[cameraViewController navigationController] pushViewController:share animated:YES];
	
    if (metadata)
    {
        [NLSession setLocation:[[metadata objectForKey:@"latitude"] floatValue] longitude:[[metadata objectForKey:@"longitude"] floatValue]];
    }
    
}
#pragma mark - UIViewControllerTransitionDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    self.animationController.presenting = YES;
    return self.animationController;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    self.animationController.presenting = NO;
    return self.animationController;
}

#pragma mark - Notifications

- (void)yumReady:(NSNotification *)aNotification
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showSettings:(NSNotification *)aNotification
{
    NLSettingsViewController *settingsVC = [[NLSettingsViewController alloc] initWithCloseButton];
    NLNavigationViewController *settingsNavController = [[NLNavigationViewController alloc] initWithRootViewController:settingsVC];
    [self presentViewController:settingsNavController animated:YES completion:nil];
}

#pragma  mark - Dismiss Notification

- (void)dismissSettings:(NSNotification *)aNotification
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Interactions

- (void)receivePopUpNotification:(NSNotification *)aNotification
{
    NSDictionary *userInfo = [aNotification userInfo];
    NSInteger likes = [[userInfo objectForKey:@"newLikes"] integerValue];
    NSInteger comments = [[userInfo objectForKey:@"newComments"] integerValue];
    NSInteger followers = [[userInfo objectForKey:@"newFollowers"] integerValue];
    
    [self showPopUpWithNumberOfLikes:likes numberOfComments:comments andFollowers:followers];
    [self showPendingInteractions:nil];
}
- (void)hidePopUpInteraction:(NSNotification *)notification
{
    [[self popUpView] setAlpha:0.0f];
}

- (void)showPendingInteractions:(NSNotification *)aNotification
{

    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
            [[self pendingNotificationsView] setAlpha:1.0f];
        } completion:nil];
    });
    
}

- (void)hidePendingInteractions:(NSNotification *)aNotification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
            [[self pendingNotificationsView] setAlpha:0.0f];
            [[self popUpView] setAlpha:0.0];
        } completion:nil];
    });
}

#pragma mark - Open Camera

- (void)openCamera:(NSNotification *)notification
{
    [self setupCamera];
}

@end