//
//  NLVenue.h
//  Yummmie
//
//  Created by bot on 7/21/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"

typedef NS_ENUM(NSInteger, NLVenueType)
{
    NLPrivateVenue,
    NLPublicVenue
};

@interface NLVenue : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *venueId;
@property (nonatomic) NLVenueType venueType;
@property (nonatomic, copy) NSString *street;
@property (nonatomic, copy) NSString *suburb;
@property (nonatomic, copy) NSString *country;

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic) CGFloat latitude;
@property (nonatomic) CGFloat longitude;
@property (nonatomic, copy) NSString *facebook_place_id;
@property (nonatomic) NSInteger distance;

@property (nonatomic, getter=isFromFoursquare) BOOL fromFoursquare;
@property (nonatomic) NSInteger numberOfDishes;

@property (nonatomic, strong) NSString *shareUrl;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (id)initWithFoursquareObject:(NSDictionary *)dictionary;
-(NSDictionary *)getDictionaryFromVenue;
+ (NSURLSessionDataTask *)getVenueWithId:(NSString *)venueId withCompletionBlock:(void (^)(NSError *error,NLVenue *venue))block;

+ (NSURLSessionDataTask *)createVenuewithId:(NSString *)venueId venueName:(NSString *)venueName andCompletionBlock:(void (^)(NSError *error, NLVenue *venue))block;

+ (NSURLSessionDataTask *)createVenueWithName:(NSString *)name latitude:(CGFloat)latitude longitude:(CGFloat)longitude andCompletionBlock:(void (^)(NSError *error, NLVenue *venue))block;

+ (NSURLSessionDataTask *)nearVenuesWithLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void (^)(NSError *error, NSArray *venues))block;

+ (NSURLSessionDataTask *)checkinVenueListWithLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void (^)(NSError *error, NSArray *venues))block;

+ (NSURLSessionDataTask *)getVenuesWithQuery:(NSString *)query afterVenueId:(NSInteger)venueId withCompletionBlock:(void (^)(NSError *error, NSArray *venues))block;

+ (NSURLSessionDataTask *)nearestVenuesWithQuery:(NSString *)query latitude:(CGFloat)latitude longitude:(CGFloat)longitude andCompletionBlock:(void (^)(NSError *error, NSDictionary *venues))block;

+ (NSURLSessionDataTask *)createPrivateVenue:(NSString *)name latitude:(CGFloat)latitude longitude:(CGFloat)longitude andCompletionBlock:(void (^)(NSError *error, NLVenue *venue))block;

+ (NSURLSessionDataTask *)createVenuewithDictionary:(NSDictionary *)dict andCompletionBlock:(void (^)(NSError *error, NLVenue *venue))block;

@end
