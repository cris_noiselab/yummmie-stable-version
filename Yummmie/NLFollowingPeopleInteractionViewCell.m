//
//  NLFollowingPeopleInteractionViewCell.m
//  Yummmie
//
//  Created by bot on 8/26/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLFollowingPeopleInteractionViewCell.h"
#import "UIColor+NLColor.h"
#import "NLConstants.h"

static NSRegularExpression *expression;

@interface NLFollowingPeopleInteractionViewCell ()

@property (strong, nonatomic) TTTAttributedLabel *contentLabel;
- (IBAction)openUser:(id)sender;

@end

@implementation NLFollowingPeopleInteractionViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.contentLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.bounds)+kTimeLabelXPadding,kTimeLabelYOrigin, 260.0f, 28.0f)];
    UIColor *commentColor = [UIColor nl_colorWith255Red:89.0f green:92.0f blue:105.0f andAlpha:255.0f];
    
    [[self contentLabel] setDelegate:self];
    [self.contentLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.contentLabel setMinimumScaleFactor:0.5f];
    [self.contentLabel setNumberOfLines:2];
    [self.contentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.contentLabel setTextColor:commentColor];
    [self.contentLabel adjustsFontSizeToFitWidth];
    [self.contentLabel setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    
    NSDictionary *linkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self contentLabel] setLinkAttributes:linkAttributes];
    
    NSDictionary *activeLinkAttributes = @{(id)kCTForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    [[self contentLabel] setActiveLinkAttributes:activeLinkAttributes];
    NSDictionary *inactiveLinkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self contentLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    
//        [self.contentLabel setBackgroundColor:[UIColor greenColor]];
    [self.contentView addSubview:self.contentLabel];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setOwner:(NSDictionary *)owner andTarget:(NSDictionary *)target
{
	NSLog(@"%@\n%@", owner, target);
	
	self.users = @{[owner objectForKey:@"userName"]: [owner objectForKey:@"userId"], [target objectForKey:@"userName"]: [target objectForKey:@"userId"]};
    NSString *content = [NSString stringWithFormat:@"@%@ %@ @%@",[owner objectForKey:@"userName"],NSLocalizedString(@"follow_other_yum_text", nil),[target objectForKey:@"userName"]];
    
    self.contentLabel.text = content;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        expression = [NSRegularExpression regularExpressionWithPattern:@"@{1}([-A-Za-z0-9_]{2,})" options:NO error:nil];
    });
    
    NSArray *matches;
    NSRange range;
    
    @try
    {
        range = NSMakeRange(0, [content length]);
        matches = [expression matchesInString:content
                                          options:0
                                            range:range];
    }
    @catch (NSException *exception)
    {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",content,NSStringFromRange(range));
    }
    @finally {
        
    }
    
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [content substringWithRange:matchRange];
        NSString* user = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
        [self.contentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    
    [self.contentLabel sizeToFit];
    __weak NLFollowingPeopleInteractionViewCell *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect timeLabelFrame = weakSelf.timeLabel.frame;
        timeLabelFrame.origin.y = CGRectGetMaxY(weakSelf.contentLabel.bounds)+20.0f;
//        [weakSelf.timeLabel setFrame:timeLabelFrame];
    });
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [[self contentLabel] setText:@""];
    
    [[self contentLabel] setFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.bounds)+kTimeLabelXPadding,kTimeLabelYOrigin, 260.0f, 35.0f)];
}

- (IBAction)openUser:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openFollowingPeopleOwnerWithTag:)])
    {
        [[self delegate] openFollowingPeopleOwnerWithTag:[self tag]];
    }
}
#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
	NSLog(@"%@, %@", [self.users objectForKey:[[[url relativeString] componentsSeparatedByString:@":"] lastObject]], [self.users objectForKey:[[[url relativeString] componentsSeparatedByString:@":"] lastObject]]);
    if ([[url scheme] isEqualToString:kUsernameScheme])
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(openFollowingPeopleOwnerWithTag:)])
        {
            [[self delegate] openUserWithId:[[self.users objectForKey:[[[url relativeString] componentsSeparatedByString:@":"] lastObject]] integerValue]];
        }
    }
}

@end
