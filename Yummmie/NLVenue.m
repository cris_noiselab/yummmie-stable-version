
//
//  NLVenue.m
//  Yummmie
//
//  Created by bot on 7/21/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLVenue.h"
#import "NLSession.h"
#import "NLUser.h"

static NSString * const kVenueFoursquareIdKey = @"venue[facebook_place_id]";
static NSString * const kVenueNameKey = @"venue[name]";
static NSString * const kVenueLatitude = @"venue[location_attributes][lat]";
static NSString * const kVenueLongitude = @"venue[location_attributes][lng]";
static NSString * const kVenueQueryKey = @"query";
static NSString * const kVenueAfterKey = @"after_id";
static NSString * const kPrivateVenueKey = @"venue[private_venue]";
static NSString * const kCreatorIdKey = @"venue[creator_id]";


static NSString * const kNLVenueLocationKey = @"location";
static NSString * const kNLVenueCityKey = @"city";
static NSString * const kNLVenueCountryKey = @"country";

static NSString * const kNLVenueLatitudeKey = @"lat";
static NSString * const kNLVenueLongitudeKey = @"lng";
static NSString * const kNLVenueStateKey = @"state";
static NSString * const kNLVenueFoursquareIdKey = @"facebook_place_id";
static NSString * const kNLVenueStreetKey = @"street";
static NSString * const kNLVenueDistanceKey = @"distance";
static NSString * const kNLVenueIdKey = @"id";
static NSString * const kNLVenueNameKey = @"name";
static NSString * const kNLVenuePrivateKey = @"private_venue";
static NSString * const kNLVenueVenueKey = @"venue";
static NSString * const kNLVenueAddressKey = @"address";


@implementation NLVenue
- (void) encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:_venueId forKey:kNLVenueIdKey];
    [encoder encodeObject:_name forKey:kNLVenueNameKey];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    return [self initWithVenueId:[decoder decodeObjectForKey:kNLVenueIdKey] name:[decoder decodeObjectForKey:kNLVenueNameKey]];
}
-(id)initWithVenueId:(NSString *)venueId name:(NSString *)name
{
    if (self = [super init])
    {
        _venueId = venueId;
        _name = name;
        
    }
    return self;
}
- (id)initWithDictionary:(NSDictionary *)dictionary
{

    if (self = [super init])
    {
        _venueId = [dictionary valueForKey:kNLVenueIdKey];
        _name = [dictionary valueForKey:kNLVenueNameKey];
			if ([dictionary objectForKey:@"branch"] && ![[dictionary objectForKey:@"branch"] isEqualToString:@""]) {
				_name = [NSString stringWithFormat:@"%@ (%@)", _name, [dictionary objectForKey:@"branch"]];
			}

        NSString *completeAddress =  [dictionary valueForKey:kNLVenueStreetKey];
        if (![completeAddress isEqual:[NSNull null]])
        {
            NSArray *addressComponents = [completeAddress componentsSeparatedByString:@", "];
            
            _street = [addressComponents objectAtIndex:0];
            if ([addressComponents count] > 1)
            {
                _suburb = [addressComponents objectAtIndex:1];
            }
        }else
        {
            _street = @"";
            _suburb = @"";
        }
        _country = [dictionary valueForKey:kNLVenueCountryKey];
        _city = [dictionary valueForKey:kNLVenueCityKey];
        _venueType = ([[dictionary valueForKey:kNLVenuePrivateKey] boolValue])?NLPrivateVenue:NLPublicVenue;
        
        if (_venueType == NLPublicVenue)
        {
            _latitude = [[dictionary valueForKey:kNLVenueLatitudeKey] floatValue];
            _longitude = [[dictionary valueForKey:kNLVenueLongitudeKey] floatValue];
        }
        
        _state = [dictionary valueForKey:kNLVenueStateKey];
        
        _state = ([_state isEqual:[NSNull null]])?nil:_state;
        _city = ([_city isEqual:[NSNull null]])?nil:_city;
        _facebook_place_id = ([dictionary valueForKey:kNLVenueFoursquareIdKey] != [NSNull null])?[dictionary valueForKey:kNLVenueFoursquareIdKey]:nil;
        if ([dictionary valueForKey:@"share_url"]) {
            _shareUrl = [dictionary valueForKey:@"share_url"];
        }
        else
        {
            _shareUrl = [NSString stringWithFormat:@"http://api.yummmie.com/v/%@", _venueId];
        }
        _fromFoursquare = (![_venueId isEqual:[NSNull null]])?NO:YES;
    }
    return self;
}


- (id)initWithFoursquareObject:(NSDictionary *)dictionary
{
    if (self = [super init])
    {
        _facebook_place_id = dictionary[kNLVenueIdKey];
        _name = dictionary[kNLVenueNameKey];
        _latitude = [dictionary[kNLVenueLocationKey][kNLVenueLatitudeKey] floatValue];
        _longitude = [dictionary[kNLVenueLocationKey][kNLVenueLongitudeKey] floatValue];
        _venueType = NLPublicVenue;
        _state = dictionary[kNLVenueLocationKey][kNLVenueStateKey];
        _city = dictionary[kNLVenueLocationKey][kNLVenueCityKey];
        _distance = [dictionary[kNLVenueLocationKey][kNLVenueDistanceKey] integerValue];
        _street = dictionary[kNLVenueLocationKey][kNLVenueAddressKey];
        _fromFoursquare = YES;

    }
    return self;
}

- (BOOL)isEqual:(id)object
{
    return [[object facebook_place_id] isEqualToString:[self facebook_place_id]];
}

-(NSDictionary *)getDictionaryFromVenue
{
	return @
	{
	kNLVenueIdKey: self.venueId,
	kNLVenueNameKey: self.name,
		
	};
}
+ (NSURLSessionDataTask *)getVenueWithId:(NSString *)venueId withCompletionBlock:(void (^)(NSError *error,NLVenue *venue))block;
{
    NSString *path = [NSString stringWithFormat:@"venues/%@",venueId];
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    return [[NLYummieApiClient sharedClient] GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NLVenue *venue = [[NLVenue alloc] initWithDictionary:[responseObject objectForKey:kNLVenueVenueKey]];
            block(nil,venue);
        }else
        {
            block([NSError new],nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"%@",task.originalRequest);
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block(error,nil);
        }
    }];
    
}
+ (NSURLSessionDataTask *)createVenuewithDictionary:(NSDictionary *)dict andCompletionBlock:(void (^)(NSError *error, NLVenue *venue))block
{
	NSDictionary *params = @{@"venue[name]": [dict objectForKey:@"name"], @"venue[private_venue]": @false, @"venue[facebook_place_id]": [dict objectForKey:@"facebook_place_id"]};
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	return [[NLYummieApiClient sharedClient] POST:@"venues" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if ([responseObject isKindOfClass:[NSDictionary class]])
						{
							NLVenue *venue = [[NLVenue alloc] initWithDictionary:[responseObject valueForKey:kNLVenueVenueKey]];
							if (block)
							{
								block(nil,venue);
							}
						}else
						{
							block(nil,nil);
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error,nil);
						}
					}];


}
+ (NSURLSessionDataTask *)createVenuewithId:(NSString *)venueId venueName:(NSString *)venueName andCompletionBlock:(void (^)(NSError *error, NLVenue *venue))block
{
	NSDictionary *params = @{@"venue[name]": venueName, @"venue[private_venue]": @false, @"venue[facebook_place_id]": venueId};

	[[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    return [[NLYummieApiClient sharedClient] POST:@"venues" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
    {
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            NLVenue *venue = [[NLVenue alloc] initWithDictionary:[responseObject valueForKey:kNLVenueVenueKey]];
            if (block)
            {
                block(nil,venue);
            }
        }else
        {
            block(nil,nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block(error,nil);
        }
    }];
}

+ (NSURLSessionDataTask *)createVenueWithName:(NSString *)name latitude:(CGFloat)latitude longitude:(CGFloat)longitude andCompletionBlock:(void (^)(NSError *, NLVenue *))block
{
    
    NSDictionary *params = @{
                             kVenueNameKey:name,
                             kVenueLatitude:[NSString stringWithFormat:@"%f",latitude],
                             kVenueLongitude:[NSString stringWithFormat:@"%f",longitude],
                             kCreatorIdKey:[NSString stringWithFormat:@"%ld",(long)[[NLSession currentUser] userID]]
                             };
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    return [[NLYummieApiClient sharedClient] POST:@"venues" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            NLVenue *venue = [[NLVenue alloc] initWithDictionary:[responseObject valueForKey:kNLVenueVenueKey]];
            if (block)
            {
                block(nil,venue);
            }
        }else
        {
            block(nil,nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block(error,nil);
        }
    }];
}

+ (NSURLSessionDataTask *)nearVenuesWithLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void (^)(NSError *error, NSArray *))block
{
    NSDictionary *params = @{@"coords": [[NSString stringWithFormat:@"%f,%f",latitude,longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], @"radius": @1};
    
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    return [[NLYummieApiClient sharedClient] GET:@"venues/near" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
    {
        if (block)
        {
            if ([[responseObject objectForKey:@"success"] boolValue])
            {
                if ([[responseObject objectForKey:@"venues"] count] > 0)
                {
                    NSMutableArray *venues = [NSMutableArray array];
                    
                    for (NSDictionary *venueDict in [responseObject objectForKey:@"venues"])
                    {
                        NLVenue *venue = [[NLVenue alloc] initWithDictionary:venueDict];
                        [venues addObject:venue];
                    }
                    block(nil, [NSArray arrayWithArray:venues]);
                }else
                {
                    block([NSError new],nil);
                }
            }
            else
            {
							NSLog(@"venues/near\n%@", responseObject);

                block([NSError new],nil);
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block(error,nil);
        }
    }];
}


+ (NSURLSessionDataTask *)checkinVenueListWithLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude withCompletionBlock:(void (^)(NSError *error, NSArray *venues))block
{
    NSDictionary *params = @{
                             @"coords":[NSString stringWithFormat:@"%f,%f",latitude,longitude],
                             @"radius":@"1"
                             };
    
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    return [[NLYummieApiClient sharedClient] GET:@"venues/near" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
            {
                if (block)
                {
                    if ([[responseObject objectForKey:@"success"] boolValue] == true)
                    {
                        if ([[responseObject objectForKey:@"venues"] count] > 0)
                        {
                            NSMutableArray *venues = [NSMutableArray array];
                            
                            for (NSDictionary *venueDict in [responseObject objectForKey:@"venues"])
                            {
                                NLVenue *venue = [[NLVenue alloc] initWithDictionary:venueDict];
                                [venues addObject:venue];
                            }
                            block(nil, [NSArray arrayWithArray:venues]);
                        }
                        else
                        {
                            block([NSError new],nil);
                        }
                    }
                    else
                    {
											NSLog(@"venues/near\n%@", responseObject);
                        block([NSError new],nil);
                    }
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
                if (block)
                {
                    block(error,nil);
                }
            }];
}
+ (NSURLSessionDataTask *)getVenuesWithQuery:(NSString *)query afterVenueId:(NSInteger)venueId withCompletionBlock:(void (^)(NSError *, NSArray *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];

    NSString *apiPath = @"venues/search";
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if (venueId != 0)
    {
        [params setObject:@(venueId) forKey:kVenueAfterKey];
    }
    
    [params setObject:query forKey:@"venue_name"];
    
    return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if (block)
        {
            if ([[responseObject objectForKey:@"success"] boolValue] == true)
            {
                NSMutableArray *venues = [NSMutableArray array];
						
                for (NSDictionary *venueDictionary in [responseObject objectForKey:@"mini_venues"])
                {
                    NLVenue *venue = [[NLVenue alloc] initWithDictionary:venueDictionary];
                    [venues addObject:venue];
                }
							
                block(nil, venues);
            }else
            {
							NSLog(@"%@\n%@", apiPath, responseObject);
                block([NSError new],nil);
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block(error, nil);
        }
    }];
}
+ (NSURLSessionDataTask *)nearestVenuesWithQuery:(NSString *)query latitude:(CGFloat)latitude longitude:(CGFloat)longitude andCompletionBlock:(void (^)(NSError *, NSDictionary *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSDictionary *params;
    if ([[query stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0)
    {
        params = @{@"coords":[NSString stringWithFormat:@"%f,%f",latitude,longitude]};
    }else
    {
        params = @{@"coords":[NSString stringWithFormat:@"%f,%f",latitude,longitude],
                   @"venue_name":query};
    }
    
    return [[NLYummieApiClient sharedClient] GET:@"venues/global" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
            {
                if (block)
                {
                    if ([[responseObject objectForKey:@"success"] boolValue] == true)
                    {
											NSMutableArray *db_venues = [NSMutableArray array];
											NSMutableArray *fb_venues = [NSMutableArray array];
											
											if ([[responseObject objectForKey:@"db_venues"] count]) {
												for (NSDictionary *venueDict in [responseObject objectForKey:@"db_venues"])
												{
													NLVenue *venue = [[NLVenue alloc] initWithDictionary:venueDict];
													[db_venues addObject:venue];
												}
											}
											if ([[responseObject objectForKey:@"fb_venues"] count])
											{
												for (NSDictionary *venueDict in [responseObject objectForKey:@"fb_venues"])
												{
													NLVenue *venue = [[NLVenue alloc] initWithDictionary:venueDict];
													[fb_venues addObject:venue];
												}
											}
											
											
											block(nil, @{@"db_venues": db_venues, @"fb_venues": fb_venues});
											
                    }
                    else
                    {
											NSLog(@"venues/global\n%@", responseObject);

                        block([NSError new],nil);
                    }
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
                if (block)
                {
                    block(error, nil);
                }
            }];
}

+ (NSURLSessionDataTask *)createPrivateVenue:(NSString *)name latitude:(CGFloat)latitude longitude:(CGFloat)longitude andCompletionBlock:(void (^)(NSError *, NLVenue *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    NSString *apiPath = @"venues";

    NSDictionary *params = @{
                             kVenueNameKey:name,
                             kCreatorIdKey:[NSString stringWithFormat:@"%ld",(long)[[NLSession currentUser] userID]],
                             kPrivateVenueKey:@"1",
                             kVenueLatitude:[NSString stringWithFormat:@"%f",latitude],
                             kVenueLongitude:[NSString stringWithFormat:@"%f",longitude]
                             };
    return [[NLYummieApiClient sharedClient] POST:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            NLVenue *venue = [[NLVenue alloc] initWithDictionary:[responseObject valueForKey:kNLVenueVenueKey]];
            if (block)
            {
                block(nil,venue);
            }
        }else
        {
            block(nil,nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block(error, nil);
        }
    }];
    
}
@end
