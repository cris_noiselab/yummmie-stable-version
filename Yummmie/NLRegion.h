//
//  NLRegion.h
//  Yummmie
//
//  Created by bot on 7/27/17.
//  Copyright © 2017 Noiselab Apps. All rights reserved.
//



#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"
#import "NLSubRegion.h"



@interface NLRegion : NSObject

@property (nonatomic, copy) NSString *region_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NLSubRegion *subRegions;

- (instancetype)initWithDictionary:(NSDictionary *)authenticationDict;
+ (NSURLSessionDataTask *)getRegions :(void (^)(NSError *error, NSArray *regions))block;


@end
