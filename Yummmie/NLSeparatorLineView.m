//
//  NLSeparatorLineView.m
//  Yummmie
//
//  Created by Mauricio Ventura on 05/07/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import "NLSeparatorLineView.h"

@implementation NLSeparatorLineView

- (void)drawRect:(CGRect)rect
{
	UIBezierPath *path = [UIBezierPath bezierPath];
	[path moveToPoint:CGPointMake(0.0, 0.0)];
	[path addLineToPoint:CGPointMake(self.bounds.size.width, 0.0)];
	path.lineWidth = 0.5;
	[[UIColor colorWithWhite:0.5 alpha:0.5] setStroke];
	[path stroke];
}
@end
