//
//  NLDiscoverUserSwipeCellView.m
//  Yummmie
//
//  Created by bot on 2/26/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLDiscoverUserSwipeCellView.h"

@interface NLDiscoverUserSwipeCellView()

@property (nonatomic, assign) NLDiscoverUserSwipeCellDirection direction;
@property (nonatomic, strong) UIView *highlightArea;

- (IBAction)userButtonAction:(id)sender;
- (IBAction)userPhotoButtonAction:(id)sender;

- (void)panGestureHandler:(UIPanGestureRecognizer *)gesture;
- (void)buttonAction:(id)sender;

@end

@implementation NLDiscoverUserSwipeCellView

- (void)awakeFromNib
{
    self.userPhotoButton.layer.cornerRadius = CGRectGetHeight(self.userPhotoButton.bounds)/2.0f;
    [self.userPhotoButton setClipsToBounds:YES];
	[self.userPhotoButton setContentMode:UIViewContentModeScaleAspectFill];

    // Initialization code
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
    [gesture setDelegate:self];
    [self addGestureRecognizer:gesture];
    
    self.dishButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.dishButton setFrame:CGRectMake(0, 0, 100, 100)];
    [[self dishButton] setBackgroundImage:[UIImage imageNamed:@"1"] forState:UIControlStateNormal];
    [[self dishButton] addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.firstDish = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"2"]];
//    self.secondDish = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"3"]];
    self.thirdDish = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"moreButton"]];
    
    [[self dishButton] setFrame:CGRectOffset(self.dishButton.frame, CGRectGetWidth(self.contentView.bounds)-CGRectGetWidth(self.dishButton.bounds), 0)];
    [self.firstDish setFrame:CGRectOffset(self.firstDish.frame, CGRectGetWidth(self.contentView.frame), 0)];
    [self.thirdDish setFrame:CGRectMake(0, 0, CGRectGetWidth(self.firstDish.bounds), CGRectGetWidth(self.firstDish.bounds))];
    self.highlightArea = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 400, CGRectGetHeight(self.firstDish.bounds))];
    [self.highlightArea setFrame:CGRectOffset(self.highlightArea.frame, CGRectGetWidth(self.firstDish.frame)+CGRectGetWidth(self.contentView.frame),  0)];
    [[self highlightArea] setBackgroundColor:self.backgroundColor];
    
    [self.thirdDish setContentMode:UIViewContentModeCenter];
    [self.thirdDish setBackgroundColor:self.backgroundColor];
    [self.highlightArea addSubview:[self thirdDish]];
    
    [[self contentView] addSubview:[self dishButton]];
    [[self contentView] addSubview:[self firstDish]];
    [[self contentView] addSubview:[self secondDish]];
    [[self contentView] addSubview:[self highlightArea]];
    
    [[self contentView] setClipsToBounds:NO];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [[[self usernameButton] titleLabel] setAdjustsFontSizeToFitWidth:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - Button Actions

- (IBAction)userButtonAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openDetailWithTag:)])
    {
        [[self delegate] openDetailWithTag:[self tag]];
    }
}

- (IBAction)userPhotoButtonAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openDetailWithTag:)])
    {
        [[self delegate] openDetailWithTag:[self tag]];
    }
}


#pragma mark - Pan Gesture Handler

- (void)panGestureHandler:(UIPanGestureRecognizer *)gesture
{
    UIGestureRecognizerState state      = [gesture state];
    CGPoint translation                 = [gesture translationInView:self];
    CGPoint velocity                    = [gesture velocityInView:self];
    
    
    float originWanted = -(self.contentView.bounds.size.width-self.firstDish.bounds.size.width);
    _direction = (velocity.x>0)?NLDiscoverUserSwipeCellDirectionRight:NLDiscoverUserSwipeCellDirectionLeft;

    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGRect rect = self.contentView.frame;
        if (rect.origin.x+translation.x >= 0 && self.direction == NLDiscoverUserSwipeCellDirectionRight)
        {
            rect.origin.x = 0;
        }else
        {
            rect.origin.x+=translation.x;
            if (rect.origin.x < originWanted)
            {
                CGRect highlightNewRect = [[self highlightArea] frame];
                highlightNewRect.origin.x = self.dishButton.frame.origin.x;
                highlightNewRect.size.width = 400;
                [UIView animateWithDuration:0.25 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [[self highlightArea] setFrame:highlightNewRect];
                } completion:^(BOOL finished) {
                    
                }];
            }else
            {
                if (self.direction == NLDiscoverUserSwipeCellDirectionRight)
                {
                    CGRect highlightNewRect = [[self highlightArea] frame];
                    if (highlightNewRect.origin.x != self.contentView.bounds.size.width)
                    {
                        highlightNewRect.origin.x = (self.contentView.bounds.size.width+CGRectGetWidth(self.firstDish.bounds));
                        highlightNewRect.size.width = 400;
                        [UIView animateWithDuration:0.25 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                            [[self highlightArea] setFrame:highlightNewRect];
                        } completion:^(BOOL finished) {
                            
                        }];
                    }
                }
            }
        }
        [[self contentView] setFrame:rect];
        [gesture setTranslation:CGPointZero inView:self.contentView];
        
    }else if (state == UIGestureRecognizerStateEnded)
    {
        if (self.contentView.frame.origin.x < originWanted)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(openDetailWithTag:)])
            {
                [[self delegate] openDetailWithTag:self.tag];
            }
        }
        [UIView animateWithDuration:0.30    delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGRect newFrame =self.contentView.frame;
            newFrame.origin = CGPointZero;
            [[self contentView] setFrame:newFrame];
            CGRect highlightNewRect = [[self highlightArea] frame];
            
            if (highlightNewRect.origin.x != self.contentView.bounds.size.width)
            {
                highlightNewRect.origin.x = (self.contentView.bounds.size.width+CGRectGetWidth(self.firstDish.bounds));
                highlightNewRect.size.width = 400;
                [UIView animateWithDuration:0.25 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [[self highlightArea] setFrame:highlightNewRect];
                } completion:^(BOOL finished) {
                    
                }];
            }
        } completion:nil];
    }
}

- (NLDiscoverUserSwipeCellDirection)directionWithPercentage:(CGFloat)percentage {
    if (percentage < 0) {
        return NLDiscoverUserSwipeCellDirectionLeft;
    }
    
    else if (percentage > 0) {
        return NLDiscoverUserSwipeCellDirectionRight;
    }
    
    else {
        return NLDiscoverUserSwipeCellDirectionCenter;
    }
}

#pragma mark - Button Action 

- (void)buttonAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openDishWithTag:)])
    {
        [[self delegate] openDishWithTag:[self tag]];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    
    if ([gestureRecognizer class] == [UIPanGestureRecognizer class]) {
        
        UIPanGestureRecognizer *g = (UIPanGestureRecognizer *)gestureRecognizer;
        CGPoint point = [g velocityInView:self];
        
        if (fabs(point.x) > fabs(point.y) )
        {
            return YES;
        }
    }
    return NO;
}
@end
