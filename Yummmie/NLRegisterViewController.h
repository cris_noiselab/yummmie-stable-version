//
//  NLRegisterViewController.h
//  Yummmie
//
//  Created by bot on 6/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import "TTTAttributedLabel.h"
#import "NLYummieApiClient.h"
#import "NLPhotoSelection.h"
#import "NLEmailViewController.h"


@interface NLRegisterViewController : NLBaseViewController<UITextFieldDelegate,TTTAttributedLabelDelegate,UIActionSheetDelegate, NLEmailDelegate>


@end
