//
//  NLAuthorization.m
//  Yummmie
//
//  Created by bot on 12/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLAuthentication.h"
#import "NLSession.h"
#import "NLUser.h"

static NSString * const kNLAuthenticationFacebookName = @"facebook";
static NSString * const kNLAuthenticationFoursquareName = @"foursquare";
static NSString * const kNLAuthenticationTwitterName = @"twitter";
static NSString * const kNLAuthenticationPushName = @"onesignal";

static NSString * const kNLAuthenticationUserIdKey = @"authentication[uid]";
static NSString * const kNLAuthenticationProviderKey  = @"provider";
static NSString * const kNLAuthenticationTokenKey = @"provider_token";
static NSString * const kNLAuthenticationTokenSecretKey = @"provider_token_secret";

static NSString * const kNLAuthenticationResponseProviderKey = @"provider";
static NSString * const kNLAuthenticationResponseTokenKey = @"token";
static NSString * const kNLAuthenticationResponseTokenSecretKey = @"token_secret";
static NSString * const kNLAuthenticationResponseUserIdKey = @"uid";
static NSString * const kNLAuthenticationResponseIdKey = @"id";

static NSString * const kNLAuthenticationResponseAuthenticationKey = @"authentication";

@interface NLAuthentication ()

@property (nonatomic, copy, readwrite) NSString * token;
@property (nonatomic, copy, readwrite) NSString * tokenSecret;
@property (nonatomic, copy, readwrite) NSString * userId;
@property (nonatomic, copy, readwrite) NSString *authenticationID;

@end

@implementation NLAuthentication


- (instancetype)initWithDictionary:(NSDictionary *)authenticationDict
{
    if (self = [super init])
    {
        _provider = [NLAuthentication authenticationTypeForString:[authenticationDict objectForKey:kNLAuthenticationResponseProviderKey]];
			
        _token = [authenticationDict objectForKey:kNLAuthenticationResponseTokenKey];
        _tokenSecret = [authenticationDict objectForKey:kNLAuthenticationResponseTokenSecretKey];
        _userId = [authenticationDict objectForKey:kNLAuthenticationResponseUserIdKey];
        _authenticationID = [authenticationDict objectForKey:kNLAuthenticationResponseIdKey];
        
    }
    
    return self;
}

- (BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[NLAuthentication class]])
        return NO;
    return [self provider] == [object provider];
}

+ (NSURLSessionDataTask *)addAuthenticationWithType:(NLAuthenticationType)authType userID:(NSUInteger )userID token:(NSString *)token tokenSecret:(NSString *)tokenSecret withCompletionBlock:(void (^)(NSError *error, NLAuthentication *authentication))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
	
    switch (authType)
    {
        case NLAuthenticationTypeFacebook:
            {
                [params setObject:token forKey:kNLAuthenticationTokenKey];
                [params setObject:kNLAuthenticationFacebookName forKey:kNLAuthenticationProviderKey];
                break;
            }
        case NLAuthenticationTypeTwitter:
            {
                [params setObject:token forKey:kNLAuthenticationTokenKey];
                [params setObject:tokenSecret forKey:kNLAuthenticationTokenSecretKey];
                [params setObject:kNLAuthenticationTwitterName forKey:kNLAuthenticationProviderKey];
                break;
            }
			case NLAuthenticationTypePushNotification:
			{
				[params setObject:token forKey:kNLAuthenticationTokenKey];
				[params setObject:kNLAuthenticationPushName forKey:kNLAuthenticationProviderKey];
				break;
			}
        case NLAuthenticationTypeFoursquare:
            {
                [params setObject:token forKey:kNLAuthenticationTokenKey];
                [params setObject:kNLAuthenticationFoursquareName forKey:kNLAuthenticationProviderKey];
                break;
            }
        default:
            break;
    }
	//NSLog(@"%@", [NSString stringWithFormat:@"users/%ld/authentications", (unsigned long)userID]);
    return [[NLYummieApiClient sharedClient] POST:[NSString stringWithFormat:@"users/%ld/authentications", (unsigned long)userID] parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
    {
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"success"] boolValue])
        {
					NLAuthentication *auth = [[NLAuthentication alloc] initWithDictionary:[responseObject objectForKey:kNLAuthenticationResponseAuthenticationKey]];
					NSMutableArray *auths = [NLSession currentUser].authentications;
					if ([NLSession containsAuthentication:auth.provider])
					{
						for (NLAuthentication *fAuth in auths)
						{
							if (fAuth.provider == authType)
							{
								[auths removeObject:fAuth];
							}
						}
					}
					[auths addObject:auth];
					[[NLSession currentUser] setAuthentications:auths];
					if (block)
					{
						block(nil, auth);
					}
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)
			{
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);        
        if (block)
        {
            block([[NSError alloc] init], nil);
        }
    }];
}

+ (NSURLSessionDataTask *)searchFriendsWithType:(NLAuthenticationType)authType withCompletionBlock:(void (^)(NSError *error, NSArray *users))block

{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    switch (authType)
    {
        case NLAuthenticationTypeFacebook:
            [params setObject:kNLAuthenticationFacebookName forKey:@"provider"];
            break;
        case NLAuthenticationTypeTwitter:
        {
            [params setObject:kNLAuthenticationTwitterName forKey:@"provider"];
            break;
        }
        case NLAuthenticationTypeFoursquare:
        {
            [params setObject:kNLAuthenticationFoursquareName forKey:@"provider"];
            break;
        }
        default:
            break;
    }
    
    NSString *apiPath = [NSString stringWithFormat:@"authentications/%ld/friends",(long)[[NLSession currentUser] userID]];
    
    return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
    {
        if (block)
        {
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                if ([responseObject count]>0)
                {
                    NSMutableArray *users = [NSMutableArray array];
                    for (NSDictionary *userDict in responseObject)
                    {
                        NLUser *user = [[NLUser alloc] initWithDictionary:userDict];
                        [users addObject:user];
                    }
                    
                    block(nil,users);
                }else
                {
                    block([NSError new],nil);
                }
            }else
            {
                block([NSError new],nil);
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block([NSError new], nil);
        }
    }];
}

+ (NSURLSessionDataTask *)searchFriendsWithEmails:(NSString *)emailsString withCompletionBlock:(void (^)(NSError *error, NSArray *users))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    NSString *apiPath = [NSString stringWithFormat:@"authentications/%ld/friends",(long)[[NLSession currentUser] userID]];
    NSDictionary *params = @{@"emails":emailsString};
    
    return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
            {
                if (block)
                {
                    
                    if ([responseObject isKindOfClass:[NSArray class]])
                    {
                        if ([responseObject count]>0)
                        {
                            NSMutableArray *users = [NSMutableArray array];
                            for (NSDictionary *userDict in responseObject)
                            {
                                NLUser *user = [[NLUser alloc] initWithDictionary:userDict];
                                [users addObject:user];
                            }
                            
                            block(nil,users);
                        }else
                        {
                            block([NSError new],nil);
                        }
                    }else
                    {
                        block([NSError new],nil);
                    }
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
                if (block)
                {
                    block([NSError new], nil);
                }
            }];
}

+ (NSURLSessionDataTask *)removeAuthenticationWithType:(NLAuthenticationType)authentication withCompletionBlock:(void (^)(NSError *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];

    NSString *apiPath = [NSString stringWithFormat:@"/users/%ld/authentications/remove",[NLSession getUserID]];
	NSMutableArray *auths = [NLSession currentUser].authentications;
	NLAuthentication *pnAuth;
	if ([NLSession containsAuthentication:authentication])
	{
		for (NLAuthentication *fAuth in auths)
		{
			if (fAuth.provider == authentication)
			{
				pnAuth = fAuth;
			}
		}
		NSString *provider;
		switch (authentication)
		{
			case 0:
				provider = @"facebook";
				break;
			case 1:
				provider = @"twitter";
				
				break;
			case 2:
				provider = @"onesignal";
				
				break;
			default:
				break;
		}
		NSDictionary *params = @{
														 @"provider": provider,
														 @"provider_token": pnAuth.token
														 };
		NSLog(@"%@", params);
		return [[NLYummieApiClient sharedClient] DELETE:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
			if (block)
			{
				block(nil);
			}
		} failure:^(NSURLSessionDataTask *task, NSError *error) {
			NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
			if (block)
			{
				block([NSError new]);
			}
		}];
	}
	return [[NLYummieApiClient sharedClient] DELETE:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			block(nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block([NSError new]);
		}
	}];
}

+ (NLAuthenticationType)authenticationTypeForString:(NSString *)authenticationName
{
    if ([authenticationName isEqualToString:kNLAuthenticationFacebookName])
    {
        return NLAuthenticationTypeFacebook;
    }else if ([authenticationName isEqualToString:kNLAuthenticationTwitterName])
    {
        return NLAuthenticationTypeTwitter;
    }else if ([authenticationName isEqualToString:kNLAuthenticationFoursquareName])
    {
        return NLAuthenticationTypeFoursquare;
    }else
    {
        return NLAuthenticationTypeNone;
    }
}


@end