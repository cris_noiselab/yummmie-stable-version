//
//  NLSession.m
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <ACSimpleKeychain/ACSimpleKeychain.h>
#import "NLSession.h"
#import "NLUser.h"
#import "NLVenue.h"
#import "NLYum.h"

static NLSession *_sharedSession;

static NSString * const kSessionStoreUsername = @"mainUser";
static NSString * const kSessionStoreService = @"yummmie";
static NSString * const kSessionEmailKey = @"email";

@interface NLSession ()

@property (nonatomic, strong) NLUser *user;
@property (nonatomic, strong) UIImage *sharedPhoto;
@property (nonatomic, strong) NLYum *yumToShare;
@property (nonatomic, strong) UIImage *sharedCover;
@property (nonatomic, strong) UIImage *sharedAvatar;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) NLVenue *sharedVenue;

@end

@implementation NLSession

+ (instancetype)sharedSession
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_sharedSession = [[NLSession alloc] init];
		_sharedSession.user = nil;
	});
	
	return _sharedSession;
}

+ (NLUser *)currentUser
{
	NSLog(@"%@", _sharedSession.user);
	return _sharedSession.user;
}

+ (void)updateUser:(NLUser *)newUser
{
	if (!newUser)
	{
		[NSException raise:@"NLSession User Exception" format:@"You should set a valid user to the session and not a nil value "];
	}
	
	[_sharedSession setUser:newUser];
	
}

+ (void)createUser:(NLUser *)newUser
{
	if (!newUser)
	{
		[NSException raise:@"NLSession User Exception" format:@"You should set a valid user to the session and not a nil value "];
	}
	
	[_sharedSession setUser:newUser];
	
	if (newUser.authToken)
	{
		
		[_sharedSession storeUsername:kSessionStoreUsername password:_sharedSession.user.authToken identifier:[NSString stringWithFormat:@"%ld",(long)_sharedSession.user.userID] forService:kSessionStoreService];
		
		//[[ACSimpleKeychain defaultKeychain] storeUsername:kSessionStoreUsername password:_sharedSession.user.authToken identifier:[NSString stringWithFormat:@"%ld",(long)_sharedSession.user.userID] forService:kSessionStoreService];
	}
}

+ (BOOL)isCurrentUser:(NSInteger)userID
{
	return  (_sharedSession.user.userID == userID);
}

+ (BOOL)isCurrentUserWithString:(NSString *)username
{
	return [_sharedSession.user.username isEqualToString:username];
}
+ (id)accessToken
{
	NSDictionary *credentials = [[ACSimpleKeychain defaultKeychain] credentialsForUsername:kSessionStoreUsername service:kSessionStoreService];
	if (credentials)
	{
		[[ACSimpleKeychain defaultKeychain] deleteCredentialsForUsername:kSessionStoreUsername service:kSessionStoreService];
		[_sharedSession storeUsername:kSessionStoreUsername password:[credentials objectForKey:@"password"] identifier:[NSString stringWithFormat:@"%ld",[[credentials objectForKey:@"identifier"] integerValue]] forService:kSessionStoreService];

	}
	 credentials = [_sharedSession credentialsForUsername:kSessionStoreUsername service:kSessionStoreService];
	
	if (!credentials)
	{
		return nil;
	}else
	{
		[_sharedSession storeUsername:kSessionStoreUsername password:_sharedSession.user.authToken identifier:[NSString stringWithFormat:@"%ld",(long)_sharedSession.user.userID] forService:kSessionStoreService];
		//[[ACSimpleKeychain defaultKeychain] storeUsername:kSessionStoreUsername password:[credentials objectForKey:ACKeychainPassword] identifier:[credentials objectForKey:ACKeychainIdentifier] forService:kSessionStoreService];
		return [credentials valueForKey:ACKeychainPassword];
	}
}

+ (NSInteger)getUserID
{
	
	NSDictionary *credentials = [_sharedSession credentialsForUsername:kSessionStoreUsername    service:kSessionStoreService];
	
	if (!credentials)
	{
		return 0;
	}
	
	return [[credentials valueForKey:ACKeychainIdentifier] integerValue];
	
}

+ (BOOL)deleteToken
{
	[[ACSimpleKeychain defaultKeychain] deleteCredentialsForUsername:kSessionStoreUsername service:kSessionStoreService];
	return [_sharedSession deleteCredentialsForUsername:kSessionStoreUsername service:kSessionStoreService];
}

+ (void)setSharedPhoto:(UIImage *)newPhoto
{
	[_sharedSession setSharedPhoto:newPhoto];
}

+ (UIImage *)getSharedPhoto
{
	return _sharedSession.sharedPhoto;
}

+ (void)setYum:(NLYum *)aYum
{
	[[self sharedSession] setYumToShare:aYum];
}

+ (NLYum *)yum
{
	return [[self sharedSession] yumToShare];
}

+ (void)setSharedAvatar:(UIImage *)image
{
	_sharedSession.sharedAvatar = image;
}

+ (void)setSharedCover:(UIImage *)image
{
	_sharedSession.sharedCover = image;
}

+ (UIImage *)sharedCover
{
	return _sharedSession.sharedCover;
}

+ (UIImage *)sharedAvatar
{
	return _sharedSession.sharedAvatar;
}

+ (CLLocation *)locationForImage
{
	return  _sharedSession.location;
}

+ (void)setLocation:(CGFloat)latitude longitude:(CGFloat)longitude
{
	CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
	_sharedSession.location = newLocation;
}

+ (void)clearLocation
{
	_sharedSession.location = nil;
}

+ (BOOL)containsAuthentication:(NLAuthenticationType)type
{
	for (NLAuthentication *authentication in _sharedSession.user.authentications)
	{
		
		if (authentication.provider == type)
		{
			return YES;
		}
	}
	return NO;
}

+ (BOOL)endSession
{
	
	for (NLAuthentication *authentication in _sharedSession.user.authentications)
	{
		
		if (authentication.provider == NLAuthenticationTypeFacebook)
		{
			[NLAuthentication removeAuthenticationWithType:authentication.provider withCompletionBlock:^(NSError *error) {
				if (error) {
					NSLog(@"Error eliminando autenticación");
				}
			}];
			[NLAuthentication removeAuthenticationWithType:authentication.provider withCompletionBlock:^(NSError *error) {
				if (error) {
					NSLog(@"Error eliminando autenticación");
				}
			}];
			
			break;
		}
	}
	
	[_sharedSession setUser:nil];
	[_sharedSession setSharedAvatar:nil];
	[_sharedSession setSharedCover:nil];
	[_sharedSession setSharedPhoto:nil];
	[_sharedSession setShowActivity:NO];
	[_sharedSession setLocation:nil];
	
	
	
	return [NLSession deleteToken];
}

#pragma mark - Get/Set SharedVenue
+ (NLVenue *)getSharedVenue
{
	return  [_sharedSession sharedVenue];
}
+ (void)setSharedVenue:(NLVenue *)newVenue
{
	if ([newVenue isKindOfClass:[NLVenue class]])
	{
		[_sharedSession setSharedVenue:newVenue];
	}
	else
	{
		[_sharedSession setSharedVenue:nil];
	}
}
-(void)storeUsername:(NSString *)username password:(NSString *)password identifier:(NSString *)identifier forService:(NSString *)service
{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	if (password && identifier && username && service && ![password isEqualToString:@""])
	{
		NSDictionary *credentials = @{ACKeychainPassword: password, ACKeychainIdentifier: identifier, ACKeychainService: service, ACKeychainUsername: username};
		NSLog(@"%@", credentials);

		[def setObject:credentials forKey:[NSString stringWithFormat:@"%@_%@", username, service]];
		[def synchronize];

	}
}
-(NSDictionary *)credentialsForUsername:(NSString *)username service:(NSString *)service
{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	NSLog(@"%@", [def objectForKey:[NSString stringWithFormat:@"%@_%@", username, service]]);
	return [def objectForKey:[NSString stringWithFormat:@"%@_%@", username, service]];
}
-(BOOL)deleteCredentialsForUsername:(NSString *)username service:(NSString *)service
{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	NSDictionary *credentials = @{ACKeychainPassword: @"", ACKeychainIdentifier: @"", ACKeychainService: @"", ACKeychainUsername: @""};
	[def setObject:nil forKey:[NSString stringWithFormat:@"%@_%@", username, service]];
	[def synchronize];
	return true; 
}
@end
