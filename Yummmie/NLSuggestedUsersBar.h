//
//  NLSuggestedUsersBar.h
//  Yummmie
//
//  Created by bot on 3/31/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NLSuggestedUsersBarDelegate;


@interface NLSuggestedUsersBar : UIView

@property (weak, nonatomic) id<NLSuggestedUsersBarDelegate>delegate;
@property (atomic, assign) BOOL hashtagging;

- (void)updateDatasource:(NSArray *)datasource;

@end

@protocol NLSuggestedUsersBarDelegate <NSObject>

- (void)suggestedBar:(NLSuggestedUsersBar *)suggestedBar didSelectUser:(id)user atIndex:(NSInteger)index;

@end