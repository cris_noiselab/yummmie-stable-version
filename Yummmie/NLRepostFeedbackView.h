//
//  NLRepostFeedbackView.h
//  Yummmie
//
//  Created by Mauricio Ventura on 10/08/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLRepostFeedbackView : UIView
-(id)initWithFrame:(CGRect)frame andImageBase:(NSString *)imageBaseName count:(NSInteger)count;
-(void)show;

@end
