//
//  NLRegisterViewController.m
//  Yummmie
//
//  Created by bot on 6/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//


#import "NLRegisterViewController.h"
#import "NLScrollView.h"
#import "UIColor+NLColor.h"
#import "NLTermsPolicyViewController.h"
#import "NLConstants.h"
#import "NLError.h"
#import "NSString+NLStringValidations.h"
#import "DBCameraLibraryViewController.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraSegueViewController.h"
#import "DBCameraView.h"
#import "NLUser.h"
#import "NLSession.h"
#import "NLNotifications.h"
#import <SDWebImage/UIButton+WebCache.h>


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
const CGFloat kTermsTopPadding =20.0f;
const CGFloat kTermsBottomPadding = 30.0f;
const CGFloat kTermsLabelHeight = 50.0f;
const CGFloat kTermsLabelWidth = 250.0f;


@interface NLRegisterViewController ()<DBCameraViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *coverPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *profilePictureButton;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIButton *joinWithFacebookButton;
@property (weak, nonatomic) IBOutlet NLScrollView *scroll;

@property (weak, nonatomic) IBOutlet UIImageView *fadingView;
@property (weak, nonatomic) IBOutlet UITextField *userActivity;
@property (weak, nonatomic) IBOutlet UITextField *venueActivity;
@property (weak, nonatomic) IBOutlet UIButton *userActivityButton;
@property (weak, nonatomic) IBOutlet UIButton *venueActivityButton;
@property (nonatomic, assign) BOOL user_venue;


@property (nonatomic) PhotoSelectionMode currentMode;
@property (strong, nonatomic) CAGradientLayer *gradientLayer;
@property (weak, nonatomic) UIImageView *camerIcon;

@property (weak, nonatomic) UITextField *currentTextfield;
@property (strong, nonatomic) TTTAttributedLabel *disclaimerLabel;
@property (nonatomic) CGSize originalSize;
@property (nonatomic) CGSize extendedSize;
@property (nonatomic) CGSize currentSize;

@property (nonatomic,strong) UIImage *coverPhoto;
@property (nonatomic,strong) UIImage *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *profileImageLabel;
@property (strong, nonatomic) IBOutlet UILabel *coverPhotoLabel;
@property (strong, nonatomic) IBOutlet UIImageView *profileCameraIcon;

@property (nonatomic) BOOL avoidScrollResize;
@property (nonatomic) BOOL animatedCover;
@property (strong, nonatomic) NSArray *animationPhotos;

- (IBAction)coverPhotoTapped:(id)sender;
- (IBAction)profilePictureTapped:(id)sender;
- (IBAction)joinAction:(id)sender;
- (IBAction)joinWithFacebookAction:(id)sender;

- (void)keyboardDidHide:(NSNotification *)notification;
- (void)validateInputs:(NSError **)error;

- (void)disableJoinButtons;
- (void)enableJoinButtons;


@end

@implementation NLRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.title = NSLocalizedString(@"join_text", nil);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        _animatedCover = YES;
        _animationPhotos = @[[UIImage imageNamed:@"cover_1.jpg"],[UIImage imageNamed:@"cover_2.jpg"],[UIImage imageNamed:@"cover_3.jpg"],[UIImage imageNamed:@"cover_4.jpg"],[UIImage imageNamed:@"cover_5.jpg"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.venueActivity.placeholder = NSLocalizedString(@"venueButton", nil);
    self.userActivity.placeholder = NSLocalizedString(@"userButton", nil);
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];
    //Google Analytics
    NSString *name = [NSString stringWithFormat:@"entrar View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }


- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    
    // Do any additional setup after loading the view from its nib.
    [[[self navigationController] navigationBar] setTranslucent:NO];
    [[[self navigationController] navigationBar] setOpaque:YES];
    
    [self addRedBackground];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [[self contentView] setBackgroundColor:[UIColor whiteColor]];
    
    //set the cover photo
    self.gradientLayer = [CAGradientLayer layer];
    [self.gradientLayer setAnchorPoint:CGPointZero];
    [self.gradientLayer setFrame:[self.coverPhotoButton bounds]];
    [self.gradientLayer setLocations:@[@0.0f,@1.0f]];
    
    UIColor *topColor = [UIColor colorWithWhite:0.0f alpha:0.4];
    UIColor *bottomColor = [UIColor colorWithWhite:0.0f alpha:0.7];
    
    [self.gradientLayer setColors:@[(id)[topColor CGColor],(id)[bottomColor CGColor]]];
    
    if ([self isBeingPresented] || [self isMovingToParentViewController]) {
        [[self.fadingView layer] insertSublayer:self.gradientLayer atIndex:(unsigned)self.coverPhotoButton.layer.sublayers.count];
    }
    
    
    UIImageView *photoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_camara"]];
    CGRect cameraRect = CGRectOffset([photoImage bounds], 10, CGRectGetHeight(self.coverPhotoButton.bounds)-25);
    [photoImage setFrame:cameraRect];
    //[[self coverPhotoButton] addSubview:photoImage];
    
    self.camerIcon = photoImage;
    
    
    
    [self.coverPhotoLabel setText:NSLocalizedString(@"add_cover_picture_title", nil)];
    
    [self.coverPhotoButton.titleLabel setFrame:CGRectOffset([self.coverPhotoButton.titleLabel bounds], 20, CGRectGetHeight(self.coverPhotoButton.bounds)-25)];
    
    
    [self.profilePictureButton.imageView setCenter:CGPointMake(self.profilePictureButton.center.x, self.profilePictureButton.center.y - (self.profilePictureButton.frame.size.height/4))];
    
    //setup profile picture
    self.profilePictureButton.layer.cornerRadius = self.profilePictureButton.bounds.size.height/2.0f;;
    self.profilePictureButton.layer.masksToBounds = YES;
    self.profilePictureButton.layer.shouldRasterize = YES;
    self.profilePictureButton.layer.rasterizationScale = [[UIScreen mainScreen] scale];
    
    [self.profileImageLabel setText:NSLocalizedString(@"add_profile_picture_title", nil)];
    //setup the fields
    
    [[self nameField] setDelegate:self];
    [[self emailField] setDelegate:self];
    [[self usernameField] setDelegate:self];
    [[self passwordField] setDelegate:self];
    
    [[self nameField] setPlaceholder:NSLocalizedString(@"join_name_text", nil)];
    [[self emailField] setPlaceholder:NSLocalizedString(@"join_email_text", nil)];
    [[self usernameField] setPlaceholder:NSLocalizedString(@"join_username_text", nil)];
    [[self passwordField] setPlaceholder:NSLocalizedString(@"join_password_text", nil)];
    
    //Set terms label
    
    self.disclaimerLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(35, self.joinWithFacebookButton.frame.origin.y+self.joinWithFacebookButton.bounds.size.height+kTermsTopPadding, kTermsLabelWidth, kTermsLabelHeight)];
    [self.disclaimerLabel setCenter:CGPointMake(rootView.center.x, self.disclaimerLabel.center.y-10)];
    
    
    [[self disclaimerLabel] setTextAlignment:NSTextAlignmentCenter];
    [[self disclaimerLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]];
    [[self disclaimerLabel] setNumberOfLines:2];
    [[self disclaimerLabel] setTextColor:[UIColor grayColor]];
    
    NSDictionary *linkAttributes = @{
                                     (id)kCTForegroundColorAttributeName:[UIColor blackColor],
                                     (id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInteger:kCTUnderlineStyleNone]
                                     };
    
    [[self disclaimerLabel] setLinkAttributes:linkAttributes];
    
    NSDictionary *activeLinkAttributes = @{
                                           (id)kCTForegroundColorAttributeName: [UIColor lightGrayColor],
                                           (id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInteger:kCTUnderlineStyleNone]
                                           };
    
    [[self disclaimerLabel] setActiveLinkAttributes:activeLinkAttributes];
    
    [[self disclaimerLabel] setDelegate:self];
    
    NSString *termsLabelText = NSLocalizedString(@"join_page_agreement_text", nil);
    
    NSRange termsLinkRange  = [termsLabelText rangeOfString:NSLocalizedString(@"join_page_agreement_terms_link", nil) options:NSCaseInsensitiveSearch];
    NSRange policyLinkRange = [termsLabelText rangeOfString:NSLocalizedString(@"join_page_agreement_policy", nil) options:NSCaseInsensitiveSearch];
    
    [[self disclaimerLabel] setText:termsLabelText afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString)
     {
         
         UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
         CTFontRef fontRef = CTFontCreateWithName((__bridge CFStringRef)boldFont.fontName, boldFont.pointSize, NULL);
         
         if (fontRef)
         {
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)fontRef range:termsLinkRange];
             [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)fontRef range:policyLinkRange];
             CFRelease(fontRef);
         }
         return mutableAttributedString;
     }];
    
    
    [[self disclaimerLabel] addLinkToURL:[NSURL URLWithString:kTermsOfUseURL] withRange:termsLinkRange];
    [[self disclaimerLabel] addLinkToURL:[NSURL URLWithString:kPrivacyPoliceURL] withRange:policyLinkRange];
    
    [[self contentView] addSubview:[self disclaimerLabel]];
    NSLog(@"%@, %@", _disclaimerLabel, NSStringFromCGRect(_disclaimerLabel.frame));
    //Set the frames for original and extended
    CGSize updatedFrame = [[self view] bounds].size;
    updatedFrame.height = 750;
    [self setOriginalSize:updatedFrame];
    
    CGSize newSize = [[self view] bounds].size;
    newSize.height = 800;
    [self setExtendedSize:newSize];
    
    [[self joinButton] setTitle:NSLocalizedString(@"join_button_text", nil) forState:UIControlStateNormal];
    [[self joinWithFacebookButton] setTitle:NSLocalizedString(@"join_facebook_text", nil) forState:UIControlStateNormal];
    [[self fadingView] setImage:[UIImage imageNamed:@"cover_1.jpg"]];
    
    [[self scroll] setContentSize:[self originalSize]];
    
    
    
    
    if ([self animatedCover]) {
        [self animatePhotos];
    }
}
-(void) viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.profilePictureButton.layer.cornerRadius = self.profilePictureButton.bounds.size.height/2.0f;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)disableJoinButtons
{
    [[self joinButton] setEnabled:NO];
    [[self joinWithFacebookButton] setEnabled:NO];
}

- (void)enableJoinButtons
{
    [[self joinWithFacebookButton] setEnabled:YES];
    [[self joinButton] setEnabled:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - NSNotification Methods

- (void)keyboardDidHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.scroll setContentOffset:CGPointZero];
    } completion:^(BOOL finished) {
        [[self scroll] setContentSize:[self originalSize]];
    }];
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    //change the frame and scroll
    [[self scroll] setContentSize:[self extendedSize]];
}

#pragma mark - Validation Methods

- (void)validateInputs:(NSError *__autoreleasing *)error
{
    //check for the inputs
    NSString *nameText = [[self nameField] text];
    NSString *emailText = [[self emailField] text];
    NSString *usernameText  = [[self usernameField] text];
    NSString *passwordText = [[self passwordField] text];
    
    NSString *stringError;
    
    BOOL foundErrors = NO;
    if ([NSString isEmpty:nameText])
    {
        foundErrors = YES;
        stringError = NSLocalizedString(@"name_field_error",nil);
    }else if (![NSString isValidEmail:emailText])
    {
        foundErrors = YES;
        stringError = NSLocalizedString(@"email_field_error", nil);
    }else if (![NSString isValidUsername:usernameText])
    {
        foundErrors = YES;
        stringError = NSLocalizedString(@"username_field_error", nil);
    }else if ([NSString isEmpty:passwordText] || passwordText.length < 6)
    {
        foundErrors = YES;
        stringError = NSLocalizedString(@"password_field_error", nil);
    }
    
    if (foundErrors)
    {
        NSDictionary *userInfo = @{
                                   NSLocalizedDescriptionKey:NSLocalizedString(@"user_registration_error_description", nil),
                                   NSLocalizedRecoverySuggestionErrorKey:stringError
                                   };
        *error = [NSError errorWithDomain:NLErrorDomain code:NLUserRegistrationError userInfo:userInfo];
    }
    
}
#pragma mark - IBActions

- (IBAction)coverPhotoTapped:(id)sender
{
    [self setAvoidScrollResize:YES];
    [[self currentTextfield] resignFirstResponder];
    self.currentMode = PhotoSelectionModeCover;
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"upload_cover_title", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel_option", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"open_gallery", nil),NSLocalizedString(@"open_camera", nil), nil] ;
    
    if ([[self scroll] contentSize].height == self.extendedSize.height)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [sheet showInView:self.view];
        });
    }else
    {
        [sheet showInView:self.view];
    }
    
}

- (IBAction)profilePictureTapped:(id)sender
{
    [self setAvoidScrollResize:YES];
    self.currentMode = PhotoSelectionModeProfile;
    [[self currentTextfield] resignFirstResponder];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"upload_avatar_title",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel_option", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"open_gallery", nil),NSLocalizedString(@"open_camera", nil), nil] ;
    [sheet showInView:self.view];
    
}

- (IBAction)joinAction:(id)sender
{
    [self showLoadingSpinner];
    [self setCurrentSize:[self originalSize]];
    [[self currentTextfield] resignFirstResponder];
    
    [self disableJoinButtons];
    
    NSError *validationError;
    
    [self validateInputs:&validationError];
    if (validationError)
    {
        [self showErrorMessage:[[validationError userInfo] objectForKey:NSLocalizedRecoverySuggestionErrorKey] withDuration:2.0];
        [self enableJoinButtons];
        [self hideLoadingSpinner];
    }else
    {
        
        __weak id weakSelf = self;
        
        [NLUser registerUserWithEmail:[[self emailField] text] name:[[self nameField] text] username:[[self usernameField] text] activity:self.user_venue password:[[self passwordField] text] coverImage:self.coverPhoto profileImage:self.profileImage withCompletionBlock:^(NSError * error,NLUser *user)
         {
             if (!error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [weakSelf enableJoinButtons];
                     [weakSelf hideLoadingSpinner];
                     user.newRegister = YES;
                     [NLSession createUser:user];
                     [[NSNotificationCenter defaultCenter] postNotificationName:kNLLoginNotification object:weakSelf];
                     
                 });
                 
                 NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                 [def setBool:true forKey:@"registro"];
                 [def synchronize];
                 
             }else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSString *message = [self errorToString:error];
                     [weakSelf hideLoadingSpinner];
                     [weakSelf showErrorMessage:message withDuration:2.0f];
                     [weakSelf enableJoinButtons];
                 });
             }
         }];
    }
}

- (IBAction)changeActivity:(UIButton *)sender
{
    self.user_venue = sender.tag;
    if (sender.tag == 0)
    {
        self.userActivityButton.backgroundColor = [UIColor nl_applicationRedColor];
        self.venueActivityButton.backgroundColor = [UIColor nl_applicationLightGrayColor];
    }
    else
    {
        self.venueActivityButton.backgroundColor = [UIColor nl_applicationRedColor];
        self.userActivityButton.backgroundColor = [UIColor nl_applicationLightGrayColor];
    }
}


- (IBAction)joinWithFacebookAction:(id)sender
{
    [[self currentTextfield] resignFirstResponder];
    [self setCurrentSize:[self originalSize]];
    [self disableJoinButtons];
    [self showLoadingSpinner];
    
    
    __weak NLRegisterViewController *weakSelf = self;
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (error)
         {
             NSLog(@"Process error %@", error);
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
             [weakSelf hideLoadingSpinner];
             [weakSelf enableJoinButtons];
             
         }
         else
         {
             NSLog(@"Logged in %@", result);
             if ([FBSDKAccessToken currentAccessToken])
             {
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                  {
                      if (!error)
                      {
                          NSLog(@"resultis:%@",result);
                          NSLog(@"Token is available : %@", [[FBSDKAccessToken currentAccessToken] tokenString]);
                          if ([result objectForKey:@"email"])
                          {
                              [self continueFBSignUp:[result objectForKey:@"email"] token:[[FBSDKAccessToken currentAccessToken] tokenString]];
                          }
                          else
                          {
                              [self continueFBSignUp:@"" token:[[FBSDKAccessToken currentAccessToken] tokenString]];
                          }
                      }
                      else
                      {
                          NSLog(@"Error %@",error);
                      }
                  }];
             }
         }
     }];
    
    
}
-(void)sendEmail:(NSString *)email token:(NSString *)token
{
    NSLog(@"email = %@\n token = %@", email, token);
    __weak NLRegisterViewController *weakSelf = self;
    [NLUser registerUserWithFacebookToken:token email:email activity:self.user_venue withCompletionBlock:^(NSError *error, NLUser *user)
     {
         if (!error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                [weakSelf enableJoinButtons];
                                [weakSelf hideLoadingSpinner];
                                user.newRegister = YES;
                                [NLSession createUser:user];
                                [[NSNotificationCenter defaultCenter] postNotificationName:kNLLoginNotification object:weakSelf];
                                
                                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                                [def setBool:true forKey:@"registro"];
                                [def synchronize];
                            });
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString *message = [self errorToString:error];
                 [weakSelf hideLoadingSpinner];
                 [weakSelf showErrorMessage:message withDuration:2.0f];
                 [weakSelf enableJoinButtons];
             });
         }
     }];
}
-(void)continueFBSignUp:(NSString *)email token:(NSString *)token
{
    __weak NLRegisterViewController *weakSelf = self;
    
    [NLUser registerUserWithFacebookToken:token email:email activity:self.user_venue withCompletionBlock:^(NSError *error, NLUser *user)
     {
         if (!error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                [weakSelf enableJoinButtons];
                                [weakSelf hideLoadingSpinner];
                                user.newRegister = YES;
                                [NLSession createUser:user];
                                [[NSNotificationCenter defaultCenter] postNotificationName:kNLLoginNotification object:weakSelf];
                                
                                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                                [def setBool:true forKey:@"registro"];
                                [def synchronize];
                            });
         }
         else if([email isEqualToString:@""])
         {
             NLEmailViewController *emailVC = [[NLEmailViewController alloc] init];
             emailVC.delegate = self;
             emailVC.token = token;
             [self.navigationController pushViewController:emailVC animated:true];
             
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString *message = [self errorToString:error];
                 [weakSelf hideLoadingSpinner];
                 [weakSelf showErrorMessage:message withDuration:2.0f];
                 [weakSelf enableJoinButtons];
             });
         }
     }];
}
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setCurrentTextfield:textField];
    [self setCurrentSize:[self extendedSize]];
    [[self scroll] setContentOffset:CGPointMake(0, [[self coverPhotoButton] bounds].size.height+[[self coverPhotoButton] frame].origin.y) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //Check the limit of every field
    if (textField == [self nameField])
    {
        if ([[[self nameField] text] length]>140 && [string length]>0)
        {
            return NO;
        }
    }
    
    if (textField == [self emailField])
    {
        
        
        if (([[[self emailField] text] length]>254 && [string length]>0) || [string isEqualToString:@" "])
        {
            return NO;
        }
    }
    
    if (textField == self.usernameField)
    {
        if ([[[self usernameField] text] length]>30 && [string length]>0)
        {
            return NO;
        }
    }
    
    if (textField == self.passwordField)
    {
        if ([[[self passwordField] text] length]>50 && [string length]>0)
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == [self nameField])
    {
        [[self emailField] becomeFirstResponder];
    }else if (textField == [self emailField])
    {
        [[self usernameField] becomeFirstResponder];
    }else if (textField ==[self usernameField])
    {
        [[self passwordField] becomeFirstResponder];
    }else if ([self passwordField])
    {
        [self setCurrentSize:[self originalSize]];
        [[self currentTextfield] resignFirstResponder];
        [self joinAction:nil];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}
#pragma mark - TTTAttributedLabel

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [self setAvoidScrollResize:YES];
    NLTermsPolicyViewController *termsVC = [[NLTermsPolicyViewController alloc] initWithNibName:NSStringFromClass([NLTermsPolicyViewController class]) bundle:nil andURL:url];
    [self presentViewController:termsVC animated:YES completion:nil];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat dimensions = rootView.frame.size.width;

    switch (buttonIndex) {
        case 0:
        {
            DBCameraLibraryViewController *libraryVC = [[DBCameraLibraryViewController alloc] init];
            [libraryVC setDelegate:self];
            [libraryVC setUseCameraSegue:YES];
            
            if ([self currentMode] == PhotoSelectionModeProfile)
            {
                [libraryVC setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions, dimensions);
                 }];
            }else
            {
                [libraryVC setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions, dimensions*0.625);
                     
                 }];
            }
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:libraryVC];
            [navController setNavigationBarHidden:YES];
            [self presentViewController:navController animated:YES completion:nil];
        }
            break;
        case 1:
        {
            DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
            
            DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self cameraSettingsBlock:^(DBCameraView *cameraView, id container) {
                cameraView.flashButton.hidden = YES;
                cameraView.gridButton.hidden = YES;
                cameraView.photoLibraryButton.hidden = YES;
            }];
            
            
            if ([self currentMode] == PhotoSelectionModeProfile)
            {
                [cameraController setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions, dimensions);
                     segue.filtersView = nil;
                 }];
                [cameraController setForceQuadCrop:YES];
                
            }else
            {
                [cameraController setUseCameraSegue:YES];
                [cameraController setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions, dimensions*0.625);
                 }];
            }
            [container setCameraViewController:cameraController];
            [container setFullScreenMode];
            
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:container];
            [nav setNavigationBarHidden:YES];
            [self presentViewController:nav animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
}

#pragma mark - DBCamera Delegate

- (void)dismissCamera:(id)cameraViewController
{
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    if (self.currentMode == PhotoSelectionModeProfile)
    {
        self.profileImage = image;
        [self.profilePictureButton setBackgroundImage:self.profileImage forState:UIControlStateNormal];
        [[self profilePictureButton] setTitle:nil forState:UIControlStateNormal];
        [[self profilePictureButton] setImage:nil forState:UIControlStateNormal];
        [self.profileImageLabel setHidden:true];
        [self.profileCameraIcon setHidden:true];
    }else
    {
        self.coverPhoto = image;
        [self.coverPhotoButton setBackgroundImage:self.coverPhoto forState:UIControlStateNormal];
        [[self coverPhotoButton] setTitle:nil forState:UIControlStateNormal];
        [[self coverPhotoButton] setImage:nil forState:UIControlStateNormal];
        [[self camerIcon] removeFromSuperview];
        [self.coverPhotoLabel setHidden:true];
        [self.gradientLayer removeFromSuperlayer];
        
        [self setAnimatedCover:NO];
    }
    
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)animatePhotos
{
    if ([self animatedCover]) {
        static NSInteger index = 0;
        [UIView transitionWithView:[self fadingView] duration:1.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            self.fadingView.image = [[self animationPhotos] objectAtIndex:index];
        } completion:^(BOOL finished) {
            index = (index+1<[self animationPhotos].count)?(index+1):0;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self animatePhotos];
            });
        }];
    }
}
@end
