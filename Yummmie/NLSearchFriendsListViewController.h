//
//  NLSearchFriendsListViewController.h
//  Yummmie
//
//  Created by bot on 12/18/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

typedef NS_ENUM(NSInteger, FriendSocialList) {
    TwitterList,
    FacebookList,
    ContactList
};

@interface NLSearchFriendsListViewController : NLBaseViewController

- (instancetype)initWithSocialListType:(FriendSocialList)listType;


@end
