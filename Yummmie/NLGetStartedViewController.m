//
//  NLGetStartedViewController.m
//  Yummmie
//
//  Created by bot on 6/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLGetStartedViewController.h"
#import "NLLoginViewController.h"
#import "NLRegisterViewController.h"
#import "UIColor+NLColor.h"


@interface NLGetStartedViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;

- (IBAction)logInAction:(id)sender;
- (IBAction)joinAction:(id)sender;


@end

@implementation NLGetStartedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self addRedBackground];    
    [self.joinButton setTitle:NSLocalizedString(@"join_text", nil) forState:UIControlStateNormal];
    [self.logInButton setTitle:NSLocalizedString(@"login_text", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES];
    [[[UIApplication sharedApplication] keyWindow] setBackgroundColor:[UIColor colorWithRed:205.0/255.0 green:36.0f/255.0f blue:49.0/255.0 alpha:1.0]];
    
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"Get Started View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.verticalSpaceConstraint.constant = (screenRect.size.height>480.0f)?100.0f:70.0f;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - IBActions
- (IBAction)logInAction:(id)sender
{
    UINavigationController *navController = [self navigationController];
    NLLoginViewController *loginVC = [[NLLoginViewController alloc] initWithNibName:NSStringFromClass([NLLoginViewController class]) bundle:nil];

    [navController pushViewController:loginVC animated:YES];
}

- (IBAction)joinAction:(id)sender
{
    UINavigationController *navController = [self navigationController];
    NLRegisterViewController *registerVC = [[NLRegisterViewController alloc] initWithNibName:NSStringFromClass([NLRegisterViewController class]) bundle:nil];

    [navController pushViewController:registerVC animated:YES];
}
@end
