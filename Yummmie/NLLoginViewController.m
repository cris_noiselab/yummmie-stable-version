//
//  NLLoginViewController.m
//  Yummmie
//
//  Created by bot on 6/9/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLLoginViewController.h"
#import "NLScrollView.h"
#import "NSString+NLStringValidations.h"
#import "NLError.h"
#import "NLNotifications.h"
#import "NLUser.h"
#import "NLSession.h"
#import "NLBrowserViewController.h"
#import "NLNavigationViewController.h"
#import "NLRecoverPasswordViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h> 

const CGFloat kForgotEmailLabelTopPadding = 50.0f;
const CGFloat kForgotEmailLabelBottomPadding = 30.0f;
const CGFloat kForgotEmailLabelWidth = 250.0f;
const CGFloat kForgotEmailLabelHeight = 50.0f;

@interface NLLoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *facebookLoginButton;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet NLScrollView *scroll;

@property (weak, nonatomic) UITextField *currentTextField;
@property (strong, nonatomic) TTTAttributedLabel *forgotPasswordLabel;


@property (nonatomic) CGSize originalSize;
@property (nonatomic) CGSize extendedSize;


- (IBAction)loginAction:(id)sender;
- (IBAction)facebookLoginAction:(id)sender;

- (void)keyboardDidHide:(NSNotification *)notification;
- (void)validateInputs:(NSError **)error;


@end

@implementation NLLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		self.title = NSLocalizedString(@"login_text", nil);
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
	}
	return self;
}

- (void)viewDidLoad
{
	
	[super viewDidLoad];
	[[[self navigationController] navigationBar] setTranslucent:NO];
	[[[self navigationController] navigationBar] setOpaque:YES];
	
	[self addRedBackground];
	
	//Setup sizes
	self.originalSize = self.view.bounds.size;
	
	CGSize newSize = self.view.bounds.size;
	newSize.height*=1.3f;
	self.extendedSize = newSize;
	
	//setup buttons
	[self.loginButton setTitle:NSLocalizedString(@"login_text", nil) forState:UIControlStateNormal];
	[self.facebookLoginButton setTitle:NSLocalizedString(@"login_facebook_text", nil) forState:UIControlStateNormal];
	
	//setup textfields
	self.usernameField.placeholder = NSLocalizedString(@"join_email_text", nil);
	self.passwordField.placeholder = NSLocalizedString(@"join_password_text", nil);
	
	self.usernameField.delegate = self;
	self.passwordField.delegate = self;
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]
                        rootViewController].view;
	
	//self.forgotPasswordLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(35.0f, rootView.frame.size.height*0.833, kForgotEmailLabelWidth, kForgotEmailLabelHeight)];
    self.forgotPasswordLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(35.0f, rootView.frame.size.height*0.610, kForgotEmailLabelWidth, kForgotEmailLabelHeight)];
    [self.forgotPasswordLabel setCenter:CGPointMake(rootView.center.x, self.forgotPasswordLabel.center.y)];
    
    NSLog(@"rottview width: %f  height: %f",rootView.frame.size.width,rootView.frame.size.height);
    NSLog(@"rootView height: %f y: %f",rootView.frame.size.height,rootView.frame.size.height*0.833);
    NSLog(@"centro de forgot... %f ,  %f",self.forgotPasswordLabel.center.x,self.forgotPasswordLabel.center.y);
	
	[[self forgotPasswordLabel] setTextAlignment:NSTextAlignmentCenter];
	[[self forgotPasswordLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]];
	[[self forgotPasswordLabel] setNumberOfLines:2];
	[[self forgotPasswordLabel] setTextColor:[UIColor whiteColor]];
	
	NSDictionary *linkAttributes = @{
																	 (id)kCTForegroundColorAttributeName:[UIColor whiteColor],
																	 (id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInteger:kCTUnderlineStyleNone]
																	 };
	[[self forgotPasswordLabel] setLinkAttributes:linkAttributes];
	
	NSDictionary *activeLinkAttributes = @{
																				 (id)kCTForegroundColorAttributeName: [UIColor lightGrayColor],
																				 (id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInteger:kCTUnderlineStyleNone]
																				 };
	[[self forgotPasswordLabel] setActiveLinkAttributes:activeLinkAttributes];
	[[self forgotPasswordLabel] setDelegate:self];
	NSString *forgotPasswordText = NSLocalizedString(@"forgot_password", nil);
	NSRange linkRange = NSMakeRange(0, forgotPasswordText.length);
	
	[[self forgotPasswordLabel] setText:forgotPasswordText afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
		
		UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
		CTFontRef fontRef = CTFontCreateWithName((__bridge CFStringRef)boldFont.fontName, boldFont.pointSize, NULL);
		
		if (fontRef)
		{
			[mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)fontRef range:linkRange];
			CFRelease(fontRef);
		}
		return mutableAttributedString;
	}];
	
	[[self forgotPasswordLabel] addLinkToURL:[NSURL URLWithString:@"http://yummmie.com/password_resets/new"] withRange:linkRange];
	
	[[self contentView] addSubview:[self forgotPasswordLabel]];
    [[self scroll] setScrollEnabled: NO];
	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:NO];
	
	//Google Analytics
	NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
   
    
}
- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	
}

#pragma mark - Validation Methods

- (void)validateInputs:(NSError *__autoreleasing *)error
{
	//check for the inputs
	NSString *usernameText  = [[self usernameField] text];
	NSString *passwordText = [[self passwordField] text];
	
	NSMutableString *stringError = [NSMutableString string];
	
	BOOL foundErrors = NO;
	
	if (![NSString isValidEmail:usernameText])
	{
		[stringError appendString:NSLocalizedString(@"email_field_error", nil)];
		foundErrors = YES;
	}else if ([NSString isEmpty:passwordText])
	{
		[stringError appendString:NSLocalizedString(@"login_password_field_error", nil)];
		foundErrors = YES;
	}
	
	if (foundErrors)
	{
		NSDictionary *userInfo = @{
															 NSLocalizedDescriptionKey:NSLocalizedString(@"user_login_error_description", nil),
															 NSLocalizedRecoverySuggestionErrorKey:stringError
															 };
		*error = [[NSError alloc] initWithDomain:NLErrorDomain code:NLUserLoginError userInfo:userInfo];
	}
}

#pragma mark - Actions
- (IBAction)loginAction:(id)sender
{
	[[self currentTextField] resignFirstResponder];
	NSError *validationError;
	[self validateInputs:&validationError];
	if (validationError)
	{
		[self showErrorMessage:[[validationError userInfo] objectForKey:NSLocalizedRecoverySuggestionErrorKey] withDuration:1.0f];
	}else
	{
		[self showLoadingSpinner];
		[[self loginButton] setEnabled:NO];
		[[self facebookLoginButton] setEnabled:NO];
		
		[NLUser loginWithEmail:[[self usernameField] text] password:[[self passwordField] text] withCompletionBlock:^(NSError *error, NLUser *user) {
			
			__weak id weakSelf = self;
			if (!error)
			{
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf hideLoadingSpinner];
					NSLog(@"EL usuario %@",user);
					[NLSession createUser:user];
					[[NSNotificationCenter defaultCenter] postNotificationName:kNLLoginNotification object:weakSelf];
					
				});
			}else
			{
				id message = [error.userInfo objectForKey:JSONResponseSerializerWithDataKey];
				dispatch_async(dispatch_get_main_queue(), ^{
					
					[[weakSelf passwordField] setText:@""];
					[weakSelf hideLoadingSpinner];
					if (message)
					{
						
						[weakSelf showErrorMessage:NSLocalizedString(@"login_user_error" , nil) withDuration:2.0f];
					}else
					{
						[weakSelf showErrorMessage:NSLocalizedString(@"connection_error", nil) withDuration:2.0f];
					}
					[[weakSelf loginButton] setEnabled:YES];
					[[weakSelf facebookLoginButton] setEnabled:YES];
				});
			}
            if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
                
            } else {
                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                [loginManager logInWithPublishPermissions:@[@"publish_actions"]
                                       fromViewController:self
                                                  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                                      //TODO: process error or result.
                                                  }];
                
            }

		}];
	}
}

- (IBAction)facebookLoginAction:(id)sender {
	
	[self showLoadingSpinner];
	[[self loginButton] setEnabled:NO];
	[[self facebookLoginButton] setEnabled:NO];
	
	__weak NLLoginViewController *weakSelf = self;
	
	
	
	FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
	[login
	 logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
	 fromViewController:self
	 handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
	 {
		 if (error)
		 {
			 NSLog(@"Process error %@", error);
			 [weakSelf hideLoadingSpinner];
			 [weakSelf showErrorMessage:error.localizedDescription withDuration:0.3];
			 [weakSelf enableLoginButtons];
		 }
		 else if (result.isCancelled)
		 {
			 [weakSelf hideLoadingSpinner];
			 [weakSelf enableLoginButtons];
		 }
		 else
		 {
			 if ([FBSDKAccessToken currentAccessToken])
			 {
				 NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken] tokenString]);
				 [NLUser loginWithFacebookToken:[[FBSDKAccessToken currentAccessToken] tokenString] withCompletionBlock:^(NSError *error, NLUser *user)
					{
						if (!error)
						{
							[weakSelf hideLoadingSpinner];
							NSLog(@"EL usuario %@",user);
							[NLSession createUser:user];
							[[NSNotificationCenter defaultCenter] postNotificationName:kNLLoginNotification object:weakSelf];
						}
						else
						{
							[weakSelf showErrorMessage:NSLocalizedString(@"login_facebook_user_not_found", nil) withDuration:3.0f];
						}
					}];
				}
		 }
  }];
}
- (void)enableLoginButtons
{
	[[self facebookLoginButton] setEnabled:YES];
	[[self loginButton] setEnabled:YES];
}
#pragma mark - NSNotification Methods

- (void)keyboardDidHide:(NSNotification *)notification
{
	[UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
		[self.scroll setContentOffset:CGPointZero];
	} completion:^(BOOL finished) {
		[[self scroll] setContentSize:[self originalSize]];
	}];
}
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	self.currentTextField = textField;
	[[self scroll] setContentSize:[self extendedSize]];
	[[self scroll] setContentOffset:CGPointMake(0.0f,self.usernameField.frame.origin.y) animated:YES];
	
	if (self.currentTextField == self.passwordField)
	{
		//trim the string in email field
		if (self.usernameField.text.length > 0)
		{
			NSString *trimmedText = [[self.usernameField text] stringByReplacingOccurrencesOfString:@" " withString:@""];
			self.usernameField.text = trimmedText;
		}
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	
	//Check the limit of every field
	if (textField == [self usernameField])
	{
		if (([[[self usernameField] text] length]>254 && [string length]>0) || [string isEqualToString:@" "])
		{
			return NO;
		}
	}
	
	if (textField == [self passwordField])
	{
		if ([[[self passwordField] text] length]>254 && [string length]>0 )
		{
			return NO;
		}
	}
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if (textField == [self usernameField])
	{
		[[self passwordField] becomeFirstResponder];
	}else
	{
		[[self currentTextField] resignFirstResponder];
		[self showLoadingSpinner];
		[self loginAction:nil];
	}
	return YES;
}

#pragma mark - TTTAttributedLabel

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
	NLRecoverPasswordViewController *recoverVC = [[NLRecoverPasswordViewController alloc] initWithNibName:NSStringFromClass([NLRecoverPasswordViewController class]) bundle:nil];
	[[self navigationController] pushViewController:recoverVC animated:YES];
}

@end
