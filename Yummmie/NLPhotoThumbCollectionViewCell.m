//
//  NLPhotoThumbCollectionViewCell.m
//  Yummmie
//
//  Created by bot on 6/23/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLPhotoThumbCollectionViewCell.h"

@interface NLPhotoThumbCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIView *highlightView;

@end

@implementation NLPhotoThumbCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)highlight
{
    [[self highlightView] setHidden:NO];
}

- (void)unHighlight
{
    [[self highlightView] setHidden:YES];
}
@end
