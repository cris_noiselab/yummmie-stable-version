//
//  NLTableViewController.h
//  Yummmie
//
//  Created by bot on 6/16/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLTableViewController : UITableViewController

@property(nonatomic, readwrite, getter = barShouldScroll) BOOL scrollableBar;

- (id)initWithScrollableBar:(BOOL)scrollable;

- (void)addRedBackground;
- (void)showErrorMessage:(NSString *)message withDuration:(float)seconds;
- (void)showErrorMessage:(NSString *)message popViewController:(BOOL)pop withDuration:(float)seconds;
- (void)updateBarButtonItems:(CGFloat)alpha;
- (void)showLoadingSpinner;
- (void)hideLoadingSpinner;

- (void)scrollToTop;

@end