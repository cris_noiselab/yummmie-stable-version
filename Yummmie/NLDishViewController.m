//
//  NLDishViewController.m
//  Yummmie
//
//  Created by bot on 7/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import "NLDishViewController.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLPhotoDetailViewController.h"
#import "NLDishFooter.h"
#import "NLConstants.h"
#import "NLYum.h"

static NSString * const kPhotoDishesViewCellIdentifier = @"kPhotoDishesViewCellIdentifier";
static NSString * const kDishFooterIdentifier = @"NLDishesFooterIdentifier";

@interface NLDishViewController ()<NLPhotoDetailViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *yummieLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *dishesCollection;
@property (nonatomic, getter = isLoading) BOOL loading;
@property (weak, nonatomic) NLDishFooter *footer;
@property (copy, nonatomic) NSString *dishName;
@property (strong, nonatomic) NSMutableArray *yums;

- (void)loadMorePhotos:(NLDishFooter *)footer;
- (void)loadIfNeeded:(UIScrollView *)scrollView;

@end

@implementation NLDishViewController
{
	NSInteger currentDishPage;
	NSInteger totalDishPages;
	NSString *service;
}

- (id)initWithDishName:(NSString *)dishName
{
	
    if (self = [super initWithNibName:NSStringFromClass([NLDishViewController class]) bundle:nil])
    {
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _dishName = dishName;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kPhotoDishesViewCellIdentifier];
    [[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLDishFooter class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kDishFooterIdentifier];
    [[self dishesCollection] setDelegate:self];
    [[self dishesCollection] setDataSource:self];
    
    self.yummieLabel.text = self.dishName;
    
    if ([self dishName])
    {
        [self loadPhotos:[self footer]];
    }
	currentDishPage = 1;
	totalDishPages = 10;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];
    CGRect frame = self.navigationController.navigationBar.frame;
    frame.origin.y = 20.0f;
    [self.navigationController.navigationBar setFrame:frame];
    self.navigationItem.titleView.alpha = 1.0;
    
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"Dish Collection View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = logoView;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load More Dishes

- (void)loadPhotos:(NLDishFooter *)footer
{
    __weak NLDishViewController *weakSelf = self;
    
    if (![self isLoading])
    {
        [self setLoading:YES];
        [[footer spinner] startAnimating];
			currentDishPage = 1;
			[NLYum searchYum:[self dishName] withYums:true page:currentDishPage withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages)
			{
					
					if (!error)
            {
                if ([yums count] > 0)
                {
                    
                    if (![weakSelf yums])
                    {
                        [weakSelf setYums:[NSMutableArray array]];
                    }
                    
                    NSMutableArray *indexPaths = [NSMutableArray array];
                    NSInteger lastIndex = [[weakSelf yums] count];
                    
                    for (NSInteger index = 0; index < [yums count]; index++)
                    {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:lastIndex+index inSection:0];
                        [indexPaths addObject:indexPath];
                    }
                    
                    [[[weakSelf footer] spinner] stopAnimating];
                    [[weakSelf dishesCollection] performBatchUpdates:^{
                        [[weakSelf yums] addObjectsFromArray:yums];
                        [[weakSelf dishesCollection] insertItemsAtIndexPaths:indexPaths];
                    } completion:^(BOOL finished) {
                        [weakSelf setLoading:NO];
                    }];
                }
								else
                {
                    CGFloat total = weakSelf.dishesCollection.contentOffset.y+weakSelf.dishesCollection.frame.size.height;
                    if (total > weakSelf.dishesCollection.contentSize.height)
                    {
                        CGPoint newPoint = CGPointMake(0.0f, footer.frame.origin.y-weakSelf.dishesCollection.frame.size.height+kTabBarHeight+kDishCollectionViewBottomInset);
                        [[weakSelf dishesCollection] setContentOffset:newPoint animated:YES];
                    }
                    [weakSelf setLoading:NO];
                    [[[weakSelf footer] spinner] stopAnimating];
                }
            }else
            {
                [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:2.0f];
                [weakSelf setLoading:NO];
                [[[weakSelf footer] spinner] stopAnimating];
            }
        }];
    }
}
- (void)loadMorePhotos:(NLDishFooter *)footer
{
	__weak NLDishViewController *weakSelf = self;
	
	if (![self isLoading])
	{
		[self setLoading:YES];
		[[footer spinner] startAnimating];
		currentDishPage++;
		[NLYum searchYum:[self dishName] withYums:true page:currentDishPage withCompletionBlock:^(NSError *error, NSArray *yums, NSInteger currentPage, NSInteger totalPages)
		{
			
			if (!error)
			{
				if ([yums count] > 0)
				{
					currentDishPage = currentPage;
					totalDishPages = totalPages;
					if (![weakSelf yums])
					{
						[weakSelf setYums:[NSMutableArray array]];
					}
					
					NSMutableArray *indexPaths = [NSMutableArray array];
					NSInteger lastIndex = [[weakSelf yums] count];
					
					for (NSInteger index = 0; index < [yums count]; index++)
					{
						NSIndexPath *indexPath = [NSIndexPath indexPathForItem:lastIndex+index inSection:0];
						[indexPaths addObject:indexPath];
					}
					
					[[[weakSelf footer] spinner] stopAnimating];
					[[weakSelf dishesCollection] performBatchUpdates:^{
						[[weakSelf yums] addObjectsFromArray:yums];
						[[weakSelf dishesCollection] insertItemsAtIndexPaths:indexPaths];
					} completion:^(BOOL finished)
					{
						[weakSelf setLoading:NO];
					}];
				}
				else
				{
					CGFloat total = weakSelf.dishesCollection.contentOffset.y+weakSelf.dishesCollection.frame.size.height;
					if (total > weakSelf.dishesCollection.contentSize.height)
					{
						CGPoint newPoint = CGPointMake(0.0f, footer.frame.origin.y-weakSelf.dishesCollection.frame.size.height+kTabBarHeight+kDishCollectionViewBottomInset);
						[[weakSelf dishesCollection] setContentOffset:newPoint animated:YES];
					}
					[weakSelf setLoading:NO];
					[[[weakSelf footer] spinner] stopAnimating];
				}
			}else
			{
				[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:2.0f];
				[weakSelf setLoading:NO];
				[[[weakSelf footer] spinner] stopAnimating];
			}
		}];
	}
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self yums] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
    NLPhotoThumbCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoDishesViewCellIdentifier forIndexPath:indexPath];
    [[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yum image]] placeholderImage:nil];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (UICollectionElementKindSectionFooter == kind)
    {
        NLDishFooter *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kDishFooterIdentifier forIndexPath:indexPath];
        self.footer = footer;
        return footer;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
    NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
    [detail setDelegate:self];
    [[self navigationController] pushViewController:detail animated:YES];
    
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell highlight];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell unHighlight];
}
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row > self.yums.count - 2)
	{
		[self loadMorePhotos:[self footer]];
	}
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width - 10) / 3, (self.view.frame.size.width - 10) / 3);
}
#pragma mark - Start Loading Helpers

- (void)loadIfNeeded:(UIScrollView *)scrollView
{
    CGFloat contentSizeHeight = scrollView.contentSize.height-200;
    CGFloat currentPosition = scrollView.contentOffset.y+scrollView.frame.size.height;
    
    if (currentPosition > contentSizeHeight)
    {
			//[self loadMorePhotos:self.footer];
    }
}

#pragma mark - UIScrollViewDelegate 

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	//[self loadIfNeeded:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{

	//[self loadIfNeeded:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	//[self loadIfNeeded:scrollView];
}

#pragma mark - NLPhotoDetailViewController

- (void)yumDeleted:(NLYum *)deletedYum
{
    NSUInteger deletedIndex = [self.yums indexOfObject:deletedYum];
    
    NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
    [[self dishesCollection] performBatchUpdates:^{
        [[self yums] removeObjectAtIndex:deletedIndex];
        [[self dishesCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
    } completion:^(BOOL finished)
     {
         
     }];
}

@end
