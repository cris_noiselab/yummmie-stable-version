//
//  NLInteractionsPopUpView.m
//  Yummmie
//
//  Created by bot on 3/31/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

static const CGFloat kNLInteractionsPopUpViewWidthWithThree = 127.f;
static const CGFloat kNLInteractionsPopUpViewWidthWithTwo = 88;
static const CGFloat kNLInteractionsPopUpViewWidthWithOne = 48.0f;
static const CGFloat kNLInteractionsPopUpViewHeight = 34.f;

static const CGFloat kNLInteractionPaddingLeft = 7.0f;
static const CGFloat kNLInteractionPaddingTop = 6.0f;

static const CGFloat kNLInteractionItemSeparation = 2.5f;
static const CGFloat kNLInteractionItemHeight =  17.0f;
static const CGFloat kNLInteractionItemWidth =  17.0f;


#import "NLInteractionsPopUpView.h"

@interface NLInteractionsPopUpView ()

@property (strong, nonatomic) NSMutableArray *imagePaths;
@property (strong, nonatomic) NSMutableArray *numbers;


- (void)setupIconsAndLabelsWithSegments:(NSInteger)segments;

@end

@implementation NLInteractionsPopUpView

- (instancetype)initWithNumberOfLikes:(NSInteger)likes numberOfComments:(NSInteger)comments andNumberOfFollowers:(NSInteger)followers
{
    if (self = [super init]) {
        
        CGRect frame = CGRectZero;
        NSString *imgPath;
        NSInteger numberOfSegments = 0;
        
        if (likes && comments && followers) {
            frame.size = CGSizeMake(kNLInteractionsPopUpViewWidthWithThree, kNLInteractionsPopUpViewHeight);
            imgPath  = @"notif_bubble_3";
            numberOfSegments = 3;
        }else if ((likes && comments) || (likes && followers) || (comments && followers))
        {
            frame.size = CGSizeMake(kNLInteractionsPopUpViewWidthWithTwo, kNLInteractionsPopUpViewHeight);
            imgPath  = @"notif_bubble_2";
            numberOfSegments = 2;
        }else if (likes || comments || followers)
        {
            frame.size = CGSizeMake(kNLInteractionsPopUpViewWidthWithOne, kNLInteractionsPopUpViewHeight);
            imgPath  = @"notif_bubble_1";
            numberOfSegments = 1;
        }
        
        _imagePaths = [NSMutableArray array];
        _numbers = [NSMutableArray array];
        if (likes > 0) {
            [_numbers addObject:@(likes)];
            [_imagePaths addObject:@"notif_icon_likes"];
        }
        
        if (comments > 0) {
            [_numbers addObject:@(comments)];
            [_imagePaths addObject:@"notif_icon_comments"];
        }
        
        if (followers > 0) {
            [_numbers addObject:@(followers)];
            [_imagePaths addObject:@"notif_icon_followers"];
        }
        
        [self setFrame:frame];
        UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgPath]];
        [self addSubview:image];
    
        [self setupIconsAndLabelsWithSegments:numberOfSegments];
    }
    
    
    return self;
}

- (void)setupIconsAndLabelsWithSegments:(NSInteger)segments
{
    CGFloat originX = kNLInteractionPaddingLeft;
    CGFloat originY = kNLInteractionPaddingTop;
    
    
    for (NSInteger index = 0; index < segments; index++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(originX, originY, kNLInteractionItemWidth, kNLInteractionItemHeight)];
        
         [imageView setImage:[UIImage imageNamed:[[self imagePaths] objectAtIndex:index]]];
        [self addSubview:imageView];
        
        originX+=(imageView.bounds.size.width+kNLInteractionItemSeparation);
    
        static UIFont *labelFont;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            labelFont = [UIFont fontWithName:@"HelveticaNeue" size:12
                         ];
        });
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(originX, originY, kNLInteractionItemWidth, kNLInteractionItemHeight)];
        [label setFont:labelFont];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor:[UIColor whiteColor]];
        [label setMinimumScaleFactor:0.5];
        [label setAdjustsFontSizeToFitWidth:YES];
        [label setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [label setText:[NSString stringWithFormat:@"%ld",(long)[[[self numbers] objectAtIndex:index] integerValue]]];
        [self addSubview:label];
        
        originX+=(label.bounds.size.width+kNLInteractionItemSeparation);
    }
}

@end