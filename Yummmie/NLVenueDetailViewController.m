//
//  NLVenueDetailViewController.m
//  Yummmie
//
//  Created by bot on 5/15/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//


#import <SDWebImage/UIImageView+WebCache.h>
#import <Social/Social.h>
#import "NLVenueDetailViewController.h"

#import "NLVenue.h"
#import "UIColor+NLColor.h"
#import "NLVenueDetailHeaderView.h"
#import "NLYum.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLPhotoDetailViewController.h"
#import "NLLoadingReusableView.h"
#import "NLMapViewController.h"
#import "NLSession.h"
#import "NLNotifications.h"

static NSString * kNLVenueDetailCellIdentifier = @"kNLVenueDetailCellIdentifier";
static NSString * kNLVenueDetailHeaderIdentifier = @"kNLVenueDetailHeaderIdentifier";
static NSString * kNLVenueDetailFooterSpinner = @"kNLVenueDetailFooterSpinner";

@interface NLVenueDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,NLVenueDetailHeaderDelegate,NLPhotoDetailViewDelegate>

@property (nonatomic, strong) NLVenue *venue;

@property (weak, nonatomic) IBOutlet UICollectionView *dishesCollection;

@property (strong, nonatomic) NSMutableArray *yummmies;

@end

@implementation NLVenueDetailViewController
{
	NSInteger currentVenuePage;
	NSInteger totalVenuePages;
}
- (instancetype)initWithVenue:(NLVenue *)aVenue
{
    if (self = [super initWithNibName:NSStringFromClass([NLVenueDetailViewController class]) bundle:nil]) {
        _venue = aVenue;
    }
    
    return self;
}

- (void)dealloc
{
    [[self dishesCollection] setDelegate:nil];
    [[self dishesCollection] setDataSource:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
	NSLog(@"venue: %@", _venue);
    if ([self venue]) {
        
        [self.venue setNumberOfDishes:30];
        
        [[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil]forCellWithReuseIdentifier:kNLVenueDetailCellIdentifier];
        
        [[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLVenueDetailHeaderView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kNLVenueDetailHeaderIdentifier];
        
        [[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLLoadingReusableView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kNLVenueDetailFooterSpinner];
        
        [self.navigationItem setHidesBackButton:NO];
        
        if ([[self venue] numberOfDishes] == 0) {
            
        }else{
            [self setYummmies:[NSMutableArray array]];
        }
    }
	currentVenuePage = 1;
	totalVenuePages = 10;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect frame = self.navigationController.navigationBar.frame;
    
    if (frame.origin.y < 20.0f)
    {
        frame.origin.y = 20.0f;
        [self.navigationController.navigationBar setFrame:frame];
    }
    
    self.navigationItem.titleView.alpha  = 1.0;
    
    
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self yummmies] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kNLVenueDetailCellIdentifier forIndexPath:indexPath];
    NLYum *yummmie = [[self yummmies] objectAtIndex:[indexPath row]];
    
    [[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yummmie thumbImage]] placeholderImage:nil];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]){
        NLVenueDetailHeaderView *reusableView;
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kNLVenueDetailHeaderIdentifier forIndexPath:indexPath];
        [[reusableView venueUsernameLabel] setText:[NSString stringWithFormat:@"@%@",[[[self venue] name] stringByReplacingOccurrencesOfString:@" " withString:@""]]];
        [[reusableView venueNameLabel] setText:[[self venue] name]];
        [reusableView bioHeightConstraint].constant = 0;
        
        [reusableView setDelegate:self];
        
        if ([[self venue] venueType] == NLPrivateVenue) {
            
            [[reusableView phoneButton] setEnabled:NO];
            [[reusableView locationButton] setEnabled:NO];
            
        }
        return reusableView;
    }else
    {
        NLLoadingReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kNLVenueDetailFooterSpinner forIndexPath:indexPath];
        
        if ([[self yummmies] count] < [[self venue] numberOfDishes]) {
            [[reusableView spinner] startAnimating];
					currentVenuePage++;
            [NLYum loadYumsFromVenueId:[[self venue] venueId] page:currentVenuePage withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages)
						{
                
                if (!error)
								{
									currentVenuePage = currentPage;
									totalVenuePages = totalPages;
                    if (yums.count > 0) {
                        [self.yummmies addObjectsFromArray:yums];
											[[self dishesCollection] reloadData];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[reusableView spinner] stopAnimating];
                    });
                }else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:4.0f];
                        
                        [[reusableView spinner] stopAnimating];
                        
                        [[self dishesCollection] scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[[self yummmies] count]-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
                    });
                }
            }];
        }
        
        return reusableView;
    }
    

}

#pragma mark - UIColectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yummmie = [[self yummmies] objectAtIndex:[indexPath row]];
    
    NLPhotoDetailViewController *detailVC = [[NLPhotoDetailViewController alloc] initWithYumId:yummmie.yumId];
    [[self navigationController] pushViewController:detailVC animated:YES];
    [detailVC setDelegate:self];
}


#pragma mark - UICollectionViewFlowLayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(320, 300);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if ([[self yummmies] count] < [[self venue] numberOfDishes]) {
        screenSize.height = 35.0f;
    }else{
        screenSize.height = 0.0f;
    }
    
    return screenSize;
}

#pragma mark - NLPhotoDetailViewDelegate

- (void)yumDeleted:(NLYum *)deletedYum{
    
    NSUInteger deletedIndex = [[self yummmies] indexOfObject:deletedYum];
    
    NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
    //remove the element of the array and the update de colelction
    [[self dishesCollection] performBatchUpdates:^{
        [[self yummmies] removeObjectAtIndex:deletedIndex];
        [[self dishesCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
    } completion:^(BOOL finished)
     {
     }];
}

#pragma mark - NLVenueHeaderDelegate

- (void)callRestaurant{
    NSLog(@"llamar al restaurante");
    NSString *callURLString = @"tel://5523349304";
    NSURL *callURL = [NSURL URLWithString:callURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:callURL]) {
        [[UIApplication sharedApplication] openURL:callURL];
    }
}

- (void)openRestarantLocation{
    NSLog(@"Abrir ubicacion restaurante");
    NLMapViewController *mapVC = [[NLMapViewController alloc] initWithLocactionCoordinate:CLLocationCoordinate2DMake(self.venue.latitude, self.venue.longitude)];
    [[self navigationController] pushViewController:mapVC animated:YES];
}

- (void)shareRestaurantInfo{
    NSLog(@"Compartir redes");
    
    NSString *message = @"Mesnajeeeasd asdas";
    NSURL *venueURL = [NSURL URLWithString:@"http://www.condesadf.com/"];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[message,venueURL] applicationActivities:nil];
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsStatusBarAppearanceUpdate];
        });
        
    }];
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)openVenueSite
{
    NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)openCamera
{
    [NLSession setSharedVenue:[self venue]];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLOpenCameraNotification object:nil];
}
@end
