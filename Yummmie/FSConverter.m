//
//  FSConverter.m
//  Foursquare2-iOS
//
//  Created by Constantine Fry on 2/7/13.
//
//

#import "FSConverter.h"
#import "FSVenue.h"
#import "NLVenue.h"

@implementation FSConverter

- (NSArray *)convertToObjects:(NSArray *)venues {
    NSMutableArray *objects = [NSMutableArray array];
    
    for (NSDictionary *v  in venues)
    {
        FSVenue *ann = [[FSVenue alloc]init];
        ann.name = v[@"name"];
        ann.venueId = v[@"id"];
        
        ann.location.address = v[@"location"][@"address"];
        ann.location.distance = v[@"location"][@"distance"];
        
        [ann.location setCoordinate:CLLocationCoordinate2DMake([v[@"location"][@"lat"] doubleValue],
                                                               [v[@"location"][@"lng"] doubleValue])];
        
        [objects addObject:ann];
    }
    return objects;
}

- (NSArray *)convertToVenues:(NSArray *)venues
{
    NSMutableArray *objects = [NSMutableArray new];
    
    for (NSDictionary *v in venues)
    {
        NLVenue *venue = [[NLVenue alloc] initWithFoursquareObject:v];
        [objects addObject:venue];
    }
    
    return objects;
}
@end
