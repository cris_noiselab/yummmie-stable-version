//
//  NLSettingConstants.m
//  Yummmie
//
//  Created by bot on 12/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLSettingConstants.h"

NSString * const kSettingsAskedForFoursqaurePermissionKey = @"kAskedForFoursaqurePermissionKey";
NSString * const kSettingsCanUseFoursquareKey = @"kSettingsCanUseFoursquareKey";
NSString * const kSettingsHideSocialReminder = @"kSettingsHideSocialReminder";
NSString * const kSettingsFirstTime = @"kSettingsFirstTime";