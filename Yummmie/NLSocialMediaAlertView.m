//
//  NLSocialMediaAlertView.m
//  Yummmie
//
//  Created by bot on 3/30/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLSocialMediaAlertView.h"
#import "UIColor+NLColor.h"

@interface NLSocialMediaAlertView ()

@property (nonatomic, strong) UIView *alertContainer;


- (void)stopShowingView:(id)sender;
- (void)close:(id)sender;
- (void)setupAccount:(id)sender;

@end

@implementation NLSocialMediaAlertView

- (instancetype)init
{
    CGRect alertFrame = CGRectMake(0, 0, 320, 280);
    if (self = [super initWithFrame:[[UIScreen mainScreen] bounds]]){
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        UIGraphicsBeginImageContext(CGSizeMake(window.frame.size.width, window.frame.size.height));
        [window drawViewHierarchyInRect:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height) afterScreenUpdates:YES];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self setBackgroundColor:[UIColor colorWithPatternImage:[self blurWithCoreImage:image]]];
//        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
        CALayer *layer = [CALayer layer];
        [layer setFrame:[self bounds]];
        [layer setBackgroundColor:[[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.6f] CGColor]];
        [[self layer] insertSublayer:layer atIndex:0];
        
        _alertContainer = [[UIView alloc] initWithFrame:alertFrame];
        _alertContainer.center = self.center;
        
//        [_alertContainer setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_reminder"]]];
        [self addSubview:_alertContainer];


        [self setUserInteractionEnabled:YES];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 50, alertFrame.size.width-60, 20)];
        [titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:NSLocalizedString(@"link_your_facebook_title", nil)];
        [_alertContainer addSubview:titleLabel];
        
        UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, titleLabel.frame.origin.y+titleLabel.bounds.size.height, alertFrame.size.width-60, 50)];
        [detailLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f]];
        [detailLabel setTextColor:[UIColor whiteColor]];
        [detailLabel setTextAlignment:NSTextAlignmentCenter];
        [detailLabel setNumberOfLines:2];
        [detailLabel setText:NSLocalizedString(@"link_your_facebook_detail", nil)];
        [_alertContainer addSubview:detailLabel];
        
        
        UIButton *acceptButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [acceptButton setFrame:CGRectMake(0, 150, alertFrame.size.width, 50)];
        [acceptButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [[acceptButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [acceptButton setImage:[UIImage imageNamed:@"logo_fb"] forState:UIControlStateNormal];
        [acceptButton setTitle:NSLocalizedString(@"link_your_facebook_confirm_button_title", nil) forState:UIControlStateNormal];
        [acceptButton addTarget:self action:@selector(setupAccount:) forControlEvents:UIControlEventTouchUpInside];
        [acceptButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [acceptButton setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
        [acceptButton setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
        [_alertContainer addSubview:acceptButton];
        
        UIButton *dontBotherMeButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [dontBotherMeButton setFrame:CGRectOffset(acceptButton.frame, 0, 55)];
        [dontBotherMeButton setTitle:NSLocalizedString(@"link_your_facebook_dont_bother_me_button_title", nil) forState:UIControlStateNormal];
        [dontBotherMeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [[dontBotherMeButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [dontBotherMeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [dontBotherMeButton addTarget:self action:@selector(stopShowingView:) forControlEvents:UIControlEventTouchUpInside];
        [_alertContainer addSubview:dontBotherMeButton];
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close:)];
        [self addGestureRecognizer:gesture];
        [self setAlpha:0.0];
    }
    
    return self;
}

- (void)setupAccount:(id)sender
{
    if ([[self delegate] respondsToSelector:@selector(setupFacebookAccount:)]) {
        [[self delegate] setupFacebookAccount:self];
    }
}

- (void)stopShowingView:(id)sender
{
    if ([[self delegate] respondsToSelector:@selector(stopShowinView:)]){
        [[self delegate] stopShowinView:self];
    }
}

- (void)close:(id)sender
{
    if ([[self delegate] respondsToSelector:@selector(closeView:)]) {
        [[self delegate] closeView:self];
    }
}

- (void)show
{
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self setAlpha:1.0f];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide:(void (^)())block
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.alpha = 0.0f;
        } completion:^(BOOL finished) {
            block();
        }];
    });
}

- (UIImage *)blurWithCoreImage:(UIImage *)sourceImage
{
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    // Apply Affine-Clamp filter to stretch the image so that it does not
    // look shrunken when gaussian blur is applied
    CGAffineTransform transform = CGAffineTransformIdentity;
    CIFilter *clampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    [clampFilter setValue:inputImage forKey:@"inputImage"];
    [clampFilter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    // Apply gaussian blur filter with radius of 30
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:clampFilter.outputImage forKey: @"inputImage"];
    [gaussianBlurFilter setValue:@8 forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:gaussianBlurFilter.outputImage fromRect:[inputImage extent]];
    
    // Set up output context.
    UIGraphicsBeginImageContext(self.frame.size);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    
    // Invert image coordinates
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.frame.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, self.frame, cgImage);
    
    // Apply white tint
    CGContextSaveGState(outputContext);
    CGContextSetFillColorWithColor(outputContext, [UIColor colorWithWhite:1 alpha:0.2].CGColor);
    CGContextFillRect(outputContext, self.frame);
    CGContextRestoreGState(outputContext);
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return outputImage;
}

@end
