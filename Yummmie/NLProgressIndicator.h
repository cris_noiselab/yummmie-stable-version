//
//  NLProgressIndicator.h
//  ProgressView
//
//  Created by bot on 8/19/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLProgressIndicator : UIView

@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, strong) UIColor *tintColor;

@end
