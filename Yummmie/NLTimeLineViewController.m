//
//  NLTimeLineViewController.m
//  Yummmie
//
//  Created by bot on 6/16/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MGInstagram/MGInstagram.h>
#import <SSToolkit/SSToolkit.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <CoreLocation/CoreLocation.h>
#import <YKTwitterHelper/YATTwitterHelper.h>
#import <CoreSpotlight/CoreSpotlight.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "NLAuthentication.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "NLLikersViewController.h"
#import "NLTimeLineViewController.h"
#import "NLNoYummmiesViewCell.h"
#import "NSURL+AssetURL.h"
#import "NLConstants.h"
#import "NLNotifications.h"
#import "NLCommentsViewController.h"
#import "NLHashtagViewController.h"
#import "NLProfileViewController.h"
#import "NLDishViewController.h"
#import "NLTemporalViewController.h"
#import "NLPhotoImageViewController.h"
#import "TGRImageViewController.h"
#import "TGRImageZoomAnimationController.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLYum.h"
#import "NLVenue.h"
#import "NLTag.h"
//#import "NSString+NLTimefromDate.h"
#import "UIColor+NLColor.h"
#import "NLSearchFriendsViewController.h"
#import "NLNavigationViewController.h"
#import "NLComment.h"
#import "NLSocialMediaAlertView.h"
#import "NLPreferences.h"
#import "NLInteraction.h"
#import "NLVenueDetailViewController.h"
#import "NLVenueViewController.h"
#import "NLInstagramReminderView.h"
#import "NLEditViewController.h"
#import "NLRepostFeedbackView.h"
#import <QuartzCore/QuartzCore.h>
#import "NLTabBarController.h"
#import "NLConfirmView.h"
#import <TwitterKit/TwitterKit.h>

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

static NSString * const kTimelineCellIdentifier = @"TimeLineCell";
static NSString * const kTimelineNoYummmiesCellIdentifier = @"kTimelineNoYummmiesCellIdentifier";

@interface NLTimeLineViewController ()<NLSocialMediaAlertViewDelegate,NLInstagramReminderDelegate, NLConfirmViewDelegate>

@property (nonatomic, strong) TGRImageZoomAnimationController *photoAnimationController;

@property (nonatomic,strong) NSArray *texto;
@property (nonatomic,strong) NSMutableArray *heights;
@property (nonatomic, strong) NSMutableArray *yums;
@property (nonatomic) NSInteger currentOptionYumIndex;
@property (nonatomic) BOOL loadingPreviousYums;
@property (nonatomic) BOOL presentInstagramOption;
@property (nonatomic) BOOL loadingTimeLine;
@property (nonatomic,getter = shouldReload) BOOL reload;
@property (nonatomic) BOOL firstTime;
@property (nonatomic) NSTimer *reloadTimer;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) YATTwitterHelper *twitterHelper;
@property (nonatomic, strong) NLSocialMediaAlertView *socialAlert;
@property (nonatomic, strong) NLInstagramReminderView *instagramReminder;
@property (nonatomic, weak) NSURLSessionDataTask *resumeInteraction;

- (void)refreshData:(UIRefreshControl *)control;

- (void)getTimeLine;


- (void)loadTimeLine:(NSNotification *)notification;
- (void)loadTimeLineError:(NSNotification *)notification;
- (void)userLogIn:(NSNotification *)notification;
- (void)addYumToTimeline:(NSNotification *)notification;
- (void)commentAdded:(NSNotification *)notification;
- (void)showInstagramOption:(NSNotification *)notification;
- (void)resignActive:(NSNotification *)notificaion;
- (void)userLogout:(NSNotification *)notification;
- (void)askForLocationPermissions;
- (void)reloadTimelineData;
- (void)userUnfollowed:(NSNotification *)notification;
- (void)userFollowed:(NSNotification *)notification;
@end

@implementation NLTimeLineViewController
{
	UIView *vistaUserAd;
	UIPageControl *pageControl;
	UIScrollView *scroll;
	UIButton *closeTutorial;
	BOOL delete;
	NSInteger currentTimelinePage;
	NSInteger totalTimelinePages;
}

- (instancetype)initWithScrollableBar:(BOOL)scrollable
{
	if (self = [super initWithScrollableBar:scrollable])
	{
		//precalculate all the heights for each cell
		_heights = [NSMutableArray array];
		_yums = [NSMutableArray array];
		_loadingPreviousYums = NO;
		_loadingTimeLine = YES;
		_reload = NO;
		_firstTime = YES;
		
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		
		[defaultCenter addObserver:self selector:@selector(loadTimeLine:) name:kNLLoadTimeLineNotification object:nil];
		[defaultCenter addObserver:self selector:@selector(loadTimeLineError:) name:kNLLoadTimeLineErrorNotification object:nil];
		[defaultCenter addObserver:self selector:@selector(userLogIn:) name:kNLLoginNotification object:nil];
		[defaultCenter addObserver:self selector:@selector(addYumToTimeline:) name:kNLYumPostReadyNotification object:nil];
		[defaultCenter addObserver:self selector:@selector(commentAdded:) name:kNLCommentAddedNotification object:nil];
		[defaultCenter addObserver:self selector:@selector(showInstagramOption:) name:kNLShowInstagramOption object:nil];
		[defaultCenter addObserver:self selector:@selector(resignActive:) name:UIApplicationWillResignActiveNotification object:nil];
		[defaultCenter addObserver:self selector:@selector(userLogout:) name:kNLLogoutNotification object:nil];
		
		[defaultCenter addObserver:self selector:@selector(userUnfollowed:) name:kNLUnfollowUserNotification object:nil];
		[defaultCenter addObserver:self selector:@selector(userFollowed:) name:kNLFollowUserNotification object:nil];
		
		//Favs, likes observers
		[defaultCenter addObserver:self selector:@selector(updateLike:) name:kNLLikeNotification object:nil];
		
		[defaultCenter addObserver:self selector:@selector(updateFav:) name:kNLFavNotification object:nil];

	}
	return self;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if ([[self yums] count]>0)
	{
		[self setScrollableBar:YES];
	}
	else
	{
		[self setScrollableBar:NO];
	}
	
	CGRect frame = self.navigationController.navigationBar.frame;
	frame.origin.y = 20.0f;
	[self.navigationController.navigationBar setFrame:frame];
	self.navigationItem.titleView.alpha  = 1.0;
	
	
	
	//Image in title view MVP
	/*NSLog(@"%f, %f", self.navigationItem.titleView.frame.size.width, self.navigationItem.titleView.frame.size.height);
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    UIImageView *cdmx = [[UIImageView alloc] initWithFrame:CGRectMake((rootView.frame.size.width)/2, 13, 51, 13)];
    NSLog(@"cdmx x:%f",rootView.frame.size.width-61);
	cdmx.image = [UIImage imageNamed:@"logo_cdmx"];
	[self.navigationItem.titleView setClipsToBounds:false];
	[self.navigationItem.titleView addSubview:cdmx];
	[self.navigationItem.titleView bringSubviewToFront:cdmx];*/
	
	
	
	[[self tableView] reloadData];
	
	if (self.firstTime)
	{
		self.firstTime = NO;
	}else
	{
		[self updateData];
	}
	
	if (!self.reloadTimer.isValid)
	{
		//self.reloadTimer = [NSTimer scheduledTimerWithTimeInterval:80 target:self selector:@selector(updateData) userInfo:nil repeats:YES];
	}
	
	
	//Google Analytics
	NSString *name = [NSString stringWithFormat:@"Timeline View"];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self setScrollableBar:NO];
	[[[UIApplication sharedApplication] keyWindow] setBackgroundColor:[UIColor blackColor]];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	if ([self presentInstagramOption])
	{
		if ([MGInstagram isAppInstalled])
		{
			UIWindow *window = [[UIApplication sharedApplication] keyWindow];
			UIGraphicsBeginImageContext(CGSizeMake(window.frame.size.width, window.frame.size.height));
			[window drawViewHierarchyInRect:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height) afterScreenUpdates:YES];
			UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
			//[self instaGramWallPost:image];

			self.instagramReminder = [[NLInstagramReminderView alloc] initWithImage:image andYumImageURL:[[[self yums] firstObject] thumbImage]];
			self.instagramReminder.center = self.view.superview.center;
			[self.instagramReminder setAlpha:0.0f];
			[self.tabBarController.view addSubview:[self instagramReminder]];
			[self.instagramReminder setDelegate:self];

			__weak NLTimeLineViewController *weakSelf = self;
			[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
				[weakSelf.instagramReminder setAlpha:1.0f];
			} completion:nil];
		}
	}
	
	if ([[NLSession currentUser] newRegister])
	{
		[[NLSession currentUser] setNewRegister:NO];
		
		NLSearchFriendsViewController *searchVC = [[NLSearchFriendsViewController alloc] initWithSkipButton:YES];
		
		NLNavigationViewController *navViewController = [[NLNavigationViewController alloc] initWithRootViewController:searchVC];
		
		[self presentViewController:navViewController animated:YES completion:nil];

	}
	[self setNeedsStatusBarAppearanceUpdate];

	
}

- (BOOL)prefersStatusBarHidden
{
	return NO;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NLTimeLineViewCell class]) bundle:nil] forCellReuseIdentifier:kTimelineCellIdentifier];
	[self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NLNoYummmiesViewCell class]) bundle:nil] forCellReuseIdentifier:kTimelineNoYummmiesCellIdentifier];
	self.refreshControl = [[UIRefreshControl alloc] init];
	self.refreshControl.tintColor = [UIColor colorWithRed:205.0/255.0 green:36.0f/255.0f blue:49.0/255.0 alpha:1.0];
	[self.refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
	[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self setScrollableBar:NO];
	[self showLoadingSpinner];
	
	if ([NLSession accessToken])
	{
		//if there's access token ask for permissions
		[self askForLocationPermissions];
	}
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTimeLine) name:@"ShouldReloadAll" object:nil];
	currentTimelinePage = 1;
	totalTimelinePages = 10;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

#pragma mark - Timeline methods
-(void)updateLike:(NSNotification *)dict
{
	NSLog(@"%@", [[dict userInfo] objectForKey:@"yum"]);
	__weak NLYum *yum = [[dict userInfo] objectForKey:@"yum"];
	__weak NLTimeLineViewController *weakSelf = self;

	NSInteger yumId = yum.yumId;
	if (yum.repost)
	{
		yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
	}
	NSInteger counter = 0;
	NSMutableArray *indexes = [NSMutableArray array];
	for (NLYum *nYum in self.yums)
	{
		NSInteger nYumId = nYum.yumId;
		if (nYum.repost)
		{
			nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
		}
		if (nYumId == yumId)
		{
			nYum.userLikes = yum.userLikes;
			nYum.likes = yum.likes;
			NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
			[indexes addObject:nindexPath];
		}
		counter++;
	}
	[[weakSelf tableView] reloadRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationNone];
	

}
-(void)updateFav:(NSNotification *)dict
{
	if (self.yums.count > 0 )
	{
		__weak NLYum *yum = [[dict userInfo] objectForKey:@"yum"];
		__weak NLTimeLineViewController *weakSelf = self;
		
		NSInteger yumId = yum.yumId;
		if (yum.repost)
		{
			yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
		}
		NSInteger counter = 0;
		NSMutableArray *indexes = [NSMutableArray array];
		for (NLYum *nYum in self.yums)
		{
			NSInteger nYumId = nYum.yumId;
			if (nYum.repost)
			{
				nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
			}
			if (nYumId == yumId)
			{
				nYum.isFav = yum.isFav;
				nYum.favs = yum.favs;
				NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
				[indexes addObject:nindexPath];
			}
			counter++;
		}
		[[weakSelf tableView] reloadRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationNone];

	}

}
- (void)reloadTimelineData
{
	__weak NLTimeLineViewController *weakSelf = self;
	
	if ([[self yums] count] > 0)
	{
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		currentTimelinePage = 1;
		[NLYum getTimelinePage:currentTimelinePage forUserId:[[NLSession currentUser] userID] withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages)
		{
			[[weakSelf refreshControl] endRefreshing];
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			if (!error)
			{
				currentTimelinePage = currentPage;
				if (totalPages > 1)
				{
					totalTimelinePages = totalPages;
				}
				if ([yums count]>0)
			 {
				 [weakSelf setYums:yums];
                 [[weakSelf heights] removeAllObjects];
				 
				 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					 for (NLYum *yum in weakSelf.yums)
					 {
						 CGSize textSize = [self sizeForText:yum.caption];
                         if(yum.caption.length<=44)
                         {
                             textSize.height=15;
                         }
						 [[weakSelf heights] addObject:[NSValue valueWithCGSize:textSize]];
					 }
					 
					 dispatch_async(dispatch_get_main_queue(), ^{
						 
						 [[weakSelf tableView] reloadData];
					 });
					 
					 [self.resumeInteraction cancel];
					 /*self.resumeInteraction =  [NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes) {
						
						NSDictionary *userInfo  = @{
						@"newComments":comments,
						@"newFollowers":followers,
						@"newLikes":likes
						};
						[[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification
						object:nil
						userInfo:userInfo];
						}];*/
				 });
			 }
			 else
			 {
				 //[weakSelf showErrorMessage:NSLocalizedString(@"no_yummies_in_tl", nil) withDuration:3.0f];
				 dispatch_async(dispatch_get_main_queue(), ^{
					 [[weakSelf tableView] reloadData];
				 });
			 }
				/*
				if ([yums count] > 0)
				{
					//set
					dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						for (NLYum *yum in yums)
						{
							if (![weakSelf.yums containsObject:yum])
							{
								CGSize textSize = [weakSelf sizeForText:yum.caption];
								[[weakSelf heights] insertObject:[NSValue valueWithCGSize:textSize] atIndex:0];
								[[weakSelf yums] insertObject:yum atIndex:0];
							}
						}

						dispatch_async(dispatch_get_main_queue(), ^{
							
							[[weakSelf tableView] reloadData];
						});
					});
				}else
				{
					//[weakSelf showErrorMessage:NSLocalizedString(@"no_yummies_in_tl", nil) withDuration:3.0f];
					dispatch_async(dispatch_get_main_queue(), ^{
						[[weakSelf tableView] reloadData];
					});
				}
				*/
			}else
			{
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:3.0f];
				});
			}
			
			
			dispatch_async(dispatch_get_main_queue(), ^{
				[weakSelf hideLoadingSpinner];
				if (weakSelf.yums.count > 0)
				{
					[weakSelf setScrollableBar:YES];
				}else
				{
					[weakSelf setScrollableBar:NO];
				}
				
			});
			weakSelf.loadingTimeLine = NO;
			[self.resumeInteraction cancel];
			/*self.resumeInteraction =  [NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes) {
			 
			 NSDictionary *userInfo  = @{
			 @"newComments":comments,
			 @"newFollowers":followers,
			 @"newLikes":likes
			 };
			 [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification
			 object:nil
			 userInfo:userInfo];
			 }];*/
		}];
	}else
	{
		[self getTimeLine];
	}
	
	
}

- (void)getTimeLine
{
	//clean the shared yum
	[NLSession setYum:nil];
	[NLSession setSharedPhoto:nil];
	
	__weak NLTimeLineViewController *weakSelf = self;
	currentTimelinePage = 1;
	[NLYum getTimelinePage:currentTimelinePage forUserId:[NLSession getUserID] withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages)
	 {
		 currentTimelinePage = currentPage;
		 if (totalPages > 1)
		 {
			 totalTimelinePages = totalPages;
		 }
		 dispatch_async(dispatch_get_main_queue(), ^{
			 [[weakSelf refreshControl] endRefreshing];
		 });
		 if (!error)
		 {
			 if ([yums count]>0)
			 {
				 [weakSelf setYums:yums];

				 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					 for (NLYum *yum in weakSelf.yums)
					 {
						 CGSize textSize = [self sizeForText:yum.caption];
                         if(yum.caption.length<=44)
                         {
                             textSize.height=15;
                         }
						 [[weakSelf heights] addObject:[NSValue valueWithCGSize:textSize]];
					 }
					 
					 dispatch_async(dispatch_get_main_queue(), ^{
						 
						 [[weakSelf tableView] reloadData];
					 });
					 
					 [self.resumeInteraction cancel];
					 /*self.resumeInteraction =  [NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes) {
						
						NSDictionary *userInfo  = @{
						@"newComments":comments,
						@"newFollowers":followers,
						@"newLikes":likes
						};
						[[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification
						object:nil
						userInfo:userInfo];
						}];*/
				 });
			 }
			 else
			 {
				 //[weakSelf showErrorMessage:NSLocalizedString(@"no_yummies_in_tl", nil) withDuration:3.0f];
				 dispatch_async(dispatch_get_main_queue(), ^{
					 [[weakSelf tableView] reloadData];
				 });
			 }
			 
		 }else
		 {
			 dispatch_async(dispatch_get_main_queue(), ^{
				 [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:3.0f];
			 });
		 }
		 
		 
		 dispatch_async(dispatch_get_main_queue(), ^{
			 [weakSelf hideLoadingSpinner];
			 if (weakSelf.yums.count > 0)
			 {
				 [weakSelf setScrollableBar:YES];
			 }else
			 {
				 [weakSelf setScrollableBar:NO];
			 }
			 
		 });
		 weakSelf.loadingTimeLine = NO;
		 /*
		 static dispatch_once_t onceToken;
		 dispatch_once(&onceToken, ^{
			 if ([NLPreferences canShowSocialReminder] && ![NLSession containsAuthentication:NLAuthenticationTypeFacebook] && ![NLPreferences firstTime])
			 {
				 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
					 self.socialAlert = [[NLSocialMediaAlertView alloc] init];
					 [self.socialAlert setDelegate:self];
					 self.socialAlert.center = self.view.superview.center;
					 [self.tabBarController.view addSubview:self.socialAlert];
					 [self.socialAlert show];
				 });
			 }
		 });
			*/
	 }];
}

- (void)addYumToTimeline:(NSNotification *)notification
{
	//Create a yum stored in the session
	NLYum *newYum = [NLSession yum];
	
	if (![self.yums containsObject:newYum])
	{
		__weak NLTimeLineViewController *weakSelf = self;
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			
			CGSize newYumSize = [weakSelf sizeForText:[newYum caption]];
            if(newYum.caption.length<=44)
            {
                newYumSize.height=15;
            }
			[[weakSelf heights] insertObject:[NSValue valueWithCGSize:newYumSize] atIndex:0];
			[[weakSelf yums] insertObject:newYum atIndex:0];
			dispatch_async(dispatch_get_main_queue(), ^{
				[[weakSelf tableView] reloadData];
				dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
					[weakSelf scrollToTop];
				});
				
			});
		});
	}
	
}

- (void)updateData
{
	
	if (self.userLoggedIn)
	{
		self.userLoggedIn = NO;
		self.loadingTimeLine = YES;
		[self showLoadingSpinner];
		[self reloadTimelineData];
		[self showLoadingSpinner];
		
	}else
	{
		__weak NLTimeLineViewController *weakSelf = self;
		[SAMRateLimit executeBlock:^{
			[weakSelf reloadTimelineData];
		} name:kReloadTimelineKey limit:80.0f];
	}
}

- (void)resignActive:(NSNotification *)notificaion
{
	if (self.reloadTimer.isValid)
	{
		[self.reloadTimer invalidate];
	}
}

- (void)userFollowed:(NSNotification *)notification
{
	[self reloadTimelineData];
}

- (void)userUnfollowed:(NSNotification *)notification
{
	[self reloadTimelineData];
}

#pragma mark - Notification Methods

- (void)loadTimeLine:(NSNotification *)notification
{
	[self updateData];
}

- (void)loadTimeLineError:(NSNotification *)notification
{
	[self hideLoadingSpinner];
}

- (void)userLogIn:(NSNotification *)notification
{
	self.userLoggedIn = YES;
	[[self yums] removeAllObjects];
	[[self heights] removeAllObjects];
	[[self tableView] reloadData];
	
	[self updateData];
	[self askForLocationPermissions];
	
	
}

- (void)userLogout:(NSNotification *)notification
{
	self.userLoggedIn = NO;
	[self.yums removeAllObjects];
	[self setLoadingTimeLine:YES];
	[[self tableView] reloadData];
	[[self reloadTimer] invalidate];
}

- (void)commentAdded:(NSNotification *)notification
{
	[[self tableView] reloadData];
}

- (void)showInstagramOption:(NSNotification *)notification
{
	self.presentInstagramOption = YES;
}

#pragma mark - UIRefreshControl Action

- (void)refreshData:(UIRefreshControl *)control
{
	__weak NLTimeLineViewController *weakSelf = self;
	
	if ([[self yums] count] > 0)
	{
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		currentTimelinePage = 1;
		[NLYum getTimelinePage:currentTimelinePage forUserId:[NLSession getUserID] withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages) {
			[[weakSelf refreshControl] endRefreshing];
			if (!error)
            {
				currentTimelinePage = currentPage;
				if (totalPages > 1)
				{
					totalTimelinePages = totalPages;
				}
            if ([yums count]>0)
			 {
				 [weakSelf setYums:yums];
                 [[weakSelf heights] removeAllObjects];
				 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					 for (NLYum *yum in weakSelf.yums)
					 {
						 CGSize textSize = [self sizeForText:yum.caption];
                         if(yum.caption.length<=44)
                         {
                             textSize.height=15;
                         }

						 [[weakSelf heights] addObject:[NSValue valueWithCGSize:textSize]];
					 }
					 
					 dispatch_async(dispatch_get_main_queue(), ^{
						 
						 [[weakSelf tableView] reloadData];
					 });
					 
					 [self.resumeInteraction cancel];
					 /*self.resumeInteraction =  [NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes) {
						
						NSDictionary *userInfo  = @{
						@"newComments":comments,
						@"newFollowers":followers,
						@"newLikes":likes
						};
						[[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification
						object:nil
						userInfo:userInfo];
						}];*/
				 });
             }
			 else
			 {
				 //[weakSelf showErrorMessage:NSLocalizedString(@"no_yummies_in_tl", nil) withDuration:3.0f];
				 dispatch_async(dispatch_get_main_queue(), ^{
					 [[weakSelf tableView] reloadData];
				 });
             }
				
            }else
			{
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			}
			
			[self.resumeInteraction cancel];
			/*self.resumeInteraction = [NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes) {
			 
			 NSDictionary *userInfo  = @{
			 @"newComments":comments,
			 @"newFollowers":followers,
			 @"newLikes":likes
			 };
			 [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification
			 object:nil
			 userInfo:userInfo];
			 }];*/
		}];
	}else
	{
		[self getTimeLine];
	}
	
}

#pragma mark - Table view data source
-(void)ok
{
	[UIView animateWithDuration:0.5 animations:^{
		vistaUserAd.alpha = 0;
	}];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if ([[self yums] count] == 0 && !self.loadingTimeLine)
	{
		return 1;
	}
	return [[self yums] count];
}
-(void)fillCellFromYum:(NLYum *)yum andCell:(NLTimeLineViewCell *)cell
{
	[cell setYumId:yum.yumId];
	
	
	if (yum.venue.venueType == NLPrivateVenue)
	{
		[cell setVenueTextColor:[UIColor colorWithRed:0.890 green:0.506 blue:0.537 alpha:1]];
		
	}else
	{
		[cell setVenueTextColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	}
	[[cell profileThumbButton] sd_setBackgroundImageWithURL:[NSURL URLWithString:[[yum user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
	
	
	[cell setNumberOfComments:[yum numberOfComments]];
	[cell setLiked:[yum userLikes]];
	[cell setNumberOfLikes:[yum likes]];
	[cell setNumberOfFavs:[yum favs]];
	[cell.repostButton setSelected:yum.isReposted];
	[cell setNumberOfReposts:[yum reposts]];
	[cell setViewCount:[yum viewsCount]];

	[cell.numberOfFavsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum favs]] forState:UIControlStateNormal];
	[cell.numberOfRepostsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum reposts]] forState:UIControlStateNormal];
	[cell.numbersOfViews setTitle:[NSString stringWithFormat:@"%ld", (long)[yum viewsCount]] forState:UIControlStateNormal];

	[cell setUsername:[NSString stringWithFormat:@"@%@",[[yum user] username]]];
	[cell setVenueName:[[yum venue] name]];
	
	
    CGSize dishSize = [self sizeForDish:yum.name];
    CGFloat dishHeight = (dishSize.height)+10;
    [cell setDishConstraint:dishHeight];
    //[cell setDishName:(!yum.name)?@"Default Name":yum.name];
    [cell setDishName:(!yum.name)?@"Default Name":yum.name withSize:dishSize];
	if (yum.repost)
	{
		[cell setRecommend:yum.repost];
		
	}
	[cell setPublishedTime:[yum createdDict]];
	[cell changeFavsNumber:yum.favs selfFav:yum.isFav];
    
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.yums.count > 0)
	{
		
		NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[tableView dequeueReusableCellWithIdentifier:kTimelineCellIdentifier forIndexPath:indexPath];
		
		[cell setClipsToBounds:true];
		for (UIView *vista in cell.subviews)
		{
			vista.userInteractionEnabled = true;
		}
		__weak NLYum *yum = (NLYum *)[[self yums] objectAtIndex:[indexPath row]];
        if (yum.repost)
        {
            [self markViewedYumId:[[yum.repost objectForKey:@"original_yum_id"] integerValue]];

        }
        else
        {
            [self markViewedYumId:yum.yumId];
        }

		
		//Testing
		//[cell fillCellFromYum:yum];
		[self fillCellFromYum:yum andCell:cell];
		
		
		
		
		[cell setComment:[yum caption] withSize:[[[self heights] objectAtIndex:indexPath.row] CGSizeValue] andComments:yum.comments];
		UIImage *placeHolderImage = nil;
		if ([NLSession yum].yumId == yum.yumId)
		{
			placeHolderImage = [NLSession getSharedPhoto];
			[[cell indicator] setAlpha:0.0f];
		}
		[[cell dishPhoto] sd_setImageWithURL:[NSURL URLWithString:[yum image]] placeholderImage:placeHolderImage options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize)
		 {
			 if (expectedSize>0 && yum.yumId != [NLSession yum].yumId)
			 {
				 [[cell indicator] setAlpha:1.0];
				 [[cell indicator] setProgress:(receivedSize*1.0f)/expectedSize];
			 }
		 } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
			 [[cell indicator] setAlpha:0.0f];
			 if (image && cacheType == SDImageCacheTypeNone && yum.yumId == [NLSession yum].yumId)
			 {
				 if ([NLSession getSharedPhoto])
				 {
					 [NLSession setYum:nil];
					 [NLSession setSharedPhoto:nil];
				 }
			 }
			 if (indexPath.row == 1)
			 {
				 //Blurred view to alerts
				 NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
				 //[def setBool:true forKey:@"registro"];
				 //[def setBool:false forKey:@"noNew"];
				 
				 if ([def boolForKey:@"registro"] == true && [def boolForKey:@"noNew"] == false)
				 {
					 [def setBool:true forKey:@"noNew"];
					 vistaUserAd = [[UIView alloc] initWithFrame:self.tabBarController.view.bounds];
					 vistaUserAd.backgroundColor = [UIColor clearColor];
					 
					 UIGraphicsBeginImageContext(CGSizeMake(self.tabBarController.view.frame.size.width, self.tabBarController.view.frame.size.height / 2));
					 
					 //Get a UIImage from the UIView
					 [self.tabBarController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
					 UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
					 UIGraphicsEndImageContext();
					 
					 //Blur the UIImage with a CIFilter
					 CIImage *imageToBlur = [CIImage imageWithCGImage:viewImage.CGImage];
					 CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
					 [gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];
					 [gaussianBlurFilter setValue:[NSNumber numberWithFloat: 3] forKey: @"inputRadius"];
					 CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
					 UIImage *endImage = [[UIImage alloc] initWithCIImage:resultImage];
					 
					 //Place the UIImage in a UIImageView
					 UIImageView *newView = [[UIImageView alloc] initWithFrame:CGRectMake(-19, -19, self.view.frame.size.width + 38, (self.view.frame.size.height / 2) + 38)];
					 newView.contentMode = UIViewContentModeScaleAspectFill;
					 newView.image = endImage;
					 [vistaUserAd addSubview:newView];
				 
					 UIView *vibView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.center.y, self.view.frame.size.width, self.view.frame.size.height / 2.0)];
					 vibView.backgroundColor = [UIColor whiteColor];
					 
					 UILabel *bienvenido = [[UILabel alloc] init];
					 bienvenido.text  = NSLocalizedString(@"aviso_follow_hola", @"aviso_follow_hola");
					 [bienvenido setFrame:CGRectMake(20, 20, self.view.frame.size.width - 40, 20)];
					 
					 [bienvenido setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17]];
					 [bienvenido setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
					 [bienvenido setTextAlignment:NSTextAlignmentCenter];
					 [vibView addSubview: bienvenido];
					 
					 
					 UILabel *text1 = [[UILabel alloc] init];
					 text1.text = NSLocalizedString(@"aviso_follow_1", @"aviso_follow_1");
					 [text1 setFrame:CGRectMake(20, bienvenido.frame.origin.y + bienvenido.frame.size.height + 20, self.view.frame.size.width - 40, 70)];
					 [text1 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
					 [text1 setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
					 [text1 setTextAlignment:NSTextAlignmentCenter];
					 text1.numberOfLines = 3;
					 [vibView addSubview: text1];
					 
					 
					 UILabel *text2 = [[UILabel alloc] init];
					 text2.text = NSLocalizedString(@"aviso_follow_2", @"aviso_follow_2");
					 [text2 setFrame:CGRectMake(20, text1.frame.origin.y + text1.frame.size.height + 20, self.view.frame.size.width - 40, 50)];
					 [text2 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
					 [text2 setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
					 text2.numberOfLines = 3;
					 [text2 setTextAlignment:NSTextAlignmentCenter];
					 [vibView addSubview: text2];
					 
					 UIButton *ok = [[UIButton alloc] initWithFrame:CGRectMake(0, vibView.frame.size.height - 66, vibView.frame.size.width, 66)];
					 [ok addTarget:self action:@selector(ok) forControlEvents:UIControlEventTouchUpInside];
					 [ok setTitle:@"OK" forState:UIControlStateNormal];
					 [ok.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
					 [ok setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
					 [ok setBackgroundColor:[UIColor colorWithRed:205.0/255.0 green:36.0/255.0 blue:49.0/255.0 alpha:1]];
					 [vibView addSubview:ok];
					 // Add label to the vibrancy view
					 //[[vibrancyEffectView contentView] addSubview:vibView];
					 
					 // Add the vibrancy view to the blur view
					 //[[vistaUserAd contentView] addSubview:vibView];
					 [vistaUserAd addSubview:vibView];
					 vistaUserAd.alpha = 0;
					 [self.tabBarController.view addSubview:vistaUserAd];
					 [UIView animateWithDuration:0.5 animations:^{
						 vistaUserAd.alpha = 1;
					 }];
					 
				 }
				 else if([def boolForKey:@"tutorialShowed"] == false && !scroll)
				 {
					 [self createCarrousel];
				 }
				 else
				 {
					 [[iRate sharedInstance] logEvent:false];
					 NSLog(@"%lu", (unsigned long)[iRate sharedInstance].eventCount);
				 }
			 }
			 
			 if (!image)
			 {
				 NSLog(@"%@", [error localizedDescription]);
			 }
		 }];
		[cell setTag:[indexPath row]];
        [cell setHashTags:@"cdmx" withSubRegion:@"cdmx" andFoodType:@"cdmx"];
		[cell setDelegate:self];
        
        //---
	/*
     
	 
		[cell setYumId:yum.yumId];
		
		[cell setComment:[yum caption] withSize:[[[self heights] objectAtIndex:indexPath.row] CGSizeValue] andComments:yum.comments];
		UIImage *placeHolderImage = nil;
		if ([NLSession yum].yumId == yum.yumId)
		{
			placeHolderImage = [NLSession getSharedPhoto];
			[[cell indicator] setAlpha:0.0f];
		}
		
		[[cell dishPhoto] sd_setImageWithURL:[NSURL URLWithString:[yum image]] placeholderImage:placeHolderImage options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize) {
			if (expectedSize>0 && yum.yumId != [NLSession yum].yumId)
			{
				[[cell indicator] setAlpha:1.0];
				[[cell indicator] setProgress:(receivedSize*1.0f)/expectedSize];
			}
		} completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
			[[cell indicator] setAlpha:0.0f];
			if (image && cacheType == SDImageCacheTypeNone && yum.yumId == [NLSession yum].yumId)
			{
				if ([NLSession getSharedPhoto] )
				{
					[NLSession setYum:nil];
					[NLSession setSharedPhoto:nil];
				}
			}
			if (indexPath.row == 1)
			{
				//Blurred view to alerts
				NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
				NSLog(@"registro = %d, noNew = %d", [def boolForKey:@"registro"], [def boolForKey:@"noNew"]);
				//[def setBool:true forKey:@"registro"];
				//[def setBool:false forKey:@"noNew"];
				
				if ([def boolForKey:@"registro"] == true && [def boolForKey:@"noNew"] == false)
				{
					[def setBool:true forKey:@"noNew"];
	 
					
					vistaUserAd = [[UIView alloc] initWithFrame:self.tabBarController.view.bounds];
					vistaUserAd.backgroundColor = [UIColor clearColor];
					
					// Label for vibrant textç
					
					
					UIGraphicsBeginImageContext(CGSizeMake(self.tabBarController.view.frame.size.width, self.tabBarController.view.frame.size.height / 2));
					
					//Get a UIImage from the UIView
					[self.tabBarController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
					UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
					UIGraphicsEndImageContext();
					
					//Blur the UIImage with a CIFilter
					CIImage *imageToBlur = [CIImage imageWithCGImage:viewImage.CGImage];
					CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
					[gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];
					[gaussianBlurFilter setValue:[NSNumber numberWithFloat: 3] forKey: @"inputRadius"];
					CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
					UIImage *endImage = [[UIImage alloc] initWithCIImage:resultImage];
					
					//Place the UIImage in a UIImageView
					UIImageView *newView = [[UIImageView alloc] initWithFrame:CGRectMake(-19, -19, self.view.frame.size.width + 38, (self.view.frame.size.height / 2) + 38)];
					newView.contentMode = UIViewContentModeScaleAspectFill;
					newView.image = endImage;
					[vistaUserAd addSubview:newView];
					
					
					
					UIView *vibView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.center.y, self.view.frame.size.width, self.view.frame.size.height / 2.0)];
					vibView.backgroundColor = [UIColor whiteColor];
					
					UILabel *bienvenido = [[UILabel alloc] init];
					bienvenido.text  = NSLocalizedString(@"aviso_follow_hola", @"aviso_follow_hola");
					[bienvenido setFrame:CGRectMake(20, 20, self.view.frame.size.width - 40, 20)];
					
					[bienvenido setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17]];
					[bienvenido setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
					[bienvenido setTextAlignment:NSTextAlignmentCenter];
					[vibView addSubview: bienvenido];
					
					
					UILabel *text1 = [[UILabel alloc] init];
					text1.text = NSLocalizedString(@"aviso_follow_1", @"aviso_follow_1");
					[text1 setFrame:CGRectMake(20, bienvenido.frame.origin.y + bienvenido.frame.size.height + 20, self.view.frame.size.width - 40, 70)];
					[text1 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
					[text1 setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
					[text1 setTextAlignment:NSTextAlignmentCenter];
					text1.numberOfLines = 3;
					[vibView addSubview: text1];
					
					
					UILabel *text2 = [[UILabel alloc] init];
					text2.text = NSLocalizedString(@"aviso_follow_2", @"aviso_follow_2");
					[text2 setFrame:CGRectMake(20, text1.frame.origin.y + text1.frame.size.height + 20, self.view.frame.size.width - 40, 50)];
					[text2 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
					[text2 setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
					text2.numberOfLines = 3;
					[text2 setTextAlignment:NSTextAlignmentCenter];
					[vibView addSubview: text2];
					
					
					
					UIButton *ok = [[UIButton alloc] initWithFrame:CGRectMake(0, vibView.frame.size.height - 66, vibView.frame.size.width, 66)];
					[ok addTarget:self action:@selector(ok) forControlEvents:UIControlEventTouchUpInside];
					[ok setTitle:@"OK" forState:UIControlStateNormal];
					[ok.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
					[ok setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
					[ok setBackgroundColor:[UIColor colorWithRed:205.0/255.0 green:36.0/255.0 blue:49.0/255.0 alpha:1]];
					[vibView addSubview:ok];
					// Add label to the vibrancy view
					//[[vibrancyEffectView contentView] addSubview:vibView];
					
					// Add the vibrancy view to the blur view
					//[[vistaUserAd contentView] addSubview:vibView];
					[vistaUserAd addSubview:vibView];
					vistaUserAd.alpha = 0;
					[self.tabBarController.view addSubview:vistaUserAd];
					[UIView animateWithDuration:0.5 animations:^{
						vistaUserAd.alpha = 1;
					}];
					
				}
				else
				{
					[[iRate sharedInstance] logEvent:false];
					NSLog(@"%lu", (unsigned long)[iRate sharedInstance].eventCount);
				}
			}
			
			if (!image)
			{
				NSLog(@"%@",[error localizedDescription]);
			}
		}];
		
		if (yum.venue.venueType == NLPrivateVenue)
		{
			[cell setVenueTextColor:[UIColor colorWithRed:0.890 green:0.506 blue:0.537 alpha:1]];
			
		}else
		{
			[cell setVenueTextColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
		}
		[[cell profileThumbButton] sd_setBackgroundImageWithURL:[NSURL URLWithString:[[yum user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
		[cell setNumberOfComments:[yum numberOfComments]];
		[cell setLiked:[yum userLikes]];
		[cell setNumberOfLikes:[yum likes]];
		[cell setNumberOfFavs:[yum favs]];
		[cell setNumberOfReposts:[yum reposts]];
		[cell.numberOfFavsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum favs]] forState:UIControlStateNormal];
		[cell setFav:yum.isFav];
		[cell.numberOfRepostsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum reposts]] forState:UIControlStateNormal];
		[cell setTag:[indexPath row]];
		[cell setDelegate:self];
		[cell setUsername:[NSString stringWithFormat:@"@%@",[[yum user] username]]];
		[cell setVenueName:[[yum venue] name]];
		[cell setDishName:(!yum.name)?@"Default Name":yum.name];
		
		if (yum.repost)
		{
			[cell setRecommend:yum.repost];

		}
		
		[cell setPublishedTime:[yum created]];
		*/
		return cell;
		
	}else
	{
		NLNoYummmiesViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTimelineNoYummmiesCellIdentifier forIndexPath:indexPath];
		cell.messageLabel.text = NSLocalizedString(@"not_yummmies_in_timeline", nil);
		return cell;
	}
}
-(void)createCarrousel
{
	scroll = [[UIScrollView alloc] initWithFrame:self.tabBarController.view.bounds];
	scroll.tag = 3;
	[scroll setBackgroundColor:[UIColor clearColor]];
	//[scroll setScrollEnabled:false];
	[scroll setBounces:false];
	[scroll setPagingEnabled:true];
	[scroll setContentSize:CGSizeMake(scroll.frame.size.width * 3, scroll.frame.size.height)];
	scroll.delegate = self;
	[scroll setShowsHorizontalScrollIndicator:false];
	[self.tabBarController.view addSubview:scroll];
	UIView *content = [[UIView alloc] initWithFrame:CGRectMake(0, 0, scroll.frame.size.width * 3, scroll.frame.size.height)];
	[content setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
	[scroll addSubview:content];
	pageControl = [[UIPageControl alloc] init];
	pageControl.numberOfPages = 3;
	pageControl.currentPage = 0;
	//[pageControl setTintColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	[pageControl setPageIndicatorTintColor:[UIColor whiteColor]];
	[pageControl setCurrentPageIndicatorTintColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	UIView *v1 = [[UIImageView alloc] initWithFrame:CGRectMake(20, 0, scroll.frame.size.width - 40, scroll.frame.size.height - 40)];
	[v1 setBackgroundColor:[UIColor clearColor]];
	[content addSubview:v1];
	pageControl.center = CGPointMake(scroll.center.x, v1.frame.size.height / 1.6 + 120);
	[self.tabBarController.view addSubview:pageControl];

	UILabel *titleV1 = [[UILabel alloc] initWithFrame:CGRectMake(0, v1.frame.size.height / 1.6, v1.frame.size.width, 20)];
	[titleV1 setText:@"WISHLIST"];
	[titleV1 setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];
	titleV1.textAlignment = NSTextAlignmentLeft;
	titleV1.textColor = [UIColor whiteColor];
	[v1 addSubview:titleV1];
	
	UILabel *contentV1 = [[UILabel alloc] initWithFrame:CGRectMake(0, v1.frame.size.height / 1.6 + 20, v1.frame.size.width, 70)];
	[contentV1 setText:NSLocalizedString(@"v1-content", nil)];
	[contentV1 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
	contentV1.textAlignment = NSTextAlignmentLeft;
	contentV1.numberOfLines = 3;
	contentV1.textColor = [UIColor whiteColor];
	[v1 addSubview:contentV1];

	UIImageView *iv1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, scroll.frame.size.width - 40, scroll.frame.size.height / 2.1)];
	[iv1 setContentMode:UIViewContentModeScaleAspectFit];
	[iv1 setBackgroundColor:[UIColor clearColor]];
	iv1.image = [UIImage imageNamed:@"v1"];
	[v1 addSubview:iv1];
	
	UIView *v2 = [[UIImageView alloc] initWithFrame:CGRectMake(scroll.frame.size.width + 20, 0, scroll.frame.size.width - 40, scroll.frame.size.height - 40)];
	[v2 setBackgroundColor:[UIColor clearColor]];
	[content addSubview:v2];
	
	UILabel *titlev2 = [[UILabel alloc] initWithFrame:CGRectMake(0, v2.frame.size.height / 1.6, v2.frame.size.width, 20)];
	[titlev2 setText:NSLocalizedString(@"v2-title", nil)];
	[titlev2 setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];
	titlev2.textAlignment = NSTextAlignmentLeft;
	titlev2.textColor = [UIColor whiteColor];
	[v2 addSubview:titlev2];
	
	UILabel *contentv2 = [[UILabel alloc] initWithFrame:CGRectMake(0, v2.frame.size.height / 1.6 + 20, v2.frame.size.width, 70)];
	[contentv2 setText:NSLocalizedString(@"v2-content", nil)];
	[contentv2 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
	contentv2.textAlignment = NSTextAlignmentLeft;
	contentv2.numberOfLines = 3;
	contentv2.textColor = [UIColor whiteColor];
	[v2 addSubview:contentv2];

	UIImageView *iv2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, scroll.frame.size.width - 40, scroll.frame.size.height / 2.1)];
	[iv2 setContentMode:UIViewContentModeScaleAspectFit];
	[iv2 setBackgroundColor:[UIColor clearColor]];
	iv2.image = [UIImage imageNamed:@"v2"];

	[v2 addSubview:iv2];
	
	
	UIView *v3 = [[UIImageView alloc] initWithFrame:CGRectMake(scroll.frame.size.width * 2 + 20, 0, scroll.frame.size.width - 40, scroll.frame.size.height)];
	[v3 setBackgroundColor:[UIColor clearColor]];
	[v3 setClipsToBounds:false];
	[content addSubview:v3];
	
	UILabel *titlev3 = [[UILabel alloc] initWithFrame:CGRectMake(0, v3.frame.size.height / 1.65, v3.frame.size.width, 20)];
	[titlev3 setText:NSLocalizedString(@"v3-title", nil)];
	[titlev3 setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:18]];
	titlev3.textAlignment = NSTextAlignmentCenter;
	titlev3.textColor = [UIColor whiteColor];
	[v3 addSubview:titlev3];
	
	UILabel *contentv3 = [[UILabel alloc] initWithFrame:CGRectMake(0, v3.frame.size.height / 1.65 + 20, v3.frame.size.width, 20)];
	[contentv3 setText:NSLocalizedString(@"v3-content", nil)];
	[contentv3 setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
	contentv3.textAlignment = NSTextAlignmentCenter;
	contentv3.numberOfLines = 3;
	contentv3.textColor = [UIColor whiteColor];
	[v3 addSubview:contentv3];
	
	closeTutorial = [[UIButton alloc] initWithFrame:CGRectMake(0, v3.frame.size.height - 60, scroll.frame.size.width, 60)];
	[closeTutorial setBackgroundColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	[closeTutorial setTitle:@"OK" forState:UIControlStateNormal];
	[closeTutorial setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[closeTutorial addTarget:self action:@selector(quitTutorial) forControlEvents:UIControlEventTouchDown];
	closeTutorial.alpha = 0;
	[self.tabBarController.view addSubview:closeTutorial];
	UIImageView *iv3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, scroll.frame.size.width - 40, scroll.frame.size.height / 2.1)];
	[iv3 setContentMode:UIViewContentModeScaleAspectFit];
	[iv3 setBackgroundColor:[UIColor clearColor]];
	iv3.image = [UIImage imageNamed:@"v3"];
	[v3 addSubview:iv3];
	
	
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (scrollView.tag == 3)
	{
		static NSInteger previousPage = 0;
		CGFloat pageWidth = scrollView.frame.size.width;
		float fractionalPage = scrollView.contentOffset.x / pageWidth;
		NSInteger page = lround(fractionalPage);
		if (previousPage != page)
		{
			[pageControl setCurrentPage:page];
			
			// Page has changed, do your thing!
			// ...
			// Finally, update previous page
			previousPage = page;
			if (page == 2)
			{
				[UIView animateWithDuration:0.3 animations:^{
					closeTutorial.alpha = 1;
				}];
			}
			else
			{
				closeTutorial.alpha = 0;

			}
		}
	}
	
}
-(void)quitTutorial
{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	[def setBool:true forKey:@"tutorialShowed"];
	[def synchronize];
	[UIView animateWithDuration:0.5 animations:^{
		scroll.alpha = 0;
		pageControl.alpha = 0;
		closeTutorial.alpha = 0;
	} completion:^(BOOL finished) {
		[scroll removeFromSuperview];
		[pageControl removeFromSuperview];
		[closeTutorial removeFromSuperview];
	}];
}
#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat width=rootView.frame.size.width;
	if ([[self yums] count] > 0)
	{
        CGFloat userHeaderHeight=40;
		CGFloat commentHeight = 0;
        CGFloat repostHeight=0;
        //CGFloat yumDetailHeight=50;
        CGFloat yumNameHeight=25;
        CGFloat venueNameHeight=25;
        
        CGFloat hashtagArea=22.0f;//set as 0.0
		NLYum *yum = [self.yums objectAtIndex:[indexPath row]];
		/*
		if (yum.comments.count == 1)
		{
			NSString *size = [[yum.comments firstObject] content];
			
			plusHeightComment = ((size.length >42)?(kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2):(kTImeLineCellMinimumCommentHeight));
			
		}else if(yum.comments.count == 2)
		{
			NSString *firstComment = [[yum.comments firstObject] content];
			NSString *secondComment = [[yum.comments lastObject] content];
			plusHeightComment = (firstComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight;
			plusHeightComment+=(kTimeLineCellCommentSeparation+((secondComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight));
		}else if(yum.comments.count >2)
		{
			NSString *firstComment = [[yum.comments objectAtIndex:(yum.comments.count-2)] content];
			NSString *secondComment = [[yum.comments lastObject] content];
			
			
			plusHeightComment = (kTimeLineCellCommentSeparation+((firstComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight));
			plusHeightComment+=(kTimeLineCellCommentSeparation+((secondComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight));
			plusHeightComment+=kTimeLineCellMoreCommentsButtonHeight;
		}*/
        
		
        if(yum.caption.length >42){
            //commentHeight = self.sizeForText:yum.caption;
            CGSize size= [self sizeForText:yum.caption];
            commentHeight = size.height;
            commentHeight+=3;
        }
        else{
            commentHeight=(kTImeLineCellMinimumCommentHeight);
            /*if(yum.caption.length == 0){
                commentHeight=20;
            }*/
            commentHeight=20;

        }
        CGSize dishSize = [self sizeForDish:yum.name];
        yumNameHeight = (dishSize.height)+10;
        
        if(yum.repost)
        {
            repostHeight += 36;
        }
        
        
        /*if(yum.hasHastags){
            hashtagArea=20.0f;
        }*/
        
        return userHeaderHeight+width+repostHeight+yumNameHeight+venueNameHeight+hashtagArea+commentHeight+kTimeLineViewCellBottomBarHeight;

        
        
    }else
	{
		return 220.0f;
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([indexPath row] == self.yums.count - 2)
	{
		if (![self loadingPreviousYums])
		{
			[self setLoadingPreviousYums:YES];
			
			
			//try to get previous yums
			__weak NLTimeLineViewController *weakSelf = self;
			currentTimelinePage++;
			if (currentTimelinePage <= totalTimelinePages)
			{
				[NLYum getTimelinePage:currentTimelinePage forUserId:[[NLSession currentUser] userID] withCompletionBlock:^(NSError *error, NSMutableArray *newYums, NSInteger currentPage, NSInteger totalPages)
				 {
					 if (!error)
					 {
						 currentTimelinePage = currentPage;
						 if (totalPages > 1)
						 {
							 totalTimelinePages = totalPages;
						 }
						 if ([newYums count] > 0)
						 {
							 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
								 
								 for (NLYum *yum in newYums)
								 {
									 if (![weakSelf.yums containsObject:yum])
									 {
										 CGSize textSize = [self sizeForText:yum.caption];
                                         if(yum.caption.length<=44)
                                         {
                                             textSize.height=15;
                                         }

										 [[weakSelf heights] addObject:[NSValue valueWithCGSize:textSize]];
										 [[weakSelf yums] addObject:yum];
									 }
								 }
								 [[weakSelf tableView] reloadData];
								 
								 dispatch_async(dispatch_get_main_queue(), ^
								 {
									[weakSelf setLoadingPreviousYums:NO];
								 });
							 });
						 }
					 }
					 else
					 {
						 [weakSelf setLoadingPreviousYums:NO];
					 }
				 }];
			}
		}
	}
}

#pragma mark - Calculate Cell Heights

-(CGSize)sizeForText:(NSString *)text
{
	static UIFont *cellFont;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		cellFont = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
	});
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat fixedWidth = rootView.frame.size.width-30;
    
	CGRect newframe = [text boundingRectWithSize:CGSizeMake(fixedWidth, 0.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellFont} context:nil];
	newframe.size.width = fixedWidth;

	return newframe.size;
}

- (CGSize)sizeForDish:(NSString *)text
{
    static UIFont *cellFont;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17.0f ];
    });
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat fixedWidth = rootView.frame.size.width-30;
    
    CGRect newframe = [text boundingRectWithSize:CGSizeMake(fixedWidth, 0.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellFont} context:nil];
    newframe.size.width = fixedWidth;
    
    
    return newframe.size;
}

#pragma mark - NLTimeLineViewCellDelegate

- (void)updateFavWithTag:(NSInteger)tag 
{
	if (self.yums.count > 0 )
	{
		__weak NLYum *yum = [[self yums] objectAtIndex:tag];
		__weak NLTimeLineViewController *weakSelf = self;
		
		NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:0];

			if ([CSSearchableItemAttributeSet class])
			{
				
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					NSString *venueName = [[yum venue] name];
					NSArray *keywords = [[yum.name componentsSeparatedByString:@" "] arrayByAddingObjectsFromArray:[venueName componentsSeparatedByString:@" "]];
					
					CSSearchableItemAttributeSet *cssset = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeImage];
					cssset.relatedUniqueIdentifier = [NSString stringWithFormat:@"yum-%ld",(long)yum.yumId];
					CSSearchableItem *item = [[CSSearchableItem alloc] initWithUniqueIdentifier:[NSString stringWithFormat:@"yum-%ld",(long)yum.yumId] domainIdentifier:@"com.noiselab.yummmie.like" attributeSet:cssset];
					cssset.title = yum.name;
					UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[yum thumbImage]]]];
					cssset.thumbnailData = UIImagePNGRepresentation(image);
					cssset.keywords = keywords;
					cssset.contentDescription = [NSString stringWithFormat:@"%@\n%ld user like this", yum.venue.name,(long)yum.likes];
					
					[[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler:^(NSError * _Nullable error) {
						
						if (error) {
							NSLog(@"%@",error.localizedDescription);
						}
					}];
				});
				
			}
			NSInteger yumId = yum.yumId;
			if (yum.repost)
			{
				yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
			}
			[NLYum favUnfav:yumId fav:!yum.isFav withCompletionBlock:^(NSError *error)
			 {
				 if (!error)
				 {
					 //[[weakSelf tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
					 yum.isFav = !yum.isFav;
					 if (yum.isFav)
					 {
						 yum.favs++;
						 
					 }
					 else
					 {
						 yum.favs--;
					 }
				 }
				 else
				 {
					 

				 }
				 //[[weakSelf tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

				 NSInteger counter = 0;
				 NSMutableArray *indexes = [NSMutableArray array];
				 for (NLYum *nYum in self.yums)
				 {
					 NSInteger nYumId = nYum.yumId;
					 if (nYum.repost)
					 {
						 nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
					 }
					 if (nYumId == yumId)
					 {
						 nYum.isFav = yum.isFav;
						 nYum.favs = yum.favs;
						 NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
						 [indexes addObject:nindexPath];
					 }
					 counter++;
				 }
				 [[weakSelf tableView] reloadRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationNone];

			 }];
	}
}

- (void)updateLikeWithTag:(NSInteger)tag value:(BOOL)value andNumberOfLikes:(NSInteger)number
{
	if (self.yums.count > 0 )
	{
		__weak NLYum *yum = [[self yums] objectAtIndex:tag];
		__weak NLTimeLineViewController *weakSelf = self;
		
		NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:0];
		/*if (value)
		{
			if ([CSSearchableItemAttributeSet class]) {
				
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					NSString *venueName = [[yum venue] name];
					NSArray *keywords = [[yum.name componentsSeparatedByString:@" "] arrayByAddingObjectsFromArray:[venueName componentsSeparatedByString:@" "]];
					
					CSSearchableItemAttributeSet *cssset = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeImage];
					cssset.relatedUniqueIdentifier = [NSString stringWithFormat:@"yum-%ld",(long)yum.yumId];
					CSSearchableItem *item = [[CSSearchableItem alloc] initWithUniqueIdentifier:[NSString stringWithFormat:@"yum-%ld",(long)yum.yumId] domainIdentifier:@"com.noiselab.yummmie.like" attributeSet:cssset];
					cssset.title = yum.name;
					UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[yum thumbImage]]]];
					cssset.thumbnailData = UIImagePNGRepresentation(image);
					cssset.keywords = keywords;
					cssset.contentDescription = [NSString stringWithFormat:@"%@\n%ld user like this", yum.venue.name,(long)yum.likes];
					
					[[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler:^(NSError * _Nullable error) {
						
						if (error) {
							NSLog(@"%@",error.localizedDescription);
						}
					}];
				});
				
			}
			NSInteger yumId = yum.yumId;
			if (yum.repost)
			{
				yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
			}
			yum.likes++;
			[yum setUserLikes:YES];
			[NLYum likeYum:yumId withCompletionBlock:^(NSError *error)
			{
				if (error)
				{
					yum.likes--;
					[yum setUserLikes:false];
				}
				else
				{
					
					NSInteger counter = 0;
					for (NLYum *nYum in self.yums)
					{
						NSInteger nYumId = nYum.yumId;
						if (nYum.repost)
						{
							nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
						}
						if (nYumId == yumId)
						{
							nYum.userLikes = yum.userLikes;
							nYum.likes = yum.likes;
							NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
							
							[[weakSelf tableView] reloadRowsAtIndexPaths:@[nindexPath] withRowAnimation:UITableViewRowAnimationNone];
						}
						counter++;
					}

				}
			}];
			
		}
		else
		{
			yum.likes-=1;
			[yum setUserLikes:NO];
			NSInteger yumId = yum.yumId;
			if (yum.repost)
			{
				yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
			}
			[NLYum unlikeYum:yum.yumId withCompletionBlock:^(NSError *error)
			{
				
				if (error)
				{
					yum.likes+=1;
					[yum setUserLikes:true];

				}
				else
				{
					NSInteger counter = 0;
					for (NLYum *nYum in self.yums)
					{
						NSInteger nYumId = nYum.yumId;
						if (nYum.repost)
						{
							nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
						}
						if (nYumId == yumId)
						{
							nYum.userLikes = yum.userLikes;
							nYum.likes = yum.likes;
							NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
							
							[[weakSelf tableView] reloadRowsAtIndexPaths:@[nindexPath] withRowAnimation:UITableViewRowAnimationNone];
						}
						counter++;
					}
				}
				
			}];
		}*/
		
		NSInteger yumId = yum.yumId;
		if (yum.repost)
		{
			yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
		}
		[NLYum likeUnlike:yumId like:!yum.userLikes withCompletionBlock:^(NSError *error)
		 {
			 if (!error)
			 {
				 //[[weakSelf tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
				 yum.userLikes = !yum.userLikes;
				 if (yum.userLikes)
				 {
					 yum.likes++;
					 
				 }
				 else
				 {
					 yum.likes--;
				 }
			 }
			 else
			 {
				 
				 
			 }			 
			 NSInteger counter = 0;
			 NSMutableArray *indexes = [NSMutableArray array];
			 for (NLYum *nYum in self.yums)
			 {
				 NSInteger nYumId = nYum.yumId;
				 if (nYum.repost)
				 {
					 nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
				 }
				 if (nYumId == yumId)
				 {
					 nYum.userLikes = yum.userLikes;
					 nYum.likes = yum.likes;
					 NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
						[indexes addObject:nindexPath];
				 }
				 counter++;
			 }
			 [[weakSelf tableView] reloadRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationNone];

		 }];

	}
}
-(void)confirm:(BOOL)confirmed yumId:(NSInteger)yumId andOriginalYumId:(NSInteger)originalYumId
{
	__weak NLTimeLineViewController *weakSelf = self;
	NSMutableArray *indexes = [NSMutableArray array];

	if (confirmed)
	{
		if (delete)
		{
			[NLYum destroy:yumId withCompletionBlock:^(NSError *error)
			 {
				 if (error)
				 {
					 dispatch_async(dispatch_get_main_queue(), ^{
						 [weakSelf showErrorMessage:NSLocalizedString(@"delete_yummmie_error", nil) withDuration:2.0f];
					 });
				 }
				 else
				 {
					 NSInteger counter = 0;
					 for (NLYum *nYum in self.yums)
					 {
						 NSInteger nYumId = nYum.yumId;
						 if (nYumId == yumId)
						 {
							 
							 NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
							 [indexes addObject:nindexPath];
						 }
						 counter++;
					 }
					 for (NSIndexPath *index in indexes)
					 {
						 [[weakSelf yums] removeObjectAtIndex:index.row];
						 [[weakSelf heights] removeObjectAtIndex:index.row];
					 }
					 [[weakSelf tableView] deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
					 [weakSelf updateBarButtonItems:1.0f];
				 }
			 }];
		}
		else
		{
			[NLYum repostYumId:originalYumId withCompletionBlock:^(NSError *error)
			 {
				 if (error)
				 {
					 [weakSelf showErrorMessage:NSLocalizedString(error.localizedDescription, nil) withDuration:3.0f];
				 }
				 else
				 {
					 NLRepostFeedbackView *reposted = [[NLRepostFeedbackView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andImageBase:@"anim-" count:11];
					 [self.tabBarController.view addSubview:reposted];
					 [reposted show];
					 
					 NSInteger counter = 0;
					 for (NLYum *nYum in self.yums)
					 {
						 NSInteger nYumId = nYum.yumId;
						 
						 if (nYumId == yumId || nYumId == originalYumId)
						 {
							 nYum.isReposted = true;
							 nYum.reposts = nYum.reposts + 1;
							 NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
							 
							 [[weakSelf tableView] reloadRowsAtIndexPaths:@[nindexPath] withRowAnimation:UITableViewRowAnimationNone];
						 }
						 counter++;
					 }
					 //[weakSelf reloadTimelineData];
				 }
				 
				 
				 
			 }];

		}
	}
	}
-(void)updateRepostWithTag:(NSInteger)tag andNumberOfLikes:(NSInteger)number;
{
	if (self.yums.count > 0 )
	{
		__weak NLYum *yum = [[self yums] objectAtIndex:tag];
		__weak NLTimeLineViewController *weakSelf = self;

		NSInteger yumId = yum.yumId;
		
		if (yum.isReposted)
		{
			delete = true;
			NSDictionary *dictConfirm = @
			{
				@"title": NSLocalizedString(@"repost-delete", nil),
				@"image": @"anim-11",
				@"botTitle": NSLocalizedString(@"dish_option_delete", nil)
			};
			NLConfirmView *confirm = [[NLConfirmView alloc] initWithDictionary:dictConfirm andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.view.frame.size.height)];
			confirm.alpha = 0;
			[self.tabBarController.view addSubview:confirm];
			confirm.yumId = yum.yumId;
			confirm.originalYumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
			[UIView animateWithDuration:0.3 animations:^{
				confirm.alpha = 1;
			}];
			confirm.delegate = self;
			
		
		}
		else if(yum.user.userID != [NLSession currentUser].userID && [[[yum.repost objectForKey:@"user"] objectForKey:@"id"] integerValue] != [NLSession currentUser].userID)
		{
			delete = false;
			NSDictionary *dictConfirm = @
			{
				@"title": NSLocalizedString(@"dish_option_repost", nil),
				@"image": @"anim-11",
				@"botTitle": NSLocalizedString(@"OK", nil)
			};
			NLConfirmView *confirm = [[NLConfirmView alloc] initWithDictionary:dictConfirm andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.view.frame.size.height)];
			confirm.alpha = 0;
			[self.tabBarController.view addSubview:confirm];
			
			
			confirm.yumId = yumId;
			confirm.originalYumId = yumId;
			if (yum.repost)
			{
				confirm.originalYumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];

			}
			
			//self.tableView.userInteractionEnabled = false;
			[UIView animateWithDuration:0.3 animations:^{
				confirm.alpha = 1;
			}];
			confirm.delegate = self;
		}
	}
		
}
- (void)openCommentsWithTag:(NSInteger)tag
{
	if (self.yums.count > 0)
	{
		NLYum *selectedYum = (NLYum *)[[self yums] objectAtIndex:tag];
		
		NLCommentsViewController *commentsVC = [[NLCommentsViewController alloc] initWithYum:selectedYum];
		[commentsVC setHidesBottomBarWhenPushed:YES];
		[self.navigationController pushViewController:commentsVC animated:YES];
	}
}

- (void)openOptionsWithTag:(NSInteger)tag
{
	if (self.yums.count > 0)
	{
		NLYum *yum = [[self yums] objectAtIndex:tag];
		self.currentOptionYumIndex = tag;
		
		
		UIAlertController *sheetVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
		
		
		
		NSIndexPath *currentIndexPath = [NSIndexPath indexPathForItem:self.currentOptionYumIndex inSection:0];
		
		NLYum *yumToDelete = [[self yums] objectAtIndex:self.currentOptionYumIndex];
		NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[[self tableView] cellForRowAtIndexPath:currentIndexPath];
		
		NSInteger yumId = yumToDelete.yumId;
		NSInteger originalYumId = yumToDelete.yumId;

		if (yumToDelete.repost)
		{
			originalYumId = [[yumToDelete.repost objectForKey:@"original_yum_id"] integerValue];
		}
		__weak NLTimeLineViewController *weakSelf = self;

		
		UIAlertAction *dltAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action)
		{
			//remove the element from the table, remove the size to
			NSMutableArray *indexes = [NSMutableArray array];
			//[indexes addObject:currentIndexPath];
						//call the delete api option
			[NLYum destroy:yumId withCompletionBlock:^(NSError *error)
			{
				
				if (error)
				{
					dispatch_async(dispatch_get_main_queue(), ^{
						[weakSelf showErrorMessage:NSLocalizedString(@"delete_yummmie_error", nil) withDuration:2.0f];
					});
				}
				else
				{
					NSInteger counter = 0;
					for (NLYum *nYum in self.yums)
					{
						NSInteger nYumId = nYum.yumId;
						if (nYum.repost)
						{
							nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
						}
						if (nYumId == yumId)
						{
							
							NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
							[indexes addObject:nindexPath];
						}
						counter++;
					}
					for (NSIndexPath *index in indexes)
					{
						[[weakSelf yums] removeObjectAtIndex:index.row];
						[[weakSelf heights] removeObjectAtIndex:index.row];
					}
					[[weakSelf tableView] deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
					[weakSelf updateBarButtonItems:1.0f];

				}
			}];
		}];
		UIAlertAction *reportAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_report", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action)
		{
			//call the report api option
			NSMutableArray *indexes = [NSMutableArray array];

			[NLYum reportYum:originalYumId withCompletionBlock:^(NSError *error) {
				
				dispatch_async(dispatch_get_main_queue(), ^
				{
					if (!error)
					{
						NSInteger counter = 0;
						for (NLYum *nYum in self.yums)
						{
							NSInteger nYumId = nYum.yumId;
							if (nYum.repost)
							{
								nYumId =[[nYum.repost objectForKey:@"original_yum_id"] integerValue];
							}
							if (nYumId == originalYumId)
							{
								
								NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:counter inSection:0];
								[indexes addObject:nindexPath];
							}
							counter++;
						}
						counter = 0;
						for (NSIndexPath *index in indexes)
						{
							[[weakSelf yums] removeObjectAtIndex:index.row - counter];
							[[weakSelf heights] removeObjectAtIndex:index.row];
							counter++;
						}
						[[weakSelf tableView] deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
						[weakSelf updateBarButtonItems:1.0f];
					}
					else
					{
						[weakSelf showErrorMessage:NSLocalizedString(@"update_error", nil) withDuration:3.0f];
					}
				});
			}];
			
		}];
		
		UIAlertAction *twAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_tweet", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			/*if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
			{
				SLComposeViewController *twitterShareVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
				[twitterShareVC setInitialText:[NSString stringWithFormat:@"%@ %@ %@ @%@ %@ (%@)",[yumToDelete name],[yumToDelete caption], NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil), yumToDelete.shareUrl]];
				UIImage *yumImage = [[cell dishPhoto] image];
				[twitterShareVC addImage:yumImage];
				[self presentViewController:twitterShareVC animated:YES completion:nil];
			}else
			{
				[self showErrorMessage:NSLocalizedString(@"twitter_not_setup", nil) withDuration:2.0f];
			}*/
            
            //---
            if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
                //TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
                NSString *initialText = [NSString stringWithFormat:@"%@ %@ %@ @%@ %@ (%@)",[yumToDelete name], [yumToDelete caption], NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil), [yumToDelete shareUrl]];
                TWTRComposerViewController *composer = [[TWTRComposerViewController alloc] initWithInitialText:initialText image:[[cell dishPhoto] image] videoURL:nil];
                
                [self presentViewController:composer animated:YES completion:nil];
            } else {
                [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
                    if (session) {
                        TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
                        [self presentViewController:composer animated:YES completion:nil];
                    } else {
                        [self showErrorMessage:NSLocalizedString(@"twitter_not_setup", nil) withDuration:2.0f];
                    }
                }];
            }
            //---

            
            
		}];
		
        UIAlertAction *fbAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_facebook", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
            content.contentURL = [NSURL URLWithString:yumToDelete.shareUrl];
            [content setContentTitle:[NSString stringWithFormat:@"%@ %@ %@ @%@ %@",[yumToDelete name],[yumToDelete caption],NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil)]];
            FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
            dialog.fromViewController = self;
            [dialog setShareContent:content];
            [dialog setDelegate:self];
            
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]]){
                dialog.mode = FBSDKShareDialogModeNative;
            }
            else {
                //dialog.mode = FBSDKShareDialogModeFeedWeb; //opt1
                dialog.mode = FBSDKShareDialogModeFeedBrowser; //opt2
            }

            
            if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
                
                [dialog show];
                
                
            } else {
                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                [loginManager logInWithPublishPermissions:@[@"publish_actions"]
                                       fromViewController:self
                                                  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                                      //TODO: process error or result.
                                                      
                                                      if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
                                                          
                                                          [dialog show];
                                                          
                                                          
                                                      } else {
                                                          [self showErrorMessage:NSLocalizedString(@"facebook_not_setup", nil) withDuration:2.0f];
                                                      }

                                                  }];
            }
        }];

		
		UIAlertAction *igAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_instagram", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
		{
			if ([MGInstagram isAppInstalled])
			{
				UIWindow *window = [[UIApplication sharedApplication] keyWindow];
				UIGraphicsBeginImageContext(CGSizeMake(window.frame.size.width, window.frame.size.height));
				[window drawViewHierarchyInRect:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height) afterScreenUpdates:YES];
				UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				//[self instaGramWallPost:image];

				self.instagramReminder = [[NLInstagramReminderView alloc] initWithImage:image andYumImageURL:[yumToDelete thumbImage]];
				self.instagramReminder.center = self.view.superview.center;
				[self.instagramReminder setAlpha:0.0f];
				[self.tabBarController.view addSubview:[self instagramReminder]];
				[self.instagramReminder setDelegate:self];
				
				__weak NLTimeLineViewController *weakSelf = self;
				
				[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
					[weakSelf.instagramReminder setAlpha:1.0f];
				} completion:nil];
			}else{
				[self showErrorMessage:NSLocalizedString(@"instagram_not_installed", nil) withDuration:2.0f];
			}
		}];
		
		UIAlertAction *waAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_whatsapp", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			NSLog(@"%@", yum.shareUrl);
			NSURL *whatsappURL = [NSURL URLWithString:[[[NSString stringWithFormat:@"whatsapp://send?text=%@ %@ %@ @%@ %@ %@",[yumToDelete name],[yumToDelete caption], NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil), [yumToDelete shareUrl]]  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"&" withString:@"and"]];
			if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
    [[UIApplication sharedApplication] openURL: whatsappURL];
			}
		}];
		UIAlertAction *saveAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"save_option", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
		{
			ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
			[library saveImage:[[cell dishPhoto] image] toAlbum:kYummmieGalleryName completion:^(NSURL *assetURL, NSError *error) {
			} failure:^(NSError *error) {
			}];
		}];
		
		UIAlertAction *editAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_edit", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
		{
			if ([NLSession isCurrentUser:yumToDelete.user.userID])
			{
				NLEditViewController *eVC = [[NLEditViewController alloc] initWithYum:yumToDelete];
				[eVC setHidesBottomBarWhenPushed:YES];
				
				[[self navigationController] pushViewController:eVC animated:YES];
			}
		}];
		/*UIAlertAction *repostAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_repost", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
		{
			[NLYum repostYumId:originalYumId withCompletionBlock:^(NSError *error)
			{
				if(!error)
				{
					NLRepostFeedbackView *reposted = [[NLRepostFeedbackView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andImageBase:@"anim-" count:11];
					[self.tabBarController.view addSubview:reposted];
					[reposted show];
								}
				else
				{
					NSLog(@"%@", error.localizedDescription);
					[weakSelf showErrorMessage:NSLocalizedString(error.localizedDescription, nil) withDuration:3.0f];

				}
			}];
		}];*/
		
		UIAlertAction *cancelAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
		{
			
		}];
		if ([NLSession isCurrentUser:yum.user.userID])
		{
			[sheetVc addAction:igAct];
			[sheetVc addAction:fbAct];

			[sheetVc addAction:twAct];
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp:"]])
            {
                [sheetVc addAction:waAct];
            }
			[sheetVc addAction:saveAct];
			if (!yum.repost)
			{
				[sheetVc addAction:editAct];
				[sheetVc addAction:dltAct];

			}
		
			
			[sheetVc addAction:cancelAct];
		}
		else
		{
			//[sheetVc addAction:repostAct];
			[sheetVc addAction:igAct];
			[sheetVc addAction:fbAct];
			[sheetVc addAction:twAct];
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp:"]])
            {
                [sheetVc addAction:waAct];
            }
			[sheetVc addAction:saveAct];
			if ([NLSession isCurrentUser:[[[yum.repost objectForKey:@"user"] objectForKey:@"id"] integerValue]])
			{
				[sheetVc addAction:dltAct];
				
			}
			else
			{
				[sheetVc addAction:reportAct];
				
			}
			[sheetVc addAction:cancelAct];
		}
		

		[self presentViewController:sheetVc animated:true completion:nil];
	}
}

-(void)openPhotoWithTag:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:0];
		NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[[self tableView] cellForRowAtIndexPath:indexPath];
		NLYum *yummmie = [[self yums] objectAtIndex:tag];
		TGRImageViewController *imageVC = [[TGRImageViewController alloc] initWithImage:cell.dishPhoto.image andYummmie:yummmie];
		self.photoAnimationController = [[TGRImageZoomAnimationController alloc] initWithReferenceImageView:cell.dishPhoto];
		imageVC.transitioningDelegate = self;
		[self presentViewController:imageVC animated:YES completion:nil];
	}
}

- (void)openProfileWithTag:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		NLUser *user = (NLUser *)[[[self yums] objectAtIndex:tag] user];
		BOOL isMainUser = [NLSession isCurrentUser:[user userID]];
		
	
		NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUsername:user.username];
		[profileVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:profileVC animated:YES];
		
	}
}

- (void)openDishWithTag:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		NSString *dishName = [[[self yums] objectAtIndex:tag] name];
		NLDishViewController *dishVC = [[NLDishViewController alloc] initWithDishName:dishName];
		[dishVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:dishVC animated:YES];
	}
}

- (void)openVenueWithTag:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		[NLVenue getVenueWithId:[[[[self yums] objectAtIndex:tag] venue] venueId] withCompletionBlock:^(NSError *error, NLVenue *venue) {
			NLVenueViewController *venueVC = [[NLVenueViewController alloc] initWithVenue:venue];
			//        NLVenueDetailViewController *detailVC = [[NLVenueDetailViewController alloc] initWithVenue:selectedVenue];
			[venueVC setHidesBottomBarWhenPushed:YES];
			[[self navigationController] pushViewController:venueVC animated:YES];

		}];
	}
}

- (void)openHashTag:(NSString *)hashtag withTag:(NSInteger)tag
{
	if (self.yums.count > 0)
	{
		NLYum *yum = [[self yums] objectAtIndex:tag];
		NLTag *selectedTag;
		
		if ([[yum tags] count] == 1)
		{
			selectedTag = [[yum tags] lastObject];
		}else
		{
			for (NLTag *tag in [yum tags])
			{
				if ([tag isTag:hashtag])
				{
					selectedTag = tag;
					break;
				}
			}
		}
		
		NLHashtagViewController *hashtagVC;
		
		if (selectedTag)
		{
			hashtagVC = [[NLHashtagViewController alloc] initWithTag:selectedTag andNumberOfYums:[selectedTag numberOfYums]];
		}else
		{
			hashtagVC = [[NLHashtagViewController alloc] initWithHashtag:hashtag];
		}
		
		[hashtagVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:hashtagVC animated:YES];
		
	}
}

- (void)openUser:(NSString *)user
{
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUsername:user];
	if ([user isEqualToString:[[NLSession currentUser] username]])
	{
		
	}
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
}

- (void)openLikers:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		NLYum *selectedYum = [[self yums] objectAtIndex:tag];
		
		NLLikersViewController *likersVC = [[NLLikersViewController alloc] initWithYum:selectedYum andOption:1];
		[likersVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:likersVC animated:YES];
	}
}
- (void)openFavers:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		NLYum *selectedYum = [[self yums] objectAtIndex:tag];
		
		NLLikersViewController *likersVC = [[NLLikersViewController alloc] initWithYum:selectedYum andOption:2];
		[likersVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:likersVC animated:YES];
	}
}
- (void)openReposters:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		NLYum *selectedYum = [[self yums] objectAtIndex:tag];
		
		NLLikersViewController *likersVC = [[NLLikersViewController alloc] initWithYum:selectedYum andOption:3];
		[likersVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:likersVC animated:YES];
	}
}
- (void)openFirstCommentUser:(NSInteger)tag
{
	if (self.yums.count >  0)
	{
		NSArray *comments = [[[self yums] objectAtIndex:tag] comments];
		
		NLUser *user;
		if (comments.count >1)
		{
			NLComment *comment = [comments objectAtIndex:comments.count-2];
			user = [comment user];
			
		}else
		{
			NLComment *comment = [comments lastObject];
			user = [comment user];
		}
		
		NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:user];
		[profileVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:profileVC animated:YES];
		
	}
}

-(void)openSecondCommentUser:(NSInteger)tag
{
	if (self.yums.count > 0 )
	{
		NSArray *comments = [[[self yums] objectAtIndex:tag] comments];
		
		NLUser *user = [(NLComment *)[comments lastObject] user];
		NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:user];
		[profileVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:profileVC animated:YES];
	}
}
- (void)openFirstCommentHashTag:(NSInteger)tag andHashtag:(NSString *)hashtag
{
	if (self.yums.count > 0)
	{
		NLYum *yum = [[self yums] objectAtIndex:tag];
		NSArray *tags = [(NLYum *)[[yum comments] firstObject] tags];
		
		NLTag *selectedTag;
		
		if ([tags count] == 1)
		{
			selectedTag  = [tags lastObject];
		}else
		{
			for (NLTag *tag in tags)
			{
				if ([tag isTag:hashtag])
				{
					selectedTag = tag;
					break;
				}
			}
		}
		
		NLHashtagViewController *hashtagVC;
		
		if (selectedTag)
		{
			hashtagVC = [[NLHashtagViewController alloc] initWithTag:selectedTag andNumberOfYums:[selectedTag numberOfYums]];
			[[self navigationController] pushViewController:hashtagVC animated:YES];
		}
	}
}

- (void)openSecondCommentHashTag:(NSInteger)tag andHashtag:(NSString *)hashtag{
	if (self.yums.count > 0)
	{
		NLYum *yum = [[self yums] objectAtIndex:tag];
		NSArray *tags = [(NLYum *)[[yum comments] firstObject] tags];
		
		NLTag *selectedTag;
		
		if ([tags count] == 1)
		{
			selectedTag  = [tags lastObject];
		}else
		{
			for (NLTag *tag in tags)
			{
				if ([tag isTag:hashtag])
				{
					selectedTag = tag;
					break;
				}
			}
		}
		
		NLHashtagViewController *hashtagVC;
		
		if (selectedTag)
		{
			hashtagVC = [[NLHashtagViewController alloc] initWithTag:selectedTag andNumberOfYums:[selectedTag numberOfYums]];
			[[self navigationController] pushViewController:hashtagVC animated:YES];
		}
	}
}
- (void)openRecommender:(NSInteger)tag
{
	if (self.yums.count > 0)
	{
		NLYum *selectedYum = (NLYum *)[[self yums] objectAtIndex:tag];
		NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUserId:[[[selectedYum.repost objectForKey:@"user"] objectForKey:@"id"] integerValue] andCloseButton:false];
		[profileVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:profileVC animated:YES];
	}
}
#pragma mark - UIViewControllerTransitionDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
	if ([presented isKindOfClass:[TGRImageViewController class]])
	{
		return self.photoAnimationController;
	}else
	{
		return nil;
	}
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
	
	if ([dismissed isKindOfClass:[TGRImageViewController class]])
	{
		return self.photoAnimationController;
	}else
	{
		return nil;
	}
}


#pragma mark - Location Permissions

- (void)askForLocationPermissions
{
	if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] ==  kCLAuthorizationStatusNotDetermined)
	{
		self.locationManager = [[CLLocationManager alloc] init];
		if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
		{
			[self.locationManager requestWhenInUseAuthorization];
		}
		[self.locationManager startUpdatingLocation];
		[self.locationManager stopUpdatingLocation];
		
		__weak NLTimeLineViewController *weakSelf;
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			[weakSelf setLocationManager:nil];
		});
	}
}

#pragma mark - NLSocialMediaAlertViewDelegate

- (void)closeView:(NLSocialMediaAlertView *)view
{
	__weak NLTimeLineViewController *weakSelf;
	[view hide:^{
		[[weakSelf socialAlert] removeFromSuperview];
		[weakSelf setSocialAlert:nil];
	}];
}

- (void)stopShowinView:(NLSocialMediaAlertView *)view
{
	__weak NLTimeLineViewController *weakSelf;
	
	[view hide:^{
		[[weakSelf socialAlert] removeFromSuperview];
		[weakSelf setSocialAlert:nil];
	}];
	[NLPreferences dontShowSocialReminderAnymore];
}

- (void)setupFacebookAccount:(NLSocialMediaAlertView *)view
{
	//__weak NLTimeLineViewController *weakSelf = self;
#warning Poner nuevo codigo de Facebook.
/*
	[FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"user_friends",@"email"] allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
		if (!error)
		{
			//guardarla y crearla
			[[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
				if (!error)
				{
					NSString *userId = [result objectForKey:@"id"];
					
					[NLAuthentication addAuthenticationWithType:NLAuthenticationTypeFacebook userID:userId token:[[[FBSession activeSession] accessTokenData] accessToken] tokenSecret:nil withCompletionBlock:^(NSError *error, NLAuthentication *authentication) {
						if (!error)
						{
							[[[NLSession currentUser] authentications] addObject:authentication];
							
							dispatch_async(dispatch_get_main_queue(), ^{
								NLSearchFriendsViewController *searchVC = [[NLSearchFriendsViewController alloc] initWithSkipButton:YES];
								NLNavigationViewController *navViewController = [[NLNavigationViewController alloc] initWithRootViewController:searchVC];
								[self presentViewController:navViewController animated:YES completion:^{
									[view hide:^{
										[[weakSelf socialAlert] removeFromSuperview];
										[weakSelf setSocialAlert:nil];
									}];
								}];
							});
							
						}else
						{
							[weakSelf showErrorMessage:NSLocalizedString(@"network_connection_error", nil) withDuration:3.0f];
						}
					}];
				}else
				{
					[weakSelf hideLoadingSpinner];
					[weakSelf showErrorMessage:NSLocalizedString(@"facebook_link_error_text", nil) withDuration:3.0f];
					dispatch_async(dispatch_get_main_queue(), ^{
					});
				}
			}];
		}
	}];
 */
}

#pragma mark - NLInstagramViewDelegate

- (void)instagramViewDismissed:(NLInstagramReminderView *)instagramView
{
	__weak NLTimeLineViewController *weakSelf = self;
	
	[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^
	{
		[[weakSelf instagramReminder] setAlpha:0.0f];
	} completion:^(BOOL finished)
	{
		[[weakSelf instagramReminder] removeFromSuperview];
		[weakSelf setInstagramReminder:nil];
		
		if (weakSelf.presentInstagramOption) {
			weakSelf.currentOptionYumIndex = 0;
		}
		
		NSIndexPath *currentIndexPath = [NSIndexPath indexPathForItem:weakSelf.currentOptionYumIndex inSection:0];
		
		NLYum *yum = [[weakSelf yums] objectAtIndex:weakSelf.currentOptionYumIndex];
		NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[[weakSelf tableView] cellForRowAtIndexPath:currentIndexPath];
		UIImage *yumImage = [[cell dishPhoto] image];;
		
		NSString *shareString;
		
		if([NLSession isCurrentUser:[[yum user] userID]]){
			shareString = [NSString stringWithFormat:@"%@ %@ %@",[yum name],[yum caption],NSLocalizedString(@"share_default_text", nil)];
		}else{
			shareString = [NSString stringWithFormat:@"%@ %@ %@ @%@ %@",[yum name],[yum caption],NSLocalizedString(@"author_by_text", nil),[[yum user] username],NSLocalizedString(@"share_default_text", nil)];
		}
		
		UIPasteboard *generalPB = [UIPasteboard generalPasteboard];
		generalPB.string = shareString;
		if ([MGInstagram isAppInstalled])
		{
			[self instaGramWallPost:yumImage];
		}
		else {
			NSLog(@"Error Instagram is either not installed or image is incorrect size");
		}
		if (weakSelf.presentInstagramOption) {
			weakSelf.presentInstagramOption = NO;
		}
	}];
}
-(void)instaGramWallPost:(UIImage *)image
{
	NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
	if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
	{
		NSString *documentDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
		// *.igo is exclusive to instagram
		NSString *saveImagePath = [documentDirectory stringByAppendingPathComponent:@"Image.igo"];
		NSData *imageData = UIImagePNGRepresentation(image);
		[imageData writeToFile:saveImagePath atomically:YES];
		
		NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
		
		self.documentController=[[UIDocumentInteractionController alloc] init];
		self.documentController.delegate=self;
		self.documentController.UTI=@"com.instagram.exclusivegram";
		[self.documentController setURL:imageURL];
		self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:@"#yourHashTagGoesHere", @"InstagramCaption", nil];
		[self.documentController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
	}

	else
	{
		NSLog (@"Instagram not found");
	}
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate
{
	NSLog(@"file url %@",fileURL);
	UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
	interactionController.delegate = interactionDelegate;
	
	return interactionController;
}
-(void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
	[self setNeedsStatusBarAppearanceUpdate];

}
-(void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
	[self setNeedsStatusBarAppearanceUpdate];

}

-(void)markViewedYumId:(NSInteger)yumId
{
    NSMutableArray *viewedIds = [[[NSUserDefaults standardUserDefaults] objectForKey:@"viewed"] mutableCopy];
    if (!viewedIds)
    {
        viewedIds = [NSMutableArray array];
    }
    BOOL present = false;
    for (NSNumber *viewedId in viewedIds)
    {
        if (viewedId.integerValue == yumId)
        {
            present = true;
        }
    }
    if (!present)
    {
        [viewedIds addObject:[NSNumber numberWithInteger:yumId]];
    }
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:viewedIds forKey:@"viewed"];
    [def synchronize];

}
#pragma mark - FBSDKSharingDelegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults :(NSDictionary *)results {
    NSLog(@"FB: SHARE RESULTS=%@\n",[results debugDescription]);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"FB: ERROR=%@\n",[error debugDescription]);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"FB: CANCELED SHARER=%@\n",[sharer debugDescription]);
}
@end
