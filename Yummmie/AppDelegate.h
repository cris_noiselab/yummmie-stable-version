//
//  AppDelegate.h
//  Yummmie
//
//  Created by bot on 5/30/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iRate.h"
#import <OneSignal/OneSignal.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate, iRateDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) OneSignal *oneSignal;

@end
