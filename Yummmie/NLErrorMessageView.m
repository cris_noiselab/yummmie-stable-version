//
//  NLErrorMessageView.m
//  Yummmie
//
//  Created by bot on 6/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLErrorMessageView.h"

const float kErrorViewWidth = 320.0f;
const float kErrorViewHeight = 1500.0f;

const float kErrorLabelWidth = 280.0f;
const float kErrorLabelHeight = 25.0f;

const float kInitialMessageHeight = 0.0f;
const float kFinalMessageHeight = 100.0f;

const float kErrorViewAnimationShowDuration = 0.25f;
const float kErrorViewAnimationHideDuration = 0.25f;
@interface NLErrorMessageView ()

@property (nonatomic, strong) UILabel *errorLabel;
@property (nonatomic, getter = isVisible) BOOL visible;

@end

@implementation NLErrorMessageView
{
    UIView *messagesView;
    UIImageView *errorImage;
    UILabel *pleaseTAM;
}
- (id)init
{
    if (self = [super init])
    {
        /*
        CGRect viewFrame = CGRectMake(0, -kErrorViewHeight, kErrorViewWidth, kErrorViewHeight);
        [self setFrame:viewFrame];
        _errorLabel = [[UILabel alloc] initWithFrame:CGRectMake((kErrorViewWidth-kErrorLabelWidth)/2.0f, (kErrorViewHeight-kErrorLabelHeight)/2.0f, kErrorLabelWidth, kErrorLabelHeight)];
        [_errorLabel setTextColor:[UIColor whiteColor]];
        [_errorLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
        [_errorLabel setMinimumScaleFactor:0.6f];
        [_errorLabel setAdjustsFontSizeToFitWidth:YES];
        [_errorLabel setNumberOfLines:1];
        [_errorLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_errorLabel];
        [self setBackgroundColor:[UIColor blackColor]];
        _visible = NO;
        [self setAlpha:0.0f];*/
        UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;

        CGRect viewFrame = CGRectMake(0, 0, rootView.frame.size.width, kErrorViewHeight);
        [self setFrame:viewFrame];
        messagesView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, rootView.frame.size.width, kFinalMessageHeight)];
        messagesView.backgroundColor = [UIColor whiteColor];
        [self addSubview:messagesView];
        _errorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0, kErrorLabelWidth, kErrorLabelHeight)];
        _errorLabel.center = CGPointMake(messagesView.frame.size.width / 2.0, (messagesView.frame.size.height / 2) - 15.0);
        [_errorLabel setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
        [_errorLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f]];
        [_errorLabel setMinimumScaleFactor:0.6f];
        [_errorLabel setAdjustsFontSizeToFitWidth:YES];
        [_errorLabel setNumberOfLines:2];
        [_errorLabel setTextAlignment:NSTextAlignmentCenter];
        [messagesView addSubview:_errorLabel];
        
        pleaseTAM = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0, kErrorLabelWidth, kErrorLabelHeight)];
        pleaseTAM.center = CGPointMake(messagesView.frame.size.width / 2.0, messagesView.frame.size.height / 2 + 20);
        [pleaseTAM setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
        [pleaseTAM setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f]];
        [pleaseTAM setMinimumScaleFactor:0.6f];
        [pleaseTAM setAdjustsFontSizeToFitWidth:YES];
        [pleaseTAM setNumberOfLines:1];
        [pleaseTAM setTextAlignment:NSTextAlignmentCenter];
        pleaseTAM.text = NSLocalizedString(@"Please try again", @"Please try again");
        [messagesView addSubview:pleaseTAM];
        
        errorImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_alerta"]];
        errorImage.center = CGPointMake(messagesView.frame.size.width / 2.0, messagesView.frame.origin.y);
        [self addSubview:errorImage];
        
    
        [self setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.65]];
        _visible = NO;
        [self setAlpha:0.0f];
        messagesView.alpha = 1;
        errorImage.alpha = 0;
        _errorLabel.alpha = 0;
        pleaseTAM.alpha = 0;
        
    }
    return self;
}


- (void)showWithMessage:(NSString *)errorMessage andDuration:(float)seconds
{
    __weak NLErrorMessageView *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.errorLabel setText:errorMessage];
        [weakSelf.superview bringSubviewToFront:self];
        if (![weakSelf isVisible])
        {//
            messagesView.transform = CGAffineTransformMakeScale(1, 0);
            errorImage.transform = CGAffineTransformMakeScale(1, 0);

            [UIView animateWithDuration:kErrorViewAnimationShowDuration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^
            {
                /*
                CGRect originRect = [weakSelf bounds];
                [weakSelf setFrame:originRect];
                [weakSelf setAlpha:1.0f];
                 */
                self.alpha = 1;
                errorImage.alpha = 1;
                errorImage.transform = CGAffineTransformIdentity;

                
            } completion:^(BOOL finished)
             {
                 [UIView animateWithDuration:kErrorViewAnimationShowDuration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^
                 {
                     messagesView.transform = CGAffineTransformIdentity;
                     _errorLabel.center = CGPointMake(_errorLabel.center.x, _errorLabel.center.y + 10);

                     _errorLabel.alpha = 1;
                 } completion:^(BOOL finished)
                  {
                      [UIView animateWithDuration:kErrorViewAnimationShowDuration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^
                      {
                          
                          pleaseTAM.alpha = 1;
                          pleaseTAM.center = CGPointMake(pleaseTAM.center.x, pleaseTAM.center.y + 5);
                      } completion:^(BOOL finished)
                       {
                           [weakSelf setVisible:YES];
                           [UIView animateWithDuration:kErrorViewAnimationHideDuration delay:seconds options:UIViewAnimationOptionCurveEaseOut animations:^{
                               errorImage.alpha = 0;
                               errorImage.center = CGPointMake(errorImage.center.x, errorImage.center.y - 20);
                               _errorLabel.alpha = 0;
                               _errorLabel.center = CGPointMake(_errorLabel.center.x, _errorLabel.center.y - 10);

                               pleaseTAM.alpha = 0;
                               pleaseTAM.center = CGPointMake(pleaseTAM.center.x, pleaseTAM.center.y - 5);

                               //CGRect nFrame = messagesView.frame;
                               //nFrame.size.height = 0;
                               //messagesView.frame = nFrame;
                               
                               
                           } completion:^(BOOL finished)
                            {
                                [UIView animateWithDuration:kErrorViewAnimationHideDuration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^
                                 {
                                     messagesView.transform = CGAffineTransformMakeScale(1, 0.0001);
                                 } completion:^(BOOL finished)
                                 {
                                     [UIView animateWithDuration:kErrorViewAnimationHideDuration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^
                                      {
                                          [weakSelf setAlpha:0.0f];

                                      } completion:^(BOOL finished)
                                      {
                                          [weakSelf setVisible:NO];
                                          
                                          [weakSelf setAlpha:0.0f];
                                          errorImage.center = CGPointMake(errorImage.center.x, errorImage.center.y + 20);

                                      }];
                                 }];
                                
                            }];
                       }];
                      
                  }];
                 /*
                 [weakSelf setVisible:YES];
                 [UIView animateWithDuration:kErrorViewAnimationHideDuration delay:seconds options:UIViewAnimationOptionCurveEaseOut animations:^{
                     
                     CGRect hideViewRect = [weakSelf bounds];
                     hideViewRect.origin.y = -kErrorViewHeight;
                     [weakSelf setFrame:hideViewRect];
                     
                 } completion:^(BOOL finished)
                  {
                      [weakSelf setVisible:NO];
                      [weakSelf setAlpha:0.0f];
                  }];*/
             }];
        }
    });
}

- (void)hideWithoutAnimation
{
    CGRect hideViewRect = self.bounds;
    hideViewRect.origin.y = -kErrorViewHeight;
    [self setFrame:hideViewRect];
    [self setAlpha:0.0f];
}
@end
