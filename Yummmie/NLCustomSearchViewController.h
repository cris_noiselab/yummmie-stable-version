//
//  NLCustomSearchViewController.h
//  Yummmie
//
//  Created by bot on 8/5/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "FSVenue.h"

@class NLVenue;

@protocol NLCustomSearchDelegate <NSObject>

- (void)didSelectVenue:(NLVenue *)foursquareVenue andIndexPath:(NSIndexPath *)indexPath;


@end

@interface NLCustomSearchViewController : NLBaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, weak) id<NLCustomSearchDelegate>delegate;

- (id)initWithSearchText:(NSString *)searchText andLocation:(CLLocation *)location;

@end
