//
//  NLTableViewController.m
//  Yummmie
//
//  Created by bot on 6/16/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLTableViewController.h"
#import "NLErrorMessageView.h"
#import "UIColor+NLColor.h"
#import "NLConstants.h"
#import "NLSession.h"
#import "AppDelegate.h"
@interface NLTableViewController ()<UIAlertViewDelegate>

@property (nonatomic) CGFloat previousScrollViewYOffset;
@property (nonatomic, strong, readwrite) NLErrorMessageView *errorMessageView;
@property (nonatomic, strong) UIView *loadingHud;
@property (nonatomic, strong) UIImageView *animationImage;
@property (nonatomic, getter=isActiveView) BOOL activeView;


@end

@implementation NLTableViewController
{
    UIBarButtonItem *loadingSpinner;
	UIActivityIndicatorView *spinner;
}
- (id)initWithScrollableBar:(BOOL)scrollable
{
    self = [super init];
    if (self)
    {
        // Custom initialization
        _scrollableBar = scrollable;
        self.title = @"";        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.

    //setup the Navigation Bar
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    UIColor *barColor = [UIColor nl_colorWith255Red:205.0f green:36.0f blue:49.0f andAlpha:255.0f];
    [[[self navigationController] navigationBar] setBarTintColor:barColor];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
    [[[self navigationController] navigationBar] setBarStyle:UIBarStyleBlack];

    //setup the Error Message View
    _errorMessageView = [[NLErrorMessageView alloc] init];
    [[self.parentViewController view] addSubview:self.errorMessageView];
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    if ([self navigationItem].leftBarButtonItem == nil)
    {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        //self.navigationItem.backBarButtonItem =
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
    
    
}
/*- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}*/
-(void)viewWillDisappear:(BOOL)animated
{
	//[self animateNavBarTo:20];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setActiveView:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self setActiveView:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers

- (void)addRedBackground
{
    [[self view] setBackgroundColor:[UIColor nl_colorWith255Red:205.0f green:36.0f blue:49.0f andAlpha:255.0f]];
}

- (void)showLoadingSpinner
{
    
		spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [spinner startAnimating];
    loadingSpinner = [[UIBarButtonItem alloc] initWithCustomView:spinner];
    self.navigationItem.leftItemsSupplementBackButton = YES;
	
    self.navigationItem.leftBarButtonItems = @[loadingSpinner];

  }

- (void)hideLoadingSpinner
{
    
	[UIView animateWithDuration:0.25 animations:^
	{
		[spinner stopAnimating];
		spinner.alpha = 0;
		
		//reduce alpha
	} completion:^(BOOL finished)
	{
		//self.navigationItem.leftBarButtonItems = nil;

	}];
    
}

#pragma mark - Error Message Methods

-(void)showErrorMessage:(NSString *)message withDuration:(float)seconds
{
    [[self errorMessageView] showWithMessage:message andDuration:seconds];
}

- (void)showErrorMessage:(NSString *)message popViewController:(BOOL)pop withDuration:(float)seconds
{
    [[self errorMessageView] showWithMessage:message andDuration:seconds];
    
    __weak NLTableViewController *weakSelf = self;
    
    if (self.navigationController && pop)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
        });
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if ([self barShouldScroll] && [self isActiveView])
    {
        CGRect frame = self.navigationController.navigationBar.frame;
        CGFloat size = frame.size.height - 21;
        CGFloat framePercentageHidden = ((20 - frame.origin.y) / (frame.size.height - 1));
        CGFloat scrollOffset = scrollView.contentOffset.y;
        CGFloat scrollDiff = scrollOffset - self.previousScrollViewYOffset;
        CGFloat scrollHeight = scrollView.frame.size.height;
        CGFloat scrollContentSizeHeight = scrollView.contentSize.height + scrollView.contentInset.bottom;
        
        if (scrollOffset <= -scrollView.contentInset.top)
        {
            frame.origin.y = 20;
        } else if ((scrollOffset + scrollHeight) >= scrollContentSizeHeight) {
            frame.origin.y = -size;
        } else {
            frame.origin.y = MIN(20, MAX(-size, frame.origin.y - scrollDiff));
        }
        
        [self.navigationController.navigationBar setFrame:frame];
        [self updateBarButtonItems:(1 - framePercentageHidden)];
        self.previousScrollViewYOffset = scrollOffset;
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([self barShouldScroll])
    {
        [self stoppedScrolling];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    if ([self barShouldScroll])
    {
        if (!decelerate)
        {
            [self stoppedScrolling];
        }
    }
}

#pragma mark - Navigation Scroll Helpers
- (void)stoppedScrolling
{
    CGRect frame = self.navigationController.navigationBar.frame;
    if (frame.origin.y < 20)
    {
        [self animateNavBarTo:-(frame.size.height - 21)];
    }
}

- (void)updateBarButtonItems:(CGFloat)alpha
{
    
    [self.navigationItem.leftBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
        item.customView.alpha = alpha;
    }];
    [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
        item.customView.alpha = alpha;
    }];
    self.navigationItem.titleView.alpha = alpha;
    self.navigationController.navigationBar.tintColor = [self.navigationController.navigationBar.tintColor colorWithAlphaComponent:alpha];
}

- (void)animateNavBarTo:(CGFloat)yPosition
{
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.navigationController.navigationBar.frame;
        CGFloat alpha = (frame.origin.y >= yPosition ? 0 : 1);
        frame.origin.y = yPosition;
        [self.navigationController.navigationBar setFrame:frame];
        [self updateBarButtonItems:alpha];
    }];
}



#pragma mark - Scroll To Top

- (void)scrollToTop
{
    CGPoint offset = self.tableView.contentOffset;
    
    if (offset.y > -64.0f)
    {
        [self.tableView setContentOffset:offset animated:NO];
        CGRect frame = self.navigationController.navigationBar.frame;
        frame.origin.y = 20.0f;
        [self.navigationController.navigationBar setFrame:frame];
        self.navigationItem.titleView.alpha  = 1.0;
        
        CGPoint topOffset = CGPointMake(0, -64);
        
        [[self tableView] setContentOffset:topOffset animated:YES];
    }
    
}

@end