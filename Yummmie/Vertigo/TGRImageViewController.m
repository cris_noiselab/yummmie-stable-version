// TGRImageZoomTransition.m
//
// Copyright (c) 2013 Guillermo Gonzalez
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "TGRImageViewController.h"
#import "NLYum.h"
#import "NLVenue.h"
#import <SDWebImage/UIImageView+WebCache.h>

const CGFloat alphaDiff = 0.1f;

@interface TGRImageViewController ()<UIScrollViewDelegate>

@property (strong, nonatomic) NLYum *yummmie;

@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *singleTapGestureRecognizer;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *doubleTapGestureRecognizer;

@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *venueButton;
@property (weak, nonatomic) IBOutlet UIButton *dishButton;
@property (weak, nonatomic) IBOutlet UIButton *numberOfLikesButton;
@property (strong, nonatomic) UIImage *downloadedImage;
@property (copy, nonatomic) NSString *imageURL;

@property (nonatomic,getter = isDataHidden) BOOL dataHidden;

- (void)hideData;
- (void)showData;

@end

@implementation TGRImageViewController


- (id)initWithImage:(UIImage *)image andYummmie:(NLYum *)yummmie{
    if (self = [super init])
    {
        _image = image;
        _yummmie = yummmie;
        _dataHidden = NO;
    }
    
    return self;
}

- (id)initWithImage:(UIImage *)image andBigImageURL:(NSString *)imageURL
{
    
    if (self = [super init])
    {
        _image = image;
        _imageURL = imageURL;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    
    
    [self.scrollView setFrame:CGRectMake(self.scrollView.frame.origin.x, self.scrollView.frame.origin.y, rootView.frame.size.width, rootView.frame.size.height)];
    
        [self.imageView setFrame:CGRectMake(0, 0, rootView.frame.size.width, rootView.frame.size.height)];
        [self.imageView setCenter:CGPointMake(rootView.center.x, rootView.center.y)];
        
    
        [self.dishButton.titleLabel setNumberOfLines:0];
        [self.dishButton.titleLabel setLineBreakMode:UILineBreakModeWordWrap];
    
    self.imageView.image = self.image;
    
    [self.singleTapGestureRecognizer requireGestureRecognizerToFail:self.doubleTapGestureRecognizer];
    if ([self yummmie]){
        self.venueButton.titleEdgeInsets = UIEdgeInsetsMake(1.0f, 5.0f, 0.0f, 0.0f);
        
        [self.venueButton setTitle:self.yummmie.venue.name forState:UIControlStateNormal];
        [self.dishButton setTitle:self.yummmie.name forState:UIControlStateNormal];
        [self.numberOfLikesButton setTitle:[NSString stringWithFormat:@"%ld",(long)self.yummmie.likes] forState:UIControlStateNormal];
        
    }else
    {
        [self.venueButton removeFromSuperview];
        [self.dishButton removeFromSuperview];
        [self.numberOfLikesButton removeFromSuperview];
        [self.likeButton removeFromSuperview];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.yummmie)
    {
        self.downloadedImage = self.imageView.image;
        [[self imageView] sd_setImageWithURL:[NSURL URLWithString:self.imageURL] placeholderImage:self.downloadedImage];
        
        
        
        
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    if ([self isDataHidden])
    {
        return YES;
    }else
    {
        return NO;
    }
}

#pragma mark - Hide/Show Data

- (void)hideData
{
    if (![self isDataHidden])
    {
        __weak TGRImageViewController *weakSelf = self;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.likeButton.alpha = 0.0f;
            self.venueButton.alpha = 0.0f;
            self.dishButton.alpha = 0.0f;
            self.numberOfLikesButton.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
            [weakSelf setDataHidden:YES];
            [weakSelf setNeedsStatusBarAppearanceUpdate];
        }];
    }
}

- (void)showData
{
    if ([self isDataHidden])
    {
        __weak TGRImageViewController *weakSelf = self;
        
        [UIView animateWithDuration:0.25 animations:^{
            self.likeButton.alpha = 1.0f;
            self.venueButton.alpha = 1.0f;
            self.dishButton.alpha = 1.0f;
            self.numberOfLikesButton.alpha = 1.0f;
            
        } completion:^(BOOL finished) {
            [weakSelf setDataHidden:NO];
            [weakSelf setNeedsStatusBarAppearanceUpdate];
        }];
    }
}
#pragma mark - UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.scrollView.zoomScale == self.scrollView.minimumZoomScale)
    {
        [self setDataHidden:NO];
        [self setNeedsStatusBarAppearanceUpdate];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    [self hideData];
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
}
#pragma mark - Private methods

- (IBAction)handleSingleTap:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (self.scrollView.zoomScale == self.scrollView.minimumZoomScale)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        // Zoom out
        [self showData];
        [self.scrollView zoomToRect:self.scrollView.bounds animated:YES];
    }
}

- (IBAction)handleDoubleTap:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (self.scrollView.zoomScale == self.scrollView.minimumZoomScale) {
        // Zoom in
        CGPoint center = [tapGestureRecognizer locationInView:self.scrollView];
        CGSize size = CGSizeMake(self.scrollView.bounds.size.width / self.scrollView.maximumZoomScale,
                                 self.scrollView.bounds.size.height / self.scrollView.maximumZoomScale);
        CGRect rect = CGRectMake(center.x - (size.width / 2.0), center.y - (size.height / 2.0), size.width, size.height);
        [self.scrollView zoomToRect:rect animated:YES];
        //        [self hideData];
    }
    else {
        // Zoom out
        [self showData];
        [self.scrollView zoomToRect:self.scrollView.bounds animated:YES];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    //CGRect viewRect = self.view.bounds;
    CGRect viewRect = rootView.bounds;
    
    CGRect dishFrame = self.dishButton.frame;
    dishFrame.size = [self sizeForText:self.dishButton.titleLabel.text];
    dishFrame.origin.x = 10.0f;
    //dishFrame.origin.y = venueFrame.origin.y-dishFrame.size.height-7;
    dishFrame.origin.y = rootView.center.y + (rootView.frame.size.width/2) + 7;
    
    [[self dishButton] setFrame:dishFrame];
    
    
    CGRect venueFrame = self.venueButton.frame;
    venueFrame.origin.x = dishFrame.origin.x;
    //venueFrame.origin.y = faceFrame.origin.y-venueFrame.size.height-10;
    //venueFrame.origin.y = CGRectGetMaxY(dishFrame) + 25;
    venueFrame.origin.y = dishFrame.origin.y+dishFrame.size.height+7;
    
    [[self venueButton] setFrame:venueFrame];
    
    CGRect faceFrame = [[self likeButton] frame];
    faceFrame.origin.x = venueFrame.origin.x;
    //faceFrame.origin.y = viewRect.size.height-10-faceFrame.size.height;
    faceFrame.origin.y = CGRectGetMaxY(self.venueButton.frame)+10;
    
    [[self likeButton] setFrame:faceFrame];
    
    
    CGRect likesFrame = self.numberOfLikesButton.frame;
    likesFrame.origin.y = faceFrame.origin.y;
    likesFrame.origin.x = faceFrame.origin.x+10+faceFrame.size.width;
    
    [[self numberOfLikesButton] setFrame:likesFrame];
}
-(CGSize)sizeForText:(NSString *)text
{
    static UIFont *cellFont;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f];
    });
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat fixedWidth = rootView.frame.size.width-10;
    
    CGRect newframe = [text boundingRectWithSize:CGSizeMake(fixedWidth, 0.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellFont} context:nil];
    newframe.size.width = fixedWidth;
    
    return newframe.size;
}

@end
