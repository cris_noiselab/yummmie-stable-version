//
//  NLDiscoverUserHeaderView.m
//  Yummmie
//
//  Created by bot on 1/29/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLDiscoverUserHeaderView.h"

@interface NLDiscoverUserHeaderView ()

- (IBAction)followAction:(id)sender;
- (IBAction)usernameAction:(id)sender;
- (IBAction)photoAction:(id)sender;

@end

@implementation NLDiscoverUserHeaderView

- (void)awakeFromNib
{
    // Initialization code
    [self.usernameButton setClipsToBounds:YES];    
    [self.usernameButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.usernameButton.titleLabel setMinimumScaleFactor:0.90f];
    self.userPicButton.layer.cornerRadius = self.userPicButton.bounds.size.height/2.0f;
    self.userPicButton.layer.masksToBounds = YES;
}

- (IBAction)followAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(discoverHeader:userFollowActionWithTag:)])
    {
        
        [self.delegate discoverHeader:self userFollowActionWithTag:self.tag];
    }
}

- (IBAction)usernameAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(discoverHeader:usernameSelectedWithTag:)])
    {
        [self.delegate discoverHeader:self usernameSelectedWithTag:self.tag];
    }
}

- (IBAction)photoAction:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(discoverHeader:userPhotoSelectedWithTag:)])
    {
        [self.delegate discoverHeader:self userPhotoSelectedWithTag:self.tag];
    }
}

- (void)setButtonType:(UserButtonType)buttonType
{
    switch (buttonType)
    {
        case FOLLOWING:
        {
            [[self followButton] setImage:[UIImage imageNamed:@"bot_unfollow"] forState:UIControlStateNormal];
            break;
        }
        case NOTFOLLOWING:
        {
            [[self followButton] setImage:[UIImage imageNamed:@"bot_follow"] forState:UIControlStateNormal];
            break;
        }
        case PENDING:
        {
            [[self followButton] setImage:[UIImage imageNamed:@"bot__pending_lista"] forState:UIControlStateNormal];
        }
        default:
            break;
    }
    _buttonType  = buttonType;
}


@end