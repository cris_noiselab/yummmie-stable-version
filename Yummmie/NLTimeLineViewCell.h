//
//  NLTimeLineViewCell.h
//  Yummmie
//
//  Created by bot on 6/16/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "NLProgressIndicator.h"

@protocol NLTimeLineViewCellDelegate;
@class NLYum;


@interface NLTimeLineViewCell : UITableViewCell<TTTAttributedLabelDelegate>

@property (nonatomic) BOOL liked;
@property (nonatomic) BOOL fav;
@property (nonatomic) NSInteger yumId;
@property (nonatomic) NSInteger numberOfComments;
@property (nonatomic) NSInteger numberOfLikes;
@property (nonatomic) NSInteger numberOfFavs;
@property (nonatomic) NSInteger numberOfReposts;
@property (nonatomic) NSInteger viewCount;

@property (weak, nonatomic) id<NLTimeLineViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *dishPhoto;
@property (weak, nonatomic) IBOutlet UIButton *profileThumbButton;
@property (weak, nonatomic) IBOutlet UIButton *numberOfLikesButton;
@property (weak, nonatomic) IBOutlet UIButton *numberOfFavsButton;
@property (weak, nonatomic) IBOutlet UIButton *numberOfRepostsButton;
@property (weak, nonatomic) IBOutlet UIButton *repostButton;
@property (weak, nonatomic) IBOutlet UIButton *numbersOfViews;

@property (nonatomic, strong) NLProgressIndicator *indicator;
@property (weak, nonatomic) IBOutlet UIView *commentContent;
@property (strong, nonatomic) IBOutlet UIView *hashtagContent;


@property (weak, nonatomic) IBOutlet UIView *recommendView;
@property (weak, nonatomic) IBOutlet UILabel *recommendUser;

@property (weak, nonatomic) IBOutlet UIView *diffTimeView;
@property (weak, nonatomic) IBOutlet UILabel *diffTimeUnityLbl;

@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *createdMonth;
@property (weak, nonatomic) IBOutlet UILabel *createdYear;

//constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recommendConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moreCommentsConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *hashtagConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dishNameConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dishNameLeftConstraint;




- (void)setComment:(NSString *)comment withSize:(CGSize)size;
- (void)setComment:(NSString *)comment withSize:(CGSize)size andComments:(NSArray *)comments;
- (void)setHashTags:(NSString *)region withSubRegion:(NSString *)subregion andFoodType:(NSArray *)foodtype;
- (void)setPublishedTime:(NSDictionary *)created;
- (void)setUsername:(NSString *)username;
- (void)setDishName:(NSString *)name;
- (void)setDishName:(NSString *)dishName withSize:(CGSize)size;
- (void)setVenueName:(NSString *)name;
- (void)setVenueTextColor:(UIColor *)color;
- (void)setRecommend:(NSDictionary *)repost;
-(void)setFavourite:(BOOL)fav;
-(void)fillCellFromYum:(NLYum *)yum;
-(void)setDishConstraint:(CGFloat)constant;

- (void)changeFavsNumber:(NSInteger)newValue selfFav:(BOOL)selfFav;

@end

@protocol NLTimeLineViewCellDelegate <NSObject>

@required

- (void)updateLikeWithTag:(NSInteger)tag value:(BOOL)value andNumberOfLikes:(NSInteger)number;
- (void)updateFavWithTag:(NSInteger)tag;
-(void)updateRepostWithTag:(NSInteger)tag andNumberOfLikes:(NSInteger)number;

- (void)openCommentsWithTag:(NSInteger)tag;
- (void)openOptionsWithTag:(NSInteger)tag;
- (void)openPhotoWithTag:(NSInteger)tag;
- (void)openProfileWithTag:(NSInteger)tag;
- (void)openDishWithTag:(NSInteger)tag;
- (void)openVenueWithTag:(NSInteger)tag;
- (void)openHashTag:(NSString *)hashtag withTag:(NSInteger)tag;
- (void)openUser:(NSString *)user;
- (void)openLikers:(NSInteger)tag;
- (void)openFavers:(NSInteger)tag;
- (void)openReposters:(NSInteger)tag;

- (void)openFirstCommentUser:(NSInteger)tag;
- (void)openSecondCommentUser:(NSInteger)tag;
- (void)openFirstCommentHashTag:(NSInteger)tag andHashtag:(NSString *)hashtag;
- (void)openSecondCommentHashTag:(NSInteger)tag andHashtag:(NSString *)hashtag;
- (void)openRecommender:(NSInteger)tag;

@end
