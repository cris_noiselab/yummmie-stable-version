//
//  NLSuggestedUserBarCell.h
//  Yummmie
//
//  Created by bot on 4/1/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLSuggestedUserBarCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@end
