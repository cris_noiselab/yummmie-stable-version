//
//  NLError.h
//  Yummmie
//
//  Created by bot on 6/12/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const NLErrorDomain;

typedef NS_ENUM(NSInteger, NLErrorType)
{
    NLUserRegistrationError = 1000,
    NLUserLoginError
};