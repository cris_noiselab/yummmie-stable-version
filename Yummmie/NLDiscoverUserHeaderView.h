//
//  NLDiscoverUserHeaderView.h
//  Yummmie
//
//  Created by bot on 1/29/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NLConstants.h"

@protocol NLDiscoverUserHeaderViewDelegate;

@interface NLDiscoverUserHeaderView : UICollectionReusableView

@property (weak, nonatomic) id<NLDiscoverUserHeaderViewDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIImageView *userCoverImage;
@property (weak, nonatomic) IBOutlet UIButton *userPicButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *usernameButton;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (nonatomic) UserButtonType buttonType;

@end

@protocol NLDiscoverUserHeaderViewDelegate <NSObject>

@required

- (void)discoverHeader:(NLDiscoverUserHeaderView *)discoverHeader userPhotoSelectedWithTag:(NSInteger)tag;
- (void)discoverHeader:(NLDiscoverUserHeaderView *)discoverHeader usernameSelectedWithTag:(NSInteger)tag;
- (void)discoverHeader:(NLDiscoverUserHeaderView *)discoverHeader userFollowActionWithTag:(NSInteger)tag;

@end

