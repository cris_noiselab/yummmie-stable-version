//
//  NLTag.h
//  Yummmie
//
//  Created by bot on 7/28/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NLTag : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic)  NSInteger tagId;
@property (nonatomic) NSInteger numberOfYums;


- (id)initWithDictionary:(NSDictionary *)tagDictionary;

- (BOOL)isTag:(NSString *)tagValue;

+ (NSURLSessionDataTask *)getTagsWithQuery:(NSString *)query afterTagId:(NSInteger)tagId withCompletionBlock:(void (^)(NSError *error, NSArray *tags))block;

@end
