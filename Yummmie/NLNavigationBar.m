//
//  NLNavigationBar.m
//  Yummmie
//
//  Created by bot on 6/20/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLNavigationBar.h"
#import "UIColor+NLColor.h"

@interface NLNavigationBar ()

@property (nonatomic, strong) CALayer *colorLayer;

@end

@implementation NLNavigationBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

static CGFloat const kDefaultColorLayerOpacity = 0.5f;
static CGFloat const kSpaceToCoverStatusBars = 20.0f;

- (void)setBarTintColor:(UIColor *)barTintColor {
    [super setBarTintColor:barTintColor];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        self.colorLayer = [CALayer layer];
        self.colorLayer.opacity = kDefaultColorLayerOpacity;
        [self.layer addSublayer:self.colorLayer];
    });

    self.colorLayer.backgroundColor = barTintColor.CGColor;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.colorLayer != nil) {
        self.colorLayer.frame = CGRectMake(0, 0 - kSpaceToCoverStatusBars, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds) + kSpaceToCoverStatusBars);
        [self.layer insertSublayer:self.colorLayer atIndex:1];
    }
}
@end
