//
//  NLZoomAnimationController.h
//  Yummmie
//
//  Created by bot on 7/4/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NLZoomAnimationController : NSObject<UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) NSTimeInterval presentationDuration;
@property (nonatomic, assign) NSTimeInterval dismissDuration;
@property (nonatomic, assign, getter = isPresenting) BOOL presenting;

@end