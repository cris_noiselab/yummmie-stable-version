//
//  NLYummieApiClient.h
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "NLJSONResponseSerializer.h"

//#if DEBUG
//#else
//

static NSString * AFAppDotNetAPIBaseURLString = @"http://api.yummmie.com/api/v2/";
//static NSString * AFAppDotNetAPIBaseURLString = @"http://stage-v2.yummmie.com/api/v2/";
//static NSString * AFAppDotNetAPIBaseURLString = @"http://192.168.190.39:3000/api/v2/";



//#endif

@interface NLYummieApiClient : AFHTTPSessionManager

+ (instancetype)sharedClient;
+ (void)setValue:(NSString *)value forHeaderField:(NSString *)field;

@end
