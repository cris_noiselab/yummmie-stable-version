//
//  MVCameraEditViewController.h
//  Yummmie
//
//  Created by Mauricio Ventura on 21/07/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MVCameraEditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *topControls;
@property (weak, nonatomic) IBOutlet UIView *imgArea;
@property (weak, nonatomic) IBOutlet UIView *botomControls;

-(instancetype)initWithImage:(UIImage *)img;


- (IBAction)back:(id)sender;
- (IBAction)imageDidPicked:(id)sender;
- (IBAction)pinch:(UIPinchGestureRecognizer *)sender;
- (IBAction)rotate:(UIRotationGestureRecognizer *)sender;



@end
