//
//  NSURL+AssetURL.h
//  Yummmie
//
//  Created by bot on 7/14/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (AssetURL)

+ (NSURL *)urlForAssetPath:(NSString *)pathToAsset;


@end
