//
//  NLPreferences.h
//  Yummmie
//
//  Created by bot on 12/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NLPreferences : NSObject

+ (BOOL)foursquarePermissionWasAsked;
+ (void)setFoursquarePermissionAsked:(BOOL)value;
+ (BOOL)useFoursquare;
+ (void)setCanUseFoursquare:(BOOL)value;
+ (BOOL)canShowSocialReminder;
+ (void)dontShowSocialReminderAnymore;
+ (BOOL)firstTime;
@end