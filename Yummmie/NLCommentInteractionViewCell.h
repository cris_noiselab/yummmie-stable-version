//
//  NLCommentInteractionViewCell.h
//  Yummmie
//
//  Created by bot on 8/22/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>

@protocol NLCommentInteractionViewCellDelegate;

@interface NLCommentInteractionViewCell : UITableViewCell<TTTAttributedLabelDelegate>

@property (weak, nonatomic) id<NLCommentInteractionViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *userImageButton;
@property (weak, nonatomic) IBOutlet UIButton *dishButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

- (void)setOwner:(NSString *)owner comment:(NSString *)comment;
- (void)setOwner:(NSString *)owner comment:(NSString *)comment andTarget:(NSString *)target;


@end

@protocol NLCommentInteractionViewCellDelegate <NSObject>

@required

- (void)openCommentOwnerWithTag:(NSInteger)tag;
- (void)openCommentYumWithTag:(NSInteger)tag;
- (void)openCommentUserWithString:(NSString *)username;

@end