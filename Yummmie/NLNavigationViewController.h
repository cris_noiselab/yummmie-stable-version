//
//  NLNavigationViewController.h
//  Yummmie
//
//  Created by bot on 7/8/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLNavigationViewController : UINavigationController

- (void)scrollToTop;

- (void)setupProgressView;

- (void)updateProgress:(float)progress;

- (void)hideProgressView;

- (void)completeProgress;

- (void)addChildOnTop:(UIView *)view;


@end
