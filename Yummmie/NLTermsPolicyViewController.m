//
//  NLTermsPolicyViewController.m
//  Yummmie
//
//  Created by bot on 6/5/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLTermsPolicyViewController.h"

static const float kTermsPolicyTitleLabelWidth = 200.0f;
static const float kTermsPolicyTitleLabelHeight = 30.0f;

@interface NLTermsPolicyViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButtonItem;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@property (nonatomic, strong) NSURL *url;

- (IBAction)closeAction:(id)sender;
@end

@implementation NLTermsPolicyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andURL:(NSURL *)url
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        _url = url;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addRedBackground];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kTermsPolicyTitleLabelWidth, kTermsPolicyTitleLabelHeight)];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16.0f]];
    [titleLabel setText:NSLocalizedString(@"terms_and_conditions_title", nil)];
    self.closeButtonItem.title = NSLocalizedString(@"close_title", nil);
    
    UIBarButtonItem *titleItem = [[UIBarButtonItem alloc] initWithCustomView:titleLabel];
    UIBarButtonItem *flexibleItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *flexibleItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.spinner setHidesWhenStopped:YES];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:self.spinner];
    NSArray *items = @[self.closeButtonItem,flexibleItem1,titleItem,flexibleItem2,rightButton];
    [self.toolbar setItems:items];

    [[self webview] setDelegate:self];
    [[self webview] loadRequest:[[NSURLRequest alloc] initWithURL:self.url]];
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"Terms & Conditions View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[self spinner] stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[self spinner] startAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [[self spinner] stopAnimating];
}
@end
