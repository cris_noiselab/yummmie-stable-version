//
//  UIColor+NLColor.h
//  Yummmie
//
//  Created by bot on 6/4/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (NLColor)

+ (UIColor *)nl_colorWith255Red:(float )red green:(float)green blue:(float)blue andAlpha:(float)alpha;
+ (UIColor *)nl_applicationRedColor;
+ (UIColor *)nl_applicationGrayColor;
+ (UIColor *)nl_applicationLightGrayColor;

@end