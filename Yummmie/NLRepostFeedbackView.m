//
//  NLRepostFeedbackView.m
//  Yummmie
//
//  Created by Mauricio Ventura on 10/08/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import "NLRepostFeedbackView.h"

@implementation NLRepostFeedbackView
{
	UIImageView *feedback;
	UILabel *text;
	NSMutableArray *images;
	NSInteger duration;
}
-(id)initWithFrame:(CGRect)frame andImageBase:(NSString *)imageBaseName count:(NSInteger)count
{
	if (self = [super initWithFrame:frame])
	{
		[self setFrame:frame];
		self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
		
		feedback = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, frame.size.width, 200)];
		images = [NSMutableArray array];
		for (int i = 0; i < count; i++)
		{
			UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%d", imageBaseName, i]];
			if (image)
			{
				[images addObject:image];
			}
		}
		feedback.animationImages = images;
		feedback.contentMode = UIViewContentModeCenter;
		text = [[UILabel alloc] initWithFrame:CGRectMake(20, frame.size.height / 1.6, frame.size.width - 40, 20)];
		text.text = NSLocalizedString(@"recomendado", nil);
		[text setTextColor:[UIColor whiteColor]];
		text.textAlignment = NSTextAlignmentCenter;
		[text setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]];
		[self addSubview:text];
		text.center = CGPointMake(self.center.x, frame.size.height / 1.6);
		text.alpha = 0;
		self.alpha = 0;
		[self addSubview:feedback];
		duration = 1;

	}
	return self;
}
-(void)show
{
	feedback.animationDuration = duration;
	[UIView animateWithDuration:0.3 animations:^{
		self.alpha = 1;
	} completion:^(BOOL finished)
	{
		[feedback startAnimating];
		[self performSelector:@selector(animationDidFinish) withObject:nil
							 afterDelay:duration];
		
	}];
}
-(void)animationDidFinish
{
	[feedback stopAnimating];
	feedback.image = [images lastObject];
	[UIView animateWithDuration:0.2 animations:^{
		text.alpha = 1;
	} completion:^(BOOL finished) {
		[UIView animateWithDuration:0.7 animations:^{
			self.alpha = 0;
		} completion:^(BOOL finished) {
			[self removeFromSuperview];
		}];
	}];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
