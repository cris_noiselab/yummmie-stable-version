//
//  NLRecoverPasswordViewController.m
//  Yummmie
//
//  Created by bot on 1/15/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLRecoverPasswordViewController.h"
#import "NSString+NLStringValidations.h"
#import "NLUser.h"

@interface NLRecoverPasswordViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *resetContentView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *resetSuccessImage;



- (IBAction)submitForm:(id)sender;

- (void)submitData;

@end

@implementation NLRecoverPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"recover_password_title", nil);
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, CGRectGetHeight(self.emailTextField.bounds))];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, CGRectGetHeight(self.emailTextField.bounds))];
    
    self.emailTextField.leftView = leftView;
    self.emailTextField.rightView = rightView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    self.emailTextField.rightViewMode = UITextFieldViewModeAlways;
    [self.emailTextField becomeFirstResponder];
    
    [self.submitButton setTitle:NSLocalizedString(@"recover_password_button_title", nil) forState:UIControlStateNormal];
    
    self.infoLabel.text = NSLocalizedString(@"reset_password_info_text", nil);
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"recuperar contraseña View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //Check the limit of every field
    
    if (textField == [self emailTextField])
    {
        if (([[[self emailTextField] text] length]>254 && [string length]>0) || [string isEqualToString:@" "])
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (![NSString isValidEmail:self.emailTextField.text])
    {
        [self showErrorMessage:NSLocalizedString(@"email_field_error", nil) withDuration:3.0f];
    }else
    {
        [self submitData];
    }
    
    return YES;
}

#pragma mark - Submit Form Method
- (IBAction)submitForm:(id)sender
{
    if (![NSString isValidEmail:self.emailTextField.text])
    {
        [self showErrorMessage:NSLocalizedString(@"email_field_error", nil) withDuration:3.0f];
    }else
    {
        [self submitData];
    }
}

#pragma mark - Submit Data Method

- (void)submitData
{
    [self showLoadingSpinner];
    [[self emailTextField] setEnabled:NO];
    [[self submitButton] setEnabled:NO];
    
    //Success Case
    __weak NLRecoverPasswordViewController *weakSelf = self;
    
    [NLUser resetPasswordWithEmail:self.emailTextField.text andCompletionBlock:^(NSError *error, BOOL emailFound)
	{
        [weakSelf hideLoadingSpinner];        
        if (!error)
        {
            if (emailFound)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.5 animations:^{
                        
                        [[weakSelf emailTextField] setAlpha:0.0f];
                        [[weakSelf submitButton] setAlpha:0.0f];
                        
                    } completion:^(BOOL finished) {
                        
                        [[weakSelf infoLabel] setText:NSLocalizedString(@"reset_password_success_info", nil)];
                        [[weakSelf infoLabel] setTextAlignment:NSTextAlignmentCenter];
                        [weakSelf topConstraint].constant = CGRectGetMidY(weakSelf.resetContentView.bounds);
                        
                        [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
                            [weakSelf.resetContentView layoutIfNeeded];
                        } completion:nil];
                        [UIView animateWithDuration:0.5f delay:0.5f options:UIViewAnimationOptionCurveEaseIn animations:^{
                            [[weakSelf resetSuccessImage] setAlpha:1.0f];
                        } completion:nil];
                    }];
                });
                
            }
						else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf showErrorMessage:@"El correo no está registrado" withDuration:3.0f];
                });
                [[weakSelf emailTextField] setEnabled:YES];
                [[weakSelf submitButton] setEnabled:YES];
            }
        }
				else
        {
					if ([[error.userInfo[JSONResponseSerializerWithDataKey] objectForKey:@"message"] isEqualToString:@"USER NOT FOUND"])
					{
						dispatch_async(dispatch_get_main_queue(), ^{
							[weakSelf showErrorMessage:@"El correo no está registrado" withDuration:3.0f];
						});
					}
					else
					{
						dispatch_async(dispatch_get_main_queue(), ^{
							[weakSelf showErrorMessage:@"Hubo un error, intente de nuevo." withDuration:3.0f];
						});
					}
					
            
            [[weakSelf emailTextField] setEnabled:YES];
            [[weakSelf submitButton] setEnabled:YES];
        }
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
									 {
        
            });
    
}
@end
