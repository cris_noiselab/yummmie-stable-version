//
//  NLVenueViewController.h
//  Yummmie
//
//  Created by bot on 7/7/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import <CoreLocation/CoreLocation.h>
@class NLVenue;

@interface NLVenueViewController : NLBaseViewController<UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate>

- (id)initWithVenue:(NLVenue *)venue;
- (id)initWithVenue:(NLVenue *)venue sharePhoto:(BOOL)sharePhoto andCloseButton:(BOOL)close;
- (id)initWithVenueId:(NSString *)venueId andCloseButton:(BOOL)flag;

@end
