//
//  NLFoodType.m
//  Yummmie
//
//  Created by bot on 7/27/17.
//  Copyright © 2017 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLFoodType.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLConstants.h"

static NSString * const kNLAuthenticationResponseAuthenticationKey = @"authentication";

@interface NLAuthentication ()

@property (nonatomic, copy, readwrite) NSString * token;
@property (nonatomic, copy, readwrite) NSString * tokenSecret;
@property (nonatomic, copy, readwrite) NSString * userId;
@property (nonatomic, copy, readwrite) NSString *authenticationID;

@end

@implementation NLFoodType


- (instancetype)initWithDictionary:(NSDictionary *)regionDict
{
    if (self = [super init])
    {
        _food_type_id = [regionDict objectForKey:@"id" ];
        _food_type_name = [regionDict objectForKey:@"name"];
        
    }
    
    return self;
}


+ (NSURLSessionDataTask *)getFoodTypes :(void (^)(NSError *error, NSArray *regions))block
{
    
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSString *apiCallPath = [[NSString stringWithFormat:@"venues/food_types"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [[NLYummieApiClient sharedClient] GET:apiCallPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (block)
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                NSLog(@"Answer: %@", [responseObject description]);
                
                NSMutableArray *food_types = [NSMutableArray array];
                food_types=[responseObject valueForKey:@"food_types"];
                NSMutableArray *typesArray = [NSMutableArray array];
                for (NSDictionary *json in food_types)
                {
                    NLFoodType *type =[[NLFoodType alloc] initWithDictionary:json];
                    [typesArray addObject:type];
                }
                
                
                
                block(nil, [NSArray arrayWithArray:typesArray]);
                NSLog(@"block done");
                
                
            }else
            {
                block([NSError new],nil);
            }
        }
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(error,nil);
    }];
}


@end
