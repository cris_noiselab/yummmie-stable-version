//
//  NLShareAppViewController.m
//  Yummmie
//
//  Created by bot on 12/16/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

#import "NLShareAppViewController.h"
#import "NLSession.h"
#import "NLUser.h"

@interface NLShareAppViewController ()<UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *providersTable;

@end

@implementation NLShareAppViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeBottom;
    self.title = NSLocalizedString(@"invite_people_title", nil);
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    [footer setBackgroundColor:[UIColor whiteColor]];
    [self.providersTable setTableFooterView:footer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"invita personas View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if ([self.providersTable respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.providersTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.providersTable respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.providersTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * kProviderCellIdentifier = @"providerCellIdentifier";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kProviderCellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kProviderCellIdentifier];
        [[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_invite_fb"]];
            [[cell textLabel] setText:@"Facebook"];
            break;
        }
        case 1:
        {
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_invite_tw"]];
            [[cell textLabel] setText:@"Twitter"];
            break;
        }
        case 2:
        {
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_invite_wa"]];
            [[cell textLabel] setText:@"WhatsApp"];
            break;
        }
        case 3:
        {
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_invite_mail"]];
            [[cell textLabel] setText:NSLocalizedString(@"invite_people_mail_title", nil)];
            break;
        }
        case 4:
        {
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_invite_message"]];
            [[cell textLabel] setText:NSLocalizedString(@"invite_people_message_title", nil)];
            break;
        }
        default:
            break;
    }
    return cell;
    
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *appURL = @"https://appsto.re/us/Rr6W0.i";
    NSString *username = [[NLSession currentUser] username];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *text = [NSString stringWithFormat:@"%@%@ %@ ",NSLocalizedString(@"share_app_text", nil),username,appURL];
    
    switch (indexPath.row)
    {
            
        case 0:
        {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
            {
                SLComposeViewController *facebookShareVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [facebookShareVC setInitialText:text];
                [facebookShareVC addURL:[NSURL URLWithString:appURL]];
                
                [self presentViewController:facebookShareVC animated:YES completion:nil];
            }else
            {
                [self showErrorMessage:NSLocalizedString(@"facebook_not_setup", nil) withDuration:2.0f];
            }
            break;
        }
        case 1:
        {
            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *twitterShareVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [twitterShareVC setInitialText:text];
                [self presentViewController:twitterShareVC animated:YES completion:nil];
            }else
            {
                [self showErrorMessage:NSLocalizedString(@"twitter_not_setup", nil) withDuration:2.0f];
            }
            break;
        }
        case 2:
        {
            
            
            NSURL *whatsappURL = [NSURL URLWithString:[[NSString stringWithFormat:@"whatsapp://send?text=%@",text] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
            {
                [[UIApplication sharedApplication] openURL: whatsappURL];
            }else
            {
                [self showErrorMessage:NSLocalizedString(@"whatsapp_not_setup", nil) withDuration:2.0f];
            }
            break;
        }
        case 3:
        {
            
            MFMailComposeViewController *mailCompose = [[MFMailComposeViewController alloc] init];
            if ([MFMailComposeViewController canSendMail])
            {
                [mailCompose setSubject:@"Yummmie*"];
                [mailCompose setMessageBody:text isHTML:NO];
                [mailCompose setMailComposeDelegate:self];
                [self presentViewController:mailCompose animated:YES completion:nil];
            }else
            {
                [self showErrorMessage:NSLocalizedString(@"email_not_setup", nil) withDuration:2.0f];
            }
            break;
        }
        case 4:
        {
            MFMessageComposeViewController *messageCompose = [[MFMessageComposeViewController alloc] init];
            if ([MFMessageComposeViewController canSendText])
            {
                [messageCompose setBody:text];
                [messageCompose setMessageComposeDelegate:self];
                [self presentViewController:messageCompose animated:YES completion:nil];
            }else
            {
                [self showErrorMessage:NSLocalizedString(@"sms_not_setup", nil) withDuration:2.0f];
            }
            break;
        }
    }
}


#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
