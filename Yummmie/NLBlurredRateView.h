//
//  NLBlurredRateView.h
//  Yummmie
//
//  Created by bot on 12/14/15.
//  Copyright © 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLBlurredRateView : UIView
-(NSArray *)configureToRate:(id)target;
@end
