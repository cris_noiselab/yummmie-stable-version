//
//  NLVenueDetailViewController.h
//  Yummmie
//
//  Created by bot on 5/15/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NLBaseViewController.h"

@class NLVenue;

@interface NLVenueDetailViewController : NLBaseViewController

- (instancetype)initWithVenue:(NLVenue *)aVenue NS_DESIGNATED_INITIALIZER;

@end
