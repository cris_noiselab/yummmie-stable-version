//
//  NLCommentViewCell.h
//  Yummmie
//
//  Created by bot on 6/25/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>

@protocol NLCommentViewCellDelegate;

@interface NLCommentViewCell : UITableViewCell<TTTAttributedLabelDelegate>

@property (nonatomic, copy) NSString *username;
@property (weak, nonatomic) IBOutlet UIButton *profileThumbButton;
@property (weak, nonatomic) id<NLCommentViewCellDelegate> delegate;
- (void)setComment:(NSString *)comments withSize:(CGSize)size;

@end

@protocol NLCommentViewCellDelegate <NSObject>


- (void)openUser:(NSString *)username;
- (void)openHashtag:(NSString *)hashtag andTag:(NSInteger)tag;


@end