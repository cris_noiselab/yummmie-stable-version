//
//  NLDishViewController.h
//  Yummmie
//
//  Created by bot on 7/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLDishViewController : NLBaseViewController<UICollectionViewDataSource, UICollectionViewDelegate>

- (id)initWithDishName:(NSString *)dishName;
@end
