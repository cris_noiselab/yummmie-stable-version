//
//  NLLikeInteractionViewCell.h
//  Yummmie
//
//  Created by bot on 8/22/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>

@protocol NLLikeInteractionViewCellDelegate;


@interface NLLikeInteractionViewCell : UITableViewCell<TTTAttributedLabelDelegate>

@property (weak, nonatomic) id<NLLikeInteractionViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *userImageButton;
@property (weak, nonatomic) IBOutlet UIButton *dishButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property BOOL repost;
- (IBAction)openUser:(id)sender;
- (IBAction)openYum:(id)sender;

- (void)setUser:(NSString *)user isMe:(BOOL)me;

@end


@protocol NLLikeInteractionViewCellDelegate <NSObject>

@required

- (void)openOwnerWithTag:(NSInteger)tag;
- (void)openYumWithTag:(NSInteger)tag;

@end