//
//  NLHashtagViewController.h
//  Yummmie
//
//  Created by bot on 6/23/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
@class  NLTag;

@interface NLHashtagViewController : NLBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate>

- (id)initWithHashtag:(NSString *)hashtag;
- (id)initWithTag:(NLTag *)tag andNumberOfYums:(NSInteger)numberOfYums;

@end
