//
//  NLBlurredRateView.m
//  Yummmie
//
//  Created by bot on 12/14/15.
//  Copyright © 2015 Noiselab Apps. All rights reserved.
//

#import "NLBlurredRateView.h"

@implementation NLBlurredRateView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
-(void)getImageFromContext:(CGSize)context view:(UIView *)view
{
    
    //Get a UIImage from the UIView
    UIGraphicsBeginImageContext(context);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    //Blur the UIImage with a CIFilter

    CIImage *imageToBlur = [CIImage imageWithCGImage:viewImage.CGImage];
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];
    [gaussianBlurFilter setValue:[NSNumber numberWithFloat: 3] forKey: @"inputRadius"];
    CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
    UIImage *endImage = [[UIImage alloc] initWithCIImage:resultImage];
    
    //Place the UIImage in a UIImageView
    UIImageView *newView = [[UIImageView alloc] initWithFrame:CGRectMake(-19, -19, view.frame.size.width + 38, (view.frame.size.height / 2) + 38)];
    newView.contentMode = UIViewContentModeScaleAspectFill;
    newView.image = endImage;
    [self addSubview:newView];
}
-(NSArray *)configureToRate:(id)target
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    //CGFloat arrangedHeight = rootView.frame.size.height*0.088;
    UIView *vibView = [[UIView alloc] initWithFrame:CGRectMake(0, rootView.center.y + 50, self.frame.size.width, rootView.center.y - 50)];
    //UIView *vibView = [[UIView alloc] initWithFrame:CGRectMake(0, rootView.frame.size.height-arrangedHeight, self.frame.size.width, arrangedHeight)];
    vibView.backgroundColor = [UIColor whiteColor];
    
    UIBlurEffect * effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView * viewWithBlurredBackground =
    [[UIVisualEffectView alloc] initWithEffect:effect];
    viewWithBlurredBackground.frame = self.frame;
    [self addSubview:viewWithBlurredBackground];

    [self addSubview:vibView];

    UILabel *title = [[UILabel alloc] init];
    title.text = @"★★★★★";
    [title setTextColor:[UIColor redColor]];
    [title setFrame:CGRectMake(20, 40, self.frame.size.width - 40, 35)];
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:40]];
    [title setTextColor:[UIColor colorWithRed:205.0/255.0 green:36.0/255.0 blue:49.0/255.0 alpha:1]];
    [title setTextAlignment:NSTextAlignmentCenter];
    title.numberOfLines = 1;
    [vibView addSubview: title];
    
    
    UILabel *text1 = [[UILabel alloc] init];
    text1.text = NSLocalizedString(@"aviso_rate", @"aviso_rate");
    [text1 setFrame:CGRectMake(20, title.frame.origin.y + title.frame.size.height + 10, self.frame.size.width - 40, 100)];
    text1.center = CGPointMake(text1.center.x, (vibView.frame.size.height / 2) + 10);
    [text1 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
    [text1 setTextColor:[UIColor colorWithRed:47.0/255.0 green:50.0/255.0 blue:53.0/255.0 alpha:1]];
    [text1 setTextAlignment:NSTextAlignmentCenter];
    text1.numberOfLines = 3;
    [vibView addSubview: text1];
    
    UIButton *rate = [[UIButton alloc] initWithFrame:CGRectMake(0, rootView.frame.size.height - 40, self.frame.size.width, 40)];
    [rate setTitle:@"Rate now" forState:UIControlStateNormal];
    [rate.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
    [rate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rate setBackgroundColor:[UIColor colorWithRed:205.0/255.0 green:36.0/255.0 blue:49.0/255.0 alpha:1]];
    [self addSubview:rate];
    [rate addTarget:target action:@selector(rateNow) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *later = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 25, 5, 20, 20)];
    [later setImage:[UIImage imageNamed:@"close_b"] forState:UIControlStateNormal];

    [vibView addSubview:later];
    
    [later addTarget:target action:@selector(noRate) forControlEvents:UIControlEventTouchUpInside];

    
    return @[rate, later];
    
}
@end
