//
//  NLCommentsViewController.h
//  Yummmie
//
//  Created by bot on 6/25/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import "NLCommentViewCell.h"

@class NLYum;

@interface NLCommentsViewController : NLBaseViewController<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,NLCommentViewCellDelegate>

- (id)initWithYum:(NLYum *)yum;

- (id)initWithYumId:(NSInteger)yumId andDismissButton:(BOOL)flag;

@end
