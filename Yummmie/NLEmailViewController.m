//
//  NLEmailViewController.m
//  Yummmie
//
//  Created by Mauricio Ventura on 10/06/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import "NLEmailViewController.h"
#import "NSString+NLStringValidations.h"

@interface NLEmailViewController ()

@end

@implementation NLEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"correo electrónico";
    // Do any additional setup after loading the view from its nib.
}
-(void)enviar:(id)sender
{
	if (![NSString isValidEmail:self.email.text])
	{
		[self showErrorMessage:NSLocalizedString(@"email_field_error",nil) withDuration:2.0];
		[self hideLoadingSpinner];
	}
	else
	{
		[self.delegate sendEmail:self.email.text token:self.token];
		[self.navigationController popViewControllerAnimated:true];
	}
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
