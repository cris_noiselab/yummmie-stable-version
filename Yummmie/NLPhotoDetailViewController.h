//
//  NLPhotoDetailViewController.h
//  Yummmie
//
//  Created by bot on 6/24/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLTableViewController.h"
#import "NLTimeLineViewCell.h"
#import "NLConfirmView.h"


@class NLYum;

@protocol NLPhotoDetailViewDelegate <NSObject>


@optional
- (void)yumDeleted:(NLYum *)deletedYum;
- (void)yumUnliked:(NLYum *)unlikedYum;
- (void)yumLiked:(NLYum *)likedYum;

@end

@interface NLPhotoDetailViewController : NLTableViewController<NLTimeLineViewCellDelegate,UIActionSheetDelegate,UIViewControllerTransitioningDelegate, UIDocumentInteractionControllerDelegate, NLConfirmViewDelegate>
@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@property (nonatomic, weak) id<NLPhotoDetailViewDelegate> delegate;

- (id)initWithYum:(NLYum *)aYum;
- (id)initWithYumId:(NSInteger)yumId;
- (id)initWithStringYumId:(NSString *)yumId andDismissButton:(BOOL)flag;
- (id)initWithYumId:(NSInteger)yumId andDismissButton:(BOOL)flag;
- (void)updateFavWithTag:(NSInteger)tag;
@end
