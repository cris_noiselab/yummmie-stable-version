//
//  NLComment.h
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"

@class NLUser;

@interface NLComment : NSObject

@property (nonatomic, copy) NSString *content;
@property (nonatomic) NSInteger commentId;
@property (nonatomic, strong) NLUser *user;
@property (nonatomic, strong) NSArray *tags;

- (id)initWithDictionary:(NSDictionary *)dict;


+ (NSURLSessionDataTask *)getCommentsForYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error, NSArray *comments))block;

+ (NSURLSessionDataTask *)getPreviousCommentsForYum:(NSInteger)yumId commentId:(NSInteger)commentId withCompletionBlock:(void (^)(NSError *error, NSArray *comments))block;

@end