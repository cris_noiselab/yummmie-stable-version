//
//  NLMapViewController.h
//  Yummmie
//
//  Created by bot on 5/20/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface NLMapViewController : UIViewController

- (instancetype)initWithLocactionCoordinate:(CLLocationCoordinate2D)locationCoordinate NS_DESIGNATED_INITIALIZER;

@end
