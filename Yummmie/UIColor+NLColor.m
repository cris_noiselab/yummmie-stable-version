//
//  UIColor+NLColor.m
//  Yummmie
//
//  Created by bot on 6/4/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "UIColor+NLColor.h"

@implementation UIColor (NLColor)

+ (UIColor *)nl_colorWith255Red:(float)red green:(float)green blue:(float)blue andAlpha:(float)alpha
{
    float clampedRed = (red > 255.0f)?255.0f:(red<0.0f)?0.0f:red;
    float clampedGreen = (green > 255.0f)?255.0f:(green<0.0f)?0.0f:green;
    float clampedBlue = (blue > 255.0f)?255.0f:(blue<0.0f)?0.0f:blue;
    float clampedAlpha = (alpha > 255.0f)?255.0f:(alpha<0.0f)?0.0f:alpha;
    
    return [UIColor colorWithRed:(clampedRed/255.0f) green:(clampedGreen/255.0f) blue:(clampedBlue/255.0f) alpha:(clampedAlpha/255.0f)];
}

+ (UIColor *)nl_applicationRedColor
{
    return [UIColor nl_colorWith255Red:205.0f green:36.0f blue:49.0f andAlpha:255.0f];
}

+ (UIColor *)nl_applicationGrayColor
{
    return  [UIColor nl_colorWith255Red:127.0f green:127.0f blue:127.0f andAlpha:255.0f];
}

+ (UIColor *)nl_applicationLightGrayColor
{
    return  [UIColor nl_colorWith255Red:179.0f green:179.0f blue:179.0f andAlpha:255.0f];
}
@end
