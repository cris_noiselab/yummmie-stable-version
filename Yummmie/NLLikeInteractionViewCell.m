//
//  NLLikeInteractionViewCell.m
//  Yummmie
//
//  Created by bot on 8/22/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLLikeInteractionViewCell.h"
#import "NLConstants.h"
#import "UIColor+NLColor.h"

@interface NLLikeInteractionViewCell ()

@property (strong, nonatomic) TTTAttributedLabel *contentLabel;

@end

@implementation NLLikeInteractionViewCell

- (void)awakeFromNib
{
    // Initialization code
    
    self.contentLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.bounds)+kTimeLabelXPadding,kTimeLabelYOrigin, 200.0f, 35.0f)];
    UIColor *commentColor = [UIColor nl_colorWith255Red:89.0f green:92.0f blue:105.0f andAlpha:255.0f];
    
    [[self contentLabel] setDelegate:self];
    [self.contentLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.contentLabel setMinimumScaleFactor:0.5f];
    [self.contentLabel setNumberOfLines:2];
    [self.contentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.contentLabel setTextColor:commentColor];
    [self.contentLabel adjustsFontSizeToFitWidth];
    [self.contentLabel setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    
    NSDictionary *linkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self contentLabel] setLinkAttributes:linkAttributes];
    
    NSDictionary *activeLinkAttributes = @{(id)kCTForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    [[self contentLabel] setActiveLinkAttributes:activeLinkAttributes];
    NSDictionary *inactiveLinkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self contentLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    
    [self.contentView addSubview:self.contentLabel];
    
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [[self contentLabel] setText:@""];
    
    [[self contentLabel] setFrame:CGRectMake(CGRectGetMaxX(self.userImageButton.bounds)+kTimeLabelXPadding,kTimeLabelYOrigin, 200.0f, 35.0f)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


- (IBAction)openUser:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openOwnerWithTag:)])
    {
        [[self delegate] openOwnerWithTag:[self tag]];
    }
}


- (IBAction)openYum:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openYumWithTag:)])
    {
        [[self delegate] openYumWithTag:[self tag]];
    }
}

- (void)setUser:(NSString *)user isMe:(BOOL)me
{
	if (self.repost)
	{
		self.contentLabel.text = [NSString stringWithFormat:@"@%@ %@", user, NSLocalizedString(@"repost_user_yum_text", nil)];
	}
	else
	{
		self.contentLabel.text = [NSString stringWithFormat:@"@%@ %@",user,(me)?NSLocalizedString(@"likes_user_yum_text", nil):NSLocalizedString(@"likes_a_user_yum_text", nil)];
	}
    NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
    NSRange range = NSMakeRange(0, 1+user.length);
    
    [self.contentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:range];
    
    __weak NLLikeInteractionViewCell *weakSelf = self;
    [self.contentLabel sizeToFit];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect timeLabelFrame = weakSelf.timeLabel.frame;
        timeLabelFrame.origin.y = CGRectGetMaxY(weakSelf.contentLabel.bounds)+20.0f;
//        [weakSelf.timeLabel setFrame:timeLabelFrame];
    });
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:kUsernameScheme])
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(openOwnerWithTag:)])
        {
            [[self delegate] openOwnerWithTag:[self tag]];
        }
    }
}
@end
