//
//  NLNotifications.m
//  Yummmie
//
//  Created by bot on 6/12/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLNotifications.h"

NSString * const kNLLoginNotification = @"NLLoginNotification";
NSString * const kNLLogoutNotification = @"kNLLogoutNotification";
NSString * const kNLLoadTimeLineNotification = @"kNLLoadTimeLineNotification";
NSString * const kNLLoadTimeLineErrorNotification = @"kNLLoadTimeLineErrorNotification";
NSString * const kNLReloadUserDataNotification = @"kNLReloadUserDataNotification";
NSString * const kNLYumPostReadyNotification = @"kNLYumPostReadyNotification";
NSString * const kNLCommentAddedNotification = @"kNLCommentAddedNotification";
NSString * const kNLReloadDishesNotification = @"kNLReloadDishesNotification";
NSString * const kNLFollowUserNotification = @"kNLFollowUserNotification";
NSString * const kNLUnfollowUserNotification = @"kNLUnfollowUserNotification";
NSString * const kNLShowSettingsNotification = @"kNLShowSettingsNotification";
NSString * const kNLDismissSettingsNotification = @"kNLDismissSettingsNotification";
NSString * const kNLUpdateSettingsNotification = @"kNLUpdateSettingsNotification";
NSString * const kNLShowInstagramOption = @"kNLShowInstagramOption";
NSString * const kNLShowActivityFeed = @"kNLShowActivityFeed";
NSString * const kNLShowInteractionPopUpNotification =  @"kNLShowInteractionPopUpNotification";
NSString * const kNLShowPendingInteractionsNotification = @"kNLShowPendingInteractionsNotification";
NSString * const kNLHidePendingInteractionsNotification = @"kNLHidePendingInteractionsNotification";
NSString * const kNLHideInteractionPopUpNotification = @"kNLHideInteractionPopUpNotification";
NSString * const kNLLoadLatestInteractions = @"kNLLoadLatestInteractions";
NSString * const kNLOpenVenueNotification = @"kNLOpenVenueNotification";
NSString * const kNLOpenCameraNotification = @"kNLOpenCameraNotification";

NSString * const kNLLikeNotification = @"kNLLikeNotification";
NSString * const kNLFavNotification = @"kNLFavNotification";
