//
//  NLAuthentication.h
//  Yummmie
//
//  Created by bot on 12/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"

typedef NS_ENUM(NSInteger, NLAuthenticationType)
{
    NLAuthenticationTypeFacebook,
    NLAuthenticationTypeTwitter,
	NLAuthenticationTypePushNotification,
    NLAuthenticationTypeFoursquare,
    NLAuthenticationTypeNone
};

@interface NLAuthentication : NSObject

@property (nonatomic) NLAuthenticationType provider;
@property (nonatomic, copy, readonly) NSString * token;
@property (nonatomic, copy, readonly) NSString * tokenSecret;
@property (nonatomic, copy, readonly) NSString * userId;
@property (nonatomic, copy, readonly) NSString *authenticationID;

- (instancetype)initWithDictionary:(NSDictionary *)authenticationDict;


+ (NSURLSessionDataTask *)addAuthenticationWithType:(NLAuthenticationType)authType userID:(NSUInteger)userID token:(NSString *)token tokenSecret:(NSString *)tokenSecret withCompletionBlock:(void (^)(NSError *error, NLAuthentication *authentication))block;

+ (NSURLSessionDataTask *)searchFriendsWithType:(NLAuthenticationType)authType withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;

+ (NSURLSessionDataTask *)searchFriendsWithEmails:(NSString *)emailsString withCompletionBlock:(void (^)(NSError *error, NSArray *users))block;
+ (NLAuthenticationType)authenticationTypeForString:(NSString *)authenticationName;

+ (NSURLSessionDataTask *)removeAuthenticationWithType:(NLAuthenticationType)authenticationId withCompletionBlock:(void (^)(NSError *))block;

@end
