//
//  NSString+NLStringValidations.h
//  Yummmie
//
//  Created by bot on 6/6/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NLStringValidations)

+ (BOOL)isValidEmail:(NSString *)email;
+ (BOOL)isEmpty:(NSString *)string;
+ (BOOL)isValidUsername:(NSString *)username;


- (BOOL)validateURL;

@end