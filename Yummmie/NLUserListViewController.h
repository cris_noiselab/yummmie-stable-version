//
//  NLUserListViewController.h
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//


#import "NLTableViewController.h"
#import "NLUserListViewCell.h"

typedef NS_ENUM(NSInteger, NLUserListType)
{
    NLUserListFollowers,
    NLUserListFollowing,
    NLUserListSuggestions
};

@interface NLUserListViewController : NLTableViewController<NLUserListViewCellDelegate>

- (id)initWithUserListType:(NLUserListType)userListType andUserId:(NSUInteger)userId;

@end