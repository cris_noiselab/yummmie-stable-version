//
//  NLVenueFlowLayout.h
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLVenueFlowLayout : UICollectionViewFlowLayout

@end
