//
//  NLFoursquareSearchViewController.h
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "NLCustomSearchViewController.h"

@class NLVenue;

@protocol NLFoursquareSearchDelegate;

@interface NLFoursquareSearchViewController : NLBaseViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate,NLCustomSearchDelegate>

@property (weak, nonatomic) id<NLFoursquareSearchDelegate> delegate;

@end

@protocol NLFoursquareSearchDelegate <NSObject>


- (void)didFinishSelectingWithVenue:(NLVenue *)venue;

@end