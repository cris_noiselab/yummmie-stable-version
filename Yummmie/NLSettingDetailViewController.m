//
//  NLSettingDetailViewController.m
//  Yummmie
//
//  Created by bot on 7/29/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLSettingDetailViewController.h"
#import "NLUser.h"
#import "NLSession.h"
#import "NLNotifications.h"
#import "NSString+NLStringValidations.h"

@interface NLSettingDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textFieldInput;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (strong, nonatomic) UIBarButtonItem *saveButton;


- (void)saveAction:(id)sender;

@end

@implementation NLSettingDetailViewController

- (id)initWithSettingDetail:(SettingDetailType)detailType
{
    if (self = [super initWithNibName:NSStringFromClass([NLSettingDetailViewController class]) bundle:nil])
    {
        _detailType = detailType;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save_button_title", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveAction:)];
    
    
    self.navigationItem.rightBarButtonItem = self.saveButton;
    
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = logoView;
    
    
    
    [self setupFields];
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    NSString *name = [NSString stringWithFormat:@"Setting Detail %ld View", (long)_detailType];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveAction:(id)sender
{
    [self showLoadingSpinner];
    __weak NLSettingDetailViewController *weakSelf = self;
    void (^block)(NSError *,NLUser *) = ^void (NSError * error, NLUser *user)
    {
        if (error)
        {
            //NSString *errorMsg = NSLocalizedString([error.userInfo objectForKey:@"message"], nil);
            //NSLog(@"%@", errorMsg);
            [weakSelf showErrorMessage:NSLocalizedString(@"update_error", nil) withDuration:2.0f];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[weakSelf navigationItem] setRightBarButtonItem:[weakSelf saveButton]];
            });
        }else
        {
            [NLSession updateUser:user];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNLUpdateSettingsNotification object:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[weakSelf navigationItem] setRightBarButtonItem:[weakSelf saveButton]];
                [[weakSelf navigationController] popViewControllerAnimated:YES];
            });
        }
    };
    NLUser *user = [NLSession currentUser];
    if ([self detailType] == NameSettingDetail)
    {
        [NLUser updateUserWithUserId:[user userID] name:[[self textFieldInput] text] website:nil bio:nil coverPhoto:nil avatarPhoto:nil email:nil username:nil withCompletionBlock:block];
        
    }else if ([self detailType] == BioSettingDetail)
    {
        [NLUser updateUserWithUserId:[user userID] name:nil website:nil bio:[[self textView] text] coverPhoto:nil avatarPhoto:nil email:nil username:nil withCompletionBlock:block];
    }else if ([self detailType] == WebSettingDetail)
    {
        [NLUser updateUserWithUserId:[user userID] name:nil website:[[self textFieldInput] text] bio:nil coverPhoto:nil avatarPhoto:nil email:nil username:nil withCompletionBlock:block];
        
    }else if ([self detailType] == EmailSettingDetail)
    {
        if ([NSString isValidEmail:[[self textFieldInput] text]])
        {
            [NLUser updateUserWithUserId:[user userID] name:nil website:nil bio:nil coverPhoto:nil avatarPhoto:nil email:[[self textFieldInput] text] username:nil withCompletionBlock:block];
        }else
        {
            [self hideLoadingSpinner];
            [self showErrorMessage:NSLocalizedString(@"email_field_error", nil) withDuration:3.0f];
            self.navigationItem.rightBarButtonItem = self.saveButton;
        }
        
    }else if ( [self detailType] == UsernameDetail)
    {
        if ([NSString isValidUsername:[[self textFieldInput] text]])
        {
            [NLUser updateUserWithUserId:[user userID] name:nil website:nil bio:nil coverPhoto:nil avatarPhoto:nil email:nil username:[[self textFieldInput] text] withCompletionBlock:block];
        }else
        {
            [self hideLoadingSpinner];
            [self showErrorMessage:NSLocalizedString(@"username_field_error", nil) withDuration:3.0f];
            self.navigationItem.rightBarButtonItem = self.saveButton;
        }
        
    }
}

- (void)setupFields
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    if ([self detailType] == NameSettingDetail)
    {
        [[self textFieldInput] setText:[[NLSession currentUser] name]];
        [[self textFieldInput] setPlaceholder:NSLocalizedString(NSLocalizedString(@"join_name_text", nil), nil)];
        [[self textView] setHidden:YES];
        
        UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frame.size.width, self.textFieldInput.frame.size.height)];
        [background setBackgroundColor:[UIColor whiteColor]];
        [[self view] insertSubview:background belowSubview:self.textFieldInput];
        [[self textFieldInput] becomeFirstResponder];
        
    }else if ([self detailType] == WebSettingDetail)
    {
        [[self textFieldInput] setText:([[[NLSession currentUser] website] length]>0)?[[NLSession currentUser] website]:@"http://"];
        [[self textFieldInput] setPlaceholder:NSLocalizedString(@"website_text", nil)];
        [[self textView] setHidden:YES];
        
        UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frame.size.width, self.textFieldInput.frame.size.height)];
        [background setBackgroundColor:[UIColor whiteColor]];
        [[self view] insertSubview:background belowSubview:self.textFieldInput];
        [[self textFieldInput] becomeFirstResponder];
        
    }else if ([self detailType] == BioSettingDetail)
    {
        [[self textView] setText:[[NLSession currentUser] bio]];
        [[self textFieldInput] setHidden:YES];
        
        UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frame.size.width, self.textView.frame.size.height)];
        [background setBackgroundColor:[UIColor whiteColor]];
        [[self view] insertSubview:background belowSubview:self.textView];
        [[self textView] becomeFirstResponder];
        
    }else if([self detailType] == EmailSettingDetail)
    {
        [[self textFieldInput] setText:([[[NLSession currentUser] email] length]>0)?[[NLSession currentUser] email]:nil];
        [[self textFieldInput] setPlaceholder:NSLocalizedString(@"email_text", nil)];
        [[self textView] setHidden:YES];
        
        UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frame.size.width, self.textFieldInput.frame.size.height)];
        [background setBackgroundColor:[UIColor whiteColor]];
        [[self view] insertSubview:background belowSubview:self.textFieldInput];
        [[self textFieldInput] becomeFirstResponder];
    }else if ([self detailType] == UsernameDetail)
    {
        [[self textFieldInput] setText:([[[NLSession currentUser] username] length]>0)?[[NLSession currentUser] username]:nil];
        [[self textFieldInput] setPlaceholder:NSLocalizedString(@"username_text", nil)];
        [[self textView] setHidden:YES];
        
        UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootView.frame.size.width, self.textFieldInput.frame.size.height)];
        [background setBackgroundColor:[UIColor whiteColor]];
        [[self view] insertSubview:background belowSubview:self.textFieldInput];
        [[self textFieldInput] becomeFirstResponder];
    }
}

@end
