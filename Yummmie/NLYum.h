//
//  NLYum.h
//  Yummmie
//
//  Created by bot on 7/15/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"
@class NLUser;
@class NLVenue;
@class NLComment;

@interface NLYum : NSObject


@property (nonatomic, readwrite) NSInteger yumId;
@property (nonatomic, copy) NSString *caption;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *thumbImage;
@property (nonatomic, copy) NSString *bigImage;
@property (nonatomic, strong) NLUser *user;
@property (nonatomic, strong) NLVenue *venue;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, readwrite) NSInteger likes;
@property (nonatomic, readwrite) BOOL userLikes;
@property (nonatomic, readwrite) BOOL isFav;
@property (nonatomic, readwrite) NSInteger favs;
@property (nonatomic, readwrite) NSInteger reposts;
@property (nonatomic, readwrite) BOOL isReposted;
@property (nonatomic, readwrite) NSInteger viewsCount;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) NSDictionary *createdDict;
@property (nonatomic, strong) NSString *created;
@property (nonatomic, strong) NSString *shareUrl;

@property (nonatomic) NSInteger numberOfComments;
@property (nonatomic, strong) NSDictionary *repost;

- (id)initWithDictionary:(NSDictionary *)yumDict;

- (BOOL)belongsToUser:(NLUser *)user;

+ (NSURLSessionDataTask *)postYum:(UIImage *)photo caption:(NSString *)caption name:(NSString *)name andVenueId:(NSString *)venueId withCompletionBlock:(void (^)(NSError *error, NLYum * yum))block;

+ (NSURLSessionDataTask *)postYum:(UIImage *)photo caption:(NSString *)caption name:(NSString *)name andVenueId:(NSString *)venueId providers:(NSString *)providers withCompletionBlock:(void (^)(NSError *error, NLYum * yum))block;

+ (NSURLSessionDataTask *)postYum:(UIImage *)photo caption:(NSString *)caption name:(NSString *)name andVenueId:(NSString *)venueId providers:(NSString *)providers progress:(NSProgress *__autoreleasing *)progress withCompletionBlock:(void (^)(NSError *, NLYum *))block;

+ (NSURLSessionDataTask *)editYumId:(NSInteger)yumId dict:(NSDictionary *)dict withCompletionBlock:(void (^)(NSError *error, NLYum * yum))block;


+ (NSURLSessionDataTask *)getYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *, NLYum *))block;



+ (NSURLSessionDataTask *)likeUnlike:(NSInteger)yumId like:(BOOL)like withCompletionBlock:(void (^)(NSError *))block;

+ (NSURLSessionDataTask *)likeYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError * error))block;

+ (NSURLSessionDataTask *)unlikeYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error))block;

+ (NSURLSessionDataTask *)favUnfav:(NSInteger)yumId fav:(BOOL)fav withCompletionBlock:(void (^)(NSError *))block;

+ (NSURLSessionDataTask *)destroy:(NSInteger)yumId withCompletionBlock:(void(^)(NSError *error))block;

//+ (NSURLSessionDataTask *)getTimelineFromUserId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block;

//+ (NSURLSessionDataTask *)getTimelineYumsBeforeId:(NSInteger)yumId userId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block;

//+ (NSURLSessionDataTask *)getTimelineYumsAfterId:(NSInteger)yumId userId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block;

+ (NSURLSessionDataTask *)getTimelinePage:(NSInteger)page forUserId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block;

+ (NSURLSessionDataTask *)postComment:(NSString *)comment yumId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error, NLComment *comment))block;
+ (NSURLSessionDataTask *)deleteCommentWithId:(NSInteger)commentId fromYumId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error))block;


//+ (NSURLSessionDataTask *)loadYumsFromUserId:(NSInteger)userId from:(NSInteger)yumId withcompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block;
+ (NSURLSessionDataTask *)loadYumsFromUserId:(NSInteger)userId page:(NSInteger)page withcompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block;

+ (NSURLSessionDataTask *)loadYumsFromVenueId:(NSString *)venueId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block;

+ (NSURLSessionDataTask *)loadYumsFromTagId:(NSInteger)tagId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages, NSInteger count))block;


+ (NSURLSessionDataTask *)loadYumsFromStringTag:(NSString *)tag;


+ (NSURLSessionDataTask *)loadYumsFromDishName:(NSString *)dishName page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block;

+ (NSURLSessionDataTask *)searchYum:(NSString *)dishName withYums:(BOOL)yums page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block;

+ (NSURLSessionDataTask *)getLikersForYum:(NSInteger)yumId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *users, NSInteger count))block;
+ (NSURLSessionDataTask *)getFaversForYum:(NSInteger)yumId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *, NSArray *, NSInteger))block;
+ (NSURLSessionDataTask *)getRepostersForYum:(NSInteger)yumId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *, NSArray *, NSInteger))block;
+ (NSURLSessionDataTask *)getYumwithId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError * error, NLYum *yum))block;
+ (NSURLSessionDataTask *)getYumwithStringId:(NSString *)yumId withCompletionBlock:(void (^)(NSError * error, NLYum *yum))block;
+ (NSURLSessionDataTask *)getUserLikes:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *yums))block;
+ (NSURLSessionDataTask *)getRegions :(void (^)(NSError *error, NSArray *yums))block;


+ (NSURLSessionDataTask *)reportYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error))block;

//+ (NSURLSessionDataTask *)loadYumsFromUserId:(NSInteger)userId fromYumId:(NSInteger)firstYum toLastYum:(NSInteger)lastYum withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block;

+ (NSURLSessionDataTask *)loadRandomYumsWithCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block;

+ (NSURLSessionDataTask *)getYumsWithName:(NSString *)yumName withCompletionBlock:(void (^)(NSError * error, NSArray *yums))block;
+ (NSURLSessionDataTask *)getYumsWithName:(NSString *)yumName page:(NSInteger)page withCompletionBlock:(void (^)(NSError * error, NSArray *yums))block;
+ (NSURLSessionDataTask *)repostYumId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError * error))block;

+ (NSURLSessionDataTask *)sendViewedYums:(NSString *)yums withCompletionBlock:(void (^)(NSError *error))block;

@end
