//
//  NLLikersViewController.h
//  Yummmie
//
//  Created by bot on 8/13/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@class NLYum;

@interface NLLikersViewController : NLBaseViewController

- (id)initWithYum:(NLYum *)yum andOption:(NSInteger)opt;

@end
