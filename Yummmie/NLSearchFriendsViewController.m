//
//  NLSearchFriendsViewController.m
//  Yummmie
//
//  Created by bot on 12/18/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <YKTwitterHelper/YATTwitterHelper.h>

#import "NLSearchFriendsViewController.h"
#import "NLAuthentication.h"
#import "NLSearchFriendsListViewController.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLSearchByUsernameViewController.h"


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface NLSearchFriendsViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *searchFriendsTable;
@property (strong, nonatomic) YATTwitterHelper *twitterHelper;
@property (nonatomic) BOOL skipButton;
@property (nonatomic,strong) UIBarButtonItem *skipBarButton;

- (void)dismissView:(id)sender;

@end

@implementation NLSearchFriendsViewController

- (instancetype)initWithSkipButton:(BOOL)addSkipButton;
{
    if (self = [super initWithNibName:NSStringFromClass([NLSearchFriendsViewController class]) bundle:nil])
    {
        _skipButton = addSkipButton;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;    
    self.title = NSLocalizedString(@"search_users_title", nil);
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    
    [self.searchFriendsTable setTableFooterView:footerView];
    
    self.skipBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"close_title", nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismissView:)];
    
    self.navigationItem.leftBarButtonItem = self.skipBarButton;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self hideLoadingSpinner];
    [[self searchFriendsTable] deselectRowAtIndexPath:[[self searchFriendsTable] indexPathForSelectedRow] animated:YES];
    
    
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"buscar amigos View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if ([self.searchFriendsTable respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.searchFriendsTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.searchFriendsTable respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.searchFriendsTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *searchFriendsCellIdentifier = @"searchFriendsCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:searchFriendsCellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchFriendsCellIdentifier];
    }
    
    switch (indexPath.row)
    {
				
        case 0:
        {
            cell.textLabel.text =NSLocalizedString(@"find_friends_twitter", nil);
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_invite_tw"]];
            break;
        }
        case 1:
        {
            cell.textLabel.text = NSLocalizedString(@"find_friends_facebook", nil);
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_invite_fb"]];
            break;
        }
        case 2:
        {
            cell.textLabel.text = NSLocalizedString(@"find_friends_by_username", nil);
            [[cell imageView] setImage:[UIImage imageNamed:@"icn_username"]];
            break;
        }
        default:
            break;
    }
    [cell.textLabel setAdjustsFontSizeToFitWidth:true];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showLoadingSpinner];
    __weak NLSearchFriendsViewController *weakSelf = self;
    
    switch (indexPath.row)
    {
        case 0:
        {
					if ([NLSession containsAuthentication:NLAuthenticationTypeTwitter])
					{
						//[[weakSelf searchFriendsTable] deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];

						[weakSelf hideLoadingSpinner];

						NLSearchFriendsListViewController *listVC = [[NLSearchFriendsListViewController alloc] initWithSocialListType:TwitterList];
						[[weakSelf navigationController] pushViewController:listVC animated:YES];
					}
					else
					{
						
					
            self.twitterHelper  = [[YATTwitterHelper alloc] initWithKey:@"Aa1eoNrMZhOpALcwz1nOEND3d"
                andSecret:@"60dQjthbgmN3Zav85v424VovMIh0FTFXB9ugH6PzjalGtBpw9p"];
            [self.twitterHelper reverseAuthWithSuccess:^(NSDictionary *data)
            {
                [[weakSelf searchFriendsTable] deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
							[NLAuthentication addAuthenticationWithType:NLAuthenticationTypeTwitter userID:[NLSession getUserID] token:[data valueForKey:@"oauth_token"] tokenSecret:[data valueForKey:@"oauth_token_secret"] withCompletionBlock:^(NSError *error, NLAuthentication *authentication)
							{
									[weakSelf hideLoadingSpinner];
                    
									if (!error)
									{
										[[[NLSession currentUser] authentications] addObject:authentication];
											NLSearchFriendsListViewController *listVC = [[NLSearchFriendsListViewController alloc] initWithSocialListType:TwitterList];
											[[weakSelf navigationController] pushViewController:listVC animated:YES];
									}
								
									
                }];
                
            } failure:^(NSError *error)
            {
                [weakSelf hideLoadingSpinner];
                [weakSelf showErrorMessage:NSLocalizedString(@"error_message_twitter_permission", nil) withDuration:3.0f];
                [[weakSelf searchFriendsTable] deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
            }];
					}
            break;
        }
        case 1:
        {
					if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"user_friends"])
					{
						NLSearchFriendsListViewController *listVC = [[NLSearchFriendsListViewController alloc] initWithSocialListType:FacebookList];
						[[weakSelf navigationController] pushViewController:listVC animated:YES];
					}
					else
					{
						FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
						[login logInWithReadPermissions: @[@"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
						{
							
							 if (error)
							 {
								 NSLog(@"Process error");
							 }
							 else if (result.isCancelled)
							 {
								 NSLog(@"Cancelled");
							 }
							 else
							 {
								 [NLAuthentication addAuthenticationWithType:NLAuthenticationTypeFacebook userID:[NLSession getUserID] token: [[FBSDKAccessToken currentAccessToken] tokenString] tokenSecret:nil withCompletionBlock:^(NSError *error, NLAuthentication *authentication)
									{
										
									}];

								 NLSearchFriendsListViewController *listVC = [[NLSearchFriendsListViewController alloc] initWithSocialListType:FacebookList];
								 [[weakSelf navigationController] pushViewController:listVC animated:YES];
							 }
						 }];
						
					}
					break;
        }
        case 2:
        {
            NLSearchByUsernameViewController *searchByUsernameVC = [[NLSearchByUsernameViewController alloc] initWithNibName:NSStringFromClass([NLSearchByUsernameViewController class]) bundle:nil];
            [[weakSelf navigationController] pushViewController:searchByUsernameVC animated:YES];
            [[weakSelf searchFriendsTable] deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
            break;
        }
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark - dismissView

- (void)dismissView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
