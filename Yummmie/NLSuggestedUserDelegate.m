//
//  NLSuggestedUserDelegate.m
//  Yummmie
//
//  Created by bot on 3/2/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import "NLSuggestedUserDelegate.h"
#import "NLDiscoverUserSwipeCellView.h"
#import "NLSuggestedUser.h"
#import "NLUser.h"
#import "NLYum.h"

@interface NLSuggestedUserDelegate ()<NLDiscoverUserSwipeCellViewDelegate>

@property (nonatomic,weak) NSString *cellId;

@end

@implementation NLSuggestedUserDelegate


- (instancetype)initWithIdentifier:(NSString *)identifier
{
    if (self = [super init])
    {
        _cellId = identifier;
    }
    
    return self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.suggestedUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLDiscoverUserSwipeCellView *cell = [tableView dequeueReusableCellWithIdentifier:[self cellId] forIndexPath:indexPath];
    
    NLSuggestedUser *suggested = [[self suggestedUsers] objectAtIndex:[indexPath row]];
    NLUser *user = [suggested user];
    
    [[cell nameLabel] setText:[user name]];
    [[cell usernameButton] setTitle:[NSString stringWithFormat:@"@%@",[user username]] forState:UIControlStateNormal];
    [[cell userPhotoButton] sd_setBackgroundImageWithURL:[NSURL URLWithString:[user avatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
    [[cell coverImage] sd_setImageWithURL:[NSURL URLWithString:[user cover]] placeholderImage:[UIImage imageNamed:@"defaultCover"]];
    [cell setTag:[indexPath row]];
    [cell setDelegate:self];
    
    NLYum *yum = [[suggested yummmies] firstObject];
    NLYum *secondYum = [[suggested yummmies] objectAtIndex:1];
    NLYum *thirdYum = [[suggested yummmies] lastObject];
    
    [[cell dishButton] sd_setBackgroundImageWithURL:[NSURL URLWithString:[yum thumbImage]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholderDish"]];
    [[cell firstDish] sd_setImageWithURL:[NSURL URLWithString:[secondYum thumbImage]] placeholderImage:[UIImage imageNamed:@"placeholderDish"]];
    [[cell secondDish] sd_setImageWithURL:[NSURL URLWithString:[thirdYum thumbImage]] placeholderImage:[UIImage imageNamed:@"placeholderDish"]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(openSuggestedDetailWithTag:)])
    {
        UITableViewCell *cell  = [tableView cellForRowAtIndexPath:indexPath];
        [[self delegate] openSuggestedDetailWithTag:cell.tag];
    }
}

#pragma mark - NLDiscoverUserSwipeCellViewDelegate

- (void)openDetailWithTag:(NSInteger)tag
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(openSuggestedDetailWithTag:)])
    {
        [[self delegate] openSuggestedDetailWithTag:tag];
    }
}

- (void)openDishWithTag:(NSInteger)tag
{
    if ([self delegate] && [self.delegate respondsToSelector:@selector(openSuggestedDishWithTag:)])
    {
        [[self delegate] openSuggestedDishWithTag:tag];
    }
}

@end