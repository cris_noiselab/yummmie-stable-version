//
//  NLZoomAnimationController.m
//  Yummmie
//
//  Created by bot on 7/4/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLZoomAnimationController.h"

@interface NLZoomAnimationController ()

@property (nonatomic, strong) id<UIViewControllerContextTransitioning> transitionContext;


- (void)presentationAnimation:(id<UIViewControllerContextTransitioning>)context;
- (void)dismissAnimation:(id<UIViewControllerContextTransitioning>)context;

@end

const CGFloat baseAlpha = 0.6f;
const CGFloat baseScale = 0.6f;


@implementation NLZoomAnimationController

- (id)init
{
    if (self = [super init])
    {
        _presentationDuration = 0.8f;
        _dismissDuration = 0.6f;
//        _dismissDuration = 10.0f;
    }
    return self;
}

#pragma mark - UIViewControllerAnimatedTransitioning Delegate

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return ([self isPresenting])?[self presentationDuration]:[self dismissDuration];
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    self.transitionContext = transitionContext;
    
    if ([self isPresenting])
    {
        [self presentationAnimation:transitionContext];
    }else
    {
        [self dismissAnimation:transitionContext];
    }
}

#pragma mark - Private Methods

- (void)presentationAnimation:(id<UIViewControllerContextTransitioning>)context
{
    
    UIView * inView = [context containerView];
    
    UIViewController *toViewController = [context viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromViewController = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    //creating a frame hided at the bottom
    toViewController.view.alpha = 0.6f;
    [toViewController.view.layer setTransform:[self secondTransformWithView:toViewController.view]];
    [inView insertSubview:toViewController.view belowSubview:fromViewController.view];
    CFTimeInterval duration = [self presentationDuration];
    
    [UIView animateKeyframesWithDuration:duration/2.0f delay:0.0f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        [UIView addKeyframeWithRelativeStartTime:duration/2.0f relativeDuration:0.5f animations:^{
            [toViewController.view.layer setTransform:CATransform3DIdentity];
            [toViewController.view setAlpha:1.0f];
        }];
    } completion:nil];
    
    [UIView animateWithDuration:duration/2 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        [fromViewController.view setFrame:CGRectOffset(fromViewController.view.frame, 0.0f, fromViewController.view.frame.size.height)];
    } completion:^(BOOL finished) {
        [self.transitionContext completeTransition:YES];
    }];
    
}

- (void)dismissAnimation:(id<UIViewControllerContextTransitioning>)context
{
    
    UIView* inView = [context containerView];
    
    UIViewController* toViewController = [context viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIViewController* fromViewController = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    CGRect offsetRect = CGRectOffset(toViewController.view.frame, 0.0f, CGRectGetHeight(inView.frame));
    CGRect originRect = [inView bounds];
    [toViewController.view setFrame:offsetRect];
    CATransform3D fromViewTransform = [self secondTransformWithView:fromViewController.view];
    //insertar la vista abajo
    [inView insertSubview:toViewController.view aboveSubview:fromViewController.view];
    CFTimeInterval duration = [self dismissDuration];
    
    [UIView animateKeyframesWithDuration:duration/2 delay:0.0f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        fromViewController.view.alpha = 0.6f;
        [fromViewController.view.layer setTransform:fromViewTransform];
    } completion:nil];
    
    [UIView animateWithDuration:duration/2 delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        [toViewController.view setFrame:originRect];
    } completion:^(BOOL finished) {
        [self.transitionContext completeTransition:YES];
    }];
}

-(CATransform3D)firstTransform{
    CATransform3D t1 = CATransform3DIdentity;
    t1.m34 = 1.0/-900;
    t1 = CATransform3DScale(t1, 0.95, 0.95, 1);
    t1 = CATransform3DRotate(t1, 15.0f*M_PI/180.0f, 1, 0, 0);
    
    return t1;
    
}

-(CATransform3D)secondTransformWithView:(UIView*)view{
    
    CATransform3D t2 = CATransform3DIdentity;
    t2.m34 = [self firstTransform].m34;
    t2 = CATransform3DTranslate(t2, 0, 0, 0);
    t2 = CATransform3DScale(t2, 0.93, 0.93, 1);
    
    return t2;
}

@end