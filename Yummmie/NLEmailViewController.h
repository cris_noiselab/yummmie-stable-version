//
//  NLEmailViewController.h
//  Yummmie
//
//  Created by Mauricio Ventura on 10/06/16.
//  Copyright © 2016 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NLBaseViewController.h"
@protocol NLEmailDelegate <NSObject>


-(void)sendEmail:(NSString *)email token:(NSString *)token;

@end
@interface NLEmailViewController : NLBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (nonatomic, weak) id <NLEmailDelegate> delegate; //define MyClassDelegate as delegate
@property (strong, nonatomic) NSString *token;
- (IBAction)enviar:(id)sender;

@end
