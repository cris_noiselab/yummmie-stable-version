//
//  NLSettingConstants.h
//  Yummmie
//
//  Created by bot on 12/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

//foursquare keys
extern NSString * const kSettingsAskedForFoursqaurePermissionKey;
extern NSString * const kSettingsCanUseFoursquareKey;
extern NSString * const kSettingsHideSocialReminder;
extern NSString * const kSettingsFirstTime;
