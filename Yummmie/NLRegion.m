//
//  NLRegion.m
//  Yummmie
//
//  Created by bot on 7/27/17.
//  Copyright © 2017 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLRegion.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLConstants.h"


static NSString * const kNLAuthenticationResponseAuthenticationKey = @"authentication";

@interface NLAuthentication ()

@property (nonatomic, copy, readwrite) NSString * token;
@property (nonatomic, copy, readwrite) NSString * tokenSecret;
@property (nonatomic, copy, readwrite) NSString * userId;
@property (nonatomic, copy, readwrite) NSString *authenticationID;

@end

@implementation NLRegion


- (instancetype)initWithDictionary:(NSDictionary *)regionDict
{
    if (self = [super init])
    {
        NSString *region_id= [NSString stringWithFormat: @"%f", [regionDict objectForKey:@"id" ]];
        
        _region_id = region_id;
        _name = [regionDict objectForKey:@"name"];
        
        NSArray *subRegions = [regionDict valueForKey:@"sub_regions"];
        if ([subRegions count])
        {
            NSMutableArray *tmpSubRegions = [NSMutableArray array];
            
            for (NSDictionary *subRegion in subRegions)
            {
                NLSubRegion *sub_Region = [[NLSubRegion alloc] initWithDictionary:@{@"id":[subRegion objectForKey:@"id"], @"name":[subRegion objectForKey:@"name"]}];
                [tmpSubRegions addObject:sub_Region];
            }
            
            _subRegions = [NSArray arrayWithArray:tmpSubRegions];
        }
    }
    
    return self;
}

+ (NSURLSessionDataTask *)getRegions :(void (^)(NSError *error, NSArray *regions))block
{
    
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSString *apiCallPath = [[NSString stringWithFormat:@"venues/food_regions"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [[NLYummieApiClient sharedClient] GET:apiCallPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (block)
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                NSLog(@"Answer: %@", [responseObject description]);
                /*
                 NSMutableArray *regions = [NSMutableArray array];
                 //NSMutableArray *subRegions =[NSMutableArray array];
                 miniYums=[responseObject valueForKey:@"mini_yums"];
                 for (NSDictionary *json in miniYums)
                 {
                 NLYum *region =[[NLYum alloc] initWithDictionary:json];
                 [regions addObject:region];
                 }
                 block(nil, [NSArray arrayWithArray:yums]);*/
                
                NSMutableArray *regions = [NSMutableArray array];
                regions=[responseObject valueForKey:@"food_regions"];
                NSMutableArray *regionsArray = [NSMutableArray array];
                for (NSDictionary *json in regions)
                {
                    NLRegion *region =[[NLRegion alloc] initWithDictionary:json];
                    [regionsArray addObject:region];
                }
                
               
                
                block(nil, [NSArray arrayWithArray:regionsArray]);
                NSLog(@"block done");
                
                
            }else
            {
                block([NSError new],nil);
            }
        }
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(error,nil);
    }];
}

@end
