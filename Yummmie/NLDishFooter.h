//
//  NLDishFooter.h
//  Yummmie
//
//  Created by bot on 7/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLDishFooter : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
