//
//  NLNavigationViewController.m
//  Yummmie
//
//  Created by bot on 7/8/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLNavigationViewController.h"
#import "NLNavigationBar.h"
#import "NLBaseViewController.h"
#import "NLTableViewController.h"
#import "UIColor+NLColor.h"
#import "NLVenue.h"
@interface NLNavigationViewController ()

@property (nonatomic, strong) UIProgressView *progressView;

@end

@implementation NLNavigationViewController
{
	NLVenue *last;
}
- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    if (self = [super initWithNavigationBarClass:[NLNavigationBar class] toolbarClass:nil])
    {
        self.viewControllers = @[rootViewController];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupProgressView
{
    CGRect rect = self.navigationBar.bounds;
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    rect.size = CGSizeMake(rootView.frame.size.width-20, 5);
    rect = CGRectOffset(rect, 10, 32);
    self.progressView = [[UIProgressView alloc] initWithFrame:rect];
    self.progressView.transform = CGAffineTransformMakeScale(1.0, 2);
    [[self progressView] setTrackTintColor:[UIColor nl_colorWith255Red:255 green:255 blue:255 andAlpha:100]];
    [[self progressView] setProgressTintColor:[UIColor whiteColor]];
    [[self navigationBar] addSubview:self.progressView];
    [[self progressView] setHidden:YES];
    
}

- (void)updateProgress:(float)progress
{
    CGFloat clampedProgress = (progress > 0.98)?0.98:progress;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self progressView] setHidden:NO];
        [self.progressView setProgress:clampedProgress animated:YES];
    });
}

- (UIViewController *)childViewControllerForStatusBarStyle
{
    return self.visibleViewController;
}

- (UIViewController *)childViewControllerForStatusBarHidden
{
    return self.visibleViewController;
}

- (void)scrollToTop
{
    if ([self.visibleViewController respondsToSelector:@selector(scrollToTop)])
    {
        id controller = self.visibleViewController;
        [controller scrollToTop];
    }
    
}

- (void)hideProgressView
{
    [self.progressView setHidden:YES];
}

- (void)completeProgress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self progressView] setProgress:1.0f animated:YES];
    });
}

- (void)addChildOnTop:(UIView *)view;
{

    [[self view] addSubview:view];
}
@end
