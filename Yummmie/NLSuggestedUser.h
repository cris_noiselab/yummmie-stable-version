//
//  NLSuggestedUser.h
//  Yummmie
//
//  Created by bot on 1/29/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NLUser;

@interface NLSuggestedUser : NSObject

@property (strong, nonatomic) NLUser *user;
@property (strong, nonatomic) NSArray *yummmies;


@end
