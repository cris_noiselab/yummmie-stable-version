//
//  NLBaseViewController.h
//  Yummmie
//
//  Created by bot on 6/2/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLBaseViewController : UIViewController

- (void)addRedBackground;
- (void)showErrorMessage:(NSString *)message withDuration:(float)seconds;
- (void)showErrorMessage:(NSString *)message popViewController:(BOOL)pop withDuration:(float)seconds;
- (void)showLoadingSpinner;
- (void)hideLoadingSpinner;

- (NSString *)errorToString:(NSError *)error;

- (void)scrollToTop;

@end