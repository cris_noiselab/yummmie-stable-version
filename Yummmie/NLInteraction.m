//
//  NLInteraction.m
//  Yummmie
//
//  Created by bot on 8/20/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLInteraction.h"
#import "NLSession.h"

static NSString * const kInteractionAfterKey = @"after_id";
static NSString * const kInteractionBeforeKey = @"before_id";
static NSString * const kInteractionOwnerKey = @"owner";
static NSString * const kInteracitonRecipientKey = @"recipient";
static NSString * const kInteractionTrackableTypeKey = @"trackable_type";
static NSString * const kInteractionIdKey = @"id";
static NSString * const kInteractionAvatarKey = @"avatar";
static NSString * const kInteractionThumbAvatarKey = @"thumb";
static NSString * const kInteractionUserNameKey = @"username";
static NSString * const kInteractionTypeRelationship = @"Relationship";
static NSString * const kInteractionTypeLike = @"Like";
static NSString * const kInteractionTypeRepost = @"Yum";
static NSString * const kInteractionTypeMention = @"Mention";
static NSString * const kInteractionTypeComment = @"Comment";
static NSString * const kInteractionTypeFav = @"Fav";

static NSString * const kInteractionTrackableKey = @"trackable";
static NSString * const kTrackableCreatedAtKey = @"created_at";
static NSString * const kTrackableYumIdKey = @"yum_id";
static NSString * const kImageKey = @"image";
static NSString * const kImageThumbKey = @"thumb";
static NSString * const kTrackableContentKey = @"content";

@implementation NLInteraction

- (instancetype)initWithDictionary:(NSDictionary *)interactionDict
{
    if (self = [super init])
    {
			NSLog(@"%@", interactionDict);
        _interactionId = [[interactionDict valueForKey:kInteractionIdKey] integerValue];
        _owner = [[interactionDict valueForKey:kInteractionOwnerKey] objectForKey:kInteractionUserNameKey];
			_ownerId = [[[interactionDict valueForKey:kInteractionOwnerKey] objectForKey:@"id"] integerValue];

        _recipient = [[interactionDict valueForKey:kInteracitonRecipientKey] objectForKey:kInteractionUserNameKey];
        _recipientAvatar = [[interactionDict valueForKey:kInteracitonRecipientKey] objectForKey:kInteractionAvatarKey];
        _ownerAvatar = [[interactionDict valueForKey:kInteractionOwnerKey] objectForKey:kInteractionAvatarKey];
        
        //_created = [NSDate sam_dateFromISO8601String:[interactionDict valueForKey:kTrackableCreatedAtKey]];
				_created = [interactionDict valueForKey:kTrackableCreatedAtKey];
			
        /*
        NSDate *tmpDate = [NSDate sam_dateFromISO8601String:[interactionDict valueForKey:kTrackableCreatedAtKey]];
        NSDate *now = [NSDate date];
        if ([now timeIntervalSinceDate:tmpDate]<0)
        {
            _created = now;
        }else
        {
            _created = tmpDate;
        }
				 */
        NSString *interactionType = [interactionDict valueForKey:kInteractionTrackableTypeKey];
        
        

        if ([interactionType isEqualToString:kInteractionTypeLike] )
        {
            _type = NLLikeInteraction;
            _yumId = [[[interactionDict valueForKey:kInteractionTrackableKey] objectForKey:kTrackableYumIdKey] integerValue];
            _imageURL = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKey:kImageKey];
            
        }
				else if ([interactionType isEqualToString:kInteractionTypeComment])
        {
            _type = NLCommentInteraction;
            _yumId = [[[interactionDict valueForKey:kInteractionTrackableKey] objectForKey:kTrackableYumIdKey] integerValue];
            _imageURL = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKey:kImageKey];
					NSLog(@"%@", interactionDict);
            _comment = [[interactionDict valueForKeyPath:@"comment"] valueForKey:kTrackableContentKey];
        }
        else if ([interactionType isEqualToString:kInteractionTypeRelationship])
        {
            _type = NLFollowInteraction;
        }else if ([interactionType isEqualToString:kInteractionTypeMention])
        {
            _type = NLMentionInteraction;
            _yumId = [[[interactionDict valueForKey:kInteractionTrackableKey] objectForKey:kTrackableYumIdKey] integerValue];
            _imageURL = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKey:kImageKey];
					_comment = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKeyPath:@"content"];
				}
				else if ([interactionType isEqualToString:kInteractionTypeRepost])
				{
					_type = NLRepostInteraction;
					_yumId = [[[interactionDict valueForKey:kInteractionTrackableKey] objectForKey:kTrackableYumIdKey] integerValue];
					_imageURL = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKey:kImageKey];
				}
			
				else
        {
            _type = NLErrorInteraction;
        }
    }
    return self;
}

- (instancetype)initWithDictionaryForPeopleInteraction:(NSDictionary *)interactionDict
{

    if (self = [super init])
    {
			NSLog(@"%@", interactionDict);
        _interactionId = [[interactionDict valueForKey:kInteractionIdKey] integerValue];
        _owner = [[interactionDict valueForKey:kInteractionOwnerKey] objectForKey:kInteractionUserNameKey];
			_ownerId = [[[interactionDict valueForKey:kInteractionOwnerKey] objectForKey:@"id"] integerValue];

        _recipient = [[interactionDict valueForKey:kInteracitonRecipientKey] objectForKey:kInteractionUserNameKey];
			_recipientId = [[[interactionDict valueForKey:kInteracitonRecipientKey] objectForKey:@"id"] integerValue];

        _recipientAvatar = [[interactionDict valueForKey:kInteracitonRecipientKey] objectForKey:kInteractionAvatarKey];
        _ownerAvatar = [[interactionDict valueForKey:kInteractionOwnerKey] objectForKey:kInteractionAvatarKey];
        /*NSDate *tmpDate = [NSDate sam_dateFromISO8601String:[interactionDict valueForKey:kTrackableCreatedAtKey]];
        NSDate *now = [NSDate date];
        if ([now timeIntervalSinceDate:tmpDate]<0)
        {
            _created = now;
        }else
        {
            _created = tmpDate;
        }*/

        NSString *interactionType = [interactionDict valueForKey:kInteractionTrackableTypeKey];
        if ([interactionType isEqualToString:kInteractionTypeLike])
        {
            _type = NLPeopleLikeInteraction;
            _yumId = [[[interactionDict valueForKey:kInteractionTrackableKey] objectForKey:kTrackableYumIdKey] integerValue];
            _imageURL = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKey:kImageKey];
            
        }
				else if ([interactionType isEqualToString:kInteractionTypeComment])
        {
            _type = NLPeopleCommentInteraction;
            _yumId = [[[interactionDict valueForKey:kInteractionTrackableKey] objectForKey:kTrackableYumIdKey] integerValue];
            _imageURL = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKey:kImageKey];
					_comment = [[interactionDict valueForKeyPath:@"comment"] valueForKey:kTrackableContentKey];

        }else if ([interactionType isEqualToString:kInteractionTypeRelationship])
        {
            _type = NLPeopleFollowInteraction;
        }
				else if ([interactionType isEqualToString:kInteractionTypeMention])
        {
            _type = NLMentionInteraction;
					_comment = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKeyPath:@"content"];
        }
				else if ([interactionType isEqualToString:kInteractionTypeFav])
				{
					_type = NLPeopleFavInteraction;
					_yumId = [[[interactionDict valueForKey:kInteractionTrackableKey] objectForKey:kTrackableYumIdKey] integerValue];
					_imageURL = [[interactionDict valueForKey:kInteractionTrackableKey] valueForKey:kImageKey];
				}
				else
        {
            _type = NLErrorInteraction;
        }
    }
    return self;
}
+ (NSURLSessionDataTask *)getUserInteractionsAfter:(NSInteger)interactionId withCompletionBlock:(void (^)(NSError *, NSArray *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSLog(@"%@", [NLSession accessToken]);

    NSString *apiPath = @"activities/me";
    NSMutableDictionary *params;
    if (interactionId > 0 )
    {
        params = [NSMutableDictionary dictionary];
        [params setObject:[NSString stringWithFormat:@"%ld",(long)interactionId] forKey:kInteractionAfterKey];
    }
    
    return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            if (block)
            {
							
                if ([[responseObject objectForKey:@"success"] boolValue] == true)
                {
                    NSMutableArray *interactions = [NSMutableArray array];
                
                    for (NSDictionary *dict in [responseObject objectForKey:@"activities"])
                    {
                        NLInteraction *interaction = [[NLInteraction alloc] initWithDictionary:dict];
                        if (interaction.type != NLErrorInteraction)
                        {
                            [interactions addObject:interaction];
                        }
                    }
                    block(nil, interactions);
                }else
                {
									NSLog(@"%@\n%@", apiPath, responseObject);
                    block([NSError new],nil);
                }
            }
        
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
            NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
            if (block)
            {
                block(error,nil);
            }
        
        }];
    
}

+ (NSURLSessionDataTask *)getFollowingInteractionsAfter:(NSInteger)interactionId withCompletionBlock:(void (^)(NSError *, NSArray *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSString *apiPath = @"activities/following";
    NSMutableDictionary *params;
    if (interactionId > 0 )
    {
        params = [NSMutableDictionary dictionary];
        [params setObject:[NSString stringWithFormat:@"%ld",(long)interactionId] forKey:kInteractionAfterKey];
    }
    
    return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {


            if (block)
            {
							NSLog(@"response: %@", responseObject);
                if ([[responseObject objectForKey:@"success"] boolValue] == true)
                {
                    NSMutableArray *interactions = [NSMutableArray array];
                
                    for (NSDictionary *dict in [responseObject objectForKey:@"activities"])
                    {
                        NLInteraction *interaction = [[NLInteraction alloc] initWithDictionaryForPeopleInteraction:dict];
                        if (interaction.type != NLErrorInteraction)
                        {
                            [interactions addObject:interaction];
                        }
                    }
                    block(nil, interactions);
                }else
                {
									NSLog(@"%@\n%@", apiPath, responseObject);
                    block([NSError new],nil);
                }
            }
        
        } failure:^(NSURLSessionDataTask *task, NSError *error)
            {
                NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
                if (block)
                {
                    block(error,nil);
                }
        
            }];
}

+ (NSURLSessionDataTask *)getResumeWithCompletionBlock:(void (^)(NSError *error,NSNumber *followers, NSNumber *comments, NSNumber *likes))block
{
    if ([NLSession currentUser])
		{
        [[NLYummieApiClient sharedClient] requestSerializer];
        [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
			NSLog(@"token %@", [NSString stringWithFormat:@"Token token=%@", [NLSession accessToken]]);
    }
		else
    {
        return nil;
    }
    
    NSString *apiPath = @"activities";

    return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
			
        if ([[responseObject objectForKey:@"success"] boolValue] == true)
				{
					NSDictionary *activities = [responseObject objectForKey:@"activities"];
					            NSLog(@"Resumen: ");
            NSLog(@"%@", responseObject);
					
					NSInteger followers = [[activities objectForKey:@"Relationship"] integerValue];
					NSInteger comments = [[activities objectForKey:@"Comment"] integerValue];
					NSInteger likes = [[activities objectForKey:@"Like"] integerValue];
					

				
            if (followers > 0 || comments > 0 || likes > 0)
						{
                block(nil, [NSNumber numberWithInteger:followers], [NSNumber numberWithInteger:comments], [NSNumber numberWithInteger:likes]);
            }

        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
            {
                NSLog(@"%@",task.originalRequest.URL);
                NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
            }];
    
}
@end
