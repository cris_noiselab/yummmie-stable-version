//
//  NLPhotoThumbCollectionViewCell.h
//  Yummmie
//
//  Created by bot on 6/23/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLPhotoThumbCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photo;

- (void)highlight;
- (void)unHighlight;

@end
