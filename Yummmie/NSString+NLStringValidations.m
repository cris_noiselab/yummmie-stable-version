//
//  NSString+NLStringValidations.m
//  Yummmie
//
//  Created by bot on 6/6/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NSString+NLStringValidations.h"

static NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
//static NSString *usernameRegex =@"(?!.*__.*)[A-Za-z0-9_]{2,10}";
static NSString *usernameRegex =@"[A-Za-z0-9_]{1,50}";
@implementation NSString (NLStringValidations)

+ (BOOL)isValidEmail:(NSString *)email
{
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    return [emailPredicate evaluateWithObject:email];
}

+ (BOOL)isEmpty:(NSString *)string
{
    if (!string)
    {
        return YES;
    }
    if ([string length] == 0)
    {
        return YES;
    }
    
    if([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isValidUsername:(NSString *)username
{
    
    NSPredicate *usernamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",usernameRegex];
    return [usernamePredicate evaluateWithObject:username];
}

- (BOOL)validateURL
{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:self];
}
@end