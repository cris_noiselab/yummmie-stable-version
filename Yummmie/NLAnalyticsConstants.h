//
//  NLAnalyticsConstants.h
//  Yummmie
//
//  Created by bot on 4/27/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

//Sections Showed
extern NSString * const kNLAnalyticsSectionViewed;
extern NSString * const kNLAnalyticsSectionNameKey;
extern NSString * const kNLAnalyticsTimelineSection;
extern NSString * const kNLAnalyticsDiscoverSection;
extern NSString * const kNLAnalyticsInteractionSection;
extern NSString * const kNLAnalyticsProfileSection;
extern NSString * const kNLAnalyticsExternalProfileSection;
extern NSString * const kNLAnalyticsSettingsSection;
extern NSString * const kNLAnalyticsLikesSection;
extern NSString * const kNLAnalyticsCommentsSection;
extern NSString * const kNLAnalyticsUsernameKey;
extern NSString * const kNLAnalyticsYumId;
extern NSString * const kNLAnalyticsYumOwnerKey;

