//
//  NLTermsPolicyViewController.h
//  Yummmie
//
//  Created by bot on 6/5/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLTermsPolicyViewController : NLBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andURL:(NSURL *)url;

@end
