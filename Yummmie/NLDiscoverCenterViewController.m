//
//  NLDiscoverCenterViewController.m
//  Yummmie
//
//  Created by bot on 1/14/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <CoreLocation/CoreLocation.h>

#import "NLDiscoverCenterViewController.h"
#import "UIColor+NLColor.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLYum.h"
#import "NLPhotoDetailViewController.h"
#import "NLUser.h"
#import "NLSuggestedUser.h"
#import "NLDiscoverUserHeaderView.h"
#import "NLProfileViewController.h"
#import "NLNotifications.h"
#import "NLPreferences.h"
#import "NLAuthentication.h"
#import "NLSession.h"
#import "NLVenue.h"
#import "NLVenueViewController.h"
#import "NLTag.h"

#import "NLLikersViewCell.h"
#import "NLVenueViewCell.h"
#import "NLHashtagViewController.h"
#import "NLDishViewController.h"
#import "NLDiscoverUserFooterView.h"
#import "BSLayout.h"
#import "NLDiscoverUserSwipeCellView.h"
#import "NLSuggestedUserDelegate.h"
#import "NLInteraction.h"

#import "NLDishesCollectionViewController.h"

#import "NLPhotoThumbCollectionViewCell.h"

static NSString * const kNLDiscoverCenterUserCellIdenfitier = @"kNLDiscoverCenterUserCellIdenfitier";
static NSString * const kNLDiscoverCenterHashtagCellIdentifier = @"kNLDiscoverCenterHashtagCellIdentifier";
static NSString * const kNLDiscoverCenterVenueCellIdentifier = @"kNLDiscoverCenterVenueCellIdentifier";
static NSString * const kNLDiscoverCenterDishCellIdentifier = @"kNLDiscoverCenterDishCellIdentifier";

static NSString * const kNLDiscoverDishesCellViewIdentifier = @"kNLDiscoverDishesCellViewIdentifier";
static NSString * const kNLDiscoverUserHeaderViewIdentifier = @"kNLDiscoverUserHeaderViewIdentifier";
static NSString * const kNLDiscoverUserFooterViewIdentifier = @"kNLDiscoverUserFooterViewIdentifier";
static NSString * const kNLDiscoverSuggestedUserDishCellViewIdentifier = @"kNLDiscoverSuggestedUserDishCellViewIdentifier";

static NSString * const kPhotoDishesViewCellIdentifier = @"kPhotoDishesViewCellIdentifier";

static NSString * const kNLDiscoverUserViewCell = @"kNLDiscoverUserViewCell";

@interface NLDiscoverCenterViewController ()<UISearchBarDelegate,UICollectionViewDataSource,UICollectionViewDelegate,GMSMapViewDelegate,UITableViewDataSource,UITableViewDelegate,NLSuggestedUserDelegateProtocol>

@property (weak, nonatomic) IBOutlet UIImageView *venueImage;

@property (weak, nonatomic) IBOutlet UIButton *centerMapButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerMapBottomConstraint;
@property (nonatomic, copy) NSString *query;

@property (weak, nonatomic) IBOutlet UIView *venueDetailView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *venueDetailConstraint;
@property (weak, nonatomic) IBOutlet UILabel *venueName;
@property (weak, nonatomic) IBOutlet UILabel *venueAddress;
@property (weak, nonatomic) IBOutlet UILabel *venueDetailAddress;
@property (weak, nonatomic) IBOutlet UILabel *venueCity;
@property (weak, nonatomic) UIView *currentView;

@property (strong, nonatomic) UICollectionView *dishesCollection;
@property (strong, nonatomic) UICollectionView *favsCollection;
@property (strong, nonatomic) UITableView *suggestedTable;
@property (strong, nonatomic) NLSuggestedUserDelegate *suggestedDelegate;

@property (strong, nonatomic) UIRefreshControl *dishesRefreshControl;
@property (strong, nonatomic) GMSMapView *nearbyView;
@property (strong, nonatomic) NSMutableArray *randomDishes;
@property (strong, nonatomic) NSArray *discoverViews;
@property (strong, nonatomic) NSArray *suggestedUsers;
@property (strong, nonatomic) NSMutableArray *favs;

@property (nonatomic, strong) NSMutableSet *pinSet;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *optionsView;

@property (weak, nonatomic) IBOutlet UIButton *dishesButton;
@property (weak, nonatomic) IBOutlet UIButton *peopleButton;
@property (weak, nonatomic) IBOutlet UIButton *nearbyButton;
@property (weak, nonatomic) IBOutlet UIButton *favsButton;

@property (weak, nonatomic) IBOutlet UIView *searchFilterView;

@property (weak, nonatomic) NSURLSessionDataTask *dishesTask;
@property (weak, nonatomic) NSURLSessionDataTask *favsTask;

@property (weak, nonatomic) NSURLSessionDataTask *suggestedTask;

@property (strong, nonatomic) UIRefreshControl *suggestedRefreshControl;
@property (strong, nonatomic) UIRefreshControl *favsRefreshControl;

@property (nonatomic) BOOL firstUpdate;

@property (nonatomic, weak) NSURLSessionDataTask *nearestVenuesTask;

@property (nonatomic, weak) GMSMarker *selectedMarker;

@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) NSMutableArray *hashtags;
@property (strong, nonatomic) NSMutableArray *venues;
@property (strong, nonatomic) NSMutableArray *dishes;

@property (weak, nonatomic) IBOutlet UIButton *filterUserButton;
@property (weak, nonatomic) IBOutlet UIButton *filterHashtagButton;
@property (weak, nonatomic) IBOutlet UIButton *filterLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *filterDishButton;

@property (nonatomic) NLSearchScope currentSearchScope;
@property (weak, nonatomic) NSURLSessionDataTask *userSearchTask;
@property (weak, nonatomic) NSURLSessionDataTask *hashtagSearchTask;
@property (weak, nonatomic) NSURLSessionDataTask *venueSearchTask;
@property (weak, nonatomic) NSURLSessionDataTask *dishSearchTask;
@property (nonatomic, strong) UITableView *resultsTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchFilterViewTopXConstraint;

@property (nonatomic,getter=isShowingSearchResults) BOOL showingSearchResults;

@property (nonatomic, getter=wasMapDragged) BOOL mapDragged;

@property (nonatomic) BOOL showingAnimation;



- (IBAction)centerMap:(id)sender;

- (void)reloadRandomDishes:(id)sender;
- (void)reloadSuggestedUsers:(id)sender;

- (void)loadDishes;
- (void)loadSuggestedUsers;

- (void)askForFoursquarePermissions;
- (void)showMarkers;
- (void)hideMarkers;

- (void)setVenueDetailData:(NLVenue *)venue;
- (void)clearCurrentMarker;;

- (void)venueDetailViewTapped:(UITapGestureRecognizer *)tapGesture;

- (void)getNearestVenues;
- (void)getNearesVenuesWithCoordinate:(CLLocationCoordinate2D)coordinate;

- (void)showResultsTable;
- (void)hideResultsTable;
- (void)showSearchFiltersBelowView:(UIView *)view;
- (void)hideSearchFiltersBelowView:(UIView *)view;

- (void)userLogout:(NSNotification *)notification;
- (void)userLogin:(NSNotification *)notification;

@end

@implementation NLDiscoverCenterViewController
{
	UIActivityIndicatorView *spinner;
	BOOL noResults;
	NLDishesCollectionViewController *dishesVC;
	
	NSInteger currentDishesSearchPage;
	NSInteger totalDishesSearchPage;
}
- (void)viewDidLoad
{
	[super viewDidLoad];
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    [self.contentView setFrame:CGRectMake(0, 64, rootView.frame.size.width, rootView.frame.size.height-64)];
    NSLog(@"1Frame ContentView width:%f  height:%f   x:%f  y:%f",self.contentView.frame.size.width,self.contentView.frame.size.height,self.contentView.frame.origin.x,self.contentView.frame.origin.y);
	self.favs = [NSMutableArray array];
	self.showingAnimation = NO;
	[self.peopleButton setTitle:NSLocalizedString(@"discover_people_section_title", nil) forState:UIControlStateNormal];
	[self.peopleButton setTitle:NSLocalizedString(@"discover_people_section_title", nil) forState:UIControlStateSelected];
	
	[self.nearbyButton setTitle:NSLocalizedString(@"discover_nearby_section_title", nil) forState:UIControlStateNormal];
	[self.nearbyButton setTitle:NSLocalizedString(@"discover_nearby_section_title", nil) forState:UIControlStateSelected];
	
	[self.dishesButton setTitle:NSLocalizedString(@"discover_dishes_section_title", nil) forState:UIControlStateNormal];
	[self.dishesButton setTitle:NSLocalizedString(@"discover_dishes_section_title", nil) forState:UIControlStateSelected];
	[self.favsButton setTitle:NSLocalizedString(@"discover_favs_section_title", nil) forState:UIControlStateNormal];
	[self.favsButton setTitle:NSLocalizedString(@"discover_favs_section_title", nil) forState:UIControlStateSelected];
	
	[self.searchBar setPlaceholder:NSLocalizedString(@"search_bar_placeholder", nil)];
	self.automaticallyAdjustsScrollViewInsets = NO;
    [self setSearchType];
	_users = [NSMutableArray array];
	_hashtags = [NSMutableArray array];
	_venues = [NSMutableArray array];
	_dishes = [NSMutableArray array];
	
	
	
	CGRect resultsTableFrame = self.contentView.bounds;
	resultsTableFrame.origin.y = resultsTableFrame.size.height;
    //resultsTableFrame.origin.y = 0;
	self.resultsTableView = [[UITableView alloc] initWithFrame:resultsTableFrame style:UITableViewStylePlain];
	[self.resultsTableView setKeyboardDismissMode:UIScrollViewKeyboardDismissModeOnDrag];
	
		//[self.resultsTableView setContentInset:UIEdgeInsetsMake(CGRectGetHeight(self.searchFilterView.bounds), 0,CGRectGetHeight(self.tabBarController.tabBar.bounds),0)];
        [self.resultsTableView setContentInset:UIEdgeInsetsMake(0, 0,CGRectGetHeight(self.tabBarController.tabBar.bounds),0)];
    [self.resultsTableView setFrame:CGRectMake(self.resultsTableView.frame.origin.x, self.resultsTableView.frame.origin.y, self.resultsTableView.frame.size.width, self.resultsTableView.frame.size.height-50)];
	
	[[self contentView] addSubview:self.resultsTableView];
	
	[self.resultsTableView setDelegate:self];
	[self.resultsTableView setDataSource:self];
	[self.resultsTableView registerNib:[UINib nibWithNibName:NSStringFromClass([NLLikersViewCell class]) bundle:nil] forCellReuseIdentifier:kNLDiscoverCenterUserCellIdenfitier];
	[self.resultsTableView registerNib:[UINib nibWithNibName:NSStringFromClass([NLVenueViewCell class]) bundle:nil] forCellReuseIdentifier:kNLDiscoverCenterVenueCellIdentifier];
	
	NSLog(@"2Frame ContentView width:%f  height:%f   x:%f  y:%f",self.contentView.frame.size.width,self.contentView.frame.size.height,self.contentView.frame.origin.x,self.contentView.frame.origin.y);
	self.pinSet = [NSMutableSet set];
	
	self.venueImage.layer.cornerRadius = self.venueImage.frame.size.height/2.0f;
	
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(venueDetailViewTapped:)];
	[self.venueDetailView addGestureRecognizer:tapGesture];
	
	// Do any additional setup after loading the view from its nib.
	[self.navigationController setNavigationBarHidden:YES];
	
	CALayer *statusLayer = [CALayer layer];
	[statusLayer setFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
	[statusLayer setBackgroundColor:[UIColor nl_applicationRedColor].CGColor];
	
	[[[self view] layer] insertSublayer:statusLayer atIndex:0];
	
	CALayer *bottomLayer = [CALayer layer];
	//[bottomLayer setFrame:CGRectMake(0, CGRectGetMaxY(self.optionsView.bounds)-1, CGRectGetWidth(self.optionsView.bounds), 2)];
    [bottomLayer setFrame:CGRectMake(0, 43, CGRectGetWidth(self.optionsView.bounds), 1)];
	[bottomLayer setBackgroundColor:[UIColor colorWithRed:0.568f green:0.568f blue:0.568 alpha:1.0f].CGColor];
	[[[self optionsView] layer] insertSublayer:bottomLayer atIndex:0];
	
	BSLayout *dishesLayout = [[BSLayout alloc] init];
	self.dishesRefreshControl = [[UIRefreshControl alloc] init];
	[self.dishesRefreshControl addTarget:self action:@selector(reloadRandomDishes:) forControlEvents:UIControlEventValueChanged];
	[self.dishesRefreshControl setTintColor:[UIColor nl_applicationRedColor]];
	
	self.dishesCollection = [[UICollectionView alloc] initWithFrame:self.contentView.bounds collectionViewLayout:dishesLayout];
	[self.dishesCollection setDelegate:self];
	[self.dishesCollection setDataSource:self];
	[[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kNLDiscoverDishesCellViewIdentifier];
	[self.dishesCollection setContentInset:UIEdgeInsetsMake(self.optionsView.bounds.size.height+2, 0, 44, 0)];
	[self.dishesCollection setBackgroundColor:[UIColor whiteColor]];
	[self.dishesCollection addSubview:self.dishesRefreshControl];
	[self.dishesCollection setAlwaysBounceVertical:YES];
	
	UICollectionViewFlowLayout *suggestedLayout = [[UICollectionViewFlowLayout alloc] init];
	[suggestedLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
	[suggestedLayout setItemSize:CGSizeMake(320.0f, 100.f)];
	[suggestedLayout setMinimumInteritemSpacing:1.0f];
	[suggestedLayout setMinimumLineSpacing:0.0f];
	[suggestedLayout setSectionInset:UIEdgeInsetsMake(0, 0, 180, 0)];
	
	self.suggestedRefreshControl = [[UIRefreshControl alloc] init];
	[self.suggestedRefreshControl addTarget:self action:@selector(reloadSuggestedUsers:) forControlEvents:UIControlEventValueChanged];
	[self.suggestedRefreshControl setTintColor:[UIColor nl_applicationRedColor]];
	NSLog(@"3Frame ContentView width:%f  height:%f   x:%f  y:%f",self.contentView.frame.size.width,self.contentView.frame.size.height,self.contentView.frame.origin.x,self.contentView.frame.origin.y);
	
	self.suggestedDelegate = [[NLSuggestedUserDelegate alloc] initWithIdentifier:kNLDiscoverUserViewCell];
	[[self suggestedDelegate] setDelegate:self];
	
	UIView *footerSuggestedTableView = [[UIView alloc] initWithFrame:CGRectZero];
	
	CGRect suggestedFrame;
	if ([[UIScreen mainScreen] bounds].size.height < 568) {
		suggestedFrame = self.contentView.bounds;
		suggestedFrame.size.height = CGRectGetHeight(suggestedFrame)-94;
	}else
	{
		suggestedFrame = self.contentView.bounds;
	}
	self.suggestedTable = [[UITableView alloc] initWithFrame:suggestedFrame];
	[self.suggestedTable setTableFooterView:footerSuggestedTableView];
	[[self suggestedTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLDiscoverUserSwipeCellView class]) bundle:nil] forCellReuseIdentifier:kNLDiscoverUserViewCell];
	[self.suggestedTable setContentInset:UIEdgeInsetsMake(self.optionsView.bounds.size.height, 0, 43, 0)];
	[self.suggestedTable setBackgroundColor:[UIColor whiteColor]];
	[self.suggestedTable addSubview:self.suggestedRefreshControl];
	[self.suggestedTable setDelegate:[self suggestedDelegate]];
	[self.suggestedTable setDataSource:[self suggestedDelegate]];
	[self.suggestedTable setSeparatorColor:[UIColor blackColor]];
	
	self.nearbyView = [[GMSMapView alloc] initWithFrame:self.contentView.bounds];
	
	[self.nearbyView addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
	[self.nearbyView setDelegate:self];
	
	NSLog(@"4Frame ContentView width:%f  height:%f   x:%f  y:%f",self.contentView.frame.size.width,self.contentView.frame.size.height,self.contentView.frame.origin.x,self.contentView.frame.origin.y);
	UICollectionViewFlowLayout *favsLAyout = [[UICollectionViewFlowLayout alloc] init];
	[favsLAyout setScrollDirection:UICollectionViewScrollDirectionVertical];
	
    NSUInteger w = (self.contentView.frame.size.width - 2) / 3;
	[favsLAyout setItemSize:CGSizeMake(w, w)];
	[favsLAyout setMinimumInteritemSpacing:1.0f];
	[favsLAyout setMinimumLineSpacing:1.0f];
	[favsLAyout setSectionInset:UIEdgeInsetsMake(0, 0,180, 0)];
	
	self.favsRefreshControl = [[UIRefreshControl alloc] init];
	[self.favsRefreshControl addTarget:self action:@selector(reloadFavs:) forControlEvents:UIControlEventValueChanged];
	[self.favsRefreshControl setTintColor:[UIColor nl_applicationRedColor]];
	
	self.favsCollection = [[UICollectionView alloc] initWithFrame:self.contentView.bounds collectionViewLayout:favsLAyout];
	[self.favsCollection setDelegate:self];
	[self.favsCollection setDataSource:self];
	[[self favsCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kNLDiscoverDishesCellViewIdentifier];
	[self.favsCollection setContentInset:UIEdgeInsetsMake(46, 0, 44, 0)];
	[self.favsCollection setBackgroundColor:[UIColor whiteColor]];
	[self.favsCollection addSubview:self.favsRefreshControl];
	[self.favsCollection setAlwaysBounceVertical:YES];
	
	[[self favsCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kPhotoDishesViewCellIdentifier];
	
	
	
	
	
	
	
	//dishesVC = [[NLDishesCollectionViewController alloc] init];
	
	
	//[self.navigationController pushViewController:dishesVC animated:YES];
	
	
	self.discoverViews = @[self.dishesCollection,self.suggestedTable,self.nearbyView, self.favsCollection];
	
	[self nearbyAction:nil];
	
	[self.dishesButton setTitleColor:[UIColor nl_applicationRedColor] forState:UIControlStateSelected];
	[self.peopleButton setTitleColor:[UIColor nl_applicationRedColor] forState:UIControlStateSelected];
	[self.nearbyButton setTitleColor:[UIColor nl_applicationRedColor] forState:UIControlStateSelected];
	[self.favsButton setTitleColor:[UIColor nl_applicationRedColor] forState:UIControlStateSelected];
	
	[self.dishesButton setTitleColor:[UIColor nl_applicationGrayColor] forState:UIControlStateNormal];
	
	[[self searchBar] setPlaceholder:NSLocalizedString(@"search_bar_placeholder", nil)];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogout:) name:kNLLogoutNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogin:) name:kNLLoginNotification object:nil];
	NSLog(@"5Frame ContentView width:%f  height:%f   x:%f  y:%f",self.contentView.frame.size.width,self.contentView.frame.size.height,self.contentView.frame.origin.x,self.contentView.frame.origin.y);
	currentDishesSearchPage = 1;
	totalDishesSearchPage = 10;
}

-(void)setSearchType{
    if ((self.filterUserButton.selected == YES)||(self.filterHashtagButton.selected == YES)||(self.filterDishButton.selected == YES)||(self.filterLocationButton.selected == YES)){
        if (self.filterUserButton.selected == YES){
            [self filterUserButtonAction:nil];
        }
        if (self.filterHashtagButton.selected == YES){
            [self filterHashtagAction:nil];
        }
        if (self.filterDishButton.selected == YES){
            [self filterDishAction:nil];
        }
        if (self.filterLocationButton.selected == YES){
            [self filterLocationAction:nil];
        }
    }
    else{
        [self filterUserButtonAction:nil];
    }
    
}

- (BOOL)prefersStatusBarHidden
{
	return [self showingAnimation];
}

- (void)viewWillLayoutSubviews
{
	[super viewWillLayoutSubviews];
	NSLog(@"6Frame ContentView width:%f  height:%f   x:%f  y:%f",self.contentView.frame.size.width,self.contentView.frame.size.height,self.contentView.frame.origin.x,self.contentView.frame.origin.y);
	if ([self.suggestedTable respondsToSelector:@selector(setSeparatorInset:)])
	{
		[self.suggestedTable setSeparatorInset:UIEdgeInsetsZero];
	}
	
	if ([self.suggestedTable respondsToSelector:@selector(setLayoutMargins:)])
	{
		[self.suggestedTable setLayoutMargins:UIEdgeInsetsZero];
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	
	NSLog(@"7Frame ContentView width:%f  height:%f   x:%f  y:%f",self.contentView.frame.size.width,self.contentView.frame.size.height,self.contentView.frame.origin.x,self.contentView.frame.origin.y);
	//Google Analytics
	NSString *name = [NSString stringWithFormat:@"DiscoverCenter View"];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
	
	[self.view setUserInteractionEnabled:true];
	self.view.layer.opacity = 1;
	
	if (spinner)
	{
		[spinner stopAnimating];
		[spinner removeFromSuperview];
		spinner = nil;
	}
	self.favs = [NSMutableArray array];
	[self loadFavs];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[[self navigationController] setNavigationBarHidden:YES animated:YES];
	switch (self.selectAction)
	{
		case 1:
			[self.searchBar becomeFirstResponder];
			break;
		case 2:
			[self nearbyAction:self.nearbyButton];
			break;
		default:
			break;
	}
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
}
-(void)saveSearch
{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	switch (self.currentSearchScope)
	{
		case NLSearchScopeDish:
		{
			NSMutableArray *dishes = [def objectForKey:@"NLSearchScopeDish"];
			if (dishes)
			{
				if (dishes.count < 5)
				{
					
				}
				else
				{
					
				}
			}
		}
			break;
		case NLSearchScopeHashtag:
		{
			
		}
			break;
		case NLSearchScopeUser:
		{
			
		}
			break;
		case NLSearchScopeVenue:
		{
			
		}
			break;
			
		default:
			break;
	}
}
- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[self removeObserver:self forKeyPath:@"myLocation"];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"myLocation"])
	{
		if (![self firstUpdate])
		{
			self.firstUpdate = YES;
			CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
			[self.nearbyView setCamera:[GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:16]];
			[self getNearestVenues];
		}
	}
}

-(void)calculateNewCoordinate
{
	CGPoint mapCenterPoint = CGPointMake(CGRectGetMidX([[self nearbyView] bounds]), CGRectGetMidY([[self nearbyView] bounds]));
	CLLocationCoordinate2D pinCoordinate = [self.nearbyView.projection coordinateForPoint:mapCenterPoint];
	
	[self getNearesVenuesWithCoordinate:pinCoordinate];
}

#pragma mark - Get Nearest Venues

- (void)getNearestVenues
{
	__weak NLDiscoverCenterViewController *weakSelf = self;
	
	self.nearestVenuesTask = [NLVenue nearVenuesWithLatitude:self.nearbyView.myLocation.coordinate.latitude andLongitude:self.nearbyView.myLocation.coordinate.longitude withCompletionBlock:^(NSError *error, NSArray *venues) {
		if (!error)
		{
			//Create the google map Pins
			[[weakSelf pinSet] removeAllObjects];
			
			for (NLVenue *venue in venues)
			{
				if (venue.venueType != NLPrivateVenue) {
					
					GMSMarker *venueMarker = [[GMSMarker alloc] init];
					venueMarker.map = nil;
					//                venueMarker.appearAnimation = kGMSMarkerAnimationPop;
					venueMarker.icon = [UIImage imageNamed:@"pin_map_b"];
					venueMarker.position = CLLocationCoordinate2DMake(venue.latitude, venue.longitude);
					venueMarker.userData = venue;
					[weakSelf.pinSet addObject:venueMarker];
					
				}
			}
			
			dispatch_async(dispatch_get_main_queue(), ^{
				[weakSelf showMarkers];
			});
		}else
		{
			
		}
		
	}];
}

- (void)getNearesVenuesWithCoordinate:(CLLocationCoordinate2D)coordinate
{
	__weak NLDiscoverCenterViewController *weakSelf = self;
	[self.nearestVenuesTask cancel];
	self.nearestVenuesTask = [NLVenue nearVenuesWithLatitude:coordinate.latitude andLongitude:coordinate.longitude withCompletionBlock:^(NSError *error, NSArray *venues) {
		if (!error)
		{
			//Create the google map Pins
			[weakSelf hideMarkers];
			[[weakSelf pinSet] removeAllObjects];
			
			for (NLVenue *venue in venues)
			{
				if (venue.venueType != NLPrivateVenue) {
					GMSMarker *venueMarker = [[GMSMarker alloc] init];
					venueMarker.map = nil;
					venueMarker.appearAnimation = kGMSMarkerAnimationPop;
					venueMarker.icon = [UIImage imageNamed:@"pin_map_b"];
					venueMarker.position = CLLocationCoordinate2DMake(venue.latitude, venue.longitude);
					venueMarker.userData = venue;
					[weakSelf.pinSet addObject:venueMarker];
				}
			}
			
			dispatch_async(dispatch_get_main_queue(), ^{
				[weakSelf showMarkers];
			});
		}
		
	}];
}


#pragma mark show/hide results table

- (void)showResultsTable
{
	CGRect resultsFrame = self.resultsTableView.frame;
	resultsFrame.origin.y = 0;
	
	[UIView animateWithDuration:0.5 animations:^{
		[self.resultsTableView setFrame:resultsFrame];
	} completion:^(BOOL finished) {
		[self setShowingSearchResults:YES];
	}];
}

- (void)hideResultsTable
{
	CGRect resultsFrame = self.resultsTableView.frame;
	resultsFrame.origin.y = resultsFrame.size.height;
	
	[UIView animateWithDuration:0.5 animations:^{
		[self.resultsTableView setFrame:resultsFrame];
	} completion:^(BOOL finished)
	 {
		 [self.users removeAllObjects];
		 [self.venues removeAllObjects];
		 [self.hashtags removeAllObjects];
		 [self.dishes removeAllObjects];
		 [self.userSearchTask cancel];
		 [self.dishSearchTask cancel];
		 [self.venueSearchTask cancel];
		 [self.hashtagSearchTask cancel];
		 
		 [self.resultsTableView reloadData];
		 [self setShowingSearchResults:NO];
	 }];
}

#pragma mark - Detail View Tapped

- (void)venueDetailViewTapped:(UITapGestureRecognizer *)tapGesture
{
	NLVenueViewController *selectedVenue = [[NLVenueViewController alloc] initWithVenue:[[self selectedMarker] userData]];
	selectedVenue.hidesBottomBarWhenPushed = YES;
	[self.navigationController pushViewController:selectedVenue animated:YES];
}

#pragma mark - clear current marker

- (void)clearCurrentMarker
{
	self.selectedMarker.icon = [UIImage imageNamed:@"pin_map_b"];
	self.selectedMarker = nil;
}

#pragma mark - hide/show markers

- (void)showMarkers
{
	for (GMSMarker *marker in self.pinSet)
	{
		if (marker.map == nil)
		{
			marker.map = self.nearbyView;
		}
	}
}

- (void)hideMarkers
{
	for (GMSMarker *marker in self.pinSet)
	{
		marker.map = nil;
	}
}

#pragma mark - Hide/Show venue detail
- (void)showVenueDetail
{
	//    [self.view layoutIfNeeded];
	
	self.venueDetailConstraint.constant = self.tabBarController.tabBar.bounds.size.height;
	self.centerMapBottomConstraint.constant = (55.0f + CGRectGetHeight(self.venueDetailView.bounds));
	[UIView animateWithDuration:0.5 animations:^{
		[self.view layoutIfNeeded];
	} completion:nil];
}

- (void)hideVenueDetail
{
	//    [self.view layoutIfNeeded];
	self.venueDetailConstraint.constant = -self.venueDetailView.bounds.size.height;
	self.centerMapBottomConstraint.constant   = 55.0f;
	[UIView animateWithDuration:0.5 animations:^{
		[self.view layoutIfNeeded];
	} completion:nil];
}

#pragma mark - Set Venue Detail Data

- (void)setVenueDetailData:(NLVenue *)venue
{
	self.venueName.text = venue.name;
	self.venueAddress.text = venue.street;
	self.venueDetailAddress.text = venue.suburb;
	
	if ([venue.state isEqualToString:@"Federal District"])
	{
		self.venueCity.text = @"Distrito Federal";
	}else
	{
		self.venueCity.text = venue.state;
	}
}


#pragma mark - Ask for foursquare permissions

/*- (void)askForFoursquarePermissions
 {
 if ([UIAlertController class])
 {
 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"foursquare_permission_title", nil) message:NSLocalizedString(@"foursquare_permission_message", nil) preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"foursquare_permission_accept_title", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
 
 [Foursquare2 authorizeWithCallback:^(BOOL success, id result) {
 if (success)
 {
 [NLPreferences setCanUseFoursquare:YES];
 
 [Foursquare2 userGetDetail:@"self" callback:^(BOOL success, id result)
 {
 NSString *userId = [[[result objectForKey:@"response"] objectForKey:@"user"] objectForKey:@"id"];
 [NLAuthentication addAuthenticationWithType:NLAuthenticationTypeFoursquare userID:userId token:[Foursquare2 accessToken] tokenSecret:nil withCompletionBlock:^(NSError *error, NLAuthentication *authentication) {
 if (!error)
 {
 [[[NLSession currentUser] authentications] addObject:authentication];
 }
 }];
 }];
 }
 }];
 }];
 UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"foursquare_permission_cancel_title", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
 
 }];
 [alertController addAction:acceptAction];
 [alertController addAction:cancelAction];
 
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
 [self presentViewController:alertController animated:YES completion:^{
 [NLPreferences setFoursquarePermissionAsked:YES];
 }];
 });
 
 }else
 {
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
 
 [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"foursquare_permission_title", nil) message:NSLocalizedString(@"foursquare_permission_message", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"foursquare_permission_cancel_title", nil) otherButtonTitles:NSLocalizedString(@"foursquare_permission_accept_title", nil), nil] show];
 [NLPreferences setFoursquarePermissionAsked:YES];
 });
 
 }
 
 }*/


#pragma mark - Load Suggested Users
- (void)loadFavs
{
	__weak NLDiscoverCenterViewController *weakSelf = self;
	NSInteger firstYum = 0;
	if (self.favs.count > 0)
	{
		firstYum = [self.favs.firstObject yumId];
	}
	self.favsTask =[NLUser getFavsAfterId:firstYum withCompletionBlock:^(NSError *error, NSArray *yums)
									{
										[[weakSelf favsRefreshControl] endRefreshing];
										
										if (!error)
										{
											if ([yums count] > 0)
											{
												weakSelf.favs = [yums mutableCopy];
											}
											else
											{
												weakSelf.favs = [NSMutableArray array];
												
												//self.sugges
											}
										}
										else
										{
											weakSelf.favs = [NSMutableArray array];
											
											//[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:2.0f];
										}
										[weakSelf.favsCollection reloadData];
										
									}];
}

- (void)loadMoreFavs
{
	__weak NLDiscoverCenterViewController *weakSelf = self;
	NSInteger lastYum = 0;
	if (self.favs.count > 0)
	{
		lastYum = [self.favs.lastObject yumId];
	}
	self.favsTask = [NLUser getFavsBeforeId:lastYum withCompletionBlock:^(NSError *error, NSArray *yums)
									 {
										 if (!error)
										 {
											 if ([yums count] > 0)
											 {
												 NSMutableArray *indexPaths = [NSMutableArray array];
												 NSInteger lastIndex = [[weakSelf favs] count];
												 
												 for (NSInteger index = 0; index < [yums count]; index++)
												 {
													 NSIndexPath *indexPath = [NSIndexPath indexPathForItem:lastIndex+index inSection:0];
													 [indexPaths addObject:indexPath];
												 }
												 
												 [[weakSelf favsCollection] performBatchUpdates:^{
													 [[weakSelf favs] addObjectsFromArray:yums];
													 [[weakSelf favsCollection] insertItemsAtIndexPaths:indexPaths];
												 } completion:^(BOOL finished)
													{
														//[weakSelf setLoading:NO];
													}];
											 }
											 else
											 {
												 //self.sugges
											 }
										 }else
										 {
											 //[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:2.0f];
										 }
									 }];
}
- (void)loadSuggestedUsers
{
	if (!self.suggestedRefreshControl.isRefreshing)
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.suggestedRefreshControl beginRefreshing];
		});
	}
	__weak NLDiscoverCenterViewController *weakSelf = self;
	
	self.suggestedTask = [NLUser randomUsersWithCompletionBlock:^(NSError *error, NSArray *users) {
		
		[[weakSelf suggestedRefreshControl] endRefreshing];
		if (!error)
		{
			if (users.count > 0)
			{
				[weakSelf setSuggestedUsers:users];
				[[weakSelf suggestedDelegate] setSuggestedUsers:[weakSelf suggestedUsers]];
			}
			
			dispatch_async(dispatch_get_main_queue(), ^{
				
				[[weakSelf suggestedTable] reloadData];
			});
		}else
		{
			CGFloat duration = 3.0f;
			[weakSelf setShowingAnimation:YES];
			[weakSelf setNeedsStatusBarAppearanceUpdate];
			[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:duration];
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
				[weakSelf setShowingAnimation:NO];
				[weakSelf setNeedsStatusBarAppearanceUpdate];
			});
		}
		
	}];
}

- (void)reloadSuggestedUsers:(id)sender
{
	if (!self.suggestedTask)
	{
		[self loadSuggestedUsers];
	}else
	{
		[[self suggestedRefreshControl] endRefreshing];
	}
}

#pragma mark - Load Dishes

-(void)loadDishes
{
	if (!self.dishesRefreshControl.isRefreshing)
	{
		[self.dishesRefreshControl beginRefreshing];
	}
	
	__weak NLDiscoverCenterViewController *weakSelf = self;
	
	self.dishesTask = [NLYum loadRandomYumsWithCompletionBlock:^(NSError *error, NSMutableArray *yums) {
		
		[[weakSelf dishesRefreshControl] endRefreshing];
		if (!error)
		{
			
			if (yums.count > 0)
			{
				[weakSelf setRandomDishes:yums];
			}
			
			dispatch_async(dispatch_get_main_queue(), ^{
				
				[[weakSelf dishesCollection] reloadData];
			});
		}else
		{
			CGFloat duration = 3.0f;
			[weakSelf setShowingAnimation:YES];
			[weakSelf setNeedsStatusBarAppearanceUpdate];
			[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:duration];
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
				[weakSelf setShowingAnimation:NO];
				[weakSelf setNeedsStatusBarAppearanceUpdate];
			});
		}
	}];
}

- (void)reloadRandomDishes:(id)sender
{
	if (!self.dishesTask)
	{
		[self loadDishes];
	}else
	{
		[[self dishesRefreshControl] endRefreshing];
	}
}
- (void)reloadFavs:(id)sender
{
	if (!self.favsTask)
	{
		[self loadFavs];
	}else
	{
		[[self favsRefreshControl] endRefreshing];
	}
}
#pragma mark - Hide/Show Search Filters

- (void)showSearchFiltersBelowView:(UIView *)view
{
	dispatch_async(dispatch_get_main_queue(), ^{
		self.searchFilterViewTopXConstraint.constant = view.frame.origin.y + view.bounds.size.height;
		
		[UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			[self.view layoutIfNeeded];
		} completion:nil];
	});
}

- (void)hideSearchFiltersBelowView:(UIView *)view
{
	dispatch_async(dispatch_get_main_queue(), ^{
		self.searchFilterViewTopXConstraint.constant = view.frame.origin.y;
		
		[UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			[self.view layoutIfNeeded];
		} completion:nil];
	});
}

#pragma mark - Filter Buttons Action

- (IBAction)filterUserButtonAction:(id)sender
{
	self.filterUserButton.selected = YES;
	self.filterHashtagButton.selected = self.filterLocationButton.selected = self.filterDishButton.selected = NO;
	self.currentSearchScope = NLSearchScopeUser;
	[[self resultsTableView] reloadData];
	self.query = nil;
	
	if (self.searchBar.text.length > 0)
	{
		//search the stuff
		[self.userSearchTask cancel];
		__weak NLDiscoverCenterViewController *weakSelf = self;
		
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		self.userSearchTask = [NLUser searchUserWithString:self.searchBar.text afterUserId:0 withCompletionBlock:^(NSError *error, NSArray *users){
			if (!error)
			{
				[[weakSelf  users] removeAllObjects];
				if (users.count > 0)
				{
					[[weakSelf users] addObjectsFromArray:users];
				}
				[[weakSelf resultsTableView] reloadData];
			}
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}];
	}
}

- (IBAction)filterHashtagAction:(id)sender
{
	self.filterHashtagButton.selected = YES;
	self.filterLocationButton.selected =self.filterUserButton.selected  = self.filterDishButton.selected = NO;
	self.currentSearchScope = NLSearchScopeHashtag;
	[[self resultsTableView] reloadData];
	self.query = nil;
	
	if (self.searchBar.text.length > 0 )
	{
		__weak NLDiscoverCenterViewController *weakSelf = self;
		
		[self.hashtagSearchTask cancel];
		
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[self.hashtagSearchTask cancel];
		self.hashtagSearchTask = [NLTag getTagsWithQuery:self.searchBar.text afterTagId:0 withCompletionBlock:^(NSError *error, NSArray *tags) {
			if (!error)
			{
				[[weakSelf  hashtags] removeAllObjects];
				if (tags.count > 0)
				{
					[[weakSelf hashtags] addObjectsFromArray:tags];
				}
				[[weakSelf resultsTableView] reloadData];
			}
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}];
	}
}

- (IBAction)filterLocationAction:(id)sender
{
	self.filterLocationButton.selected = YES;
	self.filterUserButton.selected = self.filterHashtagButton.selected = self.filterDishButton.selected = NO;
	self.currentSearchScope = NLSearchScopeVenue;
	[[self resultsTableView] reloadData];
	self.query = nil;
	
	__weak NLDiscoverCenterViewController *weakSelf = self;
	
	if (self.searchBar.text.length > 0)
	{
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[self.venueSearchTask cancel];
		self.venueSearchTask = [NLVenue getVenuesWithQuery:self.searchBar.text afterVenueId:0 withCompletionBlock:^(NSError *error, NSArray *venues) {
			if (!error)
			{
				[[weakSelf venues] removeAllObjects];
				if (venues.count > 0)
				{
					[[weakSelf venues] addObjectsFromArray:venues];
				}
				[[weakSelf resultsTableView] reloadData];
			}
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}];
	}
}

- (IBAction)filterDishAction:(id)sender
{
	self.filterDishButton.selected = YES;
	self.filterUserButton.selected = self.filterHashtagButton.selected = self.filterLocationButton.selected =NO;
	self.currentSearchScope = NLSearchScopeDish;
	
	[[self resultsTableView] reloadData];
	self.query = nil;
	
	if (self.searchBar.text.length > 0)
	{
		__weak NLDiscoverCenterViewController *weakSelf = self;
		
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[self.dishSearchTask cancel];
		self.dishSearchTask = [NLYum loadYumsFromDishName:self.searchBar.text page:1 withCompletionBlock:^(NSError *error, NSMutableArray *yums) {
			if (!error)
			{
				[[weakSelf dishes] removeAllObjects];
				if ([yums count] > 0)
				{
					[[weakSelf dishes] addObjectsFromArray:yums];
				}
				[[weakSelf resultsTableView] reloadData];
			}
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}];
	}
}

#pragma mark - Section Actions

- (IBAction)dishesAction:(id)sender
{
	[self.centerMapButton setHidden:YES];
	[self.centerMapButton setEnabled:NO];
	
	if (self.selectedMarker)
	{
		[self hideVenueDetail];
	}
	
	if (!self.dishesButton.selected)
	{
		[self.currentView removeFromSuperview];
		UIView *viewToAdd = [[self discoverViews] objectAtIndex:0];
		[[self contentView] insertSubview:viewToAdd atIndex:0];
		self.currentView = viewToAdd;
		
		self.dishesButton.selected = YES;
		self.nearbyButton.selected = self.peopleButton.selected = self.favsButton.selected = NO;
		
		if (self.randomDishes.count == 0)
		{
			[self loadDishes];
		}
	}
	
}

- (IBAction)peopleAction:(id)sender
{
	
	[self.centerMapButton setHidden:YES];
    [self.centerMapButton setAlpha:0];
	[self.centerMapButton setEnabled:NO];
	
	if (self.selectedMarker)
	{
		[self hideVenueDetail];
	}
	
	if (!self.peopleButton.selected)
	{
		[self.currentView removeFromSuperview];
		UIView *viewToAdd = [[self discoverViews] objectAtIndex:1];
		[[self contentView] insertSubview:viewToAdd atIndex:0];
		self.currentView = viewToAdd;
		
		self.peopleButton.selected = YES;
		self.dishesButton.selected = self.nearbyButton.selected = self.favsButton.selected = NO;
		
		if ([self.suggestedUsers count] == 0)
		{
			[self loadSuggestedUsers];
		}
	}
}

- (IBAction)nearbyAction:(id)sender
{
	
	[self.centerMapButton setHidden:NO];
	[self.centerMapButton setEnabled:YES];
	
	if (self.selectedMarker)
	{
		[self showVenueDetail];
	}
	
	if (!self.nearbyButton.selected)
	{
		[self.nearbyView setMyLocationEnabled:YES];
		[self.currentView removeFromSuperview];
		UIView *viewToAdd = [[self discoverViews] objectAtIndex:2];
		[[self contentView] insertSubview:viewToAdd atIndex:0];
		self.currentView = viewToAdd;
		
		self.nearbyButton.selected = YES;
		self.peopleButton.selected = self.dishesButton.selected = self.favsButton.selected = NO;
	}
}

- (IBAction)favAction:(id)sender
{
	[self.centerMapButton setHidden:YES];
	[self.centerMapButton setEnabled:NO];
	
	if (self.selectedMarker)
	{
		[self hideVenueDetail];
	}
	
	if (!self.favsButton.selected)
	{
		[self.currentView removeFromSuperview];
		UIView *viewToAdd = [[self discoverViews] objectAtIndex:3];
		[[self contentView] insertSubview:viewToAdd atIndex:0];
		self.currentView = viewToAdd;
		
		self.favsButton.selected = YES;
		self.nearbyButton.selected = self.peopleButton.selected = self.dishesButton.selected = NO;
		
		if (self.favs.count == 0)
		{
			[self loadFavs];
		}
	}
	
}

#pragma mark - Center Map Action

- (IBAction)centerMap:(id)sender
{
	if (self.nearbyView.myLocation)
	{
		[self hideVenueDetail];
		[self setSelectedMarker:nil];
		[self setMapDragged:YES];
		[self getNearestVenues];
		[self.nearbyView setCamera:[GMSCameraPosition cameraWithLatitude:self.nearbyView.myLocation.coordinate.latitude longitude:self.nearbyView.myLocation.coordinate.longitude zoom:16]];
	}
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	if(self.favsButton.selected == true)
	{
		return self.favs.count;
	}
	return self.randomDishes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	if(self.favsButton.selected == true)
	{
		NLYum *yum = [[self favs] objectAtIndex:[indexPath row]];
		NLPhotoThumbCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoDishesViewCellIdentifier forIndexPath:indexPath];
		[[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yum thumbImage]] placeholderImage:nil];
		return cell;
	}
	else
	{
		
		
		
		
		NLYum *yum;
		NSString *identifier;
		
		yum = [[self randomDishes] objectAtIndex:[indexPath row]];
		identifier = kNLDiscoverDishesCellViewIdentifier;
		NLPhotoThumbCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
		UICollectionViewLayoutAttributes *attribute = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
		[[cell photo] sd_setImageWithURL:[NSURL URLWithString:(attribute.frame.size.height == 106)?yum.thumbImage:yum.bigImage] placeholderImage:nil];
		
		return cell;
	}
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	NLYum *yum;
	
	if (self.favsButton.selected)
	{
		yum = [[self favs] objectAtIndex:[indexPath row]];
		
	}
	else
	{
		yum = [[self randomDishes] objectAtIndex:[indexPath row]];
		
	}
	NLPhotoDetailViewController *detailController = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
	[detailController setHidesBottomBarWhenPushed:YES];
	detailController.delegate = self;
	[self.navigationController pushViewController:detailController animated:YES];
}
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.favsButton.selected && indexPath.row > self.favs.count - 6 && !self.favsTask)
	{
		[self loadMoreFavs];
	}
}
#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
	if (searchBar.text.length == 0)
	{
		/*spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		 [spinner setFrame:CGRectMake(100, 100, 100, 100)];
		 [spinner setColor:[UIColor redColor]];
		 [spinner setCenter:CGPointMake(self.view.center.x, 50)];
		 [self.resultsTableView addSubview:spinner];*/
        [self.centerMapButton setHidden:YES];
        
        
		[searchBar setShowsCancelButton:YES animated:YES];
		[self showResultsTable];
        NSLog(@"Frame SearchBar width:%f  height:%f   x:%f  y:%f",self.searchBar.frame.size.width,self.searchBar.frame.size.height,self.searchBar.frame.origin.x,self.searchBar.frame.origin.y);
		[self showSearchFiltersBelowView:searchBar];
		if (self.selectedMarker)
		{
			[self clearCurrentMarker];
			[self hideVenueDetail];
		}
		NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
		_users = [NSMutableArray array];
		_hashtags = [NSMutableArray array];
		_venues = [NSMutableArray array];
		_dishes = [NSMutableArray array];
		
		if ([def objectForKey:@"NLSearchScopeUser"])
		{
			for (NSData *userData in [def objectForKey:@"NLSearchScopeUser"])
			{
				NLUser *user = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
				[_users addObject:user];
			}
			
		}
		if ([def objectForKey:@"NLSearchScopeHashtag"])
		{
			for (NSData *userData in [def objectForKey:@"NLSearchScopeHashtag"])
			{
				NLTag *tag = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
				[_hashtags addObject:tag];
			}
		}
		if ([def objectForKey:@"NLSearchScopeVenue"])
		{
			for (NSData *userData in [def objectForKey:@"NLSearchScopeVenue"])
			{
				NLVenue *venue = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
				[_venues addObject:venue];
			}
		}
		if ([def objectForKey:@"NLSearchScopeDish"])
		{
			for (NSData *userData in [def objectForKey:@"NLSearchScopeDish"])
			{
				[_dishes addObject:userData];
			}
		}
		[self.resultsTableView reloadData];
		
	}
	
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.centerMapButton setHidden:NO];
	[searchBar setShowsCancelButton:NO animated:YES];
	[searchBar setText:nil];
	[searchBar resignFirstResponder];
	[searchBar endEditing:YES];
	
	[self hideResultsTable];
	[self hideSearchFiltersBelowView:searchBar];
	
}
/*-(void)animateSpinner
 {
 [self.resultsTableView addSubview:spinner];
 [spinner startAnimating];
 }
 -(void)stopSpinner
 {
 [spinner stopAnimating];
 [spinner removeFromSuperview];
 }*/
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	//[self animateSpinner];
	__weak NLDiscoverCenterViewController *weakSelf = self;
	
	if (self.currentSearchScope == NLSearchScopeUser && searchBar.text.length > 4)
	{
		[self.userSearchTask cancel];
		//[self animateSpinner];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		
		self.userSearchTask = [NLUser searchUserWithString:searchBar.text afterUserId:0 withCompletionBlock:^(NSError *error, NSArray *users)
													 {
														 if (!error)
														 {
                [[weakSelf  users] removeAllObjects];
                if (users.count > 0)
								{
									[[weakSelf users] addObjectsFromArray:users];
								}
                
                [[weakSelf resultsTableView] reloadData];
														 }
														 //[self stopSpinner];
														 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
													 }];
	}
	else if (self.currentSearchScope == NLSearchScopeHashtag && searchBar.text.length > 4)
	{
		[self.hashtagSearchTask cancel];
		
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[self.hashtagSearchTask cancel];
		self.hashtagSearchTask = [NLTag getTagsWithQuery:searchBar.text afterTagId:0 withCompletionBlock:^(NSError *error, NSArray *tags) {
			if (!error)
			{
				[[weakSelf  hashtags] removeAllObjects];
				if (tags.count > 0)
				{
					[[weakSelf hashtags] addObjectsFromArray:tags];
				}
				[[weakSelf resultsTableView] reloadData];
			}
			
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}];
	}
	else if (self.currentSearchScope == NLSearchScopeVenue && searchBar.text.length > 4)
	{
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[self.venueSearchTask cancel];
		self.venueSearchTask = [NLVenue getVenuesWithQuery:searchBar.text afterVenueId:0 withCompletionBlock:^(NSError *error, NSArray *venues) {
			if (!error)
			{
				[[weakSelf venues] removeAllObjects];
				if (venues.count > 0)
				{
					[[weakSelf venues] addObjectsFromArray:venues];
				}
				[[weakSelf resultsTableView] reloadData];
			}
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}];
	}
	else if (self.currentSearchScope == NLSearchScopeDish && searchBar.text.length > 5)
	{
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[self.dishSearchTask cancel];
		self.dishSearchTask = [NLYum searchYum:searchBar.text withYums:false page:0 withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages) {
			if (!error)
			{
				[[weakSelf dishes] removeAllObjects];
				if ([yums count] > 0)
				{
					[[weakSelf dishes] addObjectsFromArray:yums];
				}
				[[weakSelf resultsTableView] reloadData];
			}
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}];
		
	}
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	__weak NLDiscoverCenterViewController *weakSelf = self;
    [searchBar resignFirstResponder];
	
	if (![self.query isEqualToString:searchBar.text])
	{
		[self setQuery:searchBar.text];
		switch (self.currentSearchScope)
		{
			case NLSearchScopeUser:
			{
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
				[self.userSearchTask cancel];
				self.userSearchTask = [NLUser searchUserWithString:searchBar.text afterUserId:0 withCompletionBlock:^(NSError *error, NSArray *users){
					if (!error)
					{
						[[weakSelf  users] removeAllObjects];
						if (users.count > 0)
						{
							[[weakSelf users] addObjectsFromArray:users];
						}
						[[weakSelf resultsTableView] reloadData];
					}
					
					[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
				}];
				break;
			}
			case  NLSearchScopeHashtag:
			{
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
				[self.hashtagSearchTask cancel];
				self.hashtagSearchTask = [NLTag getTagsWithQuery:searchBar.text afterTagId:0 withCompletionBlock:^(NSError *error, NSArray *tags) {
					if (!error)
					{
						[[weakSelf  hashtags] removeAllObjects];
						if (tags.count > 0)
						{
							[[weakSelf hashtags] addObjectsFromArray:tags];
						}
						[[weakSelf resultsTableView] reloadData];
					}
					
					[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
				}];
				break;
			}
			case NLSearchScopeVenue:
			{
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
				[self.venueSearchTask cancel];
				self.venueSearchTask = [NLVenue getVenuesWithQuery:searchBar.text afterVenueId:0 withCompletionBlock:^(NSError *error, NSArray *venues) {
					if (!error)
					{
						[[weakSelf venues] removeAllObjects];
						if (venues.count > 0)
						{
							[[weakSelf venues] addObjectsFromArray:venues];
						}
						[[weakSelf resultsTableView] reloadData];
					}
					[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
				}];
				break;
			}
			case NLSearchScopeDish:
			{
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
				[self.dishSearchTask cancel];
				self.dishSearchTask = [NLYum searchYum:searchBar.text withYums:false page: 0 withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages) {
					if (!error)
					{
						[[weakSelf dishes] removeAllObjects];
						if ([yums count] > 0)
						{
							[[weakSelf dishes] addObjectsFromArray:yums];
						}
						[[weakSelf resultsTableView] reloadData];
					}
					[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
				}];
				break;
			}
			default:
				break;
		}
		
	}
}

#pragma mark - GMSMapViewDelegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
	if (self.selectedMarker != marker)
	{
		self.selectedMarker.icon = [UIImage imageNamed:@"pin_map_b"];
		marker.icon = [UIImage imageNamed:@"pin_map"];
		self.selectedMarker = marker;
		[self setVenueDetailData:marker.userData];
		[self showVenueDetail];
	}
	
	return NO;
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
	if (self.selectedMarker)
	{
		[self clearCurrentMarker];
		[self hideVenueDetail];
	}
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
	if ([self wasMapDragged])
	{
		GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:[[[self nearbyView] projection] visibleRegion]];
		
		NSUInteger visiblePins = 0;
		for (GMSMarker *pin in [self pinSet]) {
			if ([bounds containsCoordinate:pin.position]) {
				visiblePins++;
			}
		}
		
		if (visiblePins < 5) {
			[self calculateNewCoordinate];
		}
		
		[self setMapDragged:NO];
		
	}
}


- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
	if (gesture)
	{
		self.mapDragged = YES;
	}else
	{
		self.mapDragged = NO;
	}
}


#pragma mark - NLDiscoverUserViewCell

- (void)userPhotoTappedWithTag:(NSInteger)tag
{
	NLSuggestedUser *suggested = [[self suggestedUsers] objectAtIndex:tag];
	NLUser *user = suggested.user;
	
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUserId:user.userID andCloseButton:NO];
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
	
}

- (void)usernameTappedWithTag:(NSInteger)tag
{
	NLSuggestedUser *suggested = [[self suggestedUsers] objectAtIndex:tag];
	NLUser *user = suggested.user;
 
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUserId:user.userID andCloseButton:NO];
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
}

- (void)firstPhotoTappedWithTag:(NSInteger)tag
{
	NLSuggestedUser *suggested = [[self suggestedUsers] objectAtIndex:tag];
	NLYum *yum = [suggested.yummmies firstObject];
	
	NLPhotoDetailViewController *detailController = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
	[detailController setHidesBottomBarWhenPushed:YES];
	detailController.delegate = self;
	[self.navigationController pushViewController:detailController animated:YES];
	
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	/*if (buttonIndex == 1)
	 {
	 [Foursquare2 authorizeWithCallback:^(BOOL success, id result) {
	 if (success)
	 {
	 [NLPreferences setCanUseFoursquare:YES];
	 
	 [Foursquare2 userGetDetail:@"self" callback:^(BOOL success, id result)
	 {
	 NSString *userId = [[[result objectForKey:@"response"] objectForKey:@"user"] objectForKey:@"id"];
	 [NLAuthentication addAuthenticationWithType:NLAuthenticationTypeFoursquare userID:userId token:[Foursquare2 accessToken] tokenSecret:nil withCompletionBlock:^(NSError *error, NLAuthentication *authentication) {
	 if (!error)
	 {
	 [[[NLSession currentUser] authentications] addObject:authentication];
	 }
	 }];
	 }];
	 
	 }
	 }];
	 
	 }*/
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//[self stopSpinner];
	noResults = false;
	switch (self.currentSearchScope)
	{
		case NLSearchScopeDish:
			if (self.dishes.count == 0)
			{
				noResults = true;
				return 1;
			}
			return self.dishes.count;
		case NLSearchScopeHashtag:
			if (self.hashtags.count == 0)
			{
				noResults = true;
				return 1;
			}
			return self.hashtags.count;
		case NLSearchScopeUser:
			if (self.users.count == 0)
			{
				noResults = true;
				return 1;
			}
			return self.users.count;
		case NLSearchScopeVenue:
			if (self.venues.count == 0)
			{
				noResults = true;
				return 1;
			}
			return self.venues.count;
		default:
			return 0;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (noResults == true)
	{
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLDiscoverCenterHashtagCellIdentifier];
		if (!cell)
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kNLDiscoverCenterHashtagCellIdentifier];
			[cell.textLabel setTextColor:[UIColor grayColor]];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		}
		[[cell textLabel] setText:NSLocalizedString(@"No results", @"No results")];
		[[cell detailTextLabel] setText:@""];
		
		//[self stopSpinner];
		return cell;
	}
	else
	{
		switch (self.currentSearchScope)
		{
			case NLSearchScopeUser:
			{
				NLUser *user = [[self users] objectAtIndex:indexPath.row];
				NLLikersViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLDiscoverCenterUserCellIdenfitier forIndexPath:indexPath];
				[[cell userNameLabel] setText:[NSString stringWithFormat:@"@%@",user.username]];
				[[cell nameLabel] setText:[user name]];
				//NSLog(@"%@", [(NSDictionary *)[user thumbAvatar] objectForKey:@"thumb"]);
				[[cell userPhoto] sd_setImageWithURL:[NSURL URLWithString:[user thumbAvatar]] placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
				[[cell userPhoto] setContentMode:UIViewContentModeScaleAspectFill];
				[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
				return cell;
			}
			case NLSearchScopeHashtag:
			{
				UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLDiscoverCenterHashtagCellIdentifier];
				if (!cell)
				{
					cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kNLDiscoverCenterHashtagCellIdentifier];
					[cell.textLabel setTextColor:[UIColor grayColor]];
					[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
				}
				NLTag *tag = [[self hashtags] objectAtIndex:indexPath.row];
				[[cell textLabel] setText:[NSString stringWithFormat:@"#%@",tag.name]];
				[[cell detailTextLabel] setText:[NSString stringWithFormat:@"%ld posts",(long)tag.numberOfYums]];
				return cell;
			}
			case NLSearchScopeVenue:
			{
				NLVenueViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLDiscoverCenterVenueCellIdentifier forIndexPath:indexPath];
				NLVenue *venue = [[self venues] objectAtIndex:indexPath.row];
				[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
				[[cell venueTitleLabel] setText:venue.name];
				return cell;
			}
			case NLSearchScopeDish:
			{
				//NLYum *yum = [[self dishes] objectAtIndex:indexPath.row];
				NLVenueViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLDiscoverCenterVenueCellIdentifier forIndexPath:indexPath];
				[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
				[[cell venueTitleLabel] setText:[[self dishes] objectAtIndex:indexPath.row]];
				return cell;
			}
			default:
				return nil;
		}
	}
	
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	const int maxHist = 5;
	switch (self.currentSearchScope)
	{
			
		case NLSearchScopeUser:
		{
			if (self.users.count > 0)
			{
				NLUser *user = [[self users] objectAtIndex:indexPath.row];
				NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:user];
				
				NSMutableArray *users = [[def objectForKey:@"NLSearchScopeUser"] mutableCopy];
				if (!users)
				{
					users = [NSMutableArray array];
				}
				NSMutableArray *unarchivedUsers = [NSMutableArray array];
				for (NSData *userData in [def objectForKey:@"NLSearchScopeUser"])
				{
					NLUser *nuser = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
					[unarchivedUsers addObject:[NSNumber numberWithInteger:nuser.userID]];
				}
				if (!unarchivedUsers)
				{
					unarchivedUsers = [[NSMutableArray alloc] init];
				}
				if (![unarchivedUsers containsObject:[NSNumber numberWithInteger:user.userID]])
				{
					if (users.count >= maxHist)
					{
						[users removeObjectAtIndex:maxHist - 1];
					}
					[users insertObject:userData atIndex:0];
				}
				[def setObject:users forKey:@"NLSearchScopeUser"];
				[def synchronize];
				
				NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:user];
				[profileVC setHidesBottomBarWhenPushed:YES];
				[self.navigationController pushViewController:profileVC animated:YES];
				
			}
			break;
		}
		case NLSearchScopeHashtag:
		{
			if (self.hashtags.count > 0)
			{
				NLTag *tag = [[self hashtags] objectAtIndex:indexPath.row];
				NSData *tagData = [NSKeyedArchiver archivedDataWithRootObject:tag];
				NSMutableArray *hashtags = [[def objectForKey:@"NLSearchScopeHashtag"] mutableCopy];
				if (!hashtags)
				{
					hashtags = [[NSMutableArray alloc] init];
				}
				if (![hashtags containsObject:tagData])
				{
					if (hashtags.count >= maxHist)
					{
						[hashtags removeObjectAtIndex:maxHist - 1];
					}
					[hashtags insertObject:tagData atIndex:0];
				}
				
				[def setObject:hashtags forKey:@"NLSearchScopeHashtag"];
				[def synchronize];
				
				NLHashtagViewController *hashtagVC  = [[NLHashtagViewController alloc] initWithTag:tag andNumberOfYums:tag.numberOfYums];
				[hashtagVC setHidesBottomBarWhenPushed:YES];
				[self.navigationController pushViewController:hashtagVC animated:YES];
			}
			break;
		}
		case NLSearchScopeVenue:
		{
			if (self.venues.count > 0)
			{
				NLVenue *venue = [[self venues] objectAtIndex:indexPath.row];
				NSData *venueData = [NSKeyedArchiver archivedDataWithRootObject:venue];
				
				NSMutableArray *venues = [[def objectForKey:@"NLSearchScopeVenue"] mutableCopy];
				if (!venues)
				{
					venues = [[NSMutableArray alloc] init];
				}
				if (![venues containsObject:venueData])
				{
					if (venues.count >= maxHist)
					{
						[venues removeObjectAtIndex:maxHist - 1];
					}
					[venues insertObject:venueData atIndex:0];
				}
				
				[def setObject:venues forKey:@"NLSearchScopeVenue"];
				[def synchronize];
				NSLog(@"<<<<<<<<<Venue>>>>>>\n%@\n<<<<<<<<<>>>>>>>", venue);
				[self.view setUserInteractionEnabled:false];
				self.view.layer.opacity = 0.9;
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
				
				[NLVenue getVenueWithId:[venue venueId] withCompletionBlock:^(NSError *error, NLVenue *venue)
				 {
					 NLVenueViewController *venueVC  = [[NLVenueViewController alloc] initWithVenue:venue];
					 [venueVC setHidesBottomBarWhenPushed:YES];
					 [self.view setUserInteractionEnabled:true];
					 self.view.layer.opacity = 1;
					 [self.navigationController pushViewController:venueVC animated:YES];
					 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
					 
				 }];
			}
			break;
		}
		case NLSearchScopeDish:
		{
			if (self.dishes.count > 0)
			{
				NSString *yumName = [[self dishes] objectAtIndex:indexPath.row];
				NSMutableArray *dishes = [[def objectForKey:@"NLSearchScopeDish"] mutableCopy];
				if (!dishes)
				{
					dishes = [[NSMutableArray alloc] init];
				}
				if (![dishes containsObject:yumName])
				{
					if (dishes.count >= maxHist)
					{
						[dishes removeObjectAtIndex:maxHist - 1];
					}
					[dishes insertObject:yumName atIndex:0];
				}
				[def setObject:dishes forKey:@"NLSearchScopeDish"];
				[def synchronize];
				
				NSLog(@"%@", yumName);
				NLDishViewController *dishVC = [[NLDishViewController alloc] initWithDishName:yumName];
				[dishVC setHidesBottomBarWhenPushed:YES];
				[self.navigationController pushViewController:dishVC animated:YES];
			}
			break;
		}
		default:
			break;
	}
	tableView.userInteractionEnabled = true;
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 56.0f;
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if ([scrollView isEqual:self.resultsTableView])
	{
		if (scrollView.contentOffset.y < -100)
		{
			if ([self isShowingSearchResults])
			{
				[self searchBarCancelButtonClicked:[self searchBar]];
			}
		}
	}
}

#pragma mark - NLSuggestedUserDElegateProtocol

- (void)openSuggestedDetailWithTag:(NSInteger)tag
{
	NLSuggestedUser *suggested = [[self suggestedUsers] objectAtIndex:tag];
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:suggested.user];
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
}

- (void)openSuggestedDishWithTag:(NSInteger)tag
{
	NLSuggestedUser *suggested = [[self suggestedUsers] objectAtIndex:tag];
	NLYum *yum = [[suggested yummmies] firstObject];
	NLPhotoDetailViewController *detailVC = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
	[detailVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:detailVC animated:YES];
}

#pragma mark - user logout

- (void)userLogout:(NSNotification *)notification
{
	NSLog(@"Cerrando sesion!");
	self.suggestedUsers = nil;
	[self.suggestedDelegate setSuggestedUsers:[self suggestedUsers]];
	[self.suggestedTable reloadData];
	
	self.randomDishes = nil;
	[self.dishesCollection reloadData];
}

#pragma mark - User login

- (void)userLogin:(NSNotification *)notification
{
	if (self.dishesButton.selected)
	{
		[self loadDishes];
	}else if (self.peopleButton.selected)
	{
		[self loadSuggestedUsers];
	}
}
- (void)yumDeleted:(NLYum *)deletedYum
{
	NSUInteger deletedIndex = [self.dishes indexOfObject:deletedYum];
	
	NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
	[[self favsCollection] performBatchUpdates:^{
		[[self favs] removeObjectAtIndex:deletedIndex];
		[[self favsCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
	} completion:^(BOOL finished)
	 {
		 
	 }];
}
- (void)updateFavWithTag:(NSInteger)tag
{
	
	NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:tag inSection:0];
	[[self favsCollection] performBatchUpdates:^{
		[[self favs] removeObjectAtIndex:tag];
		[[self favsCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
	} completion:^(BOOL finished)
	 {
		 
	 }];
}
@end
