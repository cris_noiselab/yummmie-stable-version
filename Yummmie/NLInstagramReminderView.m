


//
//  NLInstagramReminderView.m
//  Yummmie
//
//  Created by bot on 8/20/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLInstagramReminderView.h"
#import "UIColor+NLColor.h"
#import <GPUImage/GPUImage.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface NLInstagramReminderView ()

@property (nonatomic,strong) UIView *contentView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *detailLabel;
@property (nonatomic,strong) UIButton *confirmButton;
@property (nonatomic,strong) UIImageView *igBackground;
@property (nonatomic,strong) UIImageView *bubbleImg;
@property (nonatomic,strong) UILabel *bubbleLabel;
@property (nonatomic,strong) UILabel *placeholderLabel;
@property (nonatomic,strong) UIImageView *yumImage;

- (void)dismissView:(id)sender;

@end

@implementation NLInstagramReminderView
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithImage:(UIImage *)backgroundImage andYumImageURL:(NSString *)yumURL;
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGRect alertFrame = CGRectMake(0, 0, rootView.frame.size.width, 280);
    
    UIColor *fontColor = [UIColor nl_colorWith255Red:47 green:50 blue:53 andAlpha:255];
    
    if (self = [super initWithFrame:bounds]) {
        GPUImageiOSBlurFilter *blurfilter = [[GPUImageiOSBlurFilter alloc] init];
        //blurfilter.saturation = 0.6;
        blurfilter.blurRadiusInPixels = 2.0f;
        
        [self setBackgroundColor:[UIColor colorWithPatternImage:[blurfilter imageByFilteringImage:backgroundImage]]];
        
        _contentView = [[UIView alloc] initWithFrame:CGRectOffset(alertFrame, 0, bounds.size.height-alertFrame.size.height)];
        
        _igBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_inst"]];
        [_igBackground setFrame:CGRectMake(_igBackground.frame.origin.x, _igBackground.frame.origin.y, rootView.frame.size.width, _igBackground.frame.size.height)];
        
        [_contentView addSubview:_igBackground];
        
        CGRect placeHolderFrame = CGRectMake(0, 0, 150, 30);
        placeHolderFrame = CGRectOffset(placeHolderFrame, 90, 130);
        _placeholderLabel = [[UILabel alloc] initWithFrame:placeHolderFrame];
        _placeholderLabel.text = @"Write a caption...";
        [_placeholderLabel setTextAlignment:NSTextAlignmentLeft];
        [_placeholderLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:13]];
        [_placeholderLabel setTextColor:[UIColor nl_colorWith255Red:158 green:160 blue:163 andAlpha:255]];
        [_contentView addSubview:_placeholderLabel];
        _bubbleImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paste"]];
        _bubbleImg.frame = CGRectMake(60, 160, _bubbleImg.bounds.size.width, _bubbleImg.bounds.size.height);
        

        
        CGRect yumImageRect = CGRectMake(0, 0, 57, 57);
        yumImageRect = CGRectOffset(yumImageRect, 23, 130);
        _yumImage = [[UIImageView alloc] initWithFrame:yumImageRect];
        [_yumImage sd_setImageWithURL:[NSURL URLWithString:yumURL]];
        [_contentView addSubview:_yumImage];
        
        CGRect bLabelFrame = CGRectInset(_bubbleImg.bounds, 5, 10);
        bLabelFrame = CGRectOffset(bLabelFrame, 0, 2.5);
        _bubbleLabel = [[UILabel alloc] initWithFrame:bLabelFrame];
        _bubbleLabel.text = @"Paste";
        [_bubbleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12.0]];
        [_bubbleLabel setTextAlignment:NSTextAlignmentCenter];
        [_bubbleLabel setTextColor:[UIColor whiteColor]];
        [_bubbleImg addSubview:_bubbleLabel];
        
        [_contentView addSubview:_bubbleImg];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectInset(CGRectMake(0, 15, bounds.size.width, 20), 10, 0)];
        _titleLabel.text = @"Foto de Instagram";
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setMinimumScaleFactor:0.7];
        [_titleLabel setNumberOfLines:1];
        [_titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f]];
        [_titleLabel setTextColor:fontColor];
        [_contentView addSubview:_titleLabel];
        
        CGRect detailRect = CGRectMake(0, 0, bounds.size.width, 70);
        detailRect = CGRectInset(detailRect, 10, 0);
        detailRect = CGRectOffset(detailRect, 0, CGRectGetMaxY(_titleLabel.frame)+3);
        
        _detailLabel = [[UILabel alloc] initWithFrame:detailRect];
        _detailLabel.text = @"La descripción de tu foto ha sido copiada y está lista para ser pegada en tu publicación de Instagram.";
        [_detailLabel setTextAlignment:NSTextAlignmentCenter];
        [_detailLabel setMinimumScaleFactor:0.7];
        [_detailLabel setNumberOfLines:0];
        [_detailLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f]];
        [_detailLabel setTextColor:fontColor];
        [_contentView addSubview:_detailLabel];
        
        
        CGRect buttonFrame = CGRectMake(0, _contentView.bounds.size.height-66, bounds.size.width, 66);
        _confirmButton = [[UIButton alloc] initWithFrame:buttonFrame];
        [_confirmButton setBackgroundColor:[UIColor nl_applicationRedColor]];
        [[_confirmButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [_confirmButton setTitle:@"OK" forState:UIControlStateNormal];
        [_confirmButton addTarget:self action:@selector(dismissView:) forControlEvents:UIControlEventTouchUpInside];
        [_contentView addSubview:_confirmButton];
        
        [self addSubview:_contentView];
    }
    return self;
}

- (void)dismissView:(id)sender{
    if ([[self delegate] respondsToSelector:@selector(instagramViewDismissed:)]) {
        
        [[self delegate] instagramViewDismissed:self];
    }
    
}

@end
