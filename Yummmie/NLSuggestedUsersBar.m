
 //
//  NLSuggestedUsersBar.m
//  Yummmie
//
//  Created by bot on 3/31/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLSuggestedUsersBar.h"
#import "NLSuggestedUserBarCell.h"
#import "UIColor+NLColor.h"
#import "NLUser.h"

static const CGFloat kNLSuggestedUsersBarWidth = 320.0f;
static const CGFloat kNLSuggestedUsersBarHeight = 44.0f;


static NSString * const kNLSuggestedUserCellViewIdentifier = @"kNLSuggestedUserCellViewIdentifier";

@interface NLSuggestedUsersBar ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *usersCollection;
@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) NSArray *hashtags;



@end

@implementation NLSuggestedUsersBar

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.usersCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kNLSuggestedUsersBarWidth, kNLSuggestedUsersBarHeight) collectionViewLayout:flowLayout];
    [[self usersCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLSuggestedUserBarCell class]) bundle:nil] forCellWithReuseIdentifier:kNLSuggestedUserCellViewIdentifier];
    [[self usersCollection] setDelegate:self];
    [[self usersCollection] setDataSource:self];
    [[self usersCollection] setBackgroundColor:[UIColor nl_colorWith255Red:234 green:239 blue:244 andAlpha:255]];
    [[self usersCollection] setShowsHorizontalScrollIndicator:NO];
    [self addSubview:[self usersCollection]];
    
    [self setUserInteractionEnabled:YES];
    
}

- (instancetype)init
{
    if (self = [super initWithFrame:CGRectMake(0, 0, kNLSuggestedUsersBarWidth, kNLSuggestedUsersBarHeight)]) {
        
    }
    
    return self;
}

#pragma mark - Update datasource

- (void)updateDatasource:(NSArray *)datasource
{
    [self setUsers:datasource];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self usersCollection] reloadData];
    });

}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self users] count];
 
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLSuggestedUserBarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kNLSuggestedUserCellViewIdentifier forIndexPath:indexPath];

    if (self.hashtagging == false)
    {
        NLUser *user = [[self users] objectAtIndex:[indexPath row]];
        
        [[cell usernameLabel] setText:[NSString stringWithFormat:@"@%@",[user username]]];
    }
    else
    {
        [[cell usernameLabel] setText:[NSString stringWithFormat:@"#%@", [self.users objectAtIndex:indexPath.item]]];
    }
    return cell;

}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.hashtagging == false)
    {
        NLUser *user = [[self users] objectAtIndex:[indexPath row]];
        NSString *stringValue = [NSString stringWithFormat:@"@%@",[user username]];
        CGSize size = CGSizeMake(CGFLOAT_MAX, 44);
        static UIFont *font;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        });
        CGRect frame = [stringValue boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        CGSize finalSize = CGSizeMake(frame.size.width, 44);
        return finalSize;
    }
    else
    {
        NSString *stringValue = [NSString stringWithFormat:@"#%@",[self.users objectAtIndex:indexPath.item]];
        CGSize size = CGSizeMake(CGFLOAT_MAX, 44);
        static UIFont *font;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        });
        CGRect frame = [stringValue boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        CGSize finalSize = CGSizeMake(frame.size.width, 44);
        return finalSize;
    }
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(suggestedBar:didSelectUser:atIndex:)]){
        
        [[self delegate] suggestedBar:self didSelectUser:[[self users] objectAtIndex:[indexPath row]] atIndex:[indexPath row]];
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5.0f;
}

@end