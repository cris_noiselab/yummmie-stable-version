
//
//  NLYum.m
//  Yummmie
//
//  Created by bot on 7/15/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <SSToolkit/SSToolkit.h>

#import "NLYum.h"
#import "NLConstants.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLComment.h"
#import "NLVenue.h"
#import "NLTag.h"

static NSString * const kYumCaptionKey = @"yum[caption]";
static NSString * const kYumPhotoKey = @"yum[image]";
static NSString * const kYumVenueIdKey = @"yum[venue_id]";
static NSString * const kYumNameKey = @"yum[name]";
static NSString * const KYumBeforeKey = @"before_id";
//static NSString * const KYumAfterKey = @"after_id";
static NSString * const kYumContentKey = @"comment[content]";
static NSString * const kTrackableID = @"report[trackable_id]";
static NSString * const kTrackableType = @"report[trackable_type]";

static NSString * const kYumProvidersKey = @"providers";
static NSString * const KPage = @"page";

//static NSDateFormatter *dateFormatter;

@implementation NLYum
- (void) encodeWithCoder:(NSCoder *)encoder {
	
	[encoder encodeInteger:_yumId forKey:@"id"];
	[encoder encodeObject:_name forKey:@"name"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
	NSDictionary *dictionary = @{@"id": [NSNumber numberWithInteger:[decoder decodeIntegerForKey:@"id"]], @"name": [decoder decodeObjectForKey:@"name"]};
	
	return [self initWithDictionary:dictionary];
}
- (id)initWithDictionary:(NSDictionary *)yumDict
{
	if (self = [super init])
	{

		_caption = [yumDict valueForKey:@"caption"];
		_image = [yumDict valueForKey:@"image"];
		_thumbImage = [yumDict valueForKey:@"image"];
		_bigImage = [yumDict valueForKey:@"image"];
		_venue = [[NLVenue alloc] initWithDictionary:[yumDict valueForKey:@"mini_venue"]];
		_user = [[NLUser alloc] initWithDictionary:[yumDict valueForKey:@"user"]];
		_likes = [[yumDict valueForKey:@"likes"] integerValue];
		_name = ([yumDict valueForKey:@"name"]==[NSNull null]?nil:[yumDict valueForKey:@"name"]);
		_userLikes = [[yumDict valueForKey:@"user_likes"] boolValue];
		_yumId = [[yumDict valueForKey:@"id"] integerValue];
		_isFav = [[yumDict valueForKey:@"user_favourite"] boolValue];
		_favs = [[yumDict valueForKey:@"favs"] integerValue];
		_reposts = [[yumDict valueForKey:@"reposts"] integerValue];
		_isReposted = [[yumDict valueForKey:@"user_reposted"] boolValue];
		_shareUrl = [yumDict valueForKey:@"share_url"];

		_viewsCount = [[yumDict valueForKey:@"api_view_count"] integerValue];
		//NSArray *rawTags = [[yumDict valueForKey:@"tag_info"] componentsSeparatedByString:@","];
		NSArray *rawTags = [yumDict objectForKey:@"tags_info"];
		NSMutableArray *tmpTags = [NSMutableArray array];
		
		for (NSDictionary *tagDict in rawTags)
		{
			NLTag *tag = [[NLTag alloc] initWithDictionary:@{@"id": [tagDict objectForKey:@"id"], @"name":[tagDict objectForKey:@"content"]}];
			[tmpTags addObject:tag];
		}
		
		_tags = [NSArray arrayWithArray:tmpTags];
		
		/*if ([rawTags count])
		 {
		 NSMutableArray *tmpTags = [NSMutableArray array];
		 
		 for (NSDictionary *tagDict in rawTags)
		 {
		 NLTag *tag = [[NLTag alloc] initWithDictionary:tagDict];
		 [tmpTags addObject:tag];
		 }
		 
		 _tags = [NSArray arrayWithArray:tmpTags];
		 }*/
		
		//        _created =[NSDate sam_dateFromISO8601String:[yumDict valueForKey:@"created_at"]];
		/*NSDate *tmpDate = [NSDate sam_dateFromISO8601String:[yumDict valueForKey:@"created_at"]];
		 NSDate *now = [NSDate date];
		 if ([now timeIntervalSinceDate:tmpDate]<0)
		 {
		 _created = now;
		 }else
		 {
		 _created = tmpDate;
		 }*/
		_created = [yumDict valueForKey:@"created_at"];
		_createdDict = [yumDict objectForKey:@"created_at_info"];

		NSArray *comments = [[yumDict valueForKey:@"comments"] valueForKey:@"comments"];
		if ([comments count]>0)
		{
			_comments = [NSMutableArray array];
			
			for (NSDictionary *commentDict in comments)
			{
				NLComment *comment = [[NLComment alloc] initWithDictionary:commentDict];
				[_comments addObject:comment];
			}
		}else
		{
			if ([[[yumDict valueForKey:@"comments"] objectForKey:@"count"] integerValue] > 0)
			{
				for (NSDictionary *commentDict in [[yumDict valueForKey:@"comments"] objectForKey:@"comment"])
				{
					NLComment *comment = [[NLComment alloc] initWithDictionary:commentDict];
					[_comments addObject:comment];
				}
				
			}
			else
			{
				_comments = [NSMutableArray array];
				
			}
		}
		
		_numberOfComments  = [[[yumDict valueForKey:@"comments"] valueForKey:@"count"] integerValue];
		if ([yumDict objectForKey:@"repost"])
		{
			self.repost = [yumDict objectForKey:@"repost"];
		}
	}
	return self;
}

- (BOOL)isEqual:(id)object
{
	if (![object isKindOfClass:[NLYum class]])
	{
		return NO;
	}else
	{
		if ([object yumId] == self.yumId)
		{
			return YES;
		}
		return NO;
	}
}

- (BOOL)belongsToUser:(NLUser *)user
{
	return self.user.userID == user.userID;
	
}
+ (NSURLSessionDataTask *)editYumId:(NSInteger)yumId dict:(NSDictionary *)dict withCompletionBlock:(void (^)(NSError *error, NLYum * yum))block;
{
	
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	return [[NLYummieApiClient sharedClient] PUT:[NSString stringWithFormat:@"yums/%ld", (long)yumId] parameters:dict success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if ([[responseObject objectForKey:@"success"] boolValue])
						{
							[[NSNotificationCenter defaultCenter] postNotificationName:@"ShouldReloadAll" object:nil];
							NLYum *nYum = [[NLYum alloc] initWithDictionary:[responseObject valueForKey:@"yum"]];
							block(nil, nYum);
							
						}
						else
						{

						}
						
					}
																			 failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						block(error, nil);
						[[NLSession sharedSession] setErrorString:error.userInfo.description];
					}];
}
+ (NSURLSessionDataTask *)postYum:(UIImage *)photo caption:(NSString *)caption name:(NSString *)name andVenueId:(NSString *)venueId withCompletionBlock:(void (^)(NSError *error, NLYum *yum))block
{
	NSDictionary *params = @{
													 kYumCaptionKey:caption,
													 kYumVenueIdKey:venueId,
													 kYumNameKey:name
													 };
	
	
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	return [[NLYummieApiClient sharedClient] POST:@"yums" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
		NSData *imageData = UIImageJPEGRepresentation(photo, 0.8);
		[formData appendPartWithFileData:imageData name:kYumPhotoKey fileName:@"yum.png" mimeType:kJPGMimeType];
	} success:^(NSURLSessionDataTask *task, id responseObject)
					{
						NLYum *createdYum = [[NLYum alloc] initWithDictionary:[responseObject valueForKey:@"yum"]];
						if (block)
						{
							//createdYum.created = [NSDate date];
							block(nil, createdYum);
						}else
						{
							[[NLSession sharedSession] setErrorString:responseObject];
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						block(error, nil);
						[[NLSession sharedSession] setErrorString:error.userInfo.description];
					}];
}

+ (NSURLSessionDataTask *)postYum:(UIImage *)photo caption:(NSString *)caption name:(NSString *)name andVenueId:(NSString *)venueId providers:(NSString *)providers withCompletionBlock:(void (^)(NSError *error, NLYum * yum))block
{
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setObject:caption forKey:kYumCaptionKey];
	[params setObject:venueId forKey:kYumVenueIdKey];
	[params setObject:name forKey:kYumNameKey];
	
	if (providers)
	{
		[params setObject:providers forKey:kYumProvidersKey];
	}
	
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	return [[NLYummieApiClient sharedClient] POST:@"yums" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
		NSData *imageData = UIImageJPEGRepresentation(photo, 0.8);
		[formData appendPartWithFileData:imageData name:kYumPhotoKey fileName:@"yum.png" mimeType:kJPGMimeType];
	} success:^(NSURLSessionDataTask *task, id responseObject)
					{
						NLYum *createdYum = [[NLYum alloc] initWithDictionary:[responseObject valueForKey:@"yum"]];
						if (block)
						{
							createdYum.created = [NSDate date];
							block(nil, createdYum);
						}else
						{
							[[NLSession sharedSession] setErrorString:responseObject];
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						block(error, nil);
						[[NLSession sharedSession] setErrorString:error.userInfo.description];
					}];
}

+ (NSURLSessionDataTask *)postYum:(UIImage *)photo caption:(NSString *)caption name:(NSString *)name andVenueId:(NSString *)venueId providers:(NSString *)providers progress:(NSProgress *__autoreleasing *)progress withCompletionBlock:(void (^)(NSError *, NLYum *))block
{
	
	NLYummieApiClient *client = [NLYummieApiClient sharedClient];
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setObject:caption forKey:kYumCaptionKey];
	[params setObject:venueId forKey:kYumVenueIdKey];
	[params setObject:name forKey:kYumNameKey];
	
	if (providers)
	{
		[params setObject:providers forKey:kYumProvidersKey];
	}
	
	
	NSError *requestError;
	NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",AFAppDotNetAPIBaseURLString,@"yums"] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
		
		NSData *imageData = UIImageJPEGRepresentation(photo, 0.8);
		[formData appendPartWithFileData:imageData name:kYumPhotoKey fileName:@"yum.png" mimeType:kJPGMimeType];
	} error:&requestError];
	
	NSProgress *prog;
	
	[request setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHTTPHeaderField:@"Authorization"];
	NSURLSessionDataTask *uploadTask = [client uploadTaskWithStreamedRequest:request progress:&prog completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
		if (!error) {
			NLYum *createdYum = [[NLYum alloc] initWithDictionary:[responseObject valueForKey:@"yum"]];
			if (block)
			{
				createdYum.created = @"1 s";
				block(nil, createdYum);
			}else
			{
				[[NLSession sharedSession] setErrorString:responseObject];
			}
		}else
		{
			NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
			block(error, nil);
			[[NLSession sharedSession] setErrorString:error.userInfo.description];
		}
		
	}];
	*progress = prog;
	
	[uploadTask resume];
	
	return  uploadTask;
}

+ (NSURLSessionDataTask *)getYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *, NLYum *))block
{
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld",(long)yumId];
	
	if ([NLSession currentUser])
	{
		[[NLYummieApiClient sharedClient] requestSerializer];
		[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	}
	else
	{
		return nil;
	}
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue])
							{
								NLYum *yum = [[NLYum alloc] initWithDictionary:[responseObject objectForKey:@"yum"]];
								
								block(nil, yum);
							}
							else
							{
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error,nil);
						}
					}];
}
+ (NSURLSessionDataTask *)getTimelineFromUserId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *, NSMutableArray *))block
{
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/timeline",(long)userId];
	
	if ([NLSession currentUser])
	{
		[[NLYummieApiClient sharedClient] requestSerializer];
		[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	}
	else
	{
		return nil;
	}
	
	//    [[NLYummieApiClient sharedClient] requestSerializer];
	//    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue])
							{
								NSMutableArray *yums = [NSMutableArray array];
								
								for (NSDictionary *dict in [responseObject objectForKey:@"yums"])
								{
									NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
									[yums addObject:yum];
								}
								block(nil,yums);
							}else
							{

								block([NSError new],nil);
							}
						}
						
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error,nil);
						}
					}];
}
+ (NSURLSessionDataTask *)likeYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/likes",(long)yumId];
	NSDictionary *params = @{@"like": @"true"};
	
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			block(nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",task.originalRequest.URL);
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		
		if (block)
		{
			block(error);
		}
	}];
}
+ (NSURLSessionDataTask *)likeUnlike:(NSInteger)yumId like:(BOOL)like withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/likes",(long)yumId];
	NSDictionary *params = @{@"like": like?@"true":@"false"};
	
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			block(nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",task.originalRequest.URL);
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		
		if (block)
		{
			block(error);
		}
	}];
}
+ (NSURLSessionDataTask *)unlikeYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/likes",(long)yumId];
	NSDictionary *params = @{@"like": @"false"};
	
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			block(nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		
		if (block)
		{
			block(error);
		}
	}];
}
+ (NSURLSessionDataTask *)favUnfav:(NSInteger)yumId fav:(BOOL)fav withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/favs", (long)yumId];
	NSDictionary *params = @{@"fav": fav?@"true":@"false"};
	
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							block(nil);
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",task.originalRequest.URL);
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						
						if (block)
						{
							block(error);
						}
					}];
}
+ (NSURLSessionDataTask *)destroy:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld",(long)yumId];
	return [[NLYummieApiClient sharedClient] DELETE:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
					{
						[[NSNotificationCenter defaultCenter] postNotificationName:@"ShouldReloadAll" object:nil];
						
						if (block)
						{
							block(nil);
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error);
						}
					}];
}

/*
+ (NSURLSessionDataTask *)getTimelineYumsBeforeId:(NSInteger)yumId userId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *, NSMutableArray *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	//NSLog(@"Authorization %@", [NLSession accessToken]);
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/timeline",(long)userId];
	NSDictionary *params = @{KYumBeforeKey:[NSNumber numberWithInteger:yumId]};
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if([[responseObject objectForKey:@"success"] boolValue])
						{
							if ([[responseObject objectForKey:@"yums"] count] > 0)
							{
								NSMutableArray *yums = [NSMutableArray array];
								
								for (NSDictionary *dict in [responseObject objectForKey:@"yums"])
								{
									NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
									[yums addObject:yum];
								}
								block(nil,yums);
							}
							else
							{
								block(nil,nil);
							}
							
						}
						else
						{
							NSLog(@"%@\n%@", apiPath, responseObject);
							block([NSError new],nil);
						}
						
						
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						
						if (block)
						{
							block(error,nil);
						}
					}];
}
*/
+ (NSURLSessionDataTask *)getTimelinePage:(NSInteger)page forUserId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/timeline",(long)userId];
	
	NSDictionary *params = @{@"page":[NSNumber numberWithInteger:page], @"limit": @10};
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			NSLog(@"%@", responseObject);
			if([[responseObject objectForKey:@"success"] boolValue])
			{
				if ([[responseObject objectForKey:@"yums"] count] > 0)
				{
					NSMutableArray *yums = [NSMutableArray array];
					
					for (NSDictionary *dict in [responseObject objectForKey:@"yums"])
					{
						NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
						[yums addObject:yum];
					}
					block(nil, yums, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"totalPages"] integerValue]);
				}
				else
				{
					block(nil,nil, 1, 10);
				}
				
			}
			else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);
				block([NSError new],nil, 1, 10);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		
		if (block)
		{
			block(error,nil, 1, 10);
		}
	}];
}

+ (NSURLSessionDataTask *)postComment:(NSString *)comment yumId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error, NLComment *comment))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/comments",(long)yumId];
	NSDictionary *params = @{kYumContentKey:comment};
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if ([[responseObject objectForKey:@"success"] boolValue])
						{
							
							
							NLComment *comment = [[NLComment alloc] initWithDictionary:[responseObject objectForKey:@"comment"]];
							[comment setUser:[NLSession currentUser]];
							if (block)
							{
								block(nil,comment);
							}
						}
						else
						{
							NSLog(@"%@\n%@", apiPath, responseObject);
							block([NSError new], nil);
							
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						NSLog(@"%@",[[task originalRequest] URL]);
						NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
						NSLog(@"El status code : %ld",(long)response.statusCode);
						if (block)
						{
							block(error,nil);
						}
					}];
}

+ (NSURLSessionDataTask *)deleteCommentWithId:(NSInteger)commentId fromYumId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/comments/%ld",(long)yumId,(long)commentId];
	
	return [[NLYummieApiClient sharedClient] DELETE:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			block(nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error);
		}
	}];
}

/*+ (NSURLSessionDataTask *)loadYumsFromUserId:(NSInteger)userId fromYumId:(NSInteger)firstYum toLastYum:(NSInteger)lastYum withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSDictionary *params = @{};
	
	if (firstYum != 0 && lastYum != 0)
	{
		params = @{KYumBeforeKey:@(lastYum),KYumAfterKey:@(firstYum)};
	}
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/timeline",(long)userId];
	
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							NSLog(@"response %@", responseObject);
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"yums"] count] > 0)
								{
									NSMutableArray *yums = [NSMutableArray array];
									
									for (NSDictionary *dict in [responseObject objectForKey:@"yums"])
									{
										NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
										[yums addObject:yum];
									}
									block(nil,yums);
								}else
								{
									block(nil,nil);
								}
								
							}else
							{
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error,nil);
						}
					}];
}*/
/*
 after -> después
 before -> antes
 antier ayer  hoy  nuevos
 1  2  3  4  | 5 | 6  7  8  9  10
             |   |
	beforeId <- i d -> afterId
 */
//before id trae los más antiguos
+ (NSURLSessionDataTask *)loadYumsFromUserId:(NSInteger)userId page:(NSInteger)page withcompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/yums",(long)userId];;
	
	NSDictionary *params;
	params = @{KPage:[NSString stringWithFormat:@"%ld",(long)page]};

	
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([[responseObject objectForKey:@"mini_yums"] count]>0)
				{
					
					NSMutableArray *yums = [NSMutableArray array];
					
					for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
					{
						NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
						[yums addObject:yum];
					}
					block(nil, yums, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"total_pages"] integerValue]);
				}
				else
				{
					block(nil,nil, 1, 10);
				}
				
			}
			else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);

				block([NSError new],nil, 1, 10);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil, 1, 10);
		}
	}];

}
/*+ (NSURLSessionDataTask *)loadYumsFromUserId:(NSInteger)userId beforeId:(NSInteger)yumId withcompletionBlock:(void (^)(NSError *error, NSMutableArray *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/yums",(long)userId];;
	
	NSDictionary *params;
	if (yumId > 0 )
	{
		params = @{KYumBeforeKey:[NSString stringWithFormat:@"%ld",(long)yumId]};
	}
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([[responseObject objectForKey:@"mini_yums"] count]>0)
				{
					
					NSMutableArray *yums = [NSMutableArray array];
					
					for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
					{
						NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
						[yums addObject:yum];
					}
					block(nil,yums);
				}else
				{
					block(nil,nil);
				}
				
			}else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);

				block([NSError new],nil);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];

}

+ (NSURLSessionDataTask *)loadYumsFromUserId:(NSInteger)userId from:(NSInteger)yumId withcompletionBlock:(void (^)(NSError *error, NSMutableArray *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/yums",(long)userId];;
	
	NSDictionary *params;
	if (yumId > 0 )
	{
		params = @{KYumBeforeKey:[NSString stringWithFormat:@"%ld",(long)yumId]};
	}
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([[responseObject objectForKey:@"mini_yums"] count]>0)
				{
					
					NSMutableArray *yums = [NSMutableArray array];
					
					for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
					{
						NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
						[yums addObject:yum];
					}
					block(nil,yums);
				}else
				{
					block(nil,nil);
				}
				
			}else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);
				block([NSError new],nil);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];
	
}
*/
+ (NSURLSessionDataTask *)loadYumsFromVenueId:(NSString *)venueId page:(NSInteger)page withCompletionBlock:(void (^)(NSError * error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSLog(@"%@", [NLSession accessToken]);
	NSString *apiPath = [NSString stringWithFormat:@"venues/%@/yums",venueId];;
	NSDictionary *params = @{KPage:[NSString stringWithFormat:@"%ld",(long)page]};
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"mini_yums"] count] > 0)
								{
									NSMutableArray *yums = [NSMutableArray array];
									
									for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
									{
										NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
										[yums addObject:yum];
									}
									block(nil, yums, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"total_pages"] integerValue]);
								}else
								{
									block(nil,nil, 1, 10);
								}
								
							}else
							{
								NSLog(@"%@\n%@", apiPath, responseObject);
								block([NSError new], nil, 1, 10);
							}
						}
						
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error, nil, 1, 10);
						}
					}];
}

+ (NSURLSessionDataTask *)loadYumsFromTagId:(NSInteger)tagId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages, NSInteger count))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"tags/%ld/yums",(long)tagId];
	
	NSDictionary *params = @{KPage:[NSString stringWithFormat:@"%ld",(long)page]};
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue])
							{
								if ([[responseObject objectForKey:@"mini_yums"] count] > 0)
								{
									NSMutableArray *yums = [NSMutableArray array];
									
									for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
									{
										NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
										[yums addObject:yum];
									}
									block(nil,yums, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"total_pages"] integerValue], [[responseObject objectForKey:@"count"] integerValue]);
								}else
								{
									block(nil,nil, 1, 10, 0);
								}
								
							}else
							{
								NSLog(@"%@\n%@", apiPath, responseObject);
								block([NSError new],nil, 1, 10, 0);
							}
						}
						
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error,nil, 1, 10, 0);
						}
					}];
}

+ (NSURLSessionDataTask *)loadYumsFromStringTag:(NSString *)tag
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"tags/%@",tag];
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",error.localizedDescription);
	}];
}

+ (NSURLSessionDataTask *)loadYumsFromDishName:(NSString *)dishName page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = @"yums/search_dish";
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setValue:dishName forKeyPath:@"dishname"];
	[params setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:KPage];
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([responseObject count] > 0)
				{
					NSMutableArray *yums = [NSMutableArray array];
					
					for (NSDictionary *dict in [responseObject objectForKey:@"yums"])
					{
						[yums addObject:[dict objectForKey:@"yum_name"]];
					}
					block(nil,yums);
				}else
				{
					block(nil,nil);
				}
				
			}else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);

				block([NSError new],nil);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];
	
}


+ (NSURLSessionDataTask *)searchYum:(NSString *)dishName withYums:(BOOL)yums page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSLog(@"%@", [NLSession accessToken]);
	NSString *apiPath = @"yums/search";
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setValue:dishName forKeyPath:@"query"];
	if (yums)
	{
		[params setValue:@"true" forKeyPath:@"yums"];
	}
	else
	{
		[params setValue:@"false" forKeyPath:@"yums"];
	}
	if (page > 0)
	{
		[params setValue:[NSNumber numberWithInteger:page] forKeyPath:@"page"];
	}
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([responseObject count] > 0)
				{
					if (yums)
					{
						NSMutableArray *yums = [NSMutableArray array];
						
						for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
						{
							NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
							[yums addObject:yum];
						}
						block(nil,yums, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"total_pages"] integerValue]);
					}
					else
					{
						block(nil,[responseObject objectForKey:@"yum_names"], 1, 10);

					}
				}else
				{
					block(nil,nil, 1, 10);
				}
				
			}else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);
				block([NSError new],nil, 1, 10);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil, 1, 10);
		}
	}];
	
}

+ (NSURLSessionDataTask *)getLikersForYum:(NSInteger)yumId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *, NSArray *, NSInteger))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/likers",(long)yumId];
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:KPage];

	NSString *likersKey = @"users";
	return  [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([responseObject objectForKey:likersKey])
			{
				NSInteger numberOfLikers = [[responseObject objectForKey:@"count"] integerValue];
				NSArray *rawLikers = [responseObject objectForKey:likersKey];
				NSMutableArray *likers = [NSMutableArray array];
				
				for (NSDictionary *userDict in rawLikers)
				{
					NSDictionary *userLikers = [userDict objectForKey:@"user"];
					
					NLUser *user = [[NLUser alloc] initWithDictionary:userLikers];
					[likers addObject:user];
					
				}
				
				block(nil, [NSArray arrayWithArray:likers],numberOfLikers);
			}else
			{
				block([NSError new],nil,0);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil,0);
		}
	}];
}
+ (NSURLSessionDataTask *)getFaversForYum:(NSInteger)yumId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *, NSArray *, NSInteger))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/fav_users",(long)yumId];
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:KPage];

	NSString *likersKey = @"users";
	return  [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([responseObject objectForKey:likersKey])
			{
				NSInteger numberOfLikers = [[responseObject objectForKey:@"count"] integerValue];
				NSArray *rawLikers = [responseObject objectForKey:likersKey];
				NSMutableArray *likers = [NSMutableArray array];
				
				for (NSDictionary *userDict in rawLikers)
				{
					NSDictionary *userLikers = [userDict objectForKey:@"user"];
					
					NLUser *user = [[NLUser alloc] initWithDictionary:userLikers];
					[likers addObject:user];
					
				}
				
				block(nil, [NSArray arrayWithArray:likers],numberOfLikers);
			}else
			{
				block([NSError new],nil,0);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil,0);
		}
	}];
}
+ (NSURLSessionDataTask *)getRepostersForYum:(NSInteger)yumId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *, NSArray *, NSInteger))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/reposters",(long)yumId];
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:KPage];
	NSString *likersKey = @"users";
	return  [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([responseObject objectForKey:likersKey])
			{
				NSInteger numberOfLikers = [[responseObject objectForKey:@"count"] integerValue];
				NSArray *rawLikers = [responseObject objectForKey:likersKey];
				NSMutableArray *likers = [NSMutableArray array];
				
				for (NSDictionary *userDict in rawLikers)
				{
					NSDictionary *userLikers = [userDict objectForKey:@"user"];
					
					NLUser *user = [[NLUser alloc] initWithDictionary:userLikers];
					[likers addObject:user];
					
				}
				
				block(nil, [NSArray arrayWithArray:likers],numberOfLikers);
			}else
			{
				block([NSError new],nil,0);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil,0);
		}
	}];
}
+ (NSURLSessionDataTask *)getYumwithId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError * error, NLYum *yum))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld",(long)yumId];
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([responseObject valueForKey:@"yum"])
			{
				NLYum *yum = [[NLYum alloc] initWithDictionary:[responseObject valueForKey:@"yum"]];
				block(nil,yum);
			}else
			{
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error, nil);
		}
	}];
}
+ (NSURLSessionDataTask *)getYumwithStringId:(NSString *)yumId withCompletionBlock:(void (^)(NSError * error, NLYum *yum))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%@", yumId];
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([responseObject valueForKey:@"yum"])
			{
				NLYum *yum = [[NLYum alloc] initWithDictionary:[responseObject valueForKey:@"yum"]];
				block(nil,yum);
			}else
			{
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error, nil);
		}
	}];
}
+ (NSURLSessionDataTask *)getYumsWithName:(NSString *)yumName withCompletionBlock:(void (^)(NSError * error, NSArray *yums))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = @"yums";
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setValue:yumName forKeyPath:@"name"];
	
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([responseObject count] > 0)
				{
					
					NSMutableArray *yums = [NSMutableArray array];
					
					for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
					{
						NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
						[yums addObject:yum];
					}
					block(nil,yums);
					
					
				}else
				{
					block(nil,nil);
				}
				
			}else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);

				block([NSError new],nil);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];
	
}
+ (NSURLSessionDataTask *)getYumsWithName:(NSString *)yumName page:(NSInteger)page withCompletionBlock:(void (^)(NSError * error, NSArray *yums))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = @"yums";
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	[params setValue:yumName forKeyPath:@"name"];
	[params setValue:[NSNumber numberWithInteger:page] forKeyPath:KPage];

	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([responseObject count] > 0)
				{
					
					NSMutableArray *yums = [NSMutableArray array];
					
					for (NSDictionary *dict in [responseObject objectForKey:@"mini_yums"])
					{
						NLYum *yum = [[NLYum alloc] initWithDictionary:dict];
						[yums addObject:yum];
					}
					block(nil,yums);
					
					
				}else
				{
					block(nil,nil);
				}
				
			}else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);
				
				block([NSError new],nil);
			}
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];
	
}

+ (NSURLSessionDataTask *)getUserLikes:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *, NSArray *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/likes",(long)userId];
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	//[params setObject:[NSString stringWithFormat:@"%ld",(long)page] forKey:KPage];
    [params setValue:[NSNumber numberWithInteger:page] forKeyPath:KPage];
    NSLog(@"apiPath:%@ params:33 %@ 33",apiPath,params);

	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([responseObject isKindOfClass:[NSDictionary class]])
			{
				NSMutableArray *yums = [NSMutableArray array];
                NSMutableArray *miniYums =[NSMutableArray array];
                miniYums=[responseObject valueForKey:@"mini_yums"];
                for (NSDictionary *json in miniYums)
                {
                    NLYum *yum =[[NLYum alloc] initWithDictionary:json];
                    [yums addObject:yum];
                }
				block(nil, [NSArray arrayWithArray:yums]);
			}else
			{
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block([NSError new],nil);
            
		}
        
	}];
}
+ (NSURLSessionDataTask *)getUserLikes:(NSInteger)userId beforLikeId:(NSInteger)likeId withCompletionBlock:(void (^)(NSError *error, NSArray *yums, NSInteger lastLikeId))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/likes",(long)userId];
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	if (likeId > 0 )
	{
		[params setObject:[NSString stringWithFormat:@"%ld",(long)likeId] forKey:KYumBeforeKey];
	}
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			NSInteger likeId = 0;
			if ([responseObject isKindOfClass:[NSArray class]])
			{
				NSMutableArray *yums = [NSMutableArray array];
				
				for (NSDictionary *json in responseObject)
				{
					NLYum *yum =[[NLYum alloc] initWithDictionary:[json valueForKey:@"yum"]];
					[yums addObject:yum];
					likeId = [[json valueForKey:@"id"] integerValue];
				}
				
				block(nil, [NSArray arrayWithArray:yums],likeId);
			}else
			{
				block([NSError new],nil,likeId);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block([NSError new],nil,-1);
		}
	}];
}

+ (NSURLSessionDataTask *)reportYum:(NSInteger)yumId withCompletionBlock:(void (^)(NSError *error))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/report", yumId];
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:nil  success:^(NSURLSessionDataTask *task, id responseObject){
		if (responseObject)
		{
			block(nil);
		}
		
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block([NSError new]);
		}
	}];
}

+ (NSURLSessionDataTask *)loadRandomYumsWithCompletionBlock:(void (^)(NSError *error, NSMutableArray *yums))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = @"yums/random";
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject){
		if ([[responseObject objectForKey:@"success"] boolValue] == true)
		{
			NSMutableArray *yums = [NSMutableArray array];
			
			for (NSDictionary *yumDict in [responseObject objectForKey:@"yums"])
			{
				NLYum *yum = [[NLYum alloc] initWithDictionary:yumDict];
				[yums addObject:yum];
			}
			
			if (block)
			{
				block(nil, yums);
			}
		}else
		{
			NSLog(@"%@\n%@", apiPath, responseObject);
			if (block)
			{
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];
}
+ (NSURLSessionDataTask *)repostYumId:(NSInteger)yumId withCompletionBlock:(void (^)(NSError * error))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"yums/%ld/repost", (long)yumId];
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject){
		if ([[responseObject objectForKey:@"success"] boolValue])
		{
			block(nil);
			
		}
		else
		{
			NSLog(@"%@\n%@", apiPath, responseObject);
			block([NSError new]);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error){
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error);
		}
	}];
}

+ (NSURLSessionDataTask *)sendViewedYums:(NSString *)yums withCompletionBlock:(void (^)(NSError *error))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    NSLog(@"tokeeeeen %@", [NLSession accessToken]);
    NSString *apiPath = [NSString stringWithFormat:@"yums/view_count"];
    NSLog(@"Sending ids %@", yums);
    return [[NLYummieApiClient sharedClient] POST:apiPath parameters:@{@"yum_ids": yums}  success:^(NSURLSessionDataTask *task, id responseObject){
        NSLog(@"task %@", task.response);
        if (responseObject)
        {
            block(nil);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block([NSError new]);
        }
    }];
}

+ (NSURLSessionDataTask *)getRegions :(void (^)(NSError *error, NSArray *yums))block
{
    
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSString *apiCallPath = [[NSString stringWithFormat:@"venues/food_types"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [[NLYummieApiClient sharedClient] GET:apiCallPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (block)
        {
            if ([responseObject isKindOfClass:[NSDictionary class]])
            {
                NSLog(@"Answer: %@", [responseObject description]);
                /*
                NSMutableArray *regions = [NSMutableArray array];
                //NSMutableArray *subRegions =[NSMutableArray array];
                miniYums=[responseObject valueForKey:@"mini_yums"];
                for (NSDictionary *json in miniYums)
                {
                    NLYum *region =[[NLYum alloc] initWithDictionary:json];
                    [regions addObject:region];
                }
                block(nil, [NSArray arrayWithArray:yums]);*/
            }else
            {
                block([NSError new],nil);
            }
        }
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(error,nil);
    }];
}

@end
