//
//  NLProfileHeaderView.m
//  Yummmie
//
//  Created by bot on 7/1/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLProfileHeaderView.h"
#import "UIColor+NLColor.h"
#import "NSString+NLUtils.h"

@interface NLProfileHeaderView()

//@property (nonatomic, strong) UILabel *bioLabel;
//@property (nonatomic, strong) UIButton *siteButton;

@property (weak, nonatomic) IBOutlet UIButton *photosButton;
@property (weak, nonatomic) IBOutlet UIButton *followingButton;
@property (weak, nonatomic) IBOutlet UIButton *followersButton;
@property (weak, nonatomic) IBOutlet UILabel *bioLabel;
@property (weak, nonatomic) IBOutlet UIButton *siteButton;

@property (nonatomic) NSInteger dishes;
@property (nonatomic) NSInteger followers;
@property (nonatomic) NSInteger followings;

@property (nonatomic) ProfileButtonType buttonType;

- (IBAction)userAction:(id)sender;
- (IBAction)followersAction:(id)sender;
- (IBAction)followingAction:(id)sender;
- (IBAction)dishesAction:(id)sender;
- (IBAction)siteLinkAction:(id)sender;

- (void)setupFollowButton;
- (void)setupEditButton;
- (void)setupUnfollowButton;
- (void)setupPendingButton;
- (void)updateSocialData;

- (void)userPhotoTapped:(UITapGestureRecognizer *)tapGesture;

@end

@implementation NLProfileHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    //corner radius to the profile pic
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    
    [self.userPhoto setFrame:CGRectMake(self.userPhoto.frame.origin.x, self.userPhoto.frame.origin.y, rootView.frame.size.height*0.1594,  rootView.frame.size.height*0.1594)];
    self.userPhoto.layer.cornerRadius = self.userPhoto.bounds.size.height/2.0f;
    
    
   
    
    //setup action button
    [[[self actionButton] titleLabel] setAdjustsFontSizeToFitWidth:YES];
    
    //update social data
    [self updateSocialData];
    
    //setup cover photo shadow
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    [gradientLayer setAnchorPoint:CGPointZero];
    [gradientLayer setFrame:[self.coverPhoto bounds]];
    [gradientLayer setLocations:@[@0.0f,@1.0f]];
    
    UIColor *topColor = [UIColor colorWithWhite:0.0f alpha:0.4];
    UIColor *bottomColor = [UIColor colorWithWhite:0.0f alpha:0.7];
    
    [gradientLayer setColors:@[(id)[topColor CGColor],(id)[bottomColor CGColor]]];
    
    [[self.coverPhoto layer] insertSublayer:gradientLayer atIndex:(unsigned)self.coverPhoto.layer.sublayers.count];

    //setup site Button

    [[self siteButton] setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 0.0f)];
    
    [self setType:DefaultProfile];
    
    self.versionaLabel.text = [NSString stringWithFormat:@"V. %@",[[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey]];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userPhotoTapped:)];
    [[self userPhoto] addGestureRecognizer:tap];
}

- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - User Photo Tap

- (void)userPhotoTapped:(UITapGestureRecognizer *)tapGesture
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(userPhotoWasTouchedWithReference:)])
    {
        [[self delegate] userPhotoWasTouchedWithReference:self.userPhoto];
    }
}

#pragma mark - Setup Buttons
- (void)setupEditButton
{
    //setup action button
    [self.actionButton setTitle:NSLocalizedString(@"edit_profile_button_title", nil) forState:UIControlStateNormal];
    [[self actionButton] setContentEdgeInsets:UIEdgeInsetsMake(0, self.actionButton.frame.size.width/5, 0, 0)];
    [[self actionButton] setImage:[UIImage imageNamed:@"icn_edit_profile"] forState:UIControlStateNormal];
    [[self actionButton] setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [[self actionButton] setTintColor:[UIColor nl_colorWith255Red:230.0f green:230.0f blue:230.f andAlpha:255.0f]];
    [[self actionButton] setTitleColor:[UIColor nl_colorWith255Red:230.0f green:230.0f blue:230.0f andAlpha:255.0f] forState:UIControlStateNormal];
    [[self actionButton] setBackgroundColor:[UIColor nl_colorWith255Red:153.0f green:153.0f blue:153.0f andAlpha:255.0f]];
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"es"])
    {
        [self.actionButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 0.0f)];
    }else
    {
        [self.actionButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 0.0f)];
        
    }
}

- (void)setupFollowButton
{
    [[self actionButton] setTitle:NSLocalizedString(@"follow_text", nil) forState:UIControlStateNormal];
    [[self actionButton] setImage:[UIImage imageNamed:@"icn_follow_profile"] forState:UIControlStateNormal];
    [[self actionButton] setTintColor:[UIColor nl_colorWith255Red:255.0f green:251.0f blue:180.0f andAlpha:255.0f]];
    [[self actionButton] setTitleColor:[UIColor nl_colorWith255Red:255.0f green:251.0f blue:180.0f andAlpha:255.0f] forState:UIControlStateNormal];
    [[self actionButton] setBackgroundColor:[UIColor nl_colorWith255Red:177.0f green:22.0f blue:35.0f andAlpha:255.0f]];
    
    [[self actionButton] setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.actionButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 2.0f, 0.0f, 0.0f)];
}

- (void)setupUnfollowButton
{
    [[self actionButton] setTitle:NSLocalizedString(@"unfollow_text", nil) forState:UIControlStateNormal];
	[_actionButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
    [[self actionButton] setImage:[UIImage imageNamed:@"icn_unfollow_profile"] forState:UIControlStateNormal];
    [[self actionButton] setTintColor:[UIColor nl_colorWith255Red:255.0f green:251.0f blue:180.0f andAlpha:255.0f]];
    [[self actionButton] setTitleColor:[UIColor nl_colorWith255Red:255.0f green:251.0f blue:180.0f andAlpha:255.0f] forState:UIControlStateNormal];
    [[self actionButton] setBackgroundColor:[UIColor nl_colorWith255Red:229.0f green:29.0f blue:46.0f andAlpha:255.0f]];
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"es"])
    {
        [[self actionButton] setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 1.0f, 0.0f, 0.0f)];
        [self.actionButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 3.0f, 0.0f, 0.0f)];
    }else
    {
        [[self actionButton] setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        [self.actionButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 2.0f, 0.0f, 0.0f)];
    }
}

- (void)setupPendingButton
{
    [[self actionButton] setTitle:NSLocalizedString(@"requested_text", nil) forState:UIControlStateNormal];
    [[self actionButton] setImage:[UIImage imageNamed:@"icn_requested_profile"] forState:UIControlStateNormal];
    [[self actionButton] setTintColor:[UIColor nl_colorWith255Red:204.0f green:204.0f blue:204.f andAlpha:255.0f]];
    [[self actionButton] setTitleColor:[UIColor nl_colorWith255Red:204.0f green:204.0f blue:204.f andAlpha:255.0f] forState:UIControlStateNormal];
    [[self actionButton] setBackgroundColor:[UIColor nl_colorWith255Red:102.0f green:102.0f blue:102.0f andAlpha:255.0f]];
    [[self actionButton] setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"es"])
    {
        [self.actionButton setContentEdgeInsets:UIEdgeInsetsMake(0.0f, 3.0f, 0.0f, 0.0f)];
    }else
    {
        [self.actionButton setContentEdgeInsets:UIEdgeInsetsZero];
    }
    
}

#pragma mark - Action Button type
- (void)setActionType:(ProfileButtonType)buttonType
{
    [self setButtonType:buttonType];
    switch (buttonType)
    {
        case FollowButon:
        {
            [self setupFollowButton];
            break;
        }
        case UnfollowButton:
        {
            [self setupUnfollowButton];
            break;
        }
        case EditButton:
        {
            [self setupEditButton];
            break;
        }
        case PendingButton:
        {
            [self setupPendingButton];
            break;
        }
        default:
            break;
    }
}
#pragma mark - update Follow - Following

- (void)updateToFollowed
{

}
- (void)updateToUnfollowed
{
    
}
#pragma mark - Set Bio & Site URL

- (void)setBio:(NSString *)bioText andSite:(NSString *)URLString
{
    [[self siteButton] setTitle:URLString forState:UIControlStateNormal];
    [[self bioLabel] setText:bioText];
    
    [[self siteButton] setHidden:YES];
    [[self bioLabel] setHidden:YES];
    
    if ([bioText length] >0)
    {
        //at least we have a bio so
        [[self bioLabel] setHidden:NO];
        //put the site label in it's place
        CGRect siteRect = [[self siteButton] frame];
        siteRect.origin.y = 315.0f;
        [[self siteButton] setFrame:siteRect];
        
        if ([URLString length]>0)
        {
            [[self siteButton] setHidden:NO];
            
        }
    }else
    {
        CGRect siteRect = [[self siteButton] frame];
        siteRect.origin.y = 255.0f;
        [[self siteButton] setFrame:siteRect];
        if ([URLString length]>0)
        {
            [[self siteButton] setHidden:NO];

            
        }
    }
}

#pragma mark - Set Dishes , Followers & Following

- (void)updateSocialData
{
    //setup follower, photos, following buttons
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    UIFont *numberFont = [UIFont fontWithName:@"Helvetica-Light" size:18.0f];
    UIFont *subtitleFont = [UIFont fontWithName:@"Helvetica-Light"  size:11.0f];
    NSDictionary *numberAttributes = @{
                                       NSFontAttributeName:numberFont,
                                       NSParagraphStyleAttributeName:style,
                                       NSForegroundColorAttributeName:[UIColor nl_colorWith255Red:42.0f green:42.0f blue:42.0f andAlpha:255.0f]
                                       };
    NSDictionary *subtitleAttributes = @{
                                         NSFontAttributeName:subtitleFont,
                                         NSParagraphStyleAttributeName:style,
                                         NSForegroundColorAttributeName:[UIColor nl_colorWith255Red:66.0f green:66.0f blue:66.0f andAlpha:255.0f],
                                         };
    
    NSDictionary *numberHighlightedAttributes = @{
                                       NSFontAttributeName:numberFont,
                                       NSParagraphStyleAttributeName:style,
                                       NSForegroundColorAttributeName:[UIColor lightGrayColor]
                                       };
    
    NSDictionary *subtitleHightlightedAttributes = @{
                                                     NSFontAttributeName:subtitleFont,
                                                     NSParagraphStyleAttributeName:style,
                                                     NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                                     };
    
    NSMutableAttributedString *photosTitle = [[NSMutableAttributedString alloc] init];
    [photosTitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld\n",(long)[self dishes]]   attributes:numberAttributes]];
    [photosTitle appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"dishes_text", nil)  attributes:subtitleAttributes]];
    
    [self.photosButton setAttributedTitle:photosTitle forState:UIControlStateNormal];
    
    NSMutableAttributedString *followingTitle = [[NSMutableAttributedString alloc] init];
    [followingTitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d\n", [self followings]]  attributes:numberAttributes]];
    [followingTitle appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"following_text", nil)  attributes:subtitleAttributes]];
    
    
    NSMutableAttributedString *followingHighlitedTitle = [[NSMutableAttributedString alloc] init];
    [followingHighlitedTitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d\n",[self followings]]  attributes:numberHighlightedAttributes]];
    [followingHighlitedTitle appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"following_text", nil)  attributes:subtitleHightlightedAttributes]];
     
    [[self followingButton] setAttributedTitle:followingTitle forState:UIControlStateNormal];
    [[self followingButton] setAttributedTitle:followingHighlitedTitle forState:UIControlStateHighlighted];
    
    NSMutableAttributedString *followersTitle = [[NSMutableAttributedString alloc] init];
    [followersTitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d\n",[self followers]] attributes:numberAttributes]];
    [followersTitle appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"followers_text", nil) attributes:subtitleAttributes]];
    
    NSMutableAttributedString *followersHighlitedTitle = [[NSMutableAttributedString alloc] init];
    [followersHighlitedTitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d\n",[self followers]] attributes:numberHighlightedAttributes]];
    [followersHighlitedTitle appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"followers_text", nil) attributes:subtitleHightlightedAttributes]];

    [[self followersButton] setAttributedTitle:followersTitle forState:UIControlStateNormal];
    [[self followersButton] setAttributedTitle:followersHighlitedTitle forState:UIControlStateHighlighted];
    
}
-(void)setDishes:(NSInteger)dishes followers:(NSInteger)followers andFollowing:(NSInteger)following
{
    [self setDishes:dishes];
    [self setFollowers:followers];
    [self setFollowings:following];
    [self updateSocialData];
    
}
#pragma mark - Actions
- (IBAction)userAction:(id)sender
{
    switch ([self buttonType])
    {
        case EditButton:
        {
            if ([self delegate] && [[self delegate] respondsToSelector:@selector(editProfile)])
            {
                [[self delegate] editProfile];
            }
            break;
        }
        case FollowButon:
        {
            if ([self type] == DefaultProfile)
            {
                [self setupUnfollowButton];
                [self setButtonType:UnfollowButton];
                if ([self delegate] && [[self delegate] respondsToSelector:@selector(followUser)])
                {
                    [[self delegate] followUser];
                }
            }else
            {
                [self setupPendingButton];
                [self setButtonType:PendingButton];
            }
            break;
        }
        case UnfollowButton:
        {
            [self setupFollowButton];
            [self setButtonType:FollowButon];
            if ([self delegate] && [[self delegate] respondsToSelector:@selector(unfollowUser)])
            {
                [[self delegate] unfollowUser];
            }
            break;
        }
        case PendingButton:
        {
            [self setupFollowButton];
            [self setButtonType:FollowButon];
            if ([self delegate] && [[self delegate] respondsToSelector:@selector(cancelRequest)])
            {
                [[self delegate] cancelRequest];
            }
            break;
        }
        default:
            break;
    }
}

- (IBAction)followersAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openFollowers)])
    {
        [[self delegate] openFollowers];
    }
}

- (IBAction)followingAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openFollowing)])
    {
        [[self delegate] openFollowing];
    }
}

- (IBAction)dishesAction:(id)sender
{

}

- (IBAction)siteLinkAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openURL)])
    {
        [[self delegate] openURL];
    }
}
@end
