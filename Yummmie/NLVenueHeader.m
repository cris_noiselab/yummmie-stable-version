//
//  NLVenueHeader.m
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLVenueHeader.h"

@implementation NLVenueHeader

- (void)awakeFromNib
{
    //corner radius to the profile pic
    [self.venueImage setFrame:CGRectMake(self.venueImage.frame.origin.x, self.venueImage.frame.origin.y, 62, 62)];
    self.venueImage.layer.cornerRadius = self.venueImage.bounds.size.height/2.0f;

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
