//
//  NSString+NLUtils.m
//  Yummmie
//
//  Created by bot on 12/4/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NSString+NLUtils.h"

static NSString * const millionSuffix = @"M";
static NSString * const thousandSuffix = @"K";

@implementation NSString (NLUtils)

+ (NSString *)integerToFollowText:(NSInteger)quantity
{
    NSString *followText;
    if (quantity < 10000)
    {
        followText = [NSString stringWithFormat:@"%ld",(long)quantity];
    }else if(quantity < 1000000)
    {
        //thousands
        CGFloat floatQuantity = quantity/1000.0f;
        if (floatQuantity-(int)floatQuantity > 0.1)
        {
            CGFloat floatQuantity = quantity/1000.0f;
            followText = [NSString stringWithFormat:@"%.1f%@",floatQuantity,thousandSuffix];
        }else
        {
            NSInteger intQuantity = quantity/1000;
            followText = [NSString stringWithFormat:@"%ld%@",(long)intQuantity,thousandSuffix];
        }
    }else
    {
        //millions
        CGFloat floatQuantity = quantity/1000000.0f;
        if (floatQuantity-(int)floatQuantity>0.1)
        {
            CGFloat floatQuantity = quantity/1000000.0f;
            followText = [NSString stringWithFormat:@"%.1f%@",floatQuantity,millionSuffix];
        }else
        {
            NSInteger intQuantity = quantity/1000000;
            followText = [NSString stringWithFormat:@"%ld%@",(long)intQuantity,millionSuffix];;
        }
    }
    return followText;
}
@end
