//
//  NLDashboardUserCellView.h
//  Yummmie
//
//  Created by bot on 9/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLDashboardUserCellView : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@end
