//
//  NLVenueDetailHeaderView.h
//  Yummmie
//
//  Created by bot on 5/15/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol NLVenueDetailHeaderDelegate;

@interface NLVenueDetailHeaderView : UICollectionReusableView

@property (weak, nonatomic) id<NLVenueDetailHeaderDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *venuePicture;
@property (weak, nonatomic) IBOutlet UIImageView *bestPicture;
@property (weak, nonatomic) IBOutlet UILabel *venueUsernameLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bioHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webHeightConstant;
@property (weak, nonatomic) IBOutlet UILabel *bioLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;

- (IBAction)shareAction:(id)sender;
- (IBAction)openLocation:(id)sender;
- (IBAction)callAction:(id)sender;
- (IBAction)openVenueSite:(id)sender;
- (IBAction)openCamera:(id)sender;

@end

@protocol NLVenueDetailHeaderDelegate <NSObject>

- (void)openRestarantLocation;
- (void)callRestaurant;
- (void)shareRestaurantInfo;
- (void)openCamera;
- (void)openVenueSite;

@end
