//
//  NLInstagramReminderView.h
//  Yummmie
//
//  Created by bot on 8/20/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NLInstagramReminderDelegate;

@interface NLInstagramReminderView : UIView

@property (nonatomic,weak) id<NLInstagramReminderDelegate> delegate;

- (instancetype)initWithImage:(UIImage *)backgroundImage andYumImageURL:(NSString *)yumURL;
@end

@protocol NLInstagramReminderDelegate <NSObject>

- (void)instagramViewDismissed:(NLInstagramReminderView *)instagramView;


@end
