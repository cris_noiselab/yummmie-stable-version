//
//  NLUserListViewCell.m
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLUserListViewCell.h"

@interface NLUserListViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

- (void)buttonAction:(UIButton *)sender;

@end

@implementation NLUserListViewCell

- (void)awakeFromNib
{
//    [self addObserver:self forKeyPath:@"buttonType" options:NSKeyValueObservingOptionNew context:nil];
    [[self actionButton] addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.userPhoto.layer.cornerRadius =  self.userPhoto.frame.size.height/2.0f;
	[self.userPhoto setContentMode:UIViewContentModeScaleAspectFill];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setButtonType:(UserButtonType)buttonType
{
    switch (buttonType)
    {
        case FOLLOWING:
        {
            [[self actionButton] setImage:[UIImage imageNamed:@"bot__unfollow_lista"] forState:UIControlStateNormal];
            break;
        }
        case NOTFOLLOWING:
        {
            [[self actionButton] setImage:[UIImage imageNamed:@"bot__follow_lista"] forState:UIControlStateNormal];
            break;
        }
        case PENDING:
        {
            [[self actionButton] setImage:[UIImage imageNamed:@"bot__pending_lista"] forState:UIControlStateNormal];
        }
        default:
            break;
    }
    _buttonType  = buttonType;
}

- (void)setName:(NSString *)aName andUsername:(NSString *)aUsername
{
    [[self nameLabel] setText:aName];
    [[self usernameLabel] setText:aUsername];
}

- (void)buttonAction:(UIButton *)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(userCell:actionWithType:)])
    {
        [[self delegate] userCell:self actionWithType:[self buttonType]];
    }
}
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    NSLog(@"%@",keyPath);
//    NSLog(@"%@",change);
//}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.actionButton.hidden = NO;
}
@end