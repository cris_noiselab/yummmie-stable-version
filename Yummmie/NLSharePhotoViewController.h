//
//  NLSharePhotoViewController.h
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import "NLFoursquareSearchViewController.h"
#import "NLSuggestedUsersBar.h"


@interface NLSharePhotoViewController : NLBaseViewController<NLFoursquareSearchDelegate, UITextViewDelegate, NLSuggestedUsersBarDelegate,UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIImageView *dishPhoto;
@property (weak, nonatomic) IBOutlet UITextField *dishNameField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSuggestedBar;

@end
