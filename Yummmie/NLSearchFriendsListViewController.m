//
//  NLSearchFriendsListViewController.m
//  Yummmie
//
//  Created by bot on 12/18/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import <AddressBook/AddressBook.h>

#import "NLSearchFriendsListViewController.h"
#import "NLAuthentication.h"
#import "NLUser.h"
#import "NLUserListViewCell.h"
#import "NLProfileViewController.h"
#import "NLSession.h"
#import "NLNotifications.h"

static const CGFloat kUserSearchCellHeight = 57.0f;
static NSString * const kUserSearchListViewCellIdentifier = @"kUserSearchListViewCellIdentifier";

@interface NLSearchFriendsListViewController ()<UITableViewDataSource,UITableViewDelegate,NLUserListViewCellDelegate>

@property (nonatomic) FriendSocialList listType;
@property (nonatomic) NSArray *users;
@property (weak, nonatomic) IBOutlet UITableView *usersTable;


- (void)searchFriendsWithContactsEmail:(ABAddressBookRef)addressBookRef;
- (void)updateFollowStatus:(NSNotification *)notification;

@end

@implementation NLSearchFriendsListViewController
{
	NSString *gaiTrackName;
}
- (instancetype)initWithSocialListType:(FriendSocialList)listType;
{
	if (self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil])
	{
		_listType = listType;
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollowStatus:) name:kNLUnfollowUserNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollowStatus:) name:kNLFollowUserNotification object:nil];
	}
	
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	self.edgesForExtendedLayout = UIRectEdgeBottom;
	[self showLoadingSpinner];
	
	
	[[self usersTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLUserListViewCell class]) bundle:nil] forCellReuseIdentifier:kUserSearchListViewCellIdentifier];
	UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
	[footer setBackgroundColor:[UIColor whiteColor]];
	[[self usersTable] setTableFooterView:footer];
	
	[[self usersTable] setDelegate:self];
	[[self usersTable] setDataSource:self];
	
	NLAuthenticationType authType;
	NSString *provider = @"";
	
	switch (self.listType) {
		case TwitterList:
			authType = NLAuthenticationTypeTwitter;
			self.title = NSLocalizedString(@"find_friends_on_twitter_title", nil);
			gaiTrackName = @"amigos en twitter";
			provider = @"twitter";
			break;
		case FacebookList:
			authType = NLAuthenticationTypeFacebook;
			self.title = NSLocalizedString(@"find_friends_on_facebook_title", nil);
			gaiTrackName = @"amigos en facebook";
			provider = @"facebook";
			break;
		case ContactList:
			authType = NLAuthenticationTypeNone;
			gaiTrackName = @"amigos en contactos";
			
			break;
		default:
			break;
	}
	
	__weak NLSearchFriendsListViewController *weakSelf = self;
	if (self.listType != ContactList)
	{
		[NLUser findFriends:provider withCompletionBlock:^(NSError *error, NSArray *users)
		 {
			 if (!error)
			 {
				 [weakSelf setUsers:users];
				 [[weakSelf usersTable] reloadData];
				 
			 }
			 else
			 {
				 [weakSelf showErrorMessage:NSLocalizedString(@"no_friends_message", nil) popViewController:YES withDuration:3.0f];
			 }
		 }];
	}
	/*else
	 {
	 //get the contacts
	 __weak NLSearchFriendsListViewController *weakSelf = self;
	 
	 ABAddressBookRef addressBook  = ABAddressBookCreateWithOptions(NULL, NULL);
	 ABAuthorizationStatus authStatus = ABAddressBookGetAuthorizationStatus();
	 if ( authStatus == kABAuthorizationStatusNotDetermined)
	 {
	 ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
	 if (granted)
	 {
	 //get the contacts and run that shit
	 [weakSelf searchFriendsWithContactsEmail:addressBook];
	 }else
	 {
	 [self showErrorMessage:NSLocalizedString(@"address_access_error", nil) popViewController:YES withDuration:3.0f];
	 [weakSelf hideLoadingSpinner];
	 }
	 
	 });
	 }else if (authStatus == kABAuthorizationStatusAuthorized)
	 {
	 [self searchFriendsWithContactsEmail:addressBook];
	 }else
	 {
	 //alert for permissions
	 [self showErrorMessage:NSLocalizedString(@"address_access_error", nil) popViewController:YES withDuration:3.0f];
	 [self hideLoadingSpinner];
	 CFRelease(addressBook);
	 }
	 
	 }*/
}
- (void)viewWillAppear:(BOOL)animated
{
	//Google Analytics
	[super viewWillAppear:animated];
	NSString *name = [NSString stringWithFormat:@"%@ View", gaiTrackName];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)viewWillLayoutSubviews
{
	[super viewWillLayoutSubviews];
	if ([self.usersTable respondsToSelector:@selector(setSeparatorInset:)])
	{
		[self.usersTable setSeparatorInset:UIEdgeInsetsZero];
	}
	
	if ([self.usersTable respondsToSelector:@selector(setLayoutMargins:)])
	{
		[self.usersTable setLayoutMargins:UIEdgeInsetsZero];
	}
}
- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Unfollow/Follow Notifications

- (void)updateFollowStatus:(NSNotification *)notification
{
	NLUser *unfollowed = [[notification userInfo] objectForKey:@"user"];
	if (notification.object != self)
	{
		for (NLUser *user in self.users)
		{
			if ([unfollowed isEqual:user])
			{
				user.follow = !user.follow;
			}
		}
	}
	__weak NLSearchFriendsListViewController *weakSelf = self;
	
	dispatch_async(dispatch_get_main_queue(), ^{
		[[weakSelf usersTable] reloadData];
	});
}

#pragma mark - Search Friends With Contacts Email

- (void)searchFriendsWithContactsEmail:(ABAddressBookRef)addressBookRef
{
	NSMutableArray *emails = [NSMutableArray new];
	
	CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
	NSInteger arraySize = CFArrayGetCount(people);
	
	for (NSInteger index = 0; index < arraySize; index++)
	{
		ABRecordRef person = CFArrayGetValueAtIndex(people, index);
		ABMultiValueRef emailProperty = ABRecordCopyValue(person, kABPersonEmailProperty);
		
		CFArrayRef allEmailsFromPerson = ABMultiValueCopyArrayOfAllValues(emailProperty);
		
		[emails addObjectsFromArray:(__bridge NSArray*)allEmailsFromPerson];
		CFRelease(allEmailsFromPerson);
		CFRelease(emailProperty);
	}
	NSString *emailsString = [emails componentsJoinedByString:@","];
	CFRelease(people);
	//Call the method that searches for users
	
	__weak NLSearchFriendsListViewController *weakSelf = self;
	
	[NLAuthentication searchFriendsWithEmails:emailsString withCompletionBlock:^(NSError *error, NSArray *users) {
		[weakSelf hideLoadingSpinner];
		if (!error)
		{
			[weakSelf setUsers:users];
			[[weakSelf usersTable] reloadData];
		}else
		{
			[weakSelf showErrorMessage:NSLocalizedString(@"no_friends_message", nil) popViewController:YES withDuration:3.0f];
		}
	}];
	
}

#pragma mark - UITableDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [[self users] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NLUserListViewCell *cell = (NLUserListViewCell *)[tableView dequeueReusableCellWithIdentifier:kUserSearchListViewCellIdentifier];
	NLUser *user = [[self users] objectAtIndex:[indexPath row]];
	[cell setName:[user name] andUsername:[NSString stringWithFormat:@"@%@",[user username]]];
	[[cell userPhoto] sd_setImageWithURL:[NSURL URLWithString:[user thumbAvatar]] placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
	
	if (![NLSession isCurrentUser:[user userID]])
	{
		if ([user follow])
		{
			[cell setButtonType:FOLLOWING];
		}else
		{
			[cell setButtonType:NOTFOLLOWING];
		}
		[cell setDelegate:self];
	}else
	{
		[[cell actionButton] setHidden:YES];
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}
	
	if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
}

#pragma mark - UITableVewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return kUserSearchCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NLUser *selectedUser = [[self users] objectAtIndex:[indexPath row]];
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:selectedUser isProfileUser:NO];
	[[self navigationController] pushViewController:profileVC animated:YES];
}

#pragma mark - NLUserListViewCellDelegate

- (void)userCell:(NLUserListViewCell *)cell actionWithType:(UserButtonType)type
{
	NSIndexPath *indexPath = [[self usersTable] indexPathForCell:cell];
	NLUser *selectedUser = [[self users] objectAtIndex:[indexPath row]];
	__weak NLSearchFriendsListViewController *weakSelf = self;
	
	if (type == FOLLOWING)
	{
		[cell setButtonType:NOTFOLLOWING];
		[NLUser unfollowUser:[selectedUser userID] withCompletionBlock:^(NSError *error) {
			if (error)
			{
				[weakSelf showErrorMessage:@"There was an error while updating" withDuration:2.0f];
				[cell setButtonType:FOLLOWING];
			}else
			{
				[[NSNotificationCenter defaultCenter] postNotificationName:kNLUnfollowUserNotification object:self userInfo:@{@"user":selectedUser}];
				[selectedUser setFollow:NO];
			}
		}];
		
	}else if (type == NOTFOLLOWING)
	{
		[cell setButtonType:FOLLOWING];
		[NLUser followUser:[selectedUser userID] withCompletionBlock:^(NSError *error) {
			if (error)
			{
				[weakSelf showErrorMessage:@"There was an error while updating" withDuration:2.0f];
				[cell setButtonType:NOTFOLLOWING];
			}else
			{
				[selectedUser setFollow:YES];
				[[weakSelf usersTable] reloadData];
				[[NSNotificationCenter defaultCenter] postNotificationName:kNLFollowUserNotification object:self userInfo:@{@"user":selectedUser}];
			}
		}];
	}
}
@end
