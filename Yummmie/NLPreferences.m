//
//  NLPreferences.m
//  Yummmie
//
//  Created by bot on 12/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLSettingConstants.h"
#import "NLPreferences.h"

@implementation NLPreferences

+ (BOOL)foursquarePermissionWasAsked
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kSettingsAskedForFoursqaurePermissionKey];
}

+ (void)setFoursquarePermissionAsked:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kSettingsAskedForFoursqaurePermissionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)useFoursquare
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kSettingsCanUseFoursquareKey];
}

+ (void)setCanUseFoursquare:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kSettingsCanUseFoursquareKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)firstTime
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (![userDefaults valueForKey:kSettingsFirstTime]) {
        [userDefaults setBool:NO forKey:kSettingsFirstTime];
        [userDefaults synchronize];
        return YES;
    }else
    {
        return NO;
    }
}

+ (BOOL)canShowSocialReminder
{
    return ![[NSUserDefaults standardUserDefaults] boolForKey:kSettingsHideSocialReminder];
    
}
+ (void)dontShowSocialReminderAnymore
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kSettingsHideSocialReminder];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
