//
//  NLSuggestedUserDelegate.h
//  Yummmie
//
//  Created by bot on 3/2/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol NLSuggestedUserDelegateProtocol;

@interface NLSuggestedUserDelegate : NSObject<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, weak) id<NLSuggestedUserDelegateProtocol> delegate;

@property (nonatomic,strong) NSArray *suggestedUsers;

- (instancetype)initWithIdentifier:(NSString *)identifier;

@end


@protocol NLSuggestedUserDelegateProtocol <NSObject>

- (void)openSuggestedDetailWithTag:(NSInteger)tag;
- (void)openSuggestedDishWithTag:(NSInteger)tag;

@end