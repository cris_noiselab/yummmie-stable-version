//
//  NLDiscoverCenterViewController.h
//  Yummmie
//
//  Created by bot on 1/14/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLDiscoverCenterViewController : NLBaseViewController
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) int selectAction;

- (IBAction)filterUserButtonAction:(id)sender;
- (IBAction)filterHashtagAction:(id)sender;
- (IBAction)filterLocationAction:(id)sender;
- (IBAction)filterDishAction:(id)sender;

- (IBAction)dishesAction:(id)sender;
- (IBAction)peopleAction:(id)sender;
- (IBAction)nearbyAction:(id)sender;
- (IBAction)favAction:(id)sender;

@end
