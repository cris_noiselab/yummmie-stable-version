//
//  NLErrorMessageView.h
//  Yummmie
//
//  Created by bot on 6/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLErrorMessageView : UIView

- (void)showWithMessage:(NSString *)errorMessage andDuration:(float)seconds;
- (void)hideWithoutAnimation;

@end
