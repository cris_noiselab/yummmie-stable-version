//
//  NLSocialMediaAlertView.h
//  Yummmie
//
//  Created by bot on 3/30/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol NLSocialMediaAlertViewDelegate;

@interface NLSocialMediaAlertView : UIView

@property (weak, nonatomic) id<NLSocialMediaAlertViewDelegate> delegate;

- (void)show;
- (void)hide:(void (^)())block;

@end


@protocol NLSocialMediaAlertViewDelegate <NSObject>

@required

- (void)closeView:(NLSocialMediaAlertView *)view;
- (void)stopShowinView:(NLSocialMediaAlertView *)view;
- (void)setupFacebookAccount:(NLSocialMediaAlertView *)view;

@end