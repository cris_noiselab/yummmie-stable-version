//
//  NLInteractionsPopUpView.h
//  Yummmie
//
//  Created by bot on 3/31/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLInteractionsPopUpView : UIView

- (instancetype)initWithNumberOfLikes:(NSInteger)likes numberOfComments:(NSInteger)comments andNumberOfFollowers:(NSInteger)followers;


@end
