//
//  NLUserLikesViewController.m
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>

#import "NLUserLikesViewController.h"
#import "NLYum.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLPhotoDetailViewController.h"


static NSString * const kNLUserLikesCellIdentifier = @"kNLUserLikesCellIdentifier";

@interface NLUserLikesViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,NLPhotoDetailViewDelegate>

@property (strong, nonatomic) NSMutableArray *yums;
@property (weak, nonatomic) IBOutlet UICollectionView *likesCollection;
@property (nonatomic, getter=isLoadingLikes) BOOL loadingLikes;
@property (nonatomic) NSInteger lastLikeId;

@end

@implementation NLUserLikesViewController
{
	NSInteger currentPage;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _yums = [NSMutableArray array];
        _loadingLikes = NO;
        _lastLikeId = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	currentPage = 1;
    // Do any additional setup after loading the view from its nib.
    [self showLoadingSpinner];
    
    [[self likesCollection] setDataSource:self];
    [[self likesCollection] setDelegate:self];
    
    [[self likesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kNLUserLikesCellIdentifier];
    
    __weak NLUserLikesViewController *weakSelf = self;
    
    [self setLoadingLikes:YES];
	[NLYum getUserLikes:[[NLSession currentUser] userID] page:currentPage withCompletionBlock:^(NSError *error, NSArray *yums) {
		[weakSelf hideLoadingSpinner];
		[weakSelf setLoadingLikes:NO];
		
		if (!error)
		{
			[[self yums] addObjectsFromArray:yums];
			dispatch_async(dispatch_get_main_queue(), ^{
				[[weakSelf likesCollection] reloadData];
			});
		}else
		{
			[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_likes", nil) withDuration:2.0f];
		}
	}];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"User likes View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = logoView;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectioViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self yums] count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kNLUserLikesCellIdentifier forIndexPath:indexPath];
    
    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
    
    [[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yum thumbImage]] placeholderImage:nil];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
    NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
    [detail setDelegate:self];
    [[self navigationController] pushViewController:detail animated:YES];
    
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell highlight];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell unHighlight];
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width - 5) / 3, (self.view.frame.size.width - 5) / 3);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section==0){
        return UIEdgeInsetsMake(5, 1, 5, 1);
    }
    return UIEdgeInsetsMake(1, 1, 5, 1);
}



#pragma mark - NLPhotoDetailViewController

- (void)yumLiked:(NLYum *)likedYum
{
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    [[self likesCollection] performBatchUpdates:^{
        [[self yums] insertObject:likedYum atIndex:0];
        [[self likesCollection] insertItemsAtIndexPaths:@[newIndexPath]];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)yumUnliked:(NLYum *)unlikedYum
{
    NSUInteger deletedIndex = [self.yums indexOfObject:unlikedYum];
    NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
    [[self likesCollection] performBatchUpdates:^{
        [[self yums] removeObjectAtIndex:deletedIndex];
        [[self likesCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
    } completion:nil];
}

#pragma - mark UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (velocity.y >0)
    {
        
        if (!self.isLoadingLikes )
        {
            [self setLoadingLikes:YES];
            [self showLoadingSpinner];
            
            __weak NLUserLikesViewController *weakSelf = self;
					currentPage++;
					[NLYum getUserLikes:[[NLSession currentUser] userID] page:currentPage withCompletionBlock:^(NSError *error, NSArray *yums) {
						
						dispatch_async(dispatch_get_main_queue(), ^{
							[weakSelf hideLoadingSpinner];
						});
						[weakSelf setLoadingLikes:NO];
						
						if (!error)
						{
							[[self yums] addObjectsFromArray:yums];
							dispatch_async(dispatch_get_main_queue(), ^{
								[[weakSelf likesCollection] reloadData];
							});
						}else
						{
							[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_likes", nil) withDuration:2.0f];
						}

					}];
					
        }
    }
    
}
@end
