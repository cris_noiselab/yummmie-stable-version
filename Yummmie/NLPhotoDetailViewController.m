//
//  NLPhotoDetailViewController.m
//  Yummmie
//
//  Created by bot on 6/24/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <MGInstagram/MGInstagram.h>
#import <CoreSpotlight/CoreSpotlight.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "NLLikersViewController.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "TGRImageViewController.h"
#import "TGRImageZoomAnimationController.h"
#import "NLPhotoDetailViewController.h"
#import "NLVenueViewController.h"
#import "NLConstants.h"
#import "NLCommentsViewController.h"
#import "NLProfileViewController.h"
#import "NLHashtagViewController.h"
#import "NLDishViewController.h"
#import "NLYum.h"
#import "NLUser.h"
#import "NLVenue.h"
#import "NLSession.h"
#import "NLTag.h"
#import "NLNotifications.h"
//#import "NSString+NLTimefromDate.h"
#import "UIColor+NLColor.h"
#import "NLComment.h"
#import "NLInstagramReminderView.h"
#import "NLEditViewController.h"
#import <MGInstagram/MGInstagram.h>
#import "NLRepostFeedbackView.h"
#import <TwitterKit/TwitterKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


static NSString * const kPhotoDetailCellIdentifier = @"photoDetailCellIdentifier";
static const float kPhotoDetailTextMaxWidth = 265.0f;

@interface NLPhotoDetailViewController ()<NLInstagramReminderDelegate>

@property (strong, nonatomic) NLYum *yum;
@property (nonatomic) CGSize captionSize;
@property (nonatomic, strong) TGRImageZoomAnimationController *photoAnimationController;
@property (nonatomic, strong) NLInstagramReminderView *instagramReminder;
@property (nonatomic) NSInteger yumId;
@property (strong) NSString *stringYumId;

@property (nonatomic) BOOL addDismissButton;



- (void)commentAdded:(NSNotification *)notification;
- (void)dismissViewController;
- (void)scrollToTop;


@end

@implementation NLPhotoDetailViewController
{
	BOOL delete;
}
- (id)initWithYumId:(NSInteger)yumId
{
	if (self = [super init])
	{
		self.scrollableBar = NO;
		self.edgesForExtendedLayout = UIRectEdgeBottom;
		
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		[defaultCenter addObserver:self selector:@selector(commentAdded:) name:kNLCommentAddedNotification object:nil];
		_yumId = yumId;
		_yum = nil;
		
	}
	return self;
}

- (id)initWithYumId:(NSInteger)yumId andDismissButton:(BOOL)flag

{
	if (self = [super init])
	{
		self.scrollableBar = NO;
		self.edgesForExtendedLayout = UIRectEdgeBottom;
		
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		[defaultCenter addObserver:self selector:@selector(commentAdded:) name:kNLCommentAddedNotification object:nil];
		_yumId = yumId;
		_yum = nil;
		_addDismissButton = YES;
	}
	return self;
}

- (id)initWithStringYumId:(NSString *)yumId andDismissButton:(BOOL)flag

{
	if (self = [super init])
	{
		self.scrollableBar = NO;
		self.edgesForExtendedLayout = UIRectEdgeBottom;
		
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		[defaultCenter addObserver:self selector:@selector(commentAdded:) name:kNLCommentAddedNotification object:nil];
		_stringYumId = yumId;
		_yum = nil;
		_addDismissButton = YES;
	}
	return self;
}
- (id)initWithYum:(NLYum *)aYum
{
	if (self = [super init])
	{
		self.scrollableBar = NO;
		self.edgesForExtendedLayout = UIRectEdgeBottom;
		
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		[defaultCenter addObserver:self selector:@selector(commentAdded:) name:kNLCommentAddedNotification object:nil];
		_yum = aYum;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	self.captionSize = [self sizeForText:[[self yum] caption]];
	[[self navigationItem] setTitleView:nil];
	
	[[self tableView] registerNib:[UINib nibWithNibName:NSStringFromClass([NLTimeLineViewCell class]) bundle:nil] forCellReuseIdentifier:kPhotoDetailCellIdentifier];
	[[self tableView] setSeparatorColor:UITableViewCellSeparatorStyleNone];
	UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
	[footerView setBackgroundColor:[UIColor whiteColor]];
	
	[[self tableView] setTableFooterView:footerView];
	
	if (![self yum])
	{
		[self showLoadingSpinner];
		__weak NLPhotoDetailViewController *weakSelf = self;
		if (self.stringYumId)
		{
			[NLYum getYumwithStringId:[self stringYumId] withCompletionBlock:^(NSError *error, NLYum *yum) {
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf hideLoadingSpinner];
				});
				
				if (!error)
				{
					[weakSelf setYum:yum];
					NSLog(@"%@", yum.name);
					weakSelf.captionSize = [weakSelf sizeForText:yum.caption];
					dispatch_async(dispatch_get_main_queue(), ^{
						[[weakSelf tableView] reloadData];
					});
				}
				else
				{
					[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yummmie", nil) withDuration:2.0f];
					dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
						[self.navigationController dismissViewControllerAnimated:true completion:^{
						}];
					});
					
				}
			}];
		}
		else
		{
			[NLYum getYumwithId:[self yumId] withCompletionBlock:^(NSError *error, NLYum *yum) {
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf hideLoadingSpinner];
				});
				
				if (!error)
				{
					[weakSelf setYum:yum];
					NSLog(@"%@", yum.name);
					weakSelf.captionSize = [weakSelf sizeForText:yum.caption];
					dispatch_async(dispatch_get_main_queue(), ^{
						[[weakSelf tableView] reloadData];
					});
				}
				else
				{
					[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yummmie", nil) withDuration:2.0f];
					//[self.navigationController popViewControllerAnimated:true];
				}
			}];
		}
		
	}
	
	if (self.addDismissButton)
	{
		UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"close_title", nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController)];
		self.navigationItem.leftBarButtonItem = dismissButton;
		
	}
	UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
	logoView.contentMode = UIViewContentModeScaleAspectFit;
	self.navigationItem.titleView = logoView;
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateYum) name:@"ShouldReloadAll" object:nil];
	
}
-(void)updateYum
{
	[self showLoadingSpinner];
	
	__weak NLPhotoDetailViewController *weakSelf = self;
	
	[NLYum getYumwithId:[self yumId] withCompletionBlock:^(NSError *error, NLYum *yum) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[weakSelf hideLoadingSpinner];
		});
		
		if (!error)
		{
			[weakSelf setYum:yum];
			weakSelf.captionSize = [weakSelf sizeForText:yum.caption];
			dispatch_async(dispatch_get_main_queue(), ^{
				[[weakSelf tableView] reloadData];
			});
		}else
		{
			[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yummmie", nil) withDuration:2.0f];
			//[self.navigationController popViewControllerAnimated:true];
		}
	}];
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[self navigationController] setNavigationBarHidden:NO];
	[[self tableView] reloadData];
	CGRect navigationFrame = [[[self navigationController] navigationBar] frame];
	
	if (navigationFrame.origin.y<20)
	{
		//set it always in the origin
		navigationFrame.origin.y = 20.0f;
		[self.navigationController.navigationBar setFrame:navigationFrame];
	}
	
	//Google Analytics
	NSString *name = [NSString stringWithFormat:@"Photo Detail View"];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

#pragma mark - Notification Methods

- (void)commentAdded:(NSNotification *)notification
{
	[[self tableView] reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (self.yum)
	{
		return 1;
	}
	return 0;
}
-(void)fillCellFromYum:(NLYum *)yum andCell:(NLTimeLineViewCell *)cell
{
	
	[cell setYumId:yum.yumId];
	
	
	if (yum.venue.venueType == NLPrivateVenue)
	{
		[cell setVenueTextColor:[UIColor colorWithRed:0.890 green:0.506 blue:0.537 alpha:1]];
		
	}else
	{
		[cell setVenueTextColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	}
	[[cell profileThumbButton] sd_setBackgroundImageWithURL:[NSURL URLWithString:[[yum user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
	
	
	[cell setNumberOfComments:[yum numberOfComments]];
	[cell setLiked:[yum userLikes]];
	[cell setNumberOfLikes:[yum likes]];
	[cell setNumberOfFavs:[yum favs]];
	[cell.repostButton setSelected:yum.isReposted];
	[cell setNumberOfReposts:[yum reposts]];
	[cell setViewCount:[yum viewsCount]];

	[cell.numberOfFavsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum favs]] forState:UIControlStateNormal];
	[cell.numberOfRepostsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum reposts]] forState:UIControlStateNormal];
	[cell.numbersOfViews setTitle:[NSString stringWithFormat:@"%ld", (long)[yum viewsCount]] forState:UIControlStateNormal];

	[cell setUsername:[NSString stringWithFormat:@"@%@",[[yum user] username]]];
	[cell setVenueName:[[yum venue] name]];
	
    CGSize dishSize = [self sizeForDish:yum.name];
    CGFloat dishHeight = (dishSize.height)+10;
    [cell setDishConstraint:dishHeight];
    //[cell setDishName:(!yum.name)?@"Default Name":yum.name];
    [cell setDishName:(!yum.name)?@"Default Name":yum.name withSize:dishSize];
    
	if (yum.repost)
	{
		[cell setRecommend:yum.repost];
		
	}
	[cell setPublishedTime:[yum createdDict]];
	[cell changeFavsNumber:yum.favs selfFav:yum.isFav];
	
	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NLTimeLineViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPhotoDetailCellIdentifier];
	
	NLUser *user = [[self yum] user];
	cell.delegate = self;
	[cell setTag:[indexPath row]];

	[self fillCellFromYum:self.yum andCell:cell];

	 /*
	 if (self.yum.venue.venueType == NLPrivateVenue)
	 {
	 [cell setVenueTextColor:[UIColor colorWithRed:0.890 green:0.506 blue:0.537 alpha:1]];
	 
	 }else
	 {
	 [cell setVenueTextColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	 }
	 */
	
	
	
	[cell setComment:[self.yum caption] withSize:self.captionSize andComments:self.yum.comments];
	UIImage *placeHolderImage = nil;
	
	[[cell dishPhoto] sd_setImageWithURL:[NSURL URLWithString:[self.yum image]] placeholderImage:placeHolderImage options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize)
	 {
		 if (expectedSize > 0 && self.yum.yumId != [NLSession yum].yumId)
		 {
			 [[cell indicator] setAlpha:1.0];
			 [[cell indicator] setProgress:(receivedSize*1.0f)/expectedSize];
		 }
	 } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
		 [[cell indicator] setAlpha:0.0f];
		 if (image && cacheType == SDImageCacheTypeNone && self.yum.yumId == [NLSession yum].yumId)
		 {
			 if ([NLSession getSharedPhoto])
			 {
				 [NLSession setYum:nil];
				 [NLSession setSharedPhoto:nil];
			 }
		 }
		 if (indexPath.row == 1)
		 {
			 //Blurred view to alerts
			 NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
			 NSLog(@"registro = %d, noNew = %d", [def boolForKey:@"registro"], [def boolForKey:@"noNew"]);
			 //[def setBool:true forKey:@"registro"];
			 //[def setBool:false forKey:@"noNew"];
			 
			 		 }
		 
		 if (!image)
		 {
			 NSLog(@"%@", [error localizedDescription]);
		 }
	 }];
    [cell setHashTags:@"cdmx" withSubRegion:@"cdmx" andFoodType:@"cdmx"];
	return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat width=rootView.frame.size.width;

    
    CGFloat userHeaderHeight=40;
    CGFloat commentHeight = 0;
    CGFloat repostHeight=0;
    //CGFloat yumDetailHeight=50;
    CGFloat yumNameHeight=25;
    CGFloat venueNameHeight=25;
    
    CGFloat hashtagArea=22.0f;//set as 0.0

    
    /*
     if (yum.comments.count == 1)
     {
     NSString *size = [[yum.comments firstObject] content];
     
     plusHeightComment = ((size.length >42)?(kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2):(kTImeLineCellMinimumCommentHeight));
     
     }else if(yum.comments.count == 2)
     {
     NSString *firstComment = [[yum.comments firstObject] content];
     NSString *secondComment = [[yum.comments lastObject] content];
     plusHeightComment = (firstComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight;
     plusHeightComment+=(kTimeLineCellCommentSeparation+((secondComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight));
     }else if(yum.comments.count >2)
     {
     NSString *firstComment = [[yum.comments objectAtIndex:(yum.comments.count-2)] content];
     NSString *secondComment = [[yum.comments lastObject] content];
     
     
     plusHeightComment = (kTimeLineCellCommentSeparation+((firstComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight));
     plusHeightComment+=(kTimeLineCellCommentSeparation+((secondComment.length > 42)?kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2:kTImeLineCellMinimumCommentHeight));
     plusHeightComment+=kTimeLineCellMoreCommentsButtonHeight;
     }*/
    
    
    if(self.yum.caption.length >42){
        //commentHeight = self.sizeForText:yum.caption;
        CGSize size= [self sizeForText:self.yum.caption];
        commentHeight = size.height;
        commentHeight+=3;
    }
    else{
        commentHeight=(kTImeLineCellMinimumCommentHeight);
        /*if(yum.caption.length == 0){
         commentHeight=20;
         }*/
        commentHeight=20;
        
    }
    
    CGSize dishSize = [self sizeForDish:self.yum.name];
    yumNameHeight = (dishSize.height)+10;
    
    if(self.yum.repost)
    {
        repostHeight += 36;
    }
    

    
    /*if(yum.hasHastags){
     hashtagArea=20.0f;
     }*/
    
     return userHeaderHeight+width+repostHeight+yumNameHeight+venueNameHeight+hashtagArea+commentHeight+kTimeLineViewCellBottomBarHeight;
}





#pragma mark - Calculate Cell Heights

- (CGSize)sizeForText:(NSString *)text
{
    static UIFont *cellFont;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellFont = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
    });
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat fixedWidth = rootView.frame.size.width-30;
    
    CGRect newframe = [text boundingRectWithSize:CGSizeMake(fixedWidth, 0.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellFont} context:nil];
    newframe.size.width = fixedWidth;
    
    NSLog(@"sizefortext height: %f  -> %@", newframe.size.height, text);
    return newframe.size;
}

- (CGSize)sizeForDish:(NSString *)text
{
    static UIFont *cellFont;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17.0f];
    });
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat fixedWidth = rootView.frame.size.width-30;
    
    CGRect newframe = [text boundingRectWithSize:CGSizeMake(fixedWidth, 0.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellFont} context:nil];
    newframe.size.width = fixedWidth;
    
    
    return newframe.size;
}


#pragma mark - NLTimeLineCellViewDelegate

/*- (void)updateLikeWithTag:(NSInteger)tag value:(BOOL)value andNumberOfLikes:(NSInteger)number
{
	__weak NLPhotoDetailViewController *weakSelf = self;
	NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:0];
	if (value)
	{
		if ([CSSearchableItemAttributeSet class]) {
			
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				NSString *venueName = [[weakSelf.yum venue] name];
				NSArray *keywords = [[weakSelf.yum.name componentsSeparatedByString:@" "] arrayByAddingObjectsFromArray:[venueName componentsSeparatedByString:@" "]];
				
				CSSearchableItemAttributeSet *cssset = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeImage];
				cssset.relatedUniqueIdentifier = [NSString stringWithFormat:@"yum-%ld",(long)weakSelf.yum.yumId];
				CSSearchableItem *item = [[CSSearchableItem alloc] initWithUniqueIdentifier:[NSString stringWithFormat:@"yum-%ld",(long)weakSelf.yum.yumId] domainIdentifier:@"com.noiselab.yummmie.like" attributeSet:cssset];
				cssset.title = weakSelf.yum.name;
				UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[weakSelf.yum thumbImage]]]];
				cssset.thumbnailData = UIImagePNGRepresentation(image);
				cssset.keywords = keywords;
				cssset.contentDescription = [NSString stringWithFormat:@"%@\n%ld user like this", weakSelf.yum.venue.name,(long)weakSelf.yum.likes];
				
				[[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler:^(NSError * _Nullable error) {
					
					if (error) {
						NSLog(@"%@",error.localizedDescription);
					}
				}];
			});
			
		}
		NSInteger yumId = weakSelf.yum.yumId;
		if (weakSelf.yum.repost)
		{
			yumId = [[weakSelf.yum.repost objectForKey:@"original_yum_id"] integerValue];
		}
		weakSelf.yum.likes++;
		[weakSelf.yum setUserLikes:YES];
		[NLYum likeYum:yumId withCompletionBlock:^(NSError *error)
			{
				if (error)
				{
					weakSelf.yum.likes--;
					[weakSelf.yum setUserLikes:false];
				}
				[[weakSelf tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

			}];
		
	}else
	{
		weakSelf.yum.likes-=1;
		[weakSelf.yum setUserLikes:NO];
		NSInteger yumId = weakSelf.yum.yumId;
		if (weakSelf.yum.repost)
		{
			yumId = [[weakSelf.yum.repost objectForKey:@"original_yum_id"] integerValue];
		}
		[NLYum unlikeYum:weakSelf.yum.yumId withCompletionBlock:^(NSError *error)
			{
				
				if (error)
				{
					weakSelf.yum.likes+=1;
					[weakSelf.yum setUserLikes:true];
					
				}
				[[weakSelf tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

				
			}];
	}
}*/
- (void)updateLikeWithTag:(NSInteger)tag value:(BOOL)value andNumberOfLikes:(NSInteger)number
{
	
	__weak NLYum *yum = self.yum;
	NSInteger yumId = yum.yumId;
	if (yum.repost)
	{
		yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
	}
	[NLYum likeUnlike:yumId like:!yum.userLikes withCompletionBlock:^(NSError *error)
	 {
		 if (!error)
		 {
			 yum.userLikes = !yum.userLikes;
			 if (yum.userLikes)
			 {
				 yum.likes++;
				 
			 }
			 else
			 {
				 yum.likes--;
			 }
		 }
		 else
		 {
			 
			 
		 }
		 [[NSNotificationCenter defaultCenter] postNotificationName:kNLLikeNotification object:self userInfo:@{@"yum":self.yum}];

	 }];
	

}

- (void)updateFavWithTag:(NSInteger)tag
{

	__weak NLYum *yum = self.yum;
	__weak NLPhotoDetailViewController *weakSelf = self;
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:0];
	
	if ([CSSearchableItemAttributeSet class])
	{
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			NSString *venueName = [[yum venue] name];
			NSArray *keywords = [[yum.name componentsSeparatedByString:@" "] arrayByAddingObjectsFromArray:[venueName componentsSeparatedByString:@" "]];
			
			CSSearchableItemAttributeSet *cssset = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeImage];
			cssset.relatedUniqueIdentifier = [NSString stringWithFormat:@"yum-%ld",(long)yum.yumId];
			CSSearchableItem *item = [[CSSearchableItem alloc] initWithUniqueIdentifier:[NSString stringWithFormat:@"yum-%ld",(long)yum.yumId] domainIdentifier:@"com.noiselab.yummmie.like" attributeSet:cssset];
			cssset.title = yum.name;
			UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[yum thumbImage]]]];
			cssset.thumbnailData = UIImagePNGRepresentation(image);
			cssset.keywords = keywords;
			cssset.contentDescription = [NSString stringWithFormat:@"%@\n%ld user like this", yum.venue.name,(long)yum.likes];
			
			[[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler:^(NSError * _Nullable error) {
				
				if (error) {
					NSLog(@"%@",error.localizedDescription);
				}
			}];
		});
		
	}
	NSInteger yumId = yum.yumId;
	if (yum.repost)
	{
		yumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
	}
	[NLYum favUnfav:yumId fav:!yum.isFav withCompletionBlock:^(NSError *error)
	 {
		 if (!error)
		 {
			 yum.isFav = !yum.isFav;
			 if (yum.isFav)
			 {
				 yum.favs++;
				 
			 }
			 else
			 {
				 yum.favs--;
			 }
			 [[weakSelf tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

		 }
		 [[NSNotificationCenter defaultCenter] postNotificationName:kNLLikeNotification object:self userInfo:@{@"yum":self.yum}];

		 
	 }];

}
-(void)confirm:(BOOL)confirmed yumId:(NSInteger)yumId andOriginalYumId:(NSInteger)originalYumId
{
	__weak NLPhotoDetailViewController *weakSelf = self;
	NSMutableArray *indexes = [NSMutableArray array];
	
	if (confirmed)
	{
		if (delete)
		{
			[NLYum destroy:yumId withCompletionBlock:^(NSError *error)
			 {
				 if (error)
				 {
					 dispatch_async(dispatch_get_main_queue(), ^{
						 [weakSelf showErrorMessage:NSLocalizedString(@"delete_yummmie_error", nil) withDuration:2.0f];
					 });
				 }
				 else
				 {
					 [self updateYum];
				 }
			 }];
		}
		else
		{
			[NLYum repostYumId:originalYumId withCompletionBlock:^(NSError *error)
			 {
				 if (error)
				 {
					 [weakSelf showErrorMessage:NSLocalizedString(error.localizedDescription, nil) withDuration:3.0f];
				 }
				 else
				 {
					 [self updateYum];
					 NLRepostFeedbackView *reposted = [[NLRepostFeedbackView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.view.frame.size.height) andImageBase:@"anim-" count:11];
					 [self.navigationController.view addSubview:reposted];
					 [reposted show];
				 }
			 }];
			
		}
	}
}
-(void)updateRepostWithTag:(NSInteger)tag andNumberOfLikes:(NSInteger)number;
{
	__weak NLYum *yum = [self yum];
	
	NSInteger yumId = yum.yumId;
	
	if (yum.isReposted)
	{
		delete = true;
		NSDictionary *dictConfirm = @
		{
			@"title": NSLocalizedString(@"repost-delete", nil),
			@"image": @"anim-11",
			@"botTitle": NSLocalizedString(@"dish_option_delete", nil)
		};
		NLConfirmView *confirm = [[NLConfirmView alloc] initWithDictionary:dictConfirm andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.view.frame.size.height)];
		confirm.alpha = 0;
		[self.tabBarController.view addSubview:confirm];
		confirm.yumId = yumId;
		confirm.originalYumId = yumId;
		if (yum.repost)
		{
			confirm.originalYumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
			
		}
		[UIView animateWithDuration:0.3 animations:^{
			confirm.alpha = 1;
		}];
		confirm.delegate = self;
		
		
	}
	else if(yum.user.userID != [NLSession currentUser].userID && [[[yum.repost objectForKey:@"user"] objectForKey:@"id"] integerValue] != [NLSession currentUser].userID)
	{
		delete = false;
		NSDictionary *dictConfirm = @
		{
			@"title": NSLocalizedString(@"dish_option_repost", nil),
			@"image": @"anim-11",
			@"botTitle": NSLocalizedString(@"OK", nil)
		};
		NLConfirmView *confirm = [[NLConfirmView alloc] initWithDictionary:dictConfirm andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.view.frame.size.height)];
		confirm.alpha = 0;
		[self.tabBarController.view addSubview:confirm];
		confirm.yumId = yumId;
		confirm.originalYumId = yumId;
		if (yum.repost)
		{
			confirm.originalYumId = [[yum.repost objectForKey:@"original_yum_id"] integerValue];
			
		}
		//self.tableView.userInteractionEnabled = false;
		[UIView animateWithDuration:0.3 animations:^{
			confirm.alpha = 1;
		}];
		confirm.delegate = self;
	}
	
}

- (void)openCommentsWithTag:(NSInteger)tag
{
	NLCommentsViewController *commentsVC = [[NLCommentsViewController alloc] initWithYum:[self yum]];
	[commentsVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:commentsVC animated:YES];
}

- (void)openOptionsWithTag:(NSInteger)tag
{
	
	
	UIAlertController *sheetVc = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
	
	
	
	NSIndexPath *currentIndexPath = [NSIndexPath indexPathForItem:tag inSection:0];
	
	NLYum *yumToDelete = self.yum;
	NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[[self tableView] cellForRowAtIndexPath:currentIndexPath];
	
	NSInteger yumId = yumToDelete.yumId;
	NSInteger originalYumId = yumToDelete.yumId;

	if (yumToDelete.repost)
	{
		originalYumId = [[yumToDelete.repost objectForKey:@"original_yum_id"] integerValue];

	}

	__weak NLPhotoDetailViewController *weakSelf = self;
	
	
	UIAlertAction *dltAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action)
													 {
														 [NLYum destroy:yumId withCompletionBlock:^(NSError *error)
															{
																
																if (!error)
																{
																	if (self.delegate && [[self delegate] respondsToSelector:@selector(yumDeleted:)])
																	{
																		[[self delegate] yumDeleted:weakSelf.yum];
																	}
																	[[weakSelf navigationController] popViewControllerAnimated:YES];
																}else{
																	[weakSelf showErrorMessage:NSLocalizedString(@"delete_yummmie_error", nil) withDuration:2.0f];
																}
															}];
													 }];
	
	UIAlertAction *reportAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_report", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action)
															{
																//call the report api option
																
																[NLYum reportYum:yumId withCompletionBlock:^(NSError *error)
																{
																	
																	if (!error)
																	{
																		if (self.delegate && [[self delegate] respondsToSelector:@selector(yumDeleted:)])
																		{
																			[[self delegate] yumDeleted:weakSelf.yum];
																		}
																		[[weakSelf navigationController] popViewControllerAnimated:YES];
																	}else{
																		[weakSelf showErrorMessage:NSLocalizedString(@"delete_yummmie_error", nil) withDuration:2.0f];
																	}

																	
																}];
																
															}];
	
	UIAlertAction *twAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_tweet", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		/*if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
		{
			SLComposeViewController *twitterShareVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
			[twitterShareVC setInitialText:[NSString stringWithFormat:@"%@ %@ %@ @%@ %@ (%@)",[yumToDelete name], [yumToDelete caption], NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil), [yumToDelete shareUrl]]];
			UIImage *yumImage = [[cell dishPhoto] image];
			[twitterShareVC addImage:yumImage];
			[self presentViewController:twitterShareVC animated:YES completion:nil];
		}else
		{
			[self showErrorMessage:NSLocalizedString(@"twitter_not_setup", nil) withDuration:2.0f];
		}*/
        
        
        //---
        if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
            //TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
            NSString *initialText = [NSString stringWithFormat:@"%@ %@ %@ @%@ %@ (%@)",[yumToDelete name], [yumToDelete caption], NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil), [yumToDelete shareUrl]];
            TWTRComposerViewController *composer = [[TWTRComposerViewController alloc] initWithInitialText:initialText image:[[cell dishPhoto] image] videoURL:nil];
            
            [self presentViewController:composer animated:YES completion:nil];
        } else {
            [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
                if (session) {
                    TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
                    [self presentViewController:composer animated:YES completion:nil];
                } else {
                    [self showErrorMessage:NSLocalizedString(@"twitter_not_setup", nil) withDuration:2.0f];
                }
            }];
        }
        //---
	}];
	
    UIAlertAction *fbAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_facebook", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:yumToDelete.shareUrl];
        [content setContentTitle:[NSString stringWithFormat:@"%@ %@ %@ @%@ %@",[yumToDelete name],[yumToDelete caption],NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil)]];
        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
        dialog.fromViewController = self;
        [dialog setShareContent:content];
        [dialog setDelegate:self];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]]){
            dialog.mode = FBSDKShareDialogModeNative;
        }
        else {
            //dialog.mode = FBSDKShareDialogModeFeedWeb; //opt1
            dialog.mode = FBSDKShareDialogModeFeedBrowser; //opt2
        }
        
        
        if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
            
            [dialog show];
            
            
        } else {
            FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
            [loginManager logInWithPublishPermissions:@[@"publish_actions"]
                                   fromViewController:self
                                              handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                                  //TODO: process error or result.
                                                  
                                                  if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
                                                      
                                                      [dialog show];
                                                      
                                                      
                                                  } else {
                                                      [self showErrorMessage:NSLocalizedString(@"facebook_not_setup", nil) withDuration:2.0f];
                                                  }
                                                  
                                              }];
        }
    }];
    UIAlertAction *igAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_instagram", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
													{
														if ([MGInstagram isAppInstalled])
														{
															UIWindow *window = [[UIApplication sharedApplication] keyWindow];
															UIGraphicsBeginImageContext(CGSizeMake(window.frame.size.width, window.frame.size.height));
															[window drawViewHierarchyInRect:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height) afterScreenUpdates:YES];
															UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
															UIGraphicsEndImageContext();
															
															self.instagramReminder = [[NLInstagramReminderView alloc] initWithImage:image andYumImageURL:[yumToDelete thumbImage]];
															self.instagramReminder.center = self.view.superview.center;
															[self.instagramReminder setAlpha:0.0f];
															[self.tabBarController.view addSubview:[self instagramReminder]];
															[self.instagramReminder setDelegate:self];
															
															__weak NLPhotoDetailViewController *weakSelf = self;

															[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
																[weakSelf.instagramReminder setAlpha:1.0f];
															} completion:nil];
														}else{
															[self showErrorMessage:NSLocalizedString(@"instagram_not_installed", nil) withDuration:2.0f];
														}
													}];
	UIAlertAction *waAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_whatsapp", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		NSLog(@"%@", yumToDelete.shareUrl);
		
		NSURL *whatsappURL = [NSURL URLWithString:[[[NSString stringWithFormat:@"whatsapp://send?text=%@ %@ %@ @%@ %@ %@",[yumToDelete name], [yumToDelete caption], NSLocalizedString(@"author_by_text", nil),[[yumToDelete user] username],NSLocalizedString(@"share_default_text", nil), [yumToDelete shareUrl]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"&" withString:@"and"]];
		if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
			[[UIApplication sharedApplication] openURL: whatsappURL];
		}
	}];
	UIAlertAction *saveAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"save_option", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
														{
															ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
															[library saveImage:[[cell dishPhoto] image] toAlbum:kYummmieGalleryName completion:^(NSURL *assetURL, NSError *error) {
															} failure:^(NSError *error) {
															}];
														}];
	
	UIAlertAction *editAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_edit", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
														{
															if ([NLSession isCurrentUser:yumToDelete.user.userID])
															{
																NLEditViewController *eVC = [[NLEditViewController alloc] initWithYum:yumToDelete];
																[eVC setHidesBottomBarWhenPushed:YES];
																
																[[self navigationController] pushViewController:eVC animated:YES];
															}
														}];
	
	/*UIAlertAction *repostAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_repost", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
		
		self.yum.reposts+=1;
		[self.yum setIsReposted:true];
		
		[NLYum repostYumId:originalYumId withCompletionBlock:^(NSError *error)
		 {
			 if(!error)
			 {
				 NLRepostFeedbackView *reposted = [[NLRepostFeedbackView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andImageBase:@"anim-" count:11];
				 [self.tabBarController.view addSubview:reposted];
				 [reposted show];
				 
			 }
			 else
			 {
				 if (self.yum.reposts>0)
				 {
					 self.yum.reposts-=1;
					 [self.yum setIsReposted:false];
				 }
				 NSLog(@"%@", error.localizedDescription);
				 [weakSelf showErrorMessage:NSLocalizedString(error.localizedDescription, nil) withDuration:3.0f];
				 
			 }
			 NSIndexPath *nindexPath = [NSIndexPath indexPathForItem:0 inSection:0];
			 
			 [[weakSelf tableView] reloadRowsAtIndexPaths:@[nindexPath] withRowAnimation:UITableViewRowAnimationNone];
			 
		 }];
	}];*/
	
	UIAlertAction *cancelAct = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
															{
																
															}];
	if ([NLSession isCurrentUser:self.yum.user.userID])
	{
        
		[sheetVc addAction:igAct];
        
		[sheetVc addAction:fbAct];
		[sheetVc addAction:twAct];
        if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp:"]])
        {
            [sheetVc addAction:waAct];
        }
		[sheetVc addAction:saveAct];
		[sheetVc addAction:editAct];
		[sheetVc addAction:dltAct];
		[sheetVc addAction:cancelAct];
	}
	else
	{
		//[sheetVc addAction:repostAct];
		[sheetVc addAction:igAct];
		[sheetVc addAction:fbAct];
		[sheetVc addAction:twAct];
        if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp:"]])
        {
            [sheetVc addAction:waAct];
        }
		[sheetVc addAction:saveAct];
		[sheetVc addAction:reportAct];

		[sheetVc addAction:cancelAct];
	}
	[self presentViewController:sheetVc animated:true completion:nil];
	
}


- (void)openPhotoWithTag:(NSInteger)tag
{
	NSIndexPath *indexPath = [NSIndexPath indexPathForItem:tag inSection:0];
	NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[[self tableView] cellForRowAtIndexPath:indexPath];
	
	TGRImageViewController *imageVC = [[TGRImageViewController alloc] initWithImage:cell.dishPhoto.image andYummmie:self.yum];
	self.photoAnimationController = [[TGRImageZoomAnimationController alloc] initWithReferenceImageView:cell.dishPhoto];
	imageVC.transitioningDelegate = self;
	[self presentViewController:imageVC animated:YES completion:nil];
}

-(void)openProfileWithTag:(NSInteger)tag
{
	NLUser *user =[[self yum] user];
	
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:user isProfileUser:NO];
	[[self navigationController] pushViewController:profileVC animated:YES];
}

- (void)openDishWithTag:(NSInteger)tag
{
	NLDishViewController *dishVC = [[NLDishViewController alloc] initWithDishName:[[self yum] name]];
	[[self navigationController] pushViewController:dishVC animated:YES];
}

- (void)openVenueWithTag:(NSInteger)tag
{
	
	/* NLVenue *selectedVenue = [[self yum] venue];
	 NLVenueViewController *venueVC = [[NLVenueViewController alloc] initWithVenue:selectedVenue];
	 [[self navigationController] pushViewController:venueVC animated:YES];*/
	[NLVenue getVenueWithId:[[[self yum] venue] venueId] withCompletionBlock:^(NSError *error, NLVenue *venue) {
		NLVenueViewController *venueVC = [[NLVenueViewController alloc] initWithVenue:venue];
		//        NLVenueDetailViewController *detailVC = [[NLVenueDetailViewController alloc] initWithVenue:selectedVenue];
		[venueVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:venueVC animated:YES];
		
	}];
}

- (void)openHashTag:(NSString *)hashtag withTag:(NSInteger)tag
{
	NLTag *selectedTag;
	
	if ([[[self yum] tags] count] == 0)
	{
		selectedTag = [[[self yum] tags] lastObject];
	}else
	{
		for (NLTag *tag in [[self yum] tags])
		{
			if ([tag isTag:hashtag])
			{
				selectedTag = tag;
				break;
			}
		}
	}
	
	if (selectedTag) {
		NLHashtagViewController *hashtagVC = [[NLHashtagViewController alloc] initWithTag:selectedTag andNumberOfYums:[selectedTag numberOfYums]];
		[[self navigationController] pushViewController:hashtagVC animated:YES];
	}else{
		NLHashtagViewController *hashtagVC = [[NLHashtagViewController alloc] initWithHashtag:hashtag];
		[[self navigationController] pushViewController:hashtagVC animated:YES];
	}
}

- (void)openUser:(NSString *)user
{
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUsername:user];
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
	//NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUsername:user];
	//[[self navigationController] pushViewController:profileVC animated:YES];
}

- (void)openLikers:(NSInteger)tag
{
	NLLikersViewController *likersVC = [[NLLikersViewController alloc] initWithYum:self.yum andOption:1];
	[likersVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:likersVC animated:YES];
	
}
- (void)openFavers:(NSInteger)tag
{
	NLLikersViewController *likersVC = [[NLLikersViewController alloc] initWithYum:self.yum andOption:2];
	[likersVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:likersVC animated:YES];
	
}
- (void)openReposters:(NSInteger)tag
{
	NLLikersViewController *likersVC = [[NLLikersViewController alloc] initWithYum:self.yum andOption:3];
	[likersVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:likersVC animated:YES];
	
}


- (void)openFirstCommentUser:(NSInteger)tag
{
	
	NSArray *comments = [[self yum] comments];
	
	NLUser *user;
	if (comments.count >1)
	{
		NLComment *comment = [comments objectAtIndex:comments.count-2];
		user = [comment user];
		
	}else
	{
		NLComment *comment = [comments lastObject];
		user = [comment user];
	}
	
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:user];
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
	
}

- (void)openFirstCommentHashTag:(NSInteger)tag andHashtag:(NSString *)hashtag
{
	NSArray *tags = [(NLYum *)[[[self yum] comments] firstObject] tags];
	
	NLTag *selectedTag;
	
	if ([tags count] == 1)
	{
		selectedTag  = [tags lastObject];
	}else
	{
		for (NLTag *tag in tags)
		{
			if ([tag isTag:hashtag])
			{
				selectedTag = tag;
				break;
			}
		}
	}
	
	NLHashtagViewController *hashtagVC;
	
	if (selectedTag)
	{
		hashtagVC = [[NLHashtagViewController alloc] initWithTag:selectedTag andNumberOfYums:[selectedTag numberOfYums]];
		[[self navigationController] pushViewController:hashtagVC animated:YES];
	}
}

- (void)openSecondCommentHashTag:(NSInteger)tag andHashtag:(NSString *)hashtag{
	NSArray *tags = [(NLYum *)[[[self yum] comments] firstObject] tags];
	
	NLTag *selectedTag;
	
	if ([tags count] == 1)
	{
		selectedTag  = [tags lastObject];
	}else
	{
		for (NLTag *tag in tags)
		{
			if ([tag isTag:hashtag])
			{
				selectedTag = tag;
				break;
			}
		}
	}
	
	NLHashtagViewController *hashtagVC;
	
	if (selectedTag)
	{
		hashtagVC = [[NLHashtagViewController alloc] initWithTag:selectedTag andNumberOfYums:[selectedTag numberOfYums]];
		[[self navigationController] pushViewController:hashtagVC animated:YES];
	}
}
- (void)openRecommender:(NSInteger)tag
{
		NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUserId:[[[self.yum.repost objectForKey:@"user"] objectForKey:@"id"] integerValue] andCloseButton:false];
		[profileVC setHidesBottomBarWhenPushed:YES];
		[[self navigationController] pushViewController:profileVC animated:YES];
}

#pragma mark - Scroll

-(void)openSecondCommentUser:(NSInteger)tag
{
	
	NSArray *comments = [[self yum] comments];
	
	NLUser *user = [(NLComment *)[comments lastObject] user];
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:user];
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
}

- (void)scrollToTop
{
	[self.tableView setContentOffset:CGPointZero animated:YES];
}

#pragma  mark - UIActionSheetDelegate
/*
 - (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
 {
 NSInteger yumId = self.yum.yumId;
 
 __weak NLPhotoDetailViewController *weakSelf = self;
 __weak NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[[self tableView] cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
 
 switch (buttonIndex)
 {
 case 0:
 {
 if ([[actionSheet buttonTitleAtIndex:buttonIndex]isEqualToString:NSLocalizedString(@"dish_option_delete", nil) ])
 {
 //remove the element from the table, remove the size to
 //call the delete api option
 [NLYum destroy:yumId withCompletionBlock:^(NSError *error) {
 
 if (!error)
 {
 if (self.delegate && [[self delegate] respondsToSelector:@selector(yumDeleted:)])
 {
 [[self delegate] yumDeleted:weakSelf.yum];
 }
 [[weakSelf navigationController] popViewControllerAnimated:YES];
 }else{
 [weakSelf showErrorMessage:NSLocalizedString(@"delete_yummmie_error", nil) withDuration:2.0f];
 }
 }];
 }else
 {
 //call the report api option
 [weakSelf scrollToTop];
 [self showErrorMessage:NSLocalizedString(@"report_photo_text", nil) withDuration:3.0f];
 
 }
 
 break;
 }
 case 1:
 {
 if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
 {
 SLComposeViewController *twitterShareVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
 [twitterShareVC setInitialText:[NSString stringWithFormat:@"%@ %@",[[self yum] name],NSLocalizedString(@"share_default_text", nil)]];
 UIImage *yumImage = [[cell dishPhoto] image];
 [twitterShareVC addImage:yumImage];
 [self presentViewController:twitterShareVC animated:YES completion:nil];
 }else
 {
 [weakSelf scrollToTop];
 [self showErrorMessage:NSLocalizedString(@"twitter_not_setup", nil) withDuration:2.0f];
 }
 break;
 }
 case 2:
 {
 if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
 {
 SLComposeViewController *facebookShareVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
 [facebookShareVC setInitialText:[NSString stringWithFormat:@"%@ %@",[[self yum] name],NSLocalizedString(@"share_default_text", nil)]];
 UIImage *yumImage = [[cell dishPhoto] image];
 [facebookShareVC addImage:yumImage];
 
 [self presentViewController:facebookShareVC animated:YES completion:nil];
 }else
 {
 [weakSelf scrollToTop];
 [self showErrorMessage:NSLocalizedString(@"facebook_not_setup", nil) withDuration:2.0f];
 }
 break;
 }
 case 3:
 {
 if ([MGInstagram isAppInstalled])
 {
 UIWindow *window = [[UIApplication sharedApplication] keyWindow];
 UIGraphicsBeginImageContext(CGSizeMake(window.frame.size.width, window.frame.size.height));
 [window drawViewHierarchyInRect:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height) afterScreenUpdates:YES];
 UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
 UIGraphicsEndImageContext();
 
 self.instagramReminder = [[NLInstagramReminderView alloc] initWithImage:image andYumImageURL:[[self yum] thumbImage]];
 self.instagramReminder.center = self.view.superview.center;
 [self.instagramReminder setAlpha:0.0f];
 [self.tabBarController.view addSubview:[self instagramReminder]];
 [self.instagramReminder setDelegate:self];
 
 __weak NLPhotoDetailViewController *weakSelf = self;
 
 [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
 [weakSelf.instagramReminder setAlpha:1.0f];
 } completion:nil];
 }else{
 [self showErrorMessage:NSLocalizedString(@"instagram_not_installed", nil) withDuration:2.0f];
 }
 
 break;
 }
 case 4:
 {
 ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
 [library saveImage:[[cell dishPhoto] image] toAlbum:kYummmieGalleryName completion:^(NSURL *assetURL, NSError *error) {
 } failure:^(NSError *error) {
 }];
 break;
 }
 case 5:
 {
 if ([NLSession isCurrentUser:self.yum.user.userID])
 {
 NLEditViewController *eVC = [[NLEditViewController alloc] initWithYum:self.yum];
 [eVC setHidesBottomBarWhenPushed:YES];
 [[self navigationController] pushViewController:eVC animated:YES];
 }
 }
 default:
 break;
 }
 
 }
 */
#pragma mark - UIViewControllerTransitionDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
	if ([presented isKindOfClass:[TGRImageViewController class]])
	{
		return self.photoAnimationController;
	}else
	{
		return nil;
	}
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
	if ([dismissed isKindOfClass:[TGRImageViewController class]])
	{
		return self.photoAnimationController;
	}else
	{
		return nil;
	}
}

#pragma mark - Dismiss ViewController

- (void)dismissViewController
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - NLInstagramViewDelegate

- (void)instagramViewDismissed:(NLInstagramReminderView *)instagramView
{
	__weak NLPhotoDetailViewController *weakSelf = self;
	
	[UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
		[[weakSelf instagramReminder] setAlpha:0.0f];
	} completion:^(BOOL finished) {
		[[weakSelf instagramReminder] removeFromSuperview];
		[weakSelf setInstagramReminder:nil];
		
		
		NLTimeLineViewCell *cell = (NLTimeLineViewCell *)[[weakSelf tableView] cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
		UIImage *yumImage = [[cell dishPhoto] image];;
		
		NSString *shareString;
		
		if([NLSession isCurrentUser:[[[weakSelf yum] user] userID]]){
			shareString = [NSString stringWithFormat:@"%@ %@ %@",[[weakSelf yum] name],[[weakSelf yum] caption],NSLocalizedString(@"share_default_text", nil)];
		}else{
			shareString = [NSString stringWithFormat:@"%@ %@ %@ @%@ %@",[[weakSelf yum] name],[[weakSelf yum] caption],NSLocalizedString(@"author_by_text", nil),[[[weakSelf yum] user] username],NSLocalizedString(@"share_default_text", nil)];
		}
		
		UIPasteboard *generalPB = [UIPasteboard generalPasteboard];
		generalPB.string = shareString;
		if ([MGInstagram isAppInstalled])
		{
			[self instaGramWallPost:yumImage];
		}
		else {
			NSLog(@"Error Instagram is either not installed or image is incorrect size");
		}
		
		//MGInstagram *ins = [[MGInstagram alloc] init];
		//[ins postImage:yumImage inView:[weakSelf view]];
		//[MGInstagram postImage:yumImage inView:[weakSelf view]];
	}];
}
-(void)instaGramWallPost:(UIImage *)image
{
	NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
	if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
	{
		NSString *documentDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
		// *.igo is exclusive to instagram
		NSString *saveImagePath = [documentDirectory stringByAppendingPathComponent:@"Image.igo"];
		NSData *imageData = UIImagePNGRepresentation(image);
		[imageData writeToFile:saveImagePath atomically:YES];
		
		NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
		
		self.documentController=[[UIDocumentInteractionController alloc] init];
		self.documentController.delegate=self;
		self.documentController.UTI=@"com.instagram.exclusivegram";
		[self.documentController setURL:imageURL];
		self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:@"#yourHashTagGoesHere", @"InstagramCaption", nil];
		[self.documentController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
	}
	
	else
	{
		NSLog (@"Instagram not found");
	}
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate
{
	NSLog(@"file url %@",fileURL);
	UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
	interactionController.delegate = interactionDelegate;
	
	return interactionController;
}
-(void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
	[self setNeedsStatusBarAppearanceUpdate];
	
}
-(void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
	[self setNeedsStatusBarAppearanceUpdate];
	
}

#pragma mark - FBSDKSharingDelegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults :(NSDictionary *)results {
    NSLog(@"FB: SHARE RESULTS=%@\n",[results debugDescription]);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"FB: ERROR=%@\n",[error debugDescription]);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"FB: CANCELED SHARER=%@\n",[sharer debugDescription]);
}
@end
