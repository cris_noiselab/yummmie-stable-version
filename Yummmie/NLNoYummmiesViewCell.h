//
//  NLNoYummmiesViewCell.h
//  Yummmie
//
//  Created by bot on 8/12/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLNoYummmiesViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@end
