//
//  NLFoodType.h
//  Yummmie
//
//  Created by bot on 7/27/17.
//  Copyright © 2017 Noiselab Apps. All rights reserved.
//




#import <Foundation/Foundation.h>
#import "NLYummieApiClient.h"
#import "NLSubRegion.h"



@interface NLFoodType : NSObject

@property (nonatomic, copy) NSString *food_type_id;
@property (nonatomic, copy) NSString *food_type_name;

- (instancetype)initWithDictionary:(NSDictionary *)authenticationDict;
+ (NSURLSessionDataTask *)getFoodTypes :(void (^)(NSError *error, NSArray *regions))block;


@end
