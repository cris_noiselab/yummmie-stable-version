//
//  NLTag.m
//  Yummmie
//
//  Created by bot on 7/28/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLTag.h"
#import "NLYummieApiClient.h"
#import "NLConstants.h"
#import "NLSession.h"


static NSString * const KTagQueryKey = @"query";
static NSString * const kTagBeforeKey = @"before_id";
static NSString * const kTagAfterKey = @"after_id";

@implementation NLTag

- (void) encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeInteger:_tagId forKey:@"id"];
    [encoder encodeObject:_name forKey:@"content"];
    [encoder encodeInteger:_numberOfYums forKey:@"yums"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    //NSString *username = [decoder decodeObjectForKey:@"username"];
    //NSString *username = [decoder decodeObjectForKey:@"username"];
    
    NSDictionary *dictionary = @{@"content": [decoder decodeObjectForKey:@"content"], @"id": [NSNumber numberWithInteger:[decoder decodeIntegerForKey:@"id"]], @"yums": [NSNumber numberWithInteger:[decoder decodeIntegerForKey:@"yums"]]};
    
    return [self initWithDictionaryNumber:dictionary];
}
- (id)initWithDictionaryNumber:(NSDictionary *)tagDictionary
{
    if (self = [super init])
    {
        _tagId = [[tagDictionary valueForKey:@"id"] integerValue];
        _name = [tagDictionary valueForKeyPath:@"content"];
        _numberOfYums = [[tagDictionary valueForKey:@"yums"] integerValue];
    }
    
    return self;
}
- (id)initWithDictionary:(NSDictionary *)tagDictionary
{
    if (self = [super init])
    {
        _tagId = [[tagDictionary valueForKey:@"id"] integerValue];
        _name = [tagDictionary valueForKeyPath:@"name"];
				_numberOfYums = [[tagDictionary valueForKey:@"count"] integerValue];
    }
    
    return self;
}

- (BOOL)isTag:(NSString *)tagValue
{
    return [[self name] isEqualToString:[tagValue lowercaseString]];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"tag :%@ id :%ld",[self name],(long)[self tagId]];
}
+ (NSURLSessionDataTask *)getTagsWithQuery:(NSString *)query afterTagId:(NSInteger)tagId withCompletionBlock:(void (^)(NSError *error, NSArray *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSString *apiPath = @"yums/search_hashtags";
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if (tagId != 0)
    {
        [params setObject:@(tagId) forKey:kTagAfterKey];
    }
    [params setObject:query forKey:@"hashtag"];
    
    return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if (block)
        {
            if ([[responseObject objectForKey:@"success"] boolValue])
            {
                NSMutableArray *tags = [NSMutableArray array];
                for (NSDictionary *tagDict in [responseObject objectForKey:@"tags"])
                {
                    NLTag *tag = [[NLTag alloc] initWithDictionary:tagDict];
                    [tags addObject:tag];
                }
                block(nil, [NSArray arrayWithArray:tags]);
            }else
            {
							NSLog(@"%@\n%@", apiPath, responseObject);

                block([NSError new],nil);
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
            NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
        if (block)
        {
            block(error,nil);
        }
    }];
}
@end
