//
//  NLJSONResponseSerializer.h
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "AFURLResponseSerialization.h"

static NSString * const JSONResponseSerializerWithDataKey = @"JSONResponseSerializerWithDataKey";

@interface NLJSONResponseSerializerWithData : AFJSONResponseSerializer

@end