//
//  NLProfileViewController.m
//  Yummmie
//
//  Created by bot on 7/1/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.

//

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <YKTwitterHelper/YATTwitterHelper.h>

#import "NLProfileViewController.h"
#import "NLNavigationViewController.h"
#import "NLDashboardViewController.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLProfileFooter.h"
#import "NSString+NLStringValidations.h"
#import "NLPhotoDetailViewController.h"
#import "NLUserListViewController.h"
#import "NLConstants.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NSURL+AssetURL.h"
#import "NLYum.h"
#import "NLNotifications.h"
#import "UIColor+NLColor.h"
#import "TGRImageViewController.h"
#import "TGRImageZoomAnimationController.h"
#import "NLDishesCollectionViewController.h"

static NSString * const kPhotoProfileViewCellIdentifier = @"kPhotoProfileViewCellIdentifier";
static NSString * const kProfileHeaderViewIdentifier = @"kProfileHeaderViewIdentifie";
static NSString * const kProfileFooterViewIdentifier = @"kProfileFooterViewIdentifier";

static const CGFloat kSiteButtonHeight = 35.0f;
static const CGFloat kBioLabelHeight = 70.0f;
static const CGFloat kDefaultHeaderHeight = 245.0f;


@interface NLProfileViewController ()<NLPhotoDetailViewDelegate>

@property (nonatomic, strong) TGRImageZoomAnimationController *photoAnimationController;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) NLProfileHeaderView *header;
@property (nonatomic, getter = isLoading) BOOL loading;
@property (weak, nonatomic) NLProfileFooter *footer;
@property (nonatomic) BOOL mainUser;
@property (nonatomic, strong) NSMutableArray *dishes;
@property (nonatomic, copy) NSString *username;
@property (nonatomic) NSInteger userIdToLoad;
@property (nonatomic) BOOL addCloseButton;


- (void)reloadData:(id)sender;
- (void)loadMoreDishes:(NLProfileFooter *)footer;
- (CGFloat)calculateHeaderHeight;
- (void)loadIfNeeded:(UIScrollView *)scrollView;
- (void)updateUser;
- (void)reloadUserData:(NSNotification *)notification;
- (void)loadUserWithUsername:(NSString *)aUsername;
- (void)loadUserWithId;

- (void)updateUserHeader:(NSNotification *)notification;
- (void)showSettings:(id)sender;

- (void)dismissViewController;

- (void)logout:(NSNotification *)notification;

@end

@implementation NLProfileViewController
{
    NSInteger currentProfilePage;
    NSInteger totalProfilePage;
}
- (id)initWithUser:(NLUser *)aUser
{
    if (self = [super initWithNibName:NSStringFromClass([NLProfileViewController class]) bundle:nil])
    {
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _user = aUser;
        _mainUser = NO;
        _dishes = [NSMutableArray array];
        
    }
    return self;
}

- (id)initWithUserId:(NSInteger)userId andCloseButton:(BOOL)flag
{
    if (self = [super initWithNibName:NSStringFromClass([NLProfileViewController class]) bundle:nil])
    {
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _mainUser = NO;
        _dishes = [NSMutableArray array];
        _userIdToLoad = userId;
        _addCloseButton = flag;
    }
    return self;
}

- (id)initWithUser:(NLUser *)aUser isProfileUser:(BOOL)flag
{
    if (self = [super initWithNibName:NSStringFromClass([NLProfileViewController class]) bundle:nil])
    {
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _user = aUser;
        _mainUser = flag;
        _dishes = [NSMutableArray array];
        
        if (_mainUser)
        {
            [self addObserver:self forKeyPath:@"user" options:NSKeyValueObservingOptionNew context:nil];
            NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
            [defaultCenter addObserver:self selector:@selector(reloadUserData:) name:kNLReloadDishesNotification object:nil];
            [defaultCenter addObserver:self selector:@selector(updateUserHeader:) name:kNLUpdateSettingsNotification object:nil];
        }
    }
    return self;
}

- (id)initWithUsername:(NSString *)aUsername
{
    if (self = [super initWithNibName:NSStringFromClass([NLProfileViewController class]) bundle:nil])
    {
        if ([NLSession isCurrentUserWithString:aUsername])
        {
            _user = [NLSession currentUser];
            NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
            [defaultCenter addObserver:self selector:@selector(reloadUserData:) name:kNLReloadDishesNotification object:nil];
            [defaultCenter addObserver:self selector:@selector(updateUserHeader:) name:kNLUpdateSettingsNotification object:nil];
        }
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _mainUser = NO;
        _dishes = [NSMutableArray array];
        _username = [aUsername lowercaseString];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    currentProfilePage = 1;
    // Do any additional setup after loading the view from its nib.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor nl_applicationRedColor];
    [[self refreshControl] addTarget:self action:@selector(reloadData:) forControlEvents:UIControlEventValueChanged];
    [[self profileCollection] insertSubview:[self refreshControl] atIndex:0];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
    
    [[self profileCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kPhotoProfileViewCellIdentifier];
    [[self profileCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLProfileHeaderView class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kProfileHeaderViewIdentifier];
    [[self profileCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLProfileFooter class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kProfileFooterViewIdentifier];
    
    [[self profileCollection] setDelegate:self];
    [[self profileCollection] setDataSource:self];
    
    //if there's a user load it's data
    if ([self user])
    {
        [self reloadData:nil];
        if ([NLSession isCurrentUser:self.user.userID] && self.navigationController.viewControllers.count == 1)
        {
            UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bot_settings"] style:UIBarButtonItemStylePlain target:self  action:@selector(showSettings:)];
            self.navigationItem.rightBarButtonItem = settingsButton;
            
            //UIBarButtonItem *favsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bot-favs-profile"] style:UIBarButtonItemStylePlain target:self  action:@selector(showFavs:)];
            //self.navigationItem.rightBarButtonItem = favsButton;
            
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:kNLLogoutNotification object:nil];
    }else
    {
        //[self loadUserWithId];
        
        if (self.username)
        {
            [self loadUserWithUsername:[self username]];
        }else
        {
            [self loadUserWithId];
        }
        
    }
    
    
    if (self.addCloseButton)
    {
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"close_title", nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController)];
        self.navigationItem.leftBarButtonItem = closeButton;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUser) name:@"ShouldReloadAll" object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];
    
    CGRect frame = self.navigationController.navigationBar.frame;
    
    if (frame.origin.y < 20.0f)
    {
        frame.origin.y = 20.0f;
        [self.navigationController.navigationBar setFrame:frame];
    }
    
    self.navigationItem.titleView.alpha  = 1.0;
    //Google Analytics
    NSString *name = [NSString stringWithFormat:@"Profile View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)scrollToTop
{
    if (self.dishes)
    {
        if (self.dishes.count > 0)
        {
            [self.profileCollection scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:true];
            
        }
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if ([self mainUser])
    {
        [self removeObserver:self forKeyPath:@"user" context:nil];
    }
}

#pragma mark - Lgout Notitification

- (void)logout:(NSNotification *)notification
{
    self.dishes = nil;
}

#pragma  mark - Dismiss ViewController

- (void)dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Show Settings

- (void)showSettings:(id)sender
{
    NLDashboardViewController *dashboardVC = [[NLDashboardViewController alloc] initWithNibName:NSStringFromClass([NLDashboardViewController class]) bundle:nil];
    NLNavigationViewController *navController = [[NLNavigationViewController alloc] initWithRootViewController:dashboardVC];
    [self presentViewController:navController animated:YES completion:nil];
}
- (void)showFavs:(id)sender
{
    NLDishesCollectionViewController *dishesVC = [[NLDishesCollectionViewController alloc] init];
    
    
    [self.navigationController pushViewController:dishesVC animated:YES];
}
#pragma mark - Load Data

- (void)updateUser
{
    if ([self user])
    {
        __weak NLProfileViewController *weakSelf = self;
        [[weakSelf profileCollection] reloadData];
        currentProfilePage = 1;
        [NLYum loadYumsFromUserId:[[weakSelf user] userID] page:currentProfilePage withcompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages)
         {
             if (!error)
             {
                 currentProfilePage = currentPage;
                 totalProfilePage = totalPages;
                 [weakSelf setDishes:yums];
             }
             [[weakSelf profileCollection] reloadData];
             [[[weakSelf footer] spinner] stopAnimating];
         }];
    }
}

- (void)updateUserHeader:(NSNotification *)notification
{
    [self setUser:[NLSession currentUser]];
    __weak NLProfileViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[weakSelf profileCollection] reloadData];
        [[[weakSelf footer] spinner] stopAnimating];
    });
}
#pragma mark - Load More Dishes
- (void)reloadUserData:(NSNotification *)notification
{
    __weak NLProfileViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf reloadData:nil];
        [[weakSelf refreshControl] beginRefreshing];
        
    });
}

- (void)loadMoreDishes:(NLProfileFooter *)footer
{
    if (![self isLoading])
    {
        [self setLoading:YES];
        [[footer spinner] startAnimating];
        
        __weak NLProfileViewController *weakSelf = self;
        currentProfilePage++;
        [NLYum loadYumsFromUserId:[[self user] userID] page:currentProfilePage withcompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages)
         {
             [[footer spinner] stopAnimating];
             if ([yums count])
             {
                 currentProfilePage = currentPage;
                 totalProfilePage = totalPages;
                 NSMutableArray *indexPaths = [NSMutableArray array];
                 NSInteger lastIndex = [[weakSelf dishes] count];
                 
                 for (NSInteger index = 0; index < [yums count]; index++)
                 {
                     NSIndexPath *indexPath = [NSIndexPath indexPathForItem:lastIndex+index inSection:0];
                     [indexPaths addObject:indexPath];
                 }
                 
                 [[weakSelf profileCollection] performBatchUpdates:^{
                     [[weakSelf dishes] addObjectsFromArray:yums];
                     [[weakSelf profileCollection] insertItemsAtIndexPaths:indexPaths];
                 } completion:^(BOOL finished) {
                     [weakSelf setLoading:NO];
                     [[[weakSelf footer] spinner] stopAnimating];
                 }];
             }else
             {
                 CGFloat total = weakSelf.profileCollection.contentOffset.y+weakSelf.profileCollection.frame.size.height;
                 if (total > weakSelf.profileCollection.contentSize.height )
                 {
                     CGPoint newPoint = CGPointMake(0.0f, weakSelf.footer.frame.origin.y-weakSelf.profileCollection.frame.size.height+kTabBarHeight+kDishCollectionViewBottomInset);
                     if (newPoint.y > 0)
                     {
                         [[weakSelf profileCollection] setContentOffset:newPoint animated:YES];
                     }
                 }
                 [weakSelf setLoading:NO];
             }
         }];
    }
    
}

#pragma mark - Calculate Header Height

- (CGFloat)calculateHeaderHeight
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat headerHeight = rootView.frame.size.height*0.413;
    
    CGFloat blackBorder = 0;
    CGFloat arrangements = 0;
    if(rootView.frame.size.height<568){ headerHeight= rootView.frame.size.height*0.4882;}
    if(rootView.frame.size.height==667){ blackBorder = rootView.frame.size.height*0.0075; arrangements=5;}
    if(rootView.frame.size.height==736){ blackBorder = rootView.frame.size.height*0.0125; arrangements=10;}
   
    
    
    //check if the profile includes url , bio or not
    return headerHeight+(([[[self user] website] length] >0)?kSiteButtonHeight-arrangements:9-blackBorder)+(([[[self user] bio] length]>0)?kBioLabelHeight:0);
    
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dishes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoProfileViewCellIdentifier forIndexPath:indexPath];
   /*
    if(indexPath.row==0||indexPath.row==1||indexPath.row==2){
        
        [cell setFrame:CGRectMake(cell.frame.origin.x, self.header.frame.size.height+5, cell.frame.size.width, cell.frame.size.height)];
    }
    else{
        [cell setFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y+5, cell.frame.size.width, cell.frame.size.height)];
    }*/
    
    
    NLYum *yum = [[self dishes] objectAtIndex:[indexPath row]];
    [[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yum thumbImage]] placeholderImage:nil];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (UICollectionElementKindSectionHeader == kind)
    {
        NLProfileHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kProfileHeaderViewIdentifier forIndexPath:indexPath];
        
        if (![self header])
        {
            self.header = headerView;
        }
        
        //setup the header
        if ([self user])
        {
            if ([NLSession isCurrentUser:[[self user] userID]])
            {
                [headerView setActionType:EditButton];
            }else
            {
                if ([[self user] follow])
                {
                    [headerView setActionType:UnfollowButton];
                }else
                {
                    [headerView setActionType:FollowButon];
                }
            }
            if (self.user.verified)
            {
                headerView.verifiedImageView.alpha = 1;
            }
            else
            {
                headerView.verifiedImageView.alpha = 0;
            }
            [[headerView usernameLabel] setText:[[self user] name]];
            
            
            if (self.user.verified)
            {
                CGSize size = [headerView.usernameLabel.text sizeWithAttributes: @{NSFontAttributeName: [UIFont systemFontOfSize:21.0f]}];
                NSLog(@"%@, nSize = %@ %@", NSStringFromCGRect(headerView.usernameLabel.frame), NSStringFromCGSize(size), NSStringFromCGSize([headerView.usernameLabel sizeThatFits:headerView.usernameLabel.frame.size]));
                UIImageView *verified = [[UIImageView alloc] initWithFrame:CGRectMake((size.width / 2) + (headerView.usernameLabel.frame.size.width / 2), 0, 20, 30)];
                [verified setContentMode:UIViewContentModeCenter];
                [headerView.usernameLabel addSubview:verified];
                [verified setImage:[UIImage imageNamed:@"icn-verificado"]];
                if (verified.frame.origin.x > headerView.usernameLabel.frame.size.width)
                {
                    verified.center = CGPointMake(headerView.usernameLabel.frame.size.width + 10, headerView.usernameLabel.frame.size.height / 2);
                }
            }
            
            
            
            [headerView setDelegate: self];
            [headerView setDishes:[[self user] yummmies] followers:[[self user] followers] andFollowing:[[self user] following]];
            [headerView setBio:[[self user] bio] andSite:[[self user] website]];
            
            
            
            if ([self mainUser])
            {
                if ([NLSession sharedCover])
                {
                    
                    [[headerView coverImage] sd_setImageWithURL:[NSURL URLWithString:[[self user] cover]] placeholderImage:[NLSession sharedCover] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        [NLSession setSharedCover:nil];
                        
                    }];
                }else
                {
                    
                    
                    [[headerView coverImage] sd_setImageWithURL:[NSURL URLWithString:[[self user] cover]] placeholderImage:[UIImage imageNamed:@"pic_coverdefault"] options:SDWebImageRefreshCached];
                }
                
                if ([NLSession sharedAvatar])
                {
                    [[headerView profileImage] sd_setImageWithURL:[NSURL URLWithString:[[self user] avatar]] placeholderImage:[NLSession sharedAvatar] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                        
                        [NLSession setSharedAvatar:nil];
                        
                    }];
                    
                }else
                {
                    [[headerView profileImage] sd_setImageWithURL:[NSURL URLWithString:[[self user] avatar]] placeholderImage:[UIImage imageNamed:@"pic_userdefault"] options:SDWebImageRefreshCached];
                    
                }
                
                
            }else
            {
                [[headerView coverImage] sd_setImageWithURL:[NSURL URLWithString:[[self user] cover]] placeholderImage:[UIImage imageNamed:@"pic_coverdefault"]];
                [[headerView profileImage] sd_setImageWithURL:[NSURL URLWithString:[[self user] avatar]] placeholderImage:[UIImage imageNamed:@"pic_userdefault"]];
                
            }
        }
        //[headerView layoutIfNeeded];
        return headerView;
    }
    if (UICollectionElementKindSectionFooter == kind)
    {
        NLProfileFooter *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kProfileFooterViewIdentifier forIndexPath:indexPath];
        self.footer = footer;
        return footer;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    //Calculate the height
    //we are supposed to know if the
    return CGSizeMake(320.0f, [self calculateHeaderHeight]);
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yum = [[self dishes] objectAtIndex:[indexPath row]];
    
    NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
    [detail setHidesBottomBarWhenPushed:YES];
    [detail setDelegate:self];
    [[self navigationController] pushViewController:detail animated:YES];
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell highlight];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell unHighlight];
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width - 5) / 3, (self.view.frame.size.width - 5) / 3);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section==0){
        return UIEdgeInsetsMake(5, 1, 5, 1);
    }
    return UIEdgeInsetsMake(1, 1, 5, 1);
}
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dishes.count > 30 && indexPath.row > (self.dishes.count - 12))
    {
        [self loadMoreDishes:self.footer];
    }
}
#pragma mark - Load with user id

- (void)loadUserWithId
{
    __weak NLProfileViewController *weakSelf = self;
    
    [self showLoadingSpinner];
    [NLUser getUser:self.userIdToLoad withCompletionBlock:^(NSError *error, NLUser *user) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf hideLoadingSpinner];
        });
        if (!error)
        {
            [weakSelf setUser:user];
            [weakSelf updateUser];
        }else
        {
            [weakSelf showErrorMessage:NSLocalizedString(@"error_retrieving_user", nil) withDuration:2.0f];
            [[weakSelf view] setUserInteractionEnabled:NO];
            [[[weakSelf footer] spinner] stopAnimating];
        }
    }];
    
}

#pragma mark - Load user with username

- (void)loadUserWithUsername:(NSString *)aUsername
{
    __weak NLProfileViewController *weakSelf = self;
    
    [self showLoadingSpinner];
    [NLUser searchUserWithString:aUsername afterUserId:0 withCompletionBlock:^(NSError *error, NSArray *users)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             [weakSelf hideLoadingSpinner];
         });
         if (!error)
         {
             NLUser *iUser;
             for (NLUser *user in users)
             {
                 if ([[user.username lowercaseString] isEqualToString:aUsername])
                 {
                     iUser = user;
                 }
             }
             if (iUser)
             {
                 [weakSelf setUser:iUser];
                 [weakSelf updateUser];
             }
             else
             {
                 [weakSelf showErrorMessage:NSLocalizedString(@"error_no_user", nil) withDuration:2.0f];
                 [[weakSelf view] setUserInteractionEnabled:NO];
                 [[[weakSelf footer] spinner] stopAnimating];
                 [self.navigationController popViewControllerAnimated:true];
             }
         }else
         {
             [weakSelf showErrorMessage:NSLocalizedString(@"error_retrieving_user", nil) withDuration:2.0f];
             [[weakSelf view] setUserInteractionEnabled:NO];
             [[[weakSelf footer] spinner] stopAnimating];
         }
         
     }];
}

#pragma mark - Reload Data

- (void)reloadData:(id)sender
{
    [[self profileCollection] sendSubviewToBack:[self refreshControl]];
    //reload the user and reload the data
    __weak NLProfileViewController *weakSelf = self;
    
    [NLUser getUser:[[self user] userID] withCompletionBlock:^(NSError *error, NLUser *user) {
        if (!error)
        {
            if ([NLSession isCurrentUser:[user userID]])
            {
                [NLSession updateUser:user];
            }
            [weakSelf setUser:user];
            [[weakSelf refreshControl] endRefreshing];
            
            if (![weakSelf mainUser])
            {
                [weakSelf updateUser];
            }
        }else
        {
            [weakSelf showErrorMessage:@"There was an error, please try again" withDuration:3.0f];
            [[weakSelf refreshControl] endRefreshing];
        }
    }];
}


#pragma mark - NLProfileHeaderViewDelegate

- (void)editProfile
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowSettingsNotification object:nil];
}

- (void)followUser
{
    __weak NLProfileViewController *weakSelf = self;
    [self.user setFollowers:(self.user.followers+1)];
    [[self user] setFollow:YES];
    [[self header] setDishes:self.user.yummmies followers:self.user.followers andFollowing:self.user.following];
    [[self header] actionButton].userInteractionEnabled = false;
    [NLUser followUser:[[self user] userID] withCompletionBlock:^(NSError *error) {
        if (error)
        {
            [[weakSelf header] setActionType:FollowButon];
            [[weakSelf user] setFollowers:([[weakSelf user] followers]-1)];
            [[weakSelf header] setDishes:weakSelf.user.yummmies followers:weakSelf.user.followers andFollowing:weakSelf.user.following];
            [[weakSelf user] setFollow:NO];
        }else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNLReloadDishesNotification object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNLUnfollowUserNotification object:self userInfo:@{@"user":[self user]}];
        }
        [[weakSelf header] actionButton].userInteractionEnabled = true;
        
    }];
}

- (void)unfollowUser
{
    __weak NLProfileViewController *weakSelf = self;
    [[self user] setFollowers:([[self user] followers]-1)];
    [[self header] setDishes:self.user.yummmies followers:self.user.followers andFollowing:self.user.following];
    [[self user] setFollow:NO];
    [[self header] actionButton].userInteractionEnabled = false;
    
    [NLUser unfollowUser:[[self user] userID] withCompletionBlock:^(NSError *error) {
        
        if (error)
        {
            [[weakSelf user] setFollowers:([[weakSelf user] followers]+1)];
            [[weakSelf header] setDishes:weakSelf.user.yummmies followers:weakSelf.user.followers andFollowing:weakSelf.user.following];
            [[weakSelf header] setActionType:UnfollowButton];
            [[weakSelf user] setFollow:YES];
        }else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNLReloadDishesNotification object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNLFollowUserNotification object:self userInfo:@{@"user":[self user]}];
        }
        [[weakSelf header] actionButton].userInteractionEnabled = true;
        
    }];
}

- (void)openFollowers
{
    NLUserListViewController *usersVC = [[NLUserListViewController alloc] initWithUserListType:NLUserListFollowers andUserId:[[self user] userID]];
    [usersVC setHidesBottomBarWhenPushed:YES];
    [[self navigationController] pushViewController:usersVC animated:YES];
}

- (void)openFollowing
{
    NLUserListViewController *usersVC = [[NLUserListViewController alloc] initWithUserListType:NLUserListFollowing andUserId:[[self user] userID]];
    [usersVC setHidesBottomBarWhenPushed:YES];
    [[self navigationController] pushViewController:usersVC animated:YES];
}

- (void)cancelRequest
{
    
}

- (void)sendRequest
{
    
}

- (void)openURL
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"open_in", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"no", nil) otherButtonTitles:NSLocalizedString(@"yes", nil), nil] show];
}

- (void)userPhotoWasTouchedWithReference:(UIImageView *)referenceView
{
    
    TGRImageViewController *imageVC = [[TGRImageViewController alloc] initWithImage:referenceView.image andBigImageURL:self.user.bigAvatar];
    self.photoAnimationController = [[TGRImageZoomAnimationController alloc] initWithReferenceImageView:referenceView roundAnimation:YES];
    imageVC.transitioningDelegate = self;
    [self presentViewController:imageVC animated:YES completion:nil];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            NSString *url = [[self user] website];
            if ([url validateURL])
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            }else
            {
                if ([url rangeOfString:@"http://"].location == NSNotFound)
                {
                    NSString *newURL = [NSString stringWithFormat:@"http://%@",url];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newURL]];
                }
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Start Loading Helpers

- (void)loadIfNeeded:(UIScrollView *)scrollView
{
    [self loadMoreDishes:self.footer];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //[self loadIfNeeded:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    
    //[self loadIfNeeded:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //[self loadIfNeeded:scrollView];
}


#pragma mark - KVO Method

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"user"])
    {
        [self updateUser];
    }
}

#pragma mark - NLPhotoDetailViewDelegate

- (void)yumDeleted:(NLYum *)deletedYum
{
    NSUInteger deletedIndex = [self.dishes indexOfObject:deletedYum];
    
    NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
    [[self profileCollection] performBatchUpdates:^{
        [[self dishes] removeObjectAtIndex:deletedIndex];
        [[self profileCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
    } completion:^(BOOL finished)
     {
         
     }];
}

#pragma mark - UIViewControllerTransitionDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    if ([presented isKindOfClass:[TGRImageViewController class]])
    {
        return self.photoAnimationController;
    }else
    {
        return nil;
    }
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    if ([dismissed isKindOfClass:[TGRImageViewController class]])
    {
        return self.photoAnimationController;
    }else
    {
        return nil;
    }
}

@end
