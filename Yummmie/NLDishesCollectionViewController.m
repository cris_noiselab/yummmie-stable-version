//
//  NLDishesViewController.m
//  Yummmie
//
//  Created by bot on 7/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import "NLDishesCollectionViewController.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLPhotoDetailViewController.h"
#import "NLDishFooter.h"
#import "NLConstants.h"
#import "NLUser.h"
#import "NLYum.h"

static NSString * const kPhotoDishesViewCellIdentifier = @"kPhotoDishesViewCellIdentifier";
static NSString * const kDishFooterIdentifier = @"NLDishesFooterIdentifier";

@interface NLDishesCollectionViewController ()<NLPhotoDetailViewDelegate>

@property (nonatomic, getter = isLoading) BOOL loading;
@property (copy, nonatomic) NSArray *dishes;
@property (strong, nonatomic) NSMutableArray *yums;
@property (weak, nonatomic) NLDishFooter *footer;

- (void)loadMorePhotos:(NLDishFooter *)footer;
- (void)loadIfNeeded:(UIScrollView *)scrollView;

@end

@implementation NLDishesCollectionViewController
{
	
}

- (id)initWithDishes:(NSArray *)dishes
{
	if (self = [super initWithNibName:NSStringFromClass([NLDishesCollectionViewController class]) bundle:nil])
	{
		self.edgesForExtendedLayout = UIRectEdgeBottom;
		self.dishes = dishes;
	}
	
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	
	[[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kPhotoDishesViewCellIdentifier];
	[[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLDishFooter class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kDishFooterIdentifier];
	[[self dishesCollection] setDelegate:self];
	[[self dishesCollection] setDataSource:self];
	
	[self loadMorePhotos:[self footer]];
}

- (void)viewWillAppear:(BOOL)animated
{
	self.title = NSLocalizedString(@"favs", nil);
	[super viewWillAppear:animated];
	[[self navigationController] setNavigationBarHidden:NO];
	CGRect frame = self.navigationController.navigationBar.frame;
	frame.origin.y = 20.0f;
	[self.navigationController.navigationBar setFrame:frame];
	self.navigationItem.titleView.alpha = 1.0;
	
	//Google Analytics
	NSString *name = [NSString stringWithFormat:@"Favs Collection View"];
	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:name];
	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Load More Dishes

- (void)loadMorePhotos:(NLDishFooter *)footer
{
	__weak NLDishesCollectionViewController *weakSelf = self;
	
	if (![self isLoading])
	{
		[self setLoading:YES];
		[[footer spinner] startAnimating];
		[NLUser getFavsBeforeId:0 withCompletionBlock:^(NSError *error, NSArray *yums)
		{
			if (!error)
			{
				if ([yums count] > 0)
				{
					
					if (![weakSelf yums])
					{
						[weakSelf setYums:[NSMutableArray array]];
					}
					
					NSMutableArray *indexPaths = [NSMutableArray array];
					NSInteger lastIndex = [[weakSelf yums] count];
					
					for (NSInteger index = 0; index < [yums count]; index++)
					{
						NSIndexPath *indexPath = [NSIndexPath indexPathForItem:lastIndex+index inSection:0];
						[indexPaths addObject:indexPath];
					}
					
					[[[weakSelf footer] spinner] stopAnimating];
					[[weakSelf dishesCollection] performBatchUpdates:^{
						[[weakSelf yums] addObjectsFromArray:yums];
						[[weakSelf dishesCollection] insertItemsAtIndexPaths:indexPaths];
					} completion:^(BOOL finished) {
						[weakSelf setLoading:NO];
					}];
				}
				else
				{
					CGFloat total = weakSelf.dishesCollection.contentOffset.y+weakSelf.dishesCollection.frame.size.height;
					if (total > weakSelf.dishesCollection.contentSize.height)
					{
						CGPoint newPoint = CGPointMake(0.0f, footer.frame.origin.y-weakSelf.dishesCollection.frame.size.height+kTabBarHeight+kDishCollectionViewBottomInset);
						[[weakSelf dishesCollection] setContentOffset:newPoint animated:YES];
					}
					[weakSelf setLoading:NO];
					[[[weakSelf footer] spinner] stopAnimating];
				}
			}else
			{
				[weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:2.0f];
				[weakSelf setLoading:NO];
				[[[weakSelf footer] spinner] stopAnimating];
			}
		}];
	}
}
#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [[self yums] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
	NLPhotoThumbCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoDishesViewCellIdentifier forIndexPath:indexPath];
	[[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yum thumbImage]] placeholderImage:nil];
	return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	if (UICollectionElementKindSectionFooter == kind)
	{
		NLDishFooter *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kDishFooterIdentifier forIndexPath:indexPath];
		self.footer = footer;
		return footer;
	}
	return nil;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
	NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
	[detail setDelegate:self];
	[[self navigationController] pushViewController:detail animated:YES];
	
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
	NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
	[cell highlight];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
	NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
	[cell unHighlight];
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake((self.view.frame.size.width - 5) / 3, (self.view.frame.size.width - 5) / 3);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	return UIEdgeInsetsMake(1, 1, 5, 1);
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
	return 1;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return 1;
}
#pragma mark - Start Loading Helpers

- (void)loadIfNeeded:(UIScrollView *)scrollView
{
	CGFloat contentSizeHeight = scrollView.contentSize.height-200;
	CGFloat currentPosition = scrollView.contentOffset.y+scrollView.frame.size.height;
	
	if (currentPosition > contentSizeHeight)
	{
		[self loadMorePhotos:self.footer];
	}
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	//[self loadIfNeeded:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
	
	//[self loadIfNeeded:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	//[self loadIfNeeded:scrollView];
}

#pragma mark - NLPhotoDetailViewController

- (void)yumDeleted:(NLYum *)deletedYum
{
	NSUInteger deletedIndex = [self.yums indexOfObject:deletedYum];
	
	NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
	[[self dishesCollection] performBatchUpdates:^{
		[[self yums] removeObjectAtIndex:deletedIndex];
		[[self dishesCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
	} completion:^(BOOL finished)
	 {
		 
	 }];
}

@end
