//
//  NLHashtagViewController.m
//  Yummmie
//
//  Created by bot on 6/23/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLHashtagViewController.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLHashtagFooter.h"
#import "NLPhotoDetailViewController.h"
#import "NLConstants.h"
#import "NLTag.h"
#import "NLYum.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "NSURL+AssetURL.h"

static NSString * const kHashtagCollectionViewCellIdentifier = @"NLPhotoThumbCollectionViewCell";
static NSString * const kHashtagFooterIdentifier = @"NLHashtagFooter";


@interface NLHashtagViewController ()<NLPhotoDetailViewDelegate>

@property (nonatomic) CGFloat previousScrollViewYOffset;
@property (nonatomic, copy) NSString *hashtag;
@property (nonatomic, getter = isLoading) BOOL loading;
@property (strong, nonatomic) NLTag *tag;
@property (nonatomic) NSInteger numberOfYums;

@property (weak, nonatomic) IBOutlet UICollectionView *hashtagCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *yummmiesLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *hashtagLabel;
@property (weak, nonatomic) NLHashtagFooter *footer;
@property (strong, nonatomic) NSMutableArray *yums;

- (void)hideContent;
- (void)showContent;
- (void)loadMorePhotos:(NLHashtagFooter *)footer;
- (void)loadIfNeeded:(UIScrollView *)scrollView;

@end

@implementation NLHashtagViewController
{
	NSInteger currentHashtagDishesPage;
	NSInteger totalHashtagDishesPages;
	NSInteger countDishes;
}

- (id)initWithHashtag:(NSString *)hashtag
{
    if (self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil])
    {
        _hashtag = hashtag;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.title = [NSString stringWithFormat:@"#%@",_hashtag];
    }
    return self;
}

- (id)initWithTag:(NLTag *)tag andNumberOfYums:(NSInteger)numberOfYums
{
    if (self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil])
    {
        _tag = tag;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        //self.title = [NSString stringWithFormat:@"#%@",[_tag name]];
        _numberOfYums = numberOfYums;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self hashtagCollectionView] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kHashtagCollectionViewCellIdentifier];
    [[self hashtagCollectionView] registerNib:[UINib nibWithNibName:NSStringFromClass([NLHashtagFooter class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kHashtagFooterIdentifier];
    [[self hashtagCollectionView] setDelegate:self];
    [[self hashtagCollectionView] setDataSource:self];
    [[self yummmiesLabel] setText:[NSString stringWithFormat:@"%ld %@", (long)[self numberOfYums], NSLocalizedString(@"fotos", @"fotos")]];
    [self.hashtagLabel setText:[NSString stringWithFormat:@"#%@",[_tag name]]];
    
    if ([self tag])
    {
        [self loadMorePhotos:[self footer]];
    }else{
        [NLYum loadYumsFromStringTag:self.hashtag];
    }
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = logoView;
	currentHashtagDishesPage = 0;
	totalHashtagDishesPages = 10;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    if (self.navigationController.navigationBar.frame.origin.y < 20)
    {
            CGRect frame = self.navigationController.navigationBar.frame;
            frame.origin.y = 20.0f;
            [self.navigationController.navigationBar setFrame:frame];
            self.navigationItem.titleView.alpha  = 1.0;
    }
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"Hashtag View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[self hashtagCollectionView] setDelegate:nil];
    [[self hashtagCollectionView] setDataSource:nil];
}

#pragma mark - Show/Hide Content

- (void)showContent
{
    [[self headerView] setHidden:NO];
    [[self hashtagCollectionView] setHidden:NO];
}

- (void)hideContent
{
    [[self headerView] setHidden:YES];
    [[self hashtagCollectionView] setHidden:YES];
}

#pragma  mark - Load More Photos

- (void)loadMorePhotos:(NLHashtagFooter *)footer
{
    __weak NLHashtagViewController *weakSelf = self;
    
    [[[self footer] spinner] startAnimating];
    if (![self isLoading])
    {
        [self setLoading:YES];
			currentHashtagDishesPage++;
        [NLYum loadYumsFromTagId:[[self tag] tagId] page:currentHashtagDishesPage withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages, NSInteger count)
			{
				countDishes = count;
            if (!error)
            {
                if ([yums count]>0)
                {
									[[self yummmiesLabel] setText:[NSString stringWithFormat:@"%ld %@", (long)countDishes, NSLocalizedString(@"fotos", @"fotos")]];

                    if (![weakSelf yums])
                    {
                        [weakSelf setYums:[NSMutableArray array]];
                    }
                    
                    NSMutableArray *indexPaths = [NSMutableArray array];
                    NSInteger lastIndex = [[weakSelf yums] count];
                    
                    for (NSInteger index = 0; index < [yums count]; index++)
                    {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:lastIndex+index inSection:0];
                        [indexPaths addObject:indexPath];
                    }
                    [[[weakSelf footer] spinner] stopAnimating];
                    [[weakSelf hashtagCollectionView] performBatchUpdates:^{
                        [[weakSelf yums] addObjectsFromArray:yums];
                        [[weakSelf hashtagCollectionView] insertItemsAtIndexPaths:indexPaths];
                    } completion:^(BOOL finished) {
                        [weakSelf setLoading:NO];
                    }];
									currentHashtagDishesPage = currentPage;
									totalHashtagDishesPages = totalPages;
                }else
                {
                    CGFloat total = weakSelf.hashtagCollectionView.contentOffset.y+weakSelf.hashtagCollectionView.frame.size.height;
                    if (total > weakSelf.hashtagCollectionView.contentSize.height)
                    {
                        CGPoint newPoint = CGPointMake(0.0f, footer.frame.origin.y-weakSelf.hashtagCollectionView.frame.size.height+kTabBarHeight+kDishCollectionViewBottomInset);
                        [[weakSelf hashtagCollectionView] setContentOffset:newPoint animated:YES];
                    }
                    [weakSelf setLoading:NO];
                    [[[weakSelf footer] spinner] stopAnimating];
                }
            }else
            {
                [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:2.0f];
                [weakSelf setLoading:NO];
                [[footer spinner] stopAnimating];
            }
        }];
    }
}

#pragma mark - UICollectionViewDataSource
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kHashtagCollectionViewCellIdentifier forIndexPath:indexPath];
    [[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yum thumbImage]] placeholderImage:nil];
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self yums] count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (UICollectionElementKindSectionFooter == kind)
    {
        NLHashtagFooter *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kHashtagFooterIdentifier forIndexPath:indexPath];
        self.footer = footer;
        return footer;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
		NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];

	//NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYum:yum];
    detail.hidesBottomBarWhenPushed = YES;
    [detail setDelegate:self];
    [[self navigationController] pushViewController:detail animated:YES];
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell highlight];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell unHighlight];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width - 10) / 3, (self.view.frame.size.width - 10) / 3);
}



#pragma mark - Start Loading Helpers

- (void)loadIfNeeded:(UIScrollView *)scrollView
{
    CGFloat contentSizeHeight = scrollView.contentSize.height-200;
    CGFloat currentPosition = scrollView.contentOffset.y+scrollView.frame.size.height;
    
    if (currentPosition > contentSizeHeight)
    {
        [self loadMorePhotos:self.footer];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadIfNeeded:scrollView];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    
    [self loadIfNeeded:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self loadIfNeeded:scrollView];
}

#pragma mark - NLPhotoDetailViewDelegate

- (void)yumDeleted:(NLYum *)deletedYum
{
    NSUInteger deletedIndex = [self.yums indexOfObject:deletedYum];
    
    NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
    [[self hashtagCollectionView] performBatchUpdates:^{
        [[self yums] removeObjectAtIndex:deletedIndex];
        [[self hashtagCollectionView] deleteItemsAtIndexPaths:@[deletedIndexPath]];
    } completion:^(BOOL finished)
     {
         
     }];
}
@end
