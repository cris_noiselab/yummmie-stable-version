//
//  NLSearchByUsernameViewController.m
//  Yummmie
//
//  Created by bot on 1/16/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>

#import "NLSearchByUsernameViewController.h"
#import "NLUserListViewCell.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NLProfileViewController.h"
#import "NLNotifications.h"

static const CGFloat kUserSearchByUsernameCellHeight = 57.0f;
static NSString * const kUserSearchByUsernameListViewCellIdentifier = @"kUserSearchListViewCellIdentifier";

@interface NLSearchByUsernameViewController ()<UITableViewDataSource, UITableViewDelegate,NLUserListViewCellDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *userSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *usersTable;
@property (strong, nonatomic) NSMutableArray *users;
@property (weak, nonatomic) NSURLSessionDataTask *searchUserTask;

- (void)updateFollowStatus:(NSNotification *)notification;

@end

@implementation NLSearchByUsernameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    
    self.userSearchBar.placeholder = NSLocalizedString(@"type_username_text", nil);
    
    [[self usersTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLUserListViewCell class]) bundle:nil] forCellReuseIdentifier:kUserSearchByUsernameListViewCellIdentifier];
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    [footer setBackgroundColor:[UIColor whiteColor]];
    [[self usersTable] setTableFooterView:footer];
    
    self.title = NSLocalizedString(@"find_by_username_title", nil);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollowStatus:) name:kNLUnfollowUserNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollowStatus:) name:kNLFollowUserNotification object:nil];
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"busca por nombre de usuario View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if ([self.usersTable respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.usersTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.usersTable respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.usersTable setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Update Follow Status Method

- (void)updateFollowStatus:(NSNotification *)notification
{
    __weak NLSearchByUsernameViewController *weakSelf = self;
    
    NLUser *unfollowed = [[notification userInfo] objectForKey:@"user"];
    if (notification.object != self)
    {
        for (NLUser *user in self.users)
        {
            if ([unfollowed isEqual:user])
            {
                user.follow = !user.follow;
            }
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[weakSelf usersTable] reloadData];
    });
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self showLoadingSpinner];
    [[self searchUserTask] cancel];
    
    __weak NLSearchByUsernameViewController *weakSelf = self;
    
    self.searchUserTask = [NLUser searchUserWithString:searchText afterUserId:0 withCompletionBlock:^(NSError *error, NSArray *users) {
        
        [weakSelf hideLoadingSpinner];
        weakSelf.users = [NSMutableArray arrayWithArray:users];
            
        dispatch_async(dispatch_get_main_queue(), ^{
                [[weakSelf usersTable] reloadData];
            });
    }];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLUserListViewCell *cell = (NLUserListViewCell *)[tableView dequeueReusableCellWithIdentifier:kUserSearchByUsernameListViewCellIdentifier];
    NLUser *user = [[self users] objectAtIndex:[indexPath row]];
    [cell setName:[user name] andUsername:[NSString stringWithFormat:@"@%@",[user username]]];
    [[cell userPhoto] sd_setImageWithURL:[NSURL URLWithString:[user thumbAvatar]] placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
    
    if (![NLSession isCurrentUser:[user userID]])
    {
        if ([user follow])
        {
            [cell setButtonType:FOLLOWING];
        }else
        {
            [cell setButtonType:NOTFOLLOWING];
        }
        [cell setDelegate:self];
    }else
    {
        [[cell actionButton] setHidden:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UITableVewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kUserSearchByUsernameCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLUser *selectedUser = [[self users] objectAtIndex:[indexPath row]];
    NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:selectedUser isProfileUser:NO];
    [[self navigationController] pushViewController:profileVC animated:YES];
}

#pragma mark - NLUserListViewCellDelegate

- (void)userCell:(NLUserListViewCell *)cell actionWithType:(UserButtonType)type
{
    NSIndexPath *indexPath = [[self usersTable] indexPathForCell:cell];
    NLUser *selectedUser = [[self users] objectAtIndex:[indexPath row]];
    __weak NLSearchByUsernameViewController *weakSelf = self;
    
    if (type == FOLLOWING)
    {
        [cell setButtonType:NOTFOLLOWING];
        [NLUser unfollowUser:[selectedUser userID] withCompletionBlock:^(NSError *error) {
            if (error)
            {
                [weakSelf showErrorMessage:@"There was an error while updating" withDuration:2.0f];
                [cell setButtonType:FOLLOWING];
            }else
            {
                [selectedUser setFollow:NO];
                
                [[weakSelf usersTable] reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLReloadDishesNotification object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLUnfollowUserNotification object:self userInfo:@{@"user":selectedUser}];
            }
        }];
        
    }else if (type == NOTFOLLOWING)
    {
        [cell setButtonType:FOLLOWING];
        [NLUser followUser:[selectedUser userID] withCompletionBlock:^(NSError *error) {
            if (error)
            {
                [weakSelf showErrorMessage:@"There was an error while updating" withDuration:2.0f];
                [cell setButtonType:NOTFOLLOWING];
            }else
            {
                [selectedUser setFollow:YES];
                [[weakSelf usersTable] reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLReloadDishesNotification object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLFollowUserNotification object:self userInfo:@{@"user":selectedUser}];
            }
        }];
    }
}

@end