//
//  NLProfileHeaderView.h
//  Yummmie
//
//  Created by bot on 7/1/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ProfileButtonType)
{
    FollowButon,
    UnfollowButton,
    EditButton,
    PendingButton
};

typedef NS_ENUM(NSInteger, ProfileType)
{
    DefaultProfile,
    PrivateProfile
};
@protocol NLProfileHeaderViewDelegate;


@interface NLProfileHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *versionaLabel;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (strong, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *coverPhoto;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (weak, nonatomic) IBOutlet UIImageView *verifiedImageView;

@property (weak, nonatomic) id<NLProfileHeaderViewDelegate> delegate;
@property (nonatomic) ProfileType type;

- (void)setBio:(NSString *)bioText andSite:(NSString *)URLString;
- (void)setActionType:(ProfileButtonType)buttonType;
- (void)updateToFollowed;
- (void)updateToUnfollowed;
- (void)setDishes:(NSInteger)dishes followers:(NSInteger)followers andFollowing:(NSInteger)following;
@end

@protocol NLProfileHeaderViewDelegate <NSObject>

@required

- (void)editProfile;
- (void)followUser;
- (void)unfollowUser;
//- (void)openDishes;
- (void)openFollowers;
- (void)openFollowing;
- (void)cancelRequest;
- (void)sendRequest;
- (void)openURL;
- (void)userPhotoWasTouchedWithReference:(UIImageView *)referenceView;

@end
