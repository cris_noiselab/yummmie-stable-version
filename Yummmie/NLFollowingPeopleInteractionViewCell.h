//
//  NLFollowingPeopleInteractionViewCell.h
//  Yummmie
//
//  Created by bot on 8/26/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>

@protocol NLFollowingPeopleInteractionViewCellDelegate;

@interface NLFollowingPeopleInteractionViewCell : UITableViewCell<TTTAttributedLabelDelegate>

@property (weak, nonatomic) id<NLFollowingPeopleInteractionViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *userImageButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) NSDictionary *users;


- (void)setOwner:(NSDictionary *)owner andTarget:(NSDictionary *)target;

@end


@protocol NLFollowingPeopleInteractionViewCellDelegate<NSObject>

@required
- (void)openFollowingPeopleUsername:(NSString *)username;
- (void)openFollowingPeopleOwnerWithTag:(NSInteger)tag;
- (void)openUserWithId:(NSInteger)userId;

@end