//
//  NLBrowserViewController.h
//  Yummmie
//
//  Created by bot on 9/2/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLBrowserViewController : NLBaseViewController


- (id)initWithURL:(NSURL *)url;

@end
