//
//  NLFoursquareSearchViewController.m
//  Yummmie
//
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLFoursquareSearchViewController.h"
#import "FSVenue.h"
#import "Foursquare2.h"
#import "FSConverter.h"
#import "NLVenue.h"
#import "NLConstants.h"
#import "UIColor+NLColor.h"
#import "NLSession.h"



typedef NS_ENUM(NSInteger, NLResultMode)
{
    NLResultModeSearching,
    NLResultModeNoResults,
    NLResultModeError,
    NLResultModeResultsFound
};

CG_INLINE UIViewAnimationOptions animationOptionsWithCurve(UIViewAnimationCurve curve)
{
    return (UIViewAnimationOptions)curve << 16;
}

@interface NLFoursquareSearchViewController ()

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *nearVenues;
@property (strong, nonatomic) NSArray *results;
@property (strong, nonatomic) NSArray *searchingData;
@property (strong, nonatomic) NSArray *noResultsData;
@property (strong, nonatomic) NSArray *errorData;
@property (strong, nonatomic) NSArray *searchData;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *location;

@property (nonatomic, weak) NSOperation *lastSearchOperation;

@property (nonatomic) NLResultMode currentMode;
@property (nonatomic, weak) NSURLSessionDataTask *task;


- (void)dismiss:(id)sender;
- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardWillBeHidden:(NSNotification *)notification;
- (void)loadNearestVenues;

@end

@implementation NLFoursquareSearchViewController
{
	NLVenue *lastSelected;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _currentMode = NLResultModeSearching;
        _errorData = @[NSLocalizedString(@"error_loading_venues_string", nil)];
        _searchingData = @[NSLocalizedString(@"loading_venues_string", nil)];
        _noResultsData = @[@"",@""];
        _results = _searchingData;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

//    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.searchBar setDelegate:self];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    
    if ([[UIScreen mainScreen] bounds].size.height<568) {
        [[self tableView] setContentInset:UIEdgeInsetsMake(0, 0, 100, 0)];
    }
    
    if ([NLSession locationForImage])
    {
        self.location = [NLSession locationForImage];
        NSLog(@"Location Photo %@", self.location);
        [self loadNearestVenues];
    }else
    {
        if ([CLLocationManager locationServicesEnabled])
        {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
            {
                [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
                [self.locationManager startUpdatingLocation];                
            }else
            {
                [self.locationManager startUpdatingLocation];
            }
        }else
        {
            [self showErrorMessage:@"Please turn on your location services" withDuration:3.0f];
        }
    }

    
    [[self searchBar] setPlaceholder:NSLocalizedString(@"search_venues_string", nil)];
    [[self searchBar] setTintColor:[UIColor grayColor]];
    [self showLoadingSpinner];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    [self.tableView setTableFooterView:footerView];
    [[self tableView] setSeparatorInset:UIEdgeInsetsZero];
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	
	if ([def objectForKey:@"venue"])
	{
		lastSelected = [[NLVenue alloc] initWithDictionary:[def objectForKey:@"venue"]];
		
	}
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [defaultCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    if ([self.searchBar.text length]>0)
    {
        [self.searchBar becomeFirstResponder];
    }
    [[self tableView] deselectRowAtIndexPath:[[self tableView] indexPathForSelectedRow] animated:YES];
    
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"Foursquare Search View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.locationManager stopUpdatingLocation];
        [self.locationManager setDelegate:nil];
        [self setDelegate:nil];
    }];
}

#pragma mark - Foursquare Methods

- (void)loadNearestVenues
{
    [self.lastSearchOperation cancel];
    [self.task cancel];
//    __block NSArray *foursquareVenues;
//    __block NSArray *yummmieVenues;
    
    __weak NLFoursquareSearchViewController *weakSelf = self;
    
    self.task = [NLVenue checkinVenueListWithLatitude:self.location.coordinate.latitude andLongitude:self.location.coordinate.longitude withCompletionBlock:^(NSError *error, NSArray *venues) {
        if (!error && venues.count > 0)
        {
            weakSelf.currentMode = NLResultModeResultsFound;
            weakSelf.results = venues;
        }else
        {
            weakSelf.currentMode = NLResultModeError;
            weakSelf.results = weakSelf.errorData;
        }
        [weakSelf hideLoadingSpinner];
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    
    NSTimeInterval timeInterval = [[location timestamp] timeIntervalSinceNow];
    if (fabs(timeInterval) < 15)
    {
        //do something
        self.location = [locations objectAtIndex:0];
        [self.locationManager stopUpdatingLocation];
        [self loadNearestVenues];
    }

}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText length] == 0)
    {
        [self setResults:[self nearVenues]];
        [self setCurrentMode:NLResultModeResultsFound];
        [searchBar resignFirstResponder];
        [[self tableView] reloadData];
    }else
    {
        [self setResults:[self noResultsData]];
        [self setCurrentMode:NLResultModeNoResults];
        [[self tableView] reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    if ([self results] != [self nearVenues])
    {
        [searchBar setText:@""];
        [self setResults:[self nearVenues]];
        [self setCurrentMode:NLResultModeResultsFound];
        [[self tableView] reloadData];
    }
    
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NLCustomSearchViewController *customSearchVC = [[NLCustomSearchViewController alloc] initWithSearchText:self.searchBar.text andLocation:self.location];
    [customSearchVC setDelegate:self];
    [self.navigationController pushViewController:customSearchVC animated:YES];
}


#pragma mark UITableDataSourceDelegate
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [NSLocalizedString(@"recent", @"Recents") lowercaseString];
	}
	else
	{
		return [NSLocalizedString(@"nearby_title", @"Near") lowercaseString];
	}
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	if (lastSelected)
	{
		return 2;
	}
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
	{
		return 1;
	}
    return self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		static NSString *resultsFoundCellIdentifier = @"ResultsFoundIdentifier";
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:resultsFoundCellIdentifier];
		
		if (!cell)
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:resultsFoundCellIdentifier];
			[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
			[[cell detailTextLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
			[[cell detailTextLabel] setTextColor:[UIColor nl_colorWith255Red:166 green:166 blue:166 andAlpha:255]];
		}
		cell.imageView.image = nil;
		[[cell textLabel] setText:[lastSelected name]];
		[[cell detailTextLabel] setText:[lastSelected street]];
		return cell;
	}
	else
	{
		if ([self currentMode] == NLResultModeSearching)
		{
			static NSString *searchingCellIdentifier = @"searchingCellIdentifier";
			UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:searchingCellIdentifier];
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchingCellIdentifier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
			}
			cell.imageView.image = nil;
			[[cell textLabel] setText:[[self searchingData] firstObject]];
			
			return cell;
		}else if ([self currentMode] == NLResultModeError)
		{
			static NSString *errorCellIdentIdenfier = @"errorCellIdentIdenfier";
			UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:errorCellIdentIdenfier];
			
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:errorCellIdentIdenfier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
			}
			cell.imageView.image = nil;
			[[cell textLabel] setText:[[self searchingData] lastObject]];
			
			return cell;
		}else if ([self currentMode] == NLResultModeNoResults)
		{
			static NSString *noResultsCellIdentifier = @"noResultsCellIdentifier";
			UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
			
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:noResultsCellIdentifier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
				[[cell detailTextLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Italic" size:13.0f]];
				[[cell detailTextLabel] setTextColor:[UIColor nl_colorWith255Red:166 green:166 blue:166 andAlpha:255]];
			}
			cell.imageView.image = nil;
			
			if (indexPath.row == 0)
			{
				[[cell textLabel] setText:[[self searchBar] text]];
				[[cell detailTextLabel] setText:NSLocalizedString(@"search_word_string", nil)];
				cell.imageView.image = [UIImage imageNamed:@"search_venue"];
			}/*else if (indexPath.row == 1)
				{
				[[cell textLabel] setText:[[self searchBar] text]];
				[[cell detailTextLabel] setText:NSLocalizedString(@"add_venue_string", nil)];
				cell.imageView.image = [UIImage imageNamed:@"add_venue"];
				}*/
			else if (indexPath.row == 1)
			{
				[[cell textLabel] setText:[[self searchBar] text]];
				[[cell detailTextLabel] setText:NSLocalizedString(@"add_private_venue_string", nil)];
				cell.imageView.image = [UIImage imageNamed:@"add_private_venue"];
			}
			
			return cell;
		}else if ([self currentMode] == NLResultModeResultsFound)
		{
			static NSString *resultsFoundCellIdentifier = @"ResultsFoundIdentifier";
			UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:resultsFoundCellIdentifier];
			
			if (!cell)
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:resultsFoundCellIdentifier];
				[[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
				[[cell detailTextLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
				[[cell detailTextLabel] setTextColor:[UIColor nl_colorWith255Red:166 green:166 blue:166 andAlpha:255]];
			}
			cell.imageView.image = nil;
			NLVenue *venue = [[self results] objectAtIndex:[indexPath row]];
			[[cell textLabel] setText:[venue name]];
			[[cell detailTextLabel] setText:[venue street]];
			return cell;
		}
		return nil;

	}

}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		NSLog(@"%@", lastSelected.venueId);
		[self.delegate didFinishSelectingWithVenue:lastSelected];

	}
	else
	{
		__weak NLFoursquareSearchViewController *weakSelf = self;

		if (self.currentMode == NLResultModeResultsFound)
		{
			NLVenue *venue = [[self results] objectAtIndex:[indexPath row]];
			[self showLoadingSpinner];
			
			if (venue.fromFoursquare)
			{
				[NLVenue createVenuewithId:venue.facebook_place_id venueName:venue.name andCompletionBlock:^(NSError *error,NLVenue *venue) {
					if (!error)
					{
						if ([[weakSelf delegate] respondsToSelector:@selector(didFinishSelectingWithVenue:)])
						{
							[[weakSelf delegate] didFinishSelectingWithVenue:venue];
						}
					}else
					{
						[weakSelf hideLoadingSpinner];
						[weakSelf showErrorMessage:NSLocalizedString(@"error_creating_venue", nil) withDuration:2.0f];
						[[weakSelf tableView] deselectRowAtIndexPath:indexPath animated:YES];
					}
				}];
			}else
			{
				if ([[self delegate] respondsToSelector:@selector(didFinishSelectingWithVenue:)])
				{
					[[self delegate] didFinishSelectingWithVenue:venue];
				}
			}
		}else if(self.currentMode == NLResultModeNoResults)
		{
			//if we select the first index, we neeed to create the custom venue
			if (indexPath.row == 0)
			{
				//If we select this one we need to search via foursquare
				NLCustomSearchViewController *customSearchVC = [[NLCustomSearchViewController alloc] initWithSearchText:self.searchBar.text andLocation:self.location];
				[customSearchVC setDelegate:self];
				[self.navigationController pushViewController:customSearchVC animated:YES];
				[self hideLoadingSpinner];
			}/*else if(indexPath.row == 1)
				{
				[self showLoadingSpinner];
				[NLVenue createVenueWithName:self.searchBar.text latitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude andCompletionBlock:^(NSError *error, NLVenue *venue) {
				if (!error)
				{
				if ([[weakSelf delegate] respondsToSelector:@selector(didFinishSelectingWithVenue:)])
				{
				[[weakSelf delegate] didFinishSelectingWithVenue:venue];
				}
				}else
				{
				dispatch_async(dispatch_get_main_queue(), ^{
				[weakSelf hideLoadingSpinner];
				[weakSelf showErrorMessage:NSLocalizedString(@"error_creating_venue", nil) withDuration:3.0f];
				[[weakSelf tableView] deselectRowAtIndexPath:indexPath animated:YES];
				});
				
				}
				}];
				}*/else if (indexPath.row == 1)
				{
					[NLVenue createPrivateVenue:self.searchBar.text latitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude andCompletionBlock:^(NSError *error, NLVenue *venue)
					 {
						 [self hideLoadingSpinner];
						 if (!error)
						 {
							 if ([weakSelf delegate] && [[weakSelf delegate] respondsToSelector:@selector(didFinishSelectingWithVenue:)])
							 {
								 [[weakSelf delegate] didFinishSelectingWithVenue:venue];
							 }
						 }else
						 {
							 dispatch_async(dispatch_get_main_queue(), ^{
								 [weakSelf hideLoadingSpinner];
								 [weakSelf showErrorMessage:NSLocalizedString(@"error_creating_venue", nil) withDuration:3.0f];
								 [[weakSelf tableView] deselectRowAtIndexPath:indexPath animated:YES];
							 });
						 }
					 }];
				}
		}
	}
	
	

}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self currentMode] == NLResultModeError || [self currentMode] == NLResultModeSearching)
    {
        return nil;
    }
    return indexPath;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
	
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect searchBarRect = self.searchBar.frame;
    CGRect tableRect = CGRectZero;
    tableRect.size = CGSizeMake(CGRectGetWidth([[self view] bounds]), CGRectGetHeight([[self view] bounds])-CGRectGetHeight([[self inputView] bounds])-kKeyboardHeight-searchBarRect.size.height);
    tableRect.origin.y = searchBarRect.size.height;
    [UIView animateWithDuration:animationDuration delay:0.0f options:animationOptionsWithCurve(animationCurve) animations:^{
        [[self tableView] setFrame:tableRect];
    } completion:^(BOOL finished) {
        
    }];
}
- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect searchBarRect = self.searchBar.frame;
    CGRect tableRect = CGRectZero;
    tableRect.size = CGSizeMake(CGRectGetWidth([[self view] bounds]), CGRectGetHeight([[self view] bounds])-searchBarRect.size.height);
    tableRect.origin.y = searchBarRect.size.height;
    CGRect newInputViewFrame = CGRectOffset([[self inputView] bounds], 0.0f, tableRect.size.height);
    [UIView animateWithDuration:animationDuration delay:0.0f options:animationOptionsWithCurve(animationCurve) animations:^{
        [[self inputView] setFrame:newInputViewFrame];
        [[self tableView] setFrame:tableRect];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - Custom Search Delegate

- (void)didSelectVenue:(NLVenue *)foursquareVenue andIndexPath:(NSIndexPath *)indexPath
{
    __weak NLFoursquareSearchViewController *weakSelf = self;
	NSLog(@"%@", foursquareVenue);
    if (foursquareVenue)
    {
        if ([[weakSelf delegate] respondsToSelector:@selector(didFinishSelectingWithVenue:)])
        {
            [[weakSelf delegate] didFinishSelectingWithVenue:foursquareVenue];
        }
    }
}

@end
