//
//  NLSession.h
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "NLAuthentication.h"

@class NLUser,NLVenue,NLYum;

@interface NLSession : NSObject

@property (nonatomic, copy) NSString *errorString;
@property (nonatomic) BOOL needsUpdate;
@property (nonatomic) BOOL showActivity;

+ (instancetype)sharedSession;
+ (NLUser *)currentUser;
+ (void)updateUser:(NLUser *)newUser;
+ (BOOL)isCurrentUser:(NSInteger)userID;

+ (id)accessToken;
+ (BOOL)deleteToken;
+ (void)setSharedPhoto:(UIImage *)newPhoto;
+ (UIImage *)getSharedPhoto;
+ (NLVenue *)getSharedVenue;
+ (void)setSharedVenue:(NLVenue *)newVenue;
+ (NSInteger)getUserID;
+ (void)setYum:(NLYum *)aYum;
+ (NLYum *)yum;
+ (BOOL)isCurrentUserWithString:(NSString *)username;
+ (void)createUser:(NLUser *)newUser;
+ (void)setSharedAvatar:(UIImage *)image;
+ (void)setSharedCover:(UIImage *)image;
+ (UIImage *)sharedCover;
+ (UIImage *)sharedAvatar;
+ (CLLocation *)locationForImage;
+ (void)setLocation:(CGFloat)latitude longitude:(CGFloat)longitude;
+ (void)clearLocation;

+ (BOOL)containsAuthentication:(NLAuthenticationType)type;
+ (BOOL)endSession;

@end