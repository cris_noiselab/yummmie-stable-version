//
//  NLPhotoImageViewController.m
//  Yummmie
//
//  Created by bot on 7/8/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLPhotoImageViewController.h"

@interface NLPhotoImageViewController ()<UIScrollViewDelegate>

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation NLPhotoImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.scrollView = [[UIScrollView alloc] init];
    [self.scrollView setMinimumZoomScale:1.0f];
    [self.scrollView setMaximumZoomScale:2.0f];
    [self.scrollView setDelegate:self];
    [[self view] addSubview:[self scrollView]];
    
    UIImage *imageToZoom = [UIImage imageNamed:@"tl_foto_muestra"];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    self.imageView.image = imageToZoom;
    
    [[self scrollView] addSubview:[self imageView]];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [[self scrollView] setFrame:self.view.bounds];
    self.imageView.center = self.scrollView.center;
}
#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}
@end