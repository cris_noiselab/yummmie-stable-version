
//
//  NLCommentsViewController.m
//  Yummmie
//
//  Created by bot on 6/25/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/UIButton+WebCache.h>
#import "NLCommentsViewController.h"
#import "NLConstants.h"
#import "NLNotifications.h"
#import "NLHashtagViewController.h"
#import "NLProfileViewController.h"
#import "NLComment.h"
#import "NLUser.h"
#import "NSURL+AssetURL.h"
#import "NLYum.h"
#import "NLSession.h"
#import "NLConstants.h"
#import "NLTag.h"
#import "UIColor+NLColor.h"
#import "NLSuggestedUsersBar.h"


static NSString * const kCommentViewCellIdentifier = @"kCommentViewCellIdentifier";
static const CGFloat kCommentTextWidth = 240.0f;

@interface NLCommentsViewController ()<UIAlertViewDelegate,NLSuggestedUsersBarDelegate>

@property (strong, nonatomic) NLYum *yum;
@property (weak, nonatomic) IBOutlet UITableView *commentsTable;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *InputViewVerticalSpaceConstraint;

@property (strong, nonatomic) NSMutableArray *comments;
@property (strong, nonatomic) NSMutableDictionary *usersDic;

@property (strong, nonatomic) NSMutableArray *heights;
@property (strong, nonatomic) NSIndexPath *indexPathToDelete;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) NSInteger yumId;
@property (nonatomic) BOOL addCloseButton;
@property (strong, nonatomic) IBOutlet NLSuggestedUsersBar *suggestedBar;
@property (nonatomic) NLMessageMode currentMessageMode;
@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) NSArray *filteredUsers;

@property (copy, nonatomic) NSURLSessionDataTask *queryUsersTask;


@property (strong, nonatomic) NSMutableArray *hashtags;
@property (strong, nonatomic) NSArray *filteredHashtags;


- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardWillBeHidden:(NSNotification *)notification;
- (void)keyboardDidHide:(NSNotification *)notification;

- (void)hideKeyboard;

- (IBAction)submitComment:(id)sender;

- (void)loadPreviousComments:(id)control;

- (void)dismissViewController;

- (void)loadComments;

- (void)startMentionMode;
- (void)endMentionMode;


@end

@implementation NLCommentsViewController

- (id)initWithYum:(NLYum *)yum
{
	if (self = [super initWithNibName:NSStringFromClass([NLCommentsViewController class]) bundle:nil])
	{
		_yum = yum;
		_yumId = yum.yumId;

		
		/*[NLYum getYum:yum.yumId withCompletionBlock:^(NSError *error, NLYum *yumF)
			{
				self.yum = yumF;
				self.comments = [yumF comments];
				[self.commentsTable reloadData];
				
			}];*/
		//_comments = [_yum comments];
		_heights = [NSMutableArray array];
		self.edgesForExtendedLayout = UIRectEdgeNone;
		self.title = NSLocalizedString(@"comments_view_title", nil);
		_currentMessageMode = NLMessageModeNone;
	}
	
	return self;
}

- (id)initWithYumId:(NSInteger)yumId andDismissButton:(BOOL)flag
{
    if (self = [super initWithNibName:NSStringFromClass([NLCommentsViewController class]) bundle:nil])
    {
        _yumId = yumId;
        _comments = [_yum comments];
        _heights = [NSMutableArray array];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.title = NSLocalizedString(@"comments_view_title", nil);
        _addCloseButton = flag;
        _currentMessageMode = NLMessageModeNone;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self showLoadingSpinner];
    [[self suggestedBar] setDelegate:self];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setTintColor:[UIColor nl_applicationRedColor]];
    [self.refreshControl addTarget:self action:@selector(loadPreviousComments:) forControlEvents:UIControlEventValueChanged];
    [self.commentsTable addSubview:self.refreshControl];
    //setup the comments heights
    
    if (self.comments)
    {
        __weak NLCommentsViewController *weakSelf = self;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSMutableSet *usersSet = [NSMutableSet set];
            
            for (NLComment *comment in [weakSelf comments])
            {
                CGSize textSize = [weakSelf sizeForText:[comment content]];
                [[weakSelf heights] addObject:[NSValue valueWithCGSize:textSize]];
                NLUser *owner = [comment user];
                if (![owner isEqual:[NLSession currentUser]]) {
                    [usersSet addObject:[comment user]];
                }
            }
            
//            NSLog(@"Los usuarios : %@",usersSet);
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:YES];
            NSArray *commentOwners = [usersSet sortedArrayUsingDescriptors:@[descriptor]];
            
					[NLUser getFollowings:[[NLSession currentUser] userID] page:1 withCompletionBlock:^(NSError *error, NSArray *followings, NSInteger currentPage, NSInteger totalPages)
            {
                //        [weakSelf.suggestedBar updateDatasource:followings];
                NSArray *all = [commentOwners arrayByAddingObjectsFromArray:followings];
                [weakSelf setUsers:all];
                //[[weakSelf suggestedBar] updateDatasource:[weakSelf users]];
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf hideLoadingSpinner];
                [[weakSelf commentsTable] reloadData];
                NSLog(@"%@", [weakSelf comments]);
                if ([[weakSelf comments] count] > 0)
                {
                    [[weakSelf commentsTable] scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:weakSelf.comments.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                }
            });
        });
        
        
    }else
    {
        self.comments = [NSMutableArray array];
        self.heights = [NSMutableArray array];

        [self loadComments];
    }
    
    
    //setup the views
    [[self commentsTable] setDelegate:self];
    [[self commentsTable] setDataSource:self];
    [[self messageTextView] setDelegate:self];
    [[self commentsTable] registerNib:[UINib nibWithNibName:NSStringFromClass([NLCommentViewCell class]) bundle:nil] forCellReuseIdentifier:kCommentViewCellIdentifier];
    [self.commentsTable setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [[self commentsTable] setSeparatorInset:UIEdgeInsetsZero];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.commentsTable setTableFooterView:footerView];
    
    if ([self addCloseButton])
    {
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"close_title", nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismissViewController)];
        self.navigationItem.leftBarButtonItem = closeButton;
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGRect frame = self.navigationController.navigationBar.frame;
    frame.origin.y = 20.0f;
    [self.navigationController.navigationBar setFrame:frame];
    self.navigationItem.titleView.alpha  = 1.0;
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [defaultCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];

        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"Comentarios View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self messageTextView] becomeFirstResponder];
    
    NSDictionary *analyticsDict;
    if ([self yum]) {
        analyticsDict = @{
                          kNLAnalyticsSectionNameKey:kNLAnalyticsCommentsSection,
                          kNLAnalyticsYumId:[NSString stringWithFormat:@"%ld",(long)[[self yum] yumId]],
                          kNLAnalyticsYumOwnerKey:([[[self yum] user] username])?[[[self yum] user] username]:@"null",
                          kNLAnalyticsUsernameKey:[[NLSession currentUser] username]
                          };
        
    }else
    {
        analyticsDict = @{
                          kNLAnalyticsSectionNameKey:kNLAnalyticsCommentsSection,
                          kNLAnalyticsYumId:[NSString stringWithFormat:@"%ld",(long)[self yumId]],
                          kNLAnalyticsUsernameKey:[[NLSession currentUser] username]
                          };
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideKeyboard];
    [self hideLoadingSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.commentsTable respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.commentsTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.commentsTable respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.commentsTable setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma  mark - Start Mention Mode 

- (void)startMentionMode
{
    self.currentMessageMode = NLMessageModeMention;
    [[self suggestedBar] setHidden:NO];
    [self suggestedBar].hashtagging = false;

    [[self commentsTable] setContentInset:UIEdgeInsetsMake(0, 0, self.suggestedBar.bounds.size.height, 0)];
}
- (void)startHashtagMode
{
    self.currentMessageMode = NLMessageModeHashtag;
    [[self suggestedBar] setHidden:NO];
    [self suggestedBar].hashtagging = YES;
    [[self commentsTable] setContentInset:UIEdgeInsetsMake(0, 0, self.suggestedBar.bounds.size.height, 0)];
}
#pragma  mark - End Mention Mode

- (void)endMentionMode
{
    self.currentMessageMode = NLMessageModeNone;
    [[self suggestedBar] setHidden:YES];
    [[self commentsTable] setContentInset:UIEdgeInsetsZero];
}

#pragma mark - Gesture Recognizers

- (void)hideKeyboard
{
	[self.messageTextView resignFirstResponder];
}

#pragma mark - Submit Action

- (IBAction)submitComment:(id)sender
{
    if (self.messageTextView.text.length != 0 && [[self.messageTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] != 0)
    {
        
        
        NSArray *words = [[self.messageTextView text] componentsSeparatedByString:@" "];
        //    NSLog(@"Palabras %@",words);
        
        NSString *lastWord = [words lastObject];
        /*if ([lastWord isEqualToString:@""])
        {
            lastWord = self.messageTextView.text;
        }*/
        if ([lastWord containsString:@"#"])
        {
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            self.hashtags = [[def objectForKey:@"hashtags"] mutableCopy];
            if (self.hashtags == nil)
            {
                self.hashtags = [[NSMutableArray array] mutableCopy];
            }
            if (![self.hashtags containsObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]])
            {
                [self.hashtags addObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]];
                [def setObject:self.hashtags forKey:@"hashtags"];
                [def synchronize];
            }
            [self endMentionMode];
            
        }

        
        
        [self.submitButton setEnabled:NO];
        [self showLoadingSpinner];
        __weak NLCommentsViewController *weakSelf = self;
        NSString *commentValue = self.messageTextView.text;
			NSInteger yumId = (self.yum)?self.yum.yumId:self.yumId;
			if (self.yum.repost)
			{
				yumId = [[self.yum.repost objectForKey:@"original_yum_id"] integerValue];
			}
        [NLYum postComment:self.messageTextView.text yumId:yumId withCompletionBlock:^(NSError *error, NLComment *comment) {
            if (error || !comment)
            {
                [weakSelf showErrorMessage:NSLocalizedString(@"error_posting_comment", nil) withDuration:3.0f];
                [weakSelf hideLoadingSpinner];
                [[weakSelf submitButton] setEnabled:YES];
                [[weakSelf messageTextView] setText:commentValue];
            }else
            {

                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                    CGSize newSize = [weakSelf sizeForText:[comment content]];
                    [[weakSelf heights] addObject:[NSValue valueWithCGSize:newSize]];
                    
                    if (![[weakSelf yum] comments])
                    {
                        [[weakSelf yum] setComments:[NSMutableArray array]];

                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
											//NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:(weakSelf.yum)?[[[weakSelf yum] comments] count]:weakSelf.comments.count inSection:0];
											NSLog(@"%@", [weakSelf comments]);
											NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:[[weakSelf comments] count] inSection:0];

                        [[weakSelf comments] addObject:comment];
                        if (weakSelf.yum)
                        {
													NSInteger numberOfComments = [[weakSelf comments] count];
													[[weakSelf yum] setNumberOfComments:numberOfComments];
                        }
											
											
											[[weakSelf commentsTable] insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
											//[[weakSelf commentsTable] reloadData];
											
											[[weakSelf submitButton] setEnabled:YES];
                        [weakSelf hideLoadingSpinner];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNLCommentAddedNotification object:nil userInfo:nil];
                        NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:weakSelf.comments.count-1 inSection:0];
                        [[weakSelf commentsTable] scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                    });
                });
            }
        }];
        [self.messageTextView setText:nil];
        
    }
    [self hideKeyboard];
}

#pragma mark - Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
    
    NSDictionary *userInfo = [notification userInfo];
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];

    CGRect tableRect = self.commentsTable.frame;

    tableRect.size = CGSizeMake(CGRectGetWidth([[self view] bounds]), CGRectGetHeight([[self view] bounds])-CGRectGetHeight([[self inputView] bounds])-keyboardRect.size.height);
    
    self.InputViewVerticalSpaceConstraint.constant = keyboardRect.size.height;
    
    __weak NLCommentsViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [UIView animateWithDuration:animationDuration delay:0.0f options:0 animations:^{
            [UIView setAnimationCurve:animationCurve];
            if (weakSelf.comments.count > 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:weakSelf.comments.count-1 inSection:0];
                    [[weakSelf commentsTable] scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                });

            }
            [[weakSelf view] layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
        
    });
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];

    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    

    CGRect tableRect = CGRectZero;
    tableRect.size = CGSizeMake(CGRectGetWidth([[self view] bounds]), CGRectGetHeight([[self view] bounds])-CGRectGetHeight([[self inputView] bounds]));
    self.InputViewVerticalSpaceConstraint.constant = 0;
    __weak NLCommentsViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:animationDuration delay:0.0f options:0 animations:^{
            [UIView setAnimationCurve:animationCurve];
            [[weakSelf view] layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
        }];
    });

}

- (void)keyboardDidHide:(NSNotification *)notification
{

    [self.messageTextView resignFirstResponder];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	self.usersDic = [[NSMutableDictionary alloc] init];
	for (NLComment *comment in [self comments])
	{
		[self.usersDic setObject:[NSNumber numberWithInteger:[[comment user] userID]] forKey:[[comment user] username]];
	}
    return [[self comments] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
    NLComment *comment = (NLComment *)[[self comments] objectAtIndex:[indexPath row]];
    NLUser *user = ([comment user])?[comment user]:[NLSession currentUser];
    NLCommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCommentViewCellIdentifier];
    [cell setUsername:[NSString stringWithFormat:@"@%@",[user username]]];
    [cell setComment:[comment content] withSize:[[[self heights] objectAtIndex:[indexPath row]] CGSizeValue]];
    [[cell profileThumbButton] sd_setImageWithURL:[NSURL URLWithString:[user thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
    [cell setDelegate:self];
    [cell setTag:[indexPath row]];
    
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"<<<<<<<%@>>>>>>", [self heights]);
    if ([self heights].count > 0)
    {
        return [[[self heights] objectAtIndex:indexPath.row] CGSizeValue].height+kCommentViewCellHeight+kCommentViewCellBottomOffset;

    }
    else
    {
        return 0;
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideKeyboard];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLComment *comment = [[self comments] objectAtIndex:indexPath.row];
    NLUser *commentUser = [comment user];

    return [NLSession isCurrentUser:commentUser.userID];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[self messageTextView] resignFirstResponder];
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"delete_comment_title", nil) message:NSLocalizedString(@"delete_comment_message", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"delete_comment_cancel_button", nil) otherButtonTitles:NSLocalizedString(@"delete_comment_continue_button", nil), nil] show];
        self.indexPathToDelete = indexPath;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Calculate Cell Heights

- (CGSize)sizeForText:(NSString *)text
{
    static UIFont *cellFont;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellFont = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
    });
    
    CGRect newframe = [text boundingRectWithSize:CGSizeMake(240.0f, 0.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:cellFont} context:nil];
    newframe.size.width = kCommentTextWidth;
    return newframe.size;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    
}


//@ToDo: Use lastword and words in order to detect if a user is writing a  username. rewrite

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
//    NSLog(@"La ubicación del caracter ingresado : %d",range.location);
//    NSLog(@"El texto : %@",text);
    
    
    NSArray *words = [[textView text] componentsSeparatedByString:@" "];
//    NSLog(@"Palabras %@",words);
    
    NSString *lastWord = [words lastObject];
    
    NSRange previousRange = NSMakeRange(range.location-1, 1);
    
    
    if ([text isEqualToString:@"@"])
    {
        if (range.location == 0)
        {
            [self startMentionMode];
        }
        else if (self.currentMessageMode == NLMessageModeMention)
        {
            [self endMentionMode];
        }
        else if (self.currentMessageMode == NLMessageModeHashtag)
        {
            [self endMentionMode];
        }
        else
        {
            NSString *previousCharacter = [[textView text] substringWithRange:previousRange];
            if ([previousCharacter isEqual:@" "])
            {
                [self startMentionMode];
            }
        }
    }
    else if ([text isEqualToString:@"#"])
    {
        if (range.location == 0)
        {
            [self startHashtagMode];
        }
        else if (self.currentMessageMode == NLMessageModeMention)
        {
            [self endMentionMode];
        }
        else if (self.currentMessageMode == NLMessageModeHashtag)
        {
            [self endMentionMode];
        }
        else
        {
            NSString *previousCharacter = [[textView text] substringWithRange:previousRange];
            if ([previousCharacter isEqual:@" "])
            {
                [self startHashtagMode];
            }
        }
    }
    else if ([text isEqualToString:@" "])
    {
        if (self.currentMessageMode == NLMessageModeMention)
        {
            [self endMentionMode];
        }
        else if (self.currentMessageMode == NLMessageModeHashtag)
        {
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            self.hashtags = [[def objectForKey:@"hashtags"] mutableCopy];
            if (self.hashtags == nil)
            {
                self.hashtags = [[NSMutableArray array] mutableCopy];
            }
            if (![self.hashtags containsObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]])
            {
                [self.hashtags addObject:[NSString stringWithString:[lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""]]];
                [def setObject:self.hashtags forKey:@"hashtags"];
                [def synchronize];
            }
            [self endMentionMode];
        }
    }
    else if (text.length == 0)
    {
        if ([lastWord containsString:@"@"])
        {
            NSRange atRange = [lastWord rangeOfString:@"@"];
            
            
            if (atRange.location == 0 && lastWord.length > 1) {
                [self startMentionMode];
                //Get the word and ask for suggestions
            }
            else{
                [self endMentionMode];
            }
        }
        else if ([lastWord containsString:@"#"])
        {
            NSRange atRange = [lastWord rangeOfString:@"#"];
            
            
            if (atRange.location == 0 && lastWord.length > 1) {
                [self startHashtagMode];
                //Get the word and ask for suggestions
            }
            else{
                [self endMentionMode];
            }
        }
    }
    
    
    if (self.currentMessageMode == NLMessageModeMention) {
//        NSLog(@"Seguimos en modo mention!");
        //get the word and ask for sugesionts
    }else
    {
//        NSLog(@"Estamos ecribendo normal!");
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSArray *words = [[textView text] componentsSeparatedByString:@" "];
    NSString *lastWord = [words lastObject];
    
    if (self.currentMessageMode == NLMessageModeMention) {
//        NSLog(@"Buscar info sobre : %@",lastWord);
        NSString *cleanUser = [lastWord stringByReplacingOccurrencesOfString:@"@" withString:@""];
//        NSLog(@"El clean user : %@ y su tamaño : %d",cleanUser,cleanUser.length);
        
        __weak NLCommentsViewController *weakSelf = self;
        
        if (cleanUser.length > 0) {
            [self.queryUsersTask cancel];
            self.queryUsersTask = [NLUser getSuggestedFollowersFromUser:[[NLSession currentUser] userID] andQuery:cleanUser withCompletionBlock:^(NSError *error, NSArray *followings)
            {
                if (!error) {
                    [weakSelf setFilteredUsers:followings];
                    [[weakSelf suggestedBar] updateDatasource:[weakSelf filteredUsers]];
                }
            }];
        }else
        {
            [[self suggestedBar] updateDatasource:[self users]];
        }
    }
    else if (self.currentMessageMode == NLMessageModeHashtag)
    {
        //        NSLog(@"Buscar info sobre : %@",lastWord);
        NSString *cleanHashtag = [lastWord stringByReplacingOccurrencesOfString:@"#" withString:@""];
        //        NSLog(@"El clean user : %@ y su tamaño : %d",cleanUser,cleanUser.length);
        
        if (cleanHashtag.length > 0)
        {
            self.filteredHashtags = [self filterHashtagsQuery:cleanHashtag];
            [[self suggestedBar] updateDatasource:[self filteredHashtags]];
                    //[[weakSelf suggestedBar] updateDatasource:[weakSelf filteredUsers]];
        }
        else
        {
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            self.hashtags = [def objectForKey:@"hashtags"];
            [[self suggestedBar] updateDatasource:[self hashtags]];
        }
    }
}
-(NSArray *)filterHashtagsQuery:(NSString *)query
{
    NSMutableArray *coincidences = [NSMutableArray array];
    for (NSString *hashtag in self.hashtags)
    {
        if (hashtag.length >= query.length)
        {
            NSString *nStr = [hashtag substringWithRange:NSMakeRange(0, query.length)];
            if ([nStr containsString: query])
            {
                [coincidences addObject:hashtag];
            }
        }
    }
    return [NSArray arrayWithArray:coincidences];
}
#pragma mark - NLCommentViewCell

- (void)openUser:(NSString *)user
{
	NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUsername:user];
	[profileVC setHidesBottomBarWhenPushed:YES];
	[[self navigationController] pushViewController:profileVC animated:YES];
}
- (void)openHashtag:(NSString *)hashtag andTag:(NSInteger)tag
{
    NLComment *comment = [[self comments] objectAtIndex:tag];
    NLTag *selectedTag;
    
    if ([[comment tags] count] == 0)
    {
        selectedTag = [[comment tags] lastObject];
    }else
    {
        for (NLTag *tag in [comment tags])
        {
            if ([tag isTag:hashtag])
            {
                selectedTag = tag;
                break;
            }
        }
    }
    
    if (selectedTag)
    {
        NLHashtagViewController *hashtagVC = [[NLHashtagViewController alloc] initWithTag:selectedTag andNumberOfYums:[selectedTag numberOfYums]];
        [hashtagVC setHidesBottomBarWhenPushed:YES];
        [[self navigationController] pushViewController:hashtagVC animated:YES];
    }

}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [[self commentsTable] setEditing:NO animated:YES];
        NLComment *commentToDelete = [[self comments] objectAtIndex:self.indexPathToDelete.row];
        NSValue *heightValue = [[self heights] objectAtIndex:self.indexPathToDelete.row];
        
        
        [self.comments removeObjectAtIndex:self.indexPathToDelete.row];
        [self.heights removeObjectAtIndex:self.indexPathToDelete.row];
        
        [self.commentsTable deleteRowsAtIndexPaths:@[self.indexPathToDelete] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        //delete in the server;
        __weak NLCommentsViewController *weakSelf = self;
        
        [NLYum deleteCommentWithId:commentToDelete.commentId fromYumId:self.yum.yumId withCompletionBlock:^(NSError *error) {
           
            if (error)
            {
                //if there's an error add the comment again
                [[weakSelf comments] insertObject:commentToDelete atIndex:weakSelf.indexPathToDelete.row];
                [[weakSelf heights] insertObject:heightValue atIndex:weakSelf.indexPathToDelete.row];
                
                [[weakSelf commentsTable] insertRowsAtIndexPaths:@[weakSelf.indexPathToDelete] withRowAnimation:UITableViewRowAnimationAutomatic];
                [weakSelf setIndexPathToDelete:nil];
            }else
            {
                [[weakSelf yum] setComments:[weakSelf comments]];
            }
        }];
    }else
    {
        [self.commentsTable setEditing:NO animated:YES];
        self.indexPathToDelete = nil;
    }
}

#pragma mark - Load Comments

- (void)loadComments
{
    __weak NLCommentsViewController *weakSelf = self;
	NSInteger yumId = ([self yum])?[[self yum] yumId]:self.yumId;
	if (self.yum.repost)
	{
		yumId = [[self.yum.repost objectForKey:@"original_yum_id"] integerValue];
	}
    [NLComment getCommentsForYum:yumId withCompletionBlock:^(NSError *error, NSArray *comments) {
        
        [[weakSelf refreshControl] endRefreshing];
        [weakSelf hideLoadingSpinner];
        if ([comments count] > 0)
        {
            NSMutableSet *usersSet = [NSMutableSet set];
            
            //add new elements to size
            for (NLComment *comment in comments)
            {
                CGSize textSize = [weakSelf sizeForText:[comment content]];
							
							[[weakSelf heights] addObject:[NSValue valueWithCGSize:textSize]];
                [[weakSelf comments] addObject:comment];
                [usersSet addObject:[comment user]];
            }
            
//            NSLog(@"Los usuarios : %@",usersSet);
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"username" ascending:YES];
            NSArray *commentOwners = [usersSet sortedArrayUsingDescriptors:@[descriptor]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[weakSelf commentsTable] reloadData];
                
							[NLUser getFollowings:[[NLSession currentUser] userID] page:1 withCompletionBlock:^(NSError *error, NSArray *followings, NSInteger currentPage, NSInteger totalPages) {
                    
                    NSArray *all = [commentOwners arrayByAddingObjectsFromArray:followings];
                    [weakSelf setUsers:all];
                    //[[weakSelf suggestedBar] updateDatasource:[weakSelf users]];
                }];
            });
        }
    }];

    
}

- (void)loadPreviousComments:(id)control
{
    __weak NLCommentsViewController *weakSelf = self;
    
    [NLComment getPreviousCommentsForYum:([self yum])?[[self yum] yumId]:self.yumId commentId:[[[self comments] firstObject] commentId] withCompletionBlock:^(NSError *error, NSArray *comments) {
        
        [[weakSelf refreshControl] endRefreshing];
        [weakSelf hideLoadingSpinner];
        if ([comments count] > 0)
        {
            //add new elements to size
            for (NLComment *comment in comments)
            {
                CGSize textSize = [weakSelf sizeForText:[comment content]];
                [[weakSelf heights] insertObject:[NSValue valueWithCGSize:textSize] atIndex:0];
                [[weakSelf comments] insertObject:comment atIndex:0];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[weakSelf commentsTable] reloadData];
            });
        }
    }];
}


#pragma mark - NLSuggestedBar Delegate

- (void)suggestedBar:(NLSuggestedUsersBar *)suggestedBar didSelectUser:(id)user atIndex:(NSInteger)index
{
    NSArray *words = [[[self messageTextView] text] componentsSeparatedByString:@" "];
    //    NSLog(@"Palabras %@",words);
    NSString *lastWord = [words lastObject];
    
    NSRange rangeOfLastWord = [[[self messageTextView] text] rangeOfString:lastWord options:NSBackwardsSearch];
    /*rangeOfLastWord = [self.messageTextView selectedRange];
    NSString *subToWord = [self.messageTextView.text substringToIndex:rangeOfLastWord.location];
    
  */
    if (self.currentMessageMode == NLMessageModeMention)
    {
        //NSRange iniPosition = [subToWord rangeOfString:@"@" options:NSBackwardsSearch];
        //NSString *actualWord = [self.messageTextView.text substringWithRange:NSMakeRange(iniPosition.location, rangeOfLastWord.location)];
        //rangeOfLastWord = NSMakeRange(iniPosition.location, rangeOfLastWord.location);
        self.messageTextView.text = [self.messageTextView.text stringByReplacingCharactersInRange:rangeOfLastWord withString:[NSString stringWithFormat:@"@%@ ",[user username]]];
    }
    else
    {
        //NSRange iniPosition = [subToWord rangeOfString:@"#" options:NSBackwardsSearch];
        //rangeOfLastWord = NSMakeRange(iniPosition.location, rangeOfLastWord.location);
        self.messageTextView.text = [self.messageTextView.text stringByReplacingCharactersInRange:rangeOfLastWord withString:[NSString stringWithFormat:@"#%@ ",user]];

    }
    [self endMentionMode];

}

@end
