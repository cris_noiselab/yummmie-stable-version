//
//  NLSettingDetailViewController.h
//  Yummmie
//
//  Created by bot on 7/29/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

typedef NS_ENUM(NSInteger, SettingDetailType)
{
    NameSettingDetail,
    EmailSettingDetail,
    BioSettingDetail,
    WebSettingDetail,
    UsernameDetail,
};

@interface NLSettingDetailViewController : NLBaseViewController

@property (nonatomic) SettingDetailType detailType;

- (id)initWithSettingDetail:(SettingDetailType)detailType;


@end
