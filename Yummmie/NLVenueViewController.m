//
//  NLVenueViewController.m
//  Yummmie
//
//  Created by bot on 7/7/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import "NSURL+AssetURL.h"

#import "NLVenueViewController.h"
#import "UIColor+NLColor.h"
#import "NLVenueHeader.h"
#import "NLPhotoThumbCollectionViewCell.h"
#import "NLVenueFlowLayout.h"
#import "NLVenue.h"
#import "NLYum.h"
#import "NLPhotoDetailViewController.h"
#import "NLNotifications.h"
#import "NLSession.h"
#import "AFHTTPRequestOperationManager.h"
@import MapKit;
@import AddressBook;

static NSString * const kVenueHeaderId = @"kVenueHeaderId";
static NSString * const kVenuePhotoThumbCellId = @"kVenuePhotoThumbCellId";

@interface NLVenueViewController ()<NLPhotoDetailViewDelegate,GMSMapViewDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *dishesCollection;
@property (nonatomic, getter = isTransparent) BOOL transparent;
@property (nonatomic, strong) GMSMapView *mapView;
@property (strong, nonatomic) NLVenue *venue;
@property (strong, nonatomic) NSMutableArray *yums;
@property (nonatomic) BOOL loading;
@property (nonatomic,getter = isVisible) BOOL visible;
@property (nonatomic,copy) NSString *venueId;
@property (nonatomic) BOOL closeButton;
@property (nonatomic)BOOL canSharePhoto;



@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *location;


- (void)setRedBar;
- (void)setTransparentBar;
- (void)loadYums;
- (void)openFoursquareLink:(id)sender;
- (void)dismissView:(id)sender;
- (void)sharePhoto:(id)sender;

@end

@implementation NLVenueViewController
{
    NSTimer *reloadUber;
    UILabel *precio;
    UILabel *tiempo;
    GMSMarker *marker;
    NSString *uberProductId;
    
    BOOL uberEstimated;
	NSInteger currentVenuePage;
	NSInteger totalVenuePages;
}
-(id)initWithVenue:(NLVenue *)venue
{
    if (self = [super initWithNibName:NSStringFromClass([NLVenueViewController class]) bundle:nil])
    {
        _transparent =  YES;
        _venue = venue;
        _loading = NO;
    }

    return self;
}
- (id)initWithVenue:(NLVenue *)venue sharePhoto:(BOOL)sharePhoto andCloseButton:(BOOL)close
{
    if (self = [super initWithNibName:NSStringFromClass([NLVenueViewController class]) bundle:nil])
    {
        _transparent =  YES;
        _venue = venue;
        _loading = NO;
        _closeButton = close;
        _canSharePhoto = sharePhoto;
    }
    
    return self;
}

- (id)initWithVenueId:(NSString *)venueId andCloseButton:(BOOL)flag
{
    if (self = [super initWithNibName:NSStringFromClass([NLVenueViewController class]) bundle:nil])
    {
        _transparent =  YES;
        _venueId = venueId;
        _loading = YES;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    uberEstimated = false;
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    if ([CLLocationManager locationServicesEnabled])
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
            [self.locationManager startUpdatingLocation];
        }else
        {
            
            [self.locationManager startUpdatingLocation];
        }
    }else
    {
        [self showErrorMessage:@"Please turn on your location services" withDuration:3.0f];
    }
    // Do any additional setup after loading the view from its nib.
    
    [[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLVenueHeader class]) bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kVenueHeaderId];
    [[self dishesCollection] registerNib:[UINib nibWithNibName:NSStringFromClass([NLPhotoThumbCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kVenuePhotoThumbCellId];
    
    [[self dishesCollection] setDelegate:self];
    [[self dishesCollection] setDataSource:self];
    CGFloat width = rootView.frame.size.width;
    NLVenueFlowLayout *layout = [[NLVenueFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 2.0f;
    layout.itemSize = CGSizeMake((width-10)/3, (width-10)/3);
    layout.headerReferenceSize = CGSizeMake(320.0f, 82.0f);
    layout.sectionInset = UIEdgeInsetsMake(3, 2, 5, 2);
    
    [self.dishesCollection setCollectionViewLayout:layout];
    //UIEdgeInsets insets = UIEdgeInsetsMake(156, 0, 0, 0);
    UIEdgeInsets insets = UIEdgeInsetsMake(156, 0, 0, 0);
    [self.dishesCollection setContentInset:insets];
    
    

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[self venue] latitude] longitude:[[self venue] longitude] zoom:(self.venue.venueType == NLPrivateVenue)?12:17];
    
    self.mapView = [GMSMapView mapWithFrame:CGRectMake(0, -320, rootView.frame.size.width, 320) camera:camera];
    GMSCameraUpdate *update = [GMSCameraUpdate scrollByX:0.0f Y:-80];
    [self.mapView moveCamera:update];
    
    if (self.venue.venueType != NLPrivateVenue)
    {
        precio = [[UILabel alloc] initWithFrame:CGRectMake(80, 5, 94, 20)];
        precio.text = @"";
        [precio setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [precio setTextColor:[UIColor colorWithRed:89.0 / 255.0  green:92.0 / 255.0  blue:105.0 / 255.0  alpha:1]];
        precio.textAlignment = NSTextAlignmentRight;
        tiempo = [[UILabel alloc] initWithFrame:CGRectMake(40, 19, 150, 20)];
        tiempo.text = @"";
        
        [tiempo setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [tiempo setTextColor:[UIColor colorWithRed:153.0 / 255.0 green:153.0 / 255.0 blue:153.0 / 255.0 alpha:1]];
        marker = [[GMSMarker alloc] init];
        self.mapView.delegate = self;
        marker.position = camera.target;
        [self.mapView setMyLocationEnabled:YES];
        marker.icon = [UIImage imageNamed:@"pin_map"];
        marker.map = [self mapView];
        marker.infoWindowAnchor = CGPointMake(0.50f, 0.0f);

        [[self mapView] setDelegate:self];
    }
    
    [self.dishesCollection addSubview:self.mapView];
    
     
     
    if (self.venue.facebook_place_id && self.venue.facebook_place_id.length > 0)
    {
        UIButton *foursquareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [foursquareButton setBackgroundImage:[UIImage imageNamed:@"foursquare_link"] forState:UIControlStateNormal];
        CGRect foursquareButtonFrame = foursquareButton.frame;
        foursquareButtonFrame.origin = CGPointMake(CGRectGetMaxX(self.view.bounds)-foursquareButton.frame.size.height,-foursquareButton.frame.size.height);
        [foursquareButton setFrame:foursquareButtonFrame];
        [foursquareButton addTarget:self action:@selector(openFoursquareLink:) forControlEvents:UIControlEventTouchDown];
			foursquareButton.hidden = true;
        [[self dishesCollection] addSubview:foursquareButton];
    }
    
    [self loadYums];
    
    if (self.closeButton) {
        ;
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bot-cerrar-blanco"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissView:)];
        //closeButton.tintColor = [UIColor blackColor];
        self.navigationItem.leftBarButtonItem = closeButton;
    }
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navbar"]];
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = logoView;
    [self.navigationItem.titleView setAlpha:0];
    [self.navigationItem.titleView setHidden:true];
    
	currentVenuePage = 1;
	totalVenuePages = 10;
    
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
	
    [[self navigationController] setNavigationBarHidden:NO];
    self.visible = YES;
    
    CGRect frame = self.navigationController.navigationBar.frame;
    if (frame.origin.y < 20)
    {
        frame.origin.y = 20.0f;
        [self.navigationController.navigationBar setFrame:frame];
    }
    
    if (self.dishesCollection.contentOffset.y >= -64.0f)
    {
        self.transparent = YES;
        [self setRedBar];
    }else
    {
        self.transparent = NO;
        [self setTransparentBar];
        
        

        
    }
    [self hideLoadingSpinner];
    
    
   
    
    
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"Venue View"];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_transparent)
    {
        [self setTransparent:false];
        [self setTransparentBar];
    }
    

}
#pragma mark - Map view

-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    UIView *infoWindow = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 47)];
    UIImageView *backImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"uberbubble"]];
    
    [infoWindow addSubview:backImg];
    UILabel *uber = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 50, 20)];
    uber.text = @"UberX";
    [uber setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
    [uber setTextColor:[UIColor colorWithRed:42.0 / 255.0  green:42.0 / 255.0  blue:42.0 / 255.0  alpha:1]];
    [infoWindow addSubview:uber];
    [infoWindow addSubview:precio];
    [infoWindow addSubview:tiempo];
    [precio setCenter:CGPointMake(precio.center.x, (infoWindow.frame.size.height / 2) - 3)];
    return infoWindow;
}
-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"button_press" label:@"Uber" value:nil] build]];

    NSString *dropOffQuery = [[NSString stringWithFormat:@"&dropoff[latitude]=%f&dropoff[longitude]=%f&dropoff[nickname]=%@&product_id=%@", self.venue.latitude, self.venue.longitude, self.venue.name, uberProductId] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://m.uber.com/?action=setPickup&pickup=my_location%@", dropOffQuery]];
    NSLog(@"%@", url);
    //if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"uber://?action=setPickup&pickup=my_location%@", dropOffQuery]]])
    {
        // Do something awesome - the app is installed! Launch App.
        url = [NSURL URLWithString:[NSString stringWithFormat:@"uber://?action=setPickup&pickup=my_location%@", dropOffQuery]];
    }
    [[UIApplication sharedApplication] openURL:url];
}
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self.mapView setSelectedMarker:marker];

}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	// CLLocation *location = [locations lastObject];
    
	//NSTimeInterval timeInterval = [[location timestamp] timeIntervalSinceNow];
    
    if (uberEstimated == false)
    {
        uberEstimated = true;
        self.location = [locations objectAtIndex:0];
        [self reloadEstimates];
        [self.locationManager stopUpdatingLocation];
    }
    
}
-(void)reloadEstimates
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"server_token": @"At-GkaT-uINUPgLS38V6k-xsKOrAEjQqnj2sQjUe",
                             @"start_latitude": [NSNumber numberWithFloat: self.mapView.camera.target.latitude],
                             @"start_longitude": [NSNumber numberWithFloat: self.mapView.camera.target.longitude],
                             @"end_latitude": [NSNumber numberWithFloat: self.location.coordinate.latitude],
                             @"end_longitude": [NSNumber numberWithFloat: self.location.coordinate.longitude]
                             };
    [manager GET:@"https://api.uber.com/v1/estimates/price" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        if([responseObject objectForKey:@"prices"] != nil && [[responseObject objectForKey:@"prices"] count] > 0)
        {
            NSDictionary *uberXData = [[responseObject objectForKey:@"prices"] objectAtIndex:0];
            NSString *pre = [uberXData objectForKey:@"estimate"];
            pre = [pre stringByReplacingOccurrencesOfString:@"MX" withString:@""];
            precio.text = [NSString stringWithFormat:@"%@ MXN", pre];
            tiempo.text = [NSString stringWithFormat:@"%.0f mins.", roundf([[uberXData objectForKey:@"duration"] floatValue] / 60)];
            uberProductId = [uberXData objectForKey:@"product_id"];
            [self.mapView setSelectedMarker:marker];
            /*GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[self venue] latitude] longitude:[[self venue] longitude] zoom:(self.venue.venueType == NLPrivateVenue)?12:17];
            [self.mapView setCamera:camera];*/

        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.visible = NO;
    [self setRedBar];
    [self hideLoadingSpinner];
    //[self setTransparent:YES];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return ([self isTransparent])?UIStatusBarStyleLightContent:UIStatusBarStyleDefault;
}

- (void)dealloc
{
    [[self dishesCollection] setDelegate:nil];
    [[self dishesCollection] setDataSource:nil];
}

- (void)loadYums
{
    [self showLoadingSpinner];
    if (![self loading])
    {
        [self setLoading:YES];
        
        __weak NLVenueViewController *weakSelf = self;
			
			currentVenuePage++;
			[NLYum loadYumsFromVenueId:[[self venue] venueId] page:currentVenuePage withCompletionBlock:^(NSError *error, NSMutableArray *yums, NSInteger currentPage, NSInteger totalPages)
			{
            
            if (error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf showErrorMessage:NSLocalizedString(@"error_loading_yums", nil) withDuration:2.0f];
                    [weakSelf setLoading:NO];
                    [weakSelf hideLoadingSpinner];
                });
            }else
            {
							if (currentPage != 0)
							{
								currentVenuePage = currentPage;

							}
							totalVenuePages = totalPages;
                if ([yums count]>0)
                {
                    if (![weakSelf yums])
                    {
                        [weakSelf setYums:[NSMutableArray array]];
                    }
                    
                    NSMutableArray *indexPaths = [NSMutableArray array];
                    NSInteger lastIndex = [[weakSelf yums] count];
                    
                    for (NSInteger index = 0; index < [yums count]; index++)
                    {
                        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:lastIndex+index inSection:0];
                        [indexPaths addObject:indexPath];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf hideLoadingSpinner];
                        [[weakSelf dishesCollection] performBatchUpdates:^{
                            [[weakSelf yums] addObjectsFromArray:yums];
                            [[weakSelf dishesCollection] insertItemsAtIndexPaths:indexPaths];
                        } completion:^(BOOL finished)
                        {
                            [weakSelf setLoading:NO];
                        }];
                    });

                }else
                {
                    if ([weakSelf.yums count]==0)
                    {
                        //is empty totally, empty
                        UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
                        CGRect errorViewFrame;
                        
                        if (weakSelf.view.frame.size.height > 480)
                        {
                            errorViewFrame = CGRectMake(0, 130, rootView.frame.size.width, 200);
                        }else
                        {
                            errorViewFrame =  CGRectMake(0, 80, rootView.frame.size.width, 200);
                        }
                        
                        UIView *view = [[UIView alloc] initWithFrame:errorViewFrame];
                        //"not_yummmies_in_venue"
                        UIImage *image = [UIImage imageNamed:@"icon_caritaoh"];
                        UIImageView *face = [[UIImageView alloc] initWithImage:image];
                        face.center = view.center;
                        CGRect faceRect = [face frame];
                        faceRect.origin.y = 15.0f;
                        [face setFrame:faceRect];
                        [view addSubview:face];
                        
                        UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, face.frame.origin.y+face.frame.size.height, 190, 90)];
                        [detailLabel setCenter:CGPointMake(rootView.center.x, detailLabel.center.y)];
                        detailLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
                        detailLabel.numberOfLines = 0;
                        detailLabel.textAlignment = NSTextAlignmentCenter;
                        detailLabel.text = NSLocalizedString(@"not_yummmies_in_venue", nil);
                        detailLabel.textColor = [UIColor nl_colorWith255Red:107 green:107 blue:107 andAlpha:255];
                        [view addSubview:detailLabel];
                        [weakSelf.dishesCollection addSubview:view];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf hideLoadingSpinner];
                        [weakSelf setLoading:NO];
                    });

                }
            }
        }];

    }
}

#pragma mark - Open Foursquare

- (void)openFoursquareLink:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"foursquare://"]])
    {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"foursquare://venues/%@",self.venue.facebook_place_id]]];
    }else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://foursquare.com/v/%@",self.venue.facebook_place_id]]];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[self yums] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
    
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kVenuePhotoThumbCellId forIndexPath:indexPath];
    [[cell photo] sd_setImageWithURL:[NSURL URLWithString:[yum thumbImage]] placeholderImage:nil];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{

    if (kind == UICollectionElementKindSectionHeader)
    {

        NLVenueHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:kVenueHeaderId forIndexPath:indexPath];
        [[header venueNameLabel] setText:[[self venue] name]];
 
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            CALayer *topBorder = [CALayer layer];
            CALayer *bottomBorder = [CALayer layer];
            topBorder.frame = CGRectMake(0, 0, header.frame.size.width, 1.0f);
            topBorder.backgroundColor = [[UIColor nl_colorWith255Red:212 green:212 blue:212 andAlpha:255] CGColor];
            header.frame = CGRectMake(0, 2, header.frame.size.width, header.frame.size.height);//----
            bottomBorder.frame = CGRectMake(0, header.frame.size.height-1, header.frame.size.width, 1.0f);
            bottomBorder.backgroundColor = [[UIColor nl_colorWith255Red:212 green:212 blue:212 andAlpha:255] CGColor];
            [header.layer addSublayer:topBorder];
            [header.layer addSublayer:bottomBorder];
            
        });
        //M request
        if (self.venue.venueType == NLPrivateVenue)
        {
					header.sharePhoto.hidden = true;
            if (self.venue.city)
            {
                [[header venueAddressLabel] setText:[[self venue] city]];
            }
            
            if ([self.venue.state isEqualToString:@"Federal District"])
            {
                [[header venueDetailAddressLabel] setText:@"Distrito Federal"];
            }
            else
            {
                if (self.venue.state)
                {
                    [[header venueDetailAddressLabel] setText:@""];

                }
            }
            [[header venueStateLabel] setText:@""];
            
        }
        else
        {
            if (self.venue.street)
            {
                [[header venueAddressLabel] setText:self.venue.street];
            }
            if (self.venue.suburb)
            {
                [[header venueDetailAddressLabel] setText:self.venue.suburb];
            }
            
            if ([self.venue.state isEqualToString:@"Federal District"])
            {
                [[header venueStateLabel] setText:@"Distrito Federal"];
            }
						else
            {
							if (self.venue.city)
							{
								[[header venueStateLabel] setText:self.venue.city];
								if (self.venue.state)
								{
									header.venueStateLabel.text = [NSString stringWithFormat:@"%@, %@", header.venueStateLabel.text, self.venue.state];
								}
							}
							else if (self.venue.state)
							{
								[[header venueStateLabel] setText:self.venue.state];
							}
            }
        }
			
        if (self.venue.country != NULL)
        {
            //if (![self.venue.country isEqualToString:@"<null>"]) {
                [[header venueStateLabel] setText:[NSString stringWithFormat:@"%@, %@", [header venueStateLabel].text, [[self venue] country]]];

            //}
        }
        if (self.venue.venueType == NLPrivateVenue)
        {
            [[header venueStateLabel] setText:@""];
        }
        
        if ([self canSharePhoto]) {
            [[header sharePhoto] setHidden:NO];
            [[header sharePhoto] addTarget:self action:@selector(sharePhoto:) forControlEvents:UIControlEventTouchUpInside];

        }
        
        //share location
        [[header sharePhoto] setHidden:NO];
        [[header sharePhoto] addTarget:self action:@selector(shareLocation) forControlEvents:UIControlEventTouchUpInside];
        BOOL detailAddress = false;
        BOOL emptyAddress = false;
			//BOOL *stateLabel = false;
        if ([header.venueDetailAddressLabel.text containsString:@"<null>"])
        {
            emptyAddress = true;

            detailAddress = true;
            header.venueDetailAddressLabel.text = [header.venueDetailAddressLabel.text stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
            
            
            
        }
        if ([header.venueStateLabel.text containsString:@"<null>"])
        {
            emptyAddress = true;

            detailAddress = true;
            header.venueStateLabel.text = [header.venueStateLabel.text stringByReplacingOccurrencesOfString:@"<null>" withString:@""];
            header.venueStateLabel.text = [header.venueStateLabel.text stringByReplacingOccurrencesOfString:@"," withString:@""];

            if (header.venueDetailAddressLabel.text.length < 5)
            {
                header.venueDetailAddressLabel.text = header.venueStateLabel.text;
                header.venueStateLabel.text = @"";


            }
        }
       
        if (emptyAddress)
        {
            header.venueDetailAddressLabel.textColor = [UIColor lightGrayColor];
            header.venueStateLabel.textColor = [UIColor lightGrayColor];

        }
			NSString *searchWhites = [header.venueStateLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
			if ([[[searchWhites componentsSeparatedByString:@","] firstObject] isEqualToString:@""])
			{
				header.venueStateLabel.text = [[header.venueStateLabel.text componentsSeparatedByString:@","] lastObject];
			}
			searchWhites = [header.venueStateLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
			if ([[[searchWhites componentsSeparatedByString:@","] lastObject] isEqualToString:@""])
			{
				header.venueStateLabel.text = [[header.venueStateLabel.text componentsSeparatedByString:@","] firstObject];
			}
			if ([self.venue.venueId integerValue] == 21676 || [self.venueId integerValue] == 21676 || [self.venueId isEqualToString:@"zyxNkjbX"])
			{
				[header.venueImage sd_setImageWithURL:[NSURL URLWithString:@"http://s3-us-west-2.amazonaws.com/yummmie/users/avatars/000/013/635/large/avatar.png?484835"] placeholderImage:nil];
			}
        return header;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLYum *yum = [[self yums] objectAtIndex:[indexPath row]];
    NLPhotoDetailViewController *detail = [[NLPhotoDetailViewController alloc] initWithYumId:yum.yumId];
    detail.hidesBottomBarWhenPushed = YES;
    [detail setDelegate:self];
    [[self navigationController] pushViewController:detail animated:YES];
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell highlight];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    NLPhotoThumbCollectionViewCell *cell = (NLPhotoThumbCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell unHighlight];
}

#pragma mark - Toggle Bar Color
- (void)setRedBar
{
    if ([self isTransparent])
    {
        self.navigationController.navigationBar.shadowImage = nil;
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:205.0/255.0 green:36.0f/255.0f blue:49.0/255.0 alpha:1.0];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setBackgroundImage:nil
                                                      forBarMetrics:UIBarMetricsDefault];
        [self.navigationController setNeedsStatusBarAppearanceUpdate];
        [self.navigationItem.titleView setHidden:false];
        [self.navigationItem.titleView setAlpha:1];

        _transparent = NO;
    }
}

- (void)setTransparentBar
{
    if (![self isTransparent])
    {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [UIImage new];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        [self.navigationController setNeedsStatusBarAppearanceUpdate];
        self.navigationItem.titleView.alpha = 0;
        _transparent = YES;
    }
}
#pragma mark - UIScrollViewDelegate

- (void)loadIfNeeded:(UIScrollView *)scrollView
{
    if ([[self yums] count] > 6)
    {
        [self loadYums];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;

    if ([self isVisible])
    {
        if (scrollView.contentOffset.y >=-64.0)
        {
            [self setRedBar];
        }else
        {
            [self setTransparentBar];
            
            UIEdgeInsets insets = UIEdgeInsetsMake(156, 0, 0, 0);
            [self.dishesCollection setFrame:CGRectMake(0, 0, rootView.frame.size.width, self.dishesCollection.frame.size.height)];
            NSLog(@"Puso los insets");
                        
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadIfNeeded:scrollView];
}

#pragma mark - NLPhotoDetailViewDelegate

- (void)yumDeleted:(NLYum *)deletedYum
{
    NSUInteger deletedIndex = [self.yums indexOfObject:deletedYum];
    
    NSIndexPath *deletedIndexPath = [NSIndexPath indexPathForItem:deletedIndex inSection:0];
    //remove the element of the array and the update de colelction
    [[self dishesCollection] performBatchUpdates:^{
        [[self yums] removeObjectAtIndex:deletedIndex];
        [[self dishesCollection] deleteItemsAtIndexPaths:@[deletedIndexPath]];
    } completion:^(BOOL finished)
     {
         
     }];
}

#pragma mark - GMSMapViewDelegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    if ([UIAlertController class])
    {
        __weak NLVenueViewController *weakSelf = self;
        
        //create an alert controller
        UIAlertController *actionController = [UIAlertController alertControllerWithTitle:self.venue.name message:@"Abrir Ruta" preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *openInGoogleMaps;
        
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]])
        {
            openInGoogleMaps = [UIAlertAction actionWithTitle:@"Open in Google Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
            {
                NSString *urlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",self.mapView.myLocation.coordinate.latitude,self.mapView.myLocation.coordinate.longitude,self.venue.latitude,self.venue.longitude];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }];
        }else
        {
            NSString *urlString = [NSString stringWithFormat:@"https://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f&hl?en",self.mapView.myLocation.coordinate.latitude,self.mapView.myLocation.coordinate.longitude,self.venue.latitude,self.venue.longitude];
            openInGoogleMaps = [UIAlertAction actionWithTitle:@"Open in Google Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }];
        }
        [actionController addAction:openInGoogleMaps];
        

        UIAlertAction *openInMapsAction = [UIAlertAction actionWithTitle:@"Open in Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            MKPlacemark *place = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.venue.latitude, self.venue.longitude) addressDictionary:nil];
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            annotation.title = weakSelf.venue.name;
            
            MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:place];
            [mapItem setName:weakSelf.venue.name];
            NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     MKLaunchOptionsDirectionsModeDriving,
                                     MKLaunchOptionsDirectionsModeKey, nil];
            [mapItem openInMapsWithLaunchOptions:options];
        }];
        [actionController addAction:openInMapsAction];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [actionController addAction:cancelAction];
    
        UIAlertAction *uberAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"uber", @"uber") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"button_press" label:@"Uber" value:nil] build]];
                    //uber://?client_id=YOUR_CLIENT_ID&action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d

            NSString *dropOffQuery = [[NSString stringWithFormat:@"&dropoff[latitude]=%f&dropoff[longitude]=%f&dropoff[nickname]=%@", self.venue.latitude, self.venue.longitude, self.venue.name] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://m.uber.com/?action=setPickup&pickup=my_location%@", dropOffQuery]];
            //if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"uber://?action=setPickup&pickup=my_location%@", dropOffQuery]]])
            {
                // Do something awesome - the app is installed! Launch App.
                url = [NSURL URLWithString:[NSString stringWithFormat:@"uber://?action=setPickup&pickup=my_location%@", dropOffQuery]];
            }
            [[UIApplication sharedApplication] openURL:url];

        }];
        [actionController addAction:uberAction];


        
        [self presentViewController:actionController animated:YES completion:nil];
    }
    else
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:( [[UIApplication sharedApplication]canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]])?@"Open in Google Maps":@"Open google map site",@"Open in Maps",nil];
        [actionSheet showInView:self.view];
    }
    return YES;
}

#pragma mark - UIActionSheetDelegate 

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0 )
    {
        if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]])
        {
            NSString *urlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",self.mapView.myLocation.coordinate.latitude,self.mapView.myLocation.coordinate.longitude,self.venue.latitude,self.venue.longitude];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }else
        {
            NSString *urlString = [NSString stringWithFormat:@"https://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f&hl?en",self.mapView.myLocation.coordinate.latitude,self.mapView.myLocation.coordinate.longitude,self.venue.latitude,self.venue.longitude];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
    }else if(buttonIndex == 1)
    {
        MKPlacemark *place = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.venue.latitude, self.venue.longitude) addressDictionary:nil];
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.title = self.venue.name;
        
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:place];
        [mapItem setName:self.venue.name];
        
        NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 MKLaunchOptionsDirectionsModeDriving,
                                 MKLaunchOptionsDirectionsModeKey, nil];
        
        [mapItem openInMapsWithLaunchOptions:options];
        
    }else if(buttonIndex == 2)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsStatusBarAppearanceUpdate];
        });
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{

}


#pragma mark - Close View

- (void)dismissView:(id)sender
{
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
}

#pragma mark - share photo

- (void)sharePhoto:(id)sender
{
    [NLSession setSharedVenue:[self venue]];
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kNLOpenCameraNotification object:nil];
    }];
    
}
#pragma mark - share location

- (void)shareLocation
{
    //[NLSession setSharedVenue:[self venue]];
    NSURL *whatsappURL = [NSURL URLWithString:[[[NSString stringWithFormat:@"whatsapp://send?text=%@ %@",self.venue.name, self.venue.shareUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"&" withString:@"and"]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}

@end
