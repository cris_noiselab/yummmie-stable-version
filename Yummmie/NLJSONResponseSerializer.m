//
//  NLJSONResponseSerializer.m
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLJSONResponseSerializer.h"


@implementation NLJSONResponseSerializerWithData

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
    if (![self validateResponse:(NSHTTPURLResponse *)response data:data error:error]) {
        if (*error != nil) {
            NSMutableDictionary *userInfo = [(*error).userInfo mutableCopy];
					
            userInfo[JSONResponseSerializerWithDataKey] = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSError *newError = [NSError errorWithDomain:(*error).domain code:(*error).code userInfo:userInfo];
            (*error) = newError;
        }
        
        return (nil);
    }
    
    return ([super responseObjectForResponse:response data:data error:error]);
}

@end