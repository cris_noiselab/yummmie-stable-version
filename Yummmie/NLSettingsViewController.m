//
//  NLSettingsViewController.m
//  Yummmie
//
//  Created by bot on 7/29/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "NLSettingsViewController.h"
#import "NLNotifications.h"
#import "NLSession.h"
#import "NLUser.h"
#import "NSURL+AssetURL.h"
#import "UIColor+NLColor.h"
#import "NLSettingDetailViewController.h"

#import "DBCameraLibraryViewController.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraSegueViewController.h"
#import "DBCameraView.h"
#import "NLPhotoSelection.h"


static NSString * const kNLSettingsCellIdentifier = @"kNLSettingsCellIdentifier";

@interface NLSettingsViewController ()<DBCameraViewControllerDelegate>


@property (weak, nonatomic) IBOutlet UIButton *profilePicButton;
@property (weak, nonatomic) IBOutlet UIButton *coverPicButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITableView *settingsTable;
@property (strong ,nonatomic) NSArray *datasource;
@property (nonatomic) PhotoSelectionMode currentMode;
@property (nonatomic) BOOL coverChanged;
@property (nonatomic) BOOL avatarChanged;

@property (strong, nonatomic) UIBarButtonItem *saveButton;

@property (nonatomic,strong) UIImage *coverPhoto;
@property (nonatomic,strong) UIImage *profileImage;


@property (nonatomic) BOOL withCloseButton;

- (IBAction)changeAvatar:(id)sender;
- (IBAction)changeCover:(id)sender;
- (void)dismissView:(id)sender;

- (void)updateData:(NSNotification *)aNotification;
- (void)saveAction:(id)sender;

@end

@implementation NLSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        
    }
    return self;
}

- (id)initWithCloseButton
{
    self =[super initWithNibName:NSStringFromClass([NLSettingsViewController class]) bundle:nil];
    if (self)
    {
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        _withCloseButton = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"edit_profile_title", nil);
    
    if ([self withCloseButton])
    {
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"close_title", nil) style:UIBarButtonItemStylePlain target:self action:@selector(dismissView:)];
    }
    
    
    //[self.profilePicButton setFrame:CGRectMake(self.profilePicButton.frame.origin.x, self.profilePicButton.frame.origin.y, 90.0f, 90.0f)];
    //self.profilePicButton.layer.cornerRadius = self.profilePicButton.bounds.size.height/2.0f;
    
    NLUser *user = [NLSession currentUser];
    
    [[self profilePicButton] sd_setImageWithURL:[NSURL URLWithString:[user avatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"pic_userdefault"]];
    [[self coverPicButton] sd_setImageWithURL:[NSURL URLWithString:[user cover]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"pic_coverdefault"]];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    [gradientLayer setAnchorPoint:CGPointZero];
    [gradientLayer setFrame:[self.coverPicButton bounds]];
    [gradientLayer setLocations:@[@0.0f,@1.0f]];
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    UIColor *topColor = [UIColor colorWithWhite:0.0f alpha:0.3];
    UIColor *bottomColor = [UIColor colorWithWhite:0.0f alpha:0.6];
    [gradientLayer setFrame: CGRectMake(self.coverPicButton.frame.origin.x, self.coverPicButton.frame.origin.y, rootView.frame.size.width,rootView.frame.size.height*0.37)];
    
    [gradientLayer setColors:@[(id)[topColor CGColor],(id)[bottomColor CGColor]]];
    
    [[self.coverPicButton layer] insertSublayer:gradientLayer atIndex:(unsigned)self.coverPicButton.layer.sublayers.count];
    
    [[self nameLabel] setText:[user name]];
    
    
    [[self settingsTable] setDelegate:self];
    [[self settingsTable] setDataSource:self];
    
    self.datasource = @[
                        @{
                            @"name":NSLocalizedString(@"join_name_text", nil),
                            @"value":[user name]
                            },
                        @{
                            @"name":NSLocalizedString(@"bio_text", nil),
                            @"value":([user bio])?[user bio]:@""
                            },
                        @{
                            @"name":NSLocalizedString(@"website_text", nil),
                            @"value":([user website])?[user website]:@""
                            },
                        @{
                            @"name":NSLocalizedString(@"email_text", nil),
                            @"value":([user email])?[user email]:@""
                            },
                        @{
                            @"name":NSLocalizedString(@"username_text", nil),
                            @"value":([user username])?[user username]:@""
                            }
                        ];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [defaultCenter addObserver:self selector:@selector(updateData:) name:kNLUpdateSettingsNotification object:nil];
    
    self.saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save_button_title", nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveAction:)];
    self.navigationItem.rightBarButtonItem = self.saveButton;
    self.navigationItem.rightBarButtonItem.customView.hidden = YES;
    
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self settingsTable] deselectRowAtIndexPath:[[self settingsTable] indexPathForSelectedRow] animated:YES];
    //Google Analytics
    NSString *name = [NSString stringWithFormat:@"editar perfil View"];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.profilePicButton.layer.cornerRadius = self.profilePicButton.bounds.size.height/2.0f;
    
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)changeAvatar:(id)sender
{
    self.currentMode = PhotoSelectionModeProfile;
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"change_avatar_title", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel_option", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"open_gallery", nil),NSLocalizedString(@"open_camera", nil), nil] ;
    [sheet showInView:self.view];
}

- (IBAction)changeCover:(id)sender
{
    self.currentMode = PhotoSelectionModeCover;
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"change_cover_title",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel_option", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"open_gallery", nil),NSLocalizedString(@"open_camera", nil), nil] ;
    [sheet showInView:self.view];
}
#pragma mark - dismiss
- (void)dismissView:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLDismissSettingsNotification object:nil];
}

#pragma mark - Save changes

- (void)saveAction:(id)sender
{
    if ([self avatarChanged] || [self coverChanged])
    {
        [self showLoadingSpinner];
        __weak NLSettingsViewController *weakSelf = self;
        
        void (^block)(NSError *,NLUser *) = ^void (NSError * error, NLUser *user)
        {
            if (error)
            {
                [weakSelf showErrorMessage:NSLocalizedString(@"update_error", nil) withDuration:2.0f];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[weakSelf navigationItem] setRightBarButtonItem:[weakSelf saveButton]];
                });
            }else
            {
                [NLSession updateUser:user];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLUpdateSettingsNotification object:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[weakSelf navigationItem] setRightBarButtonItem:[weakSelf saveButton]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNLDismissSettingsNotification object:nil];
                });
            }
        };
        
        NLUser *user = [NLSession currentUser];
        
        UIImage *cover,*avatar;
        
        if ([self coverChanged])
        {
            cover = [self coverPhoto];
            [NLSession setSharedCover:[self coverPhoto]];
        }
        
        if ([self avatarChanged])
        {
            avatar = [self profileImage];
            [NLSession setSharedAvatar:[self profileImage]];
        }
        
        [NLUser updateUserWithUserId:[user userID] name:nil website:nil bio:nil coverPhoto:cover avatarPhoto:avatar email:nil username:nil withCompletionBlock:block];
        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kNLDismissSettingsNotification object:nil];//optional for new functionality
}

#pragma mark - Notification Methods

- (void)updateData:(NSNotification *)aNotification
{
    NLUser *user = [NLSession currentUser];
    
    self.datasource = @[
                        @{
                            @"name":NSLocalizedString(@"join_name_text", nil),
                            @"value":[user name]
                            },
                        @{
                            @"name":NSLocalizedString(@"bio_text", nil),
                            @"value":([user bio])?[user bio]:@""
                            },
                        @{
                            @"name":NSLocalizedString(@"website_text", nil),
                            @"value":([user website])?[user website]:@""
                            },
                        @{
                            @"name":NSLocalizedString(@"email_text", nil),
                            @"value":([user email])?[user email]:@""
                            },
                        @{
                            @"name":NSLocalizedString(@"username_text", nil),
                            @"value":([user username])?[user username]:@""
                            }
                        ];
    
    __weak NLSettingsViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[weakSelf settingsTable] reloadData];
        [[weakSelf nameLabel] setText:[user name]];
    });
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [[self datasource] count];
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNLSettingsCellIdentifier];
        
        NSDictionary *dataDictionary = [[self datasource] objectAtIndex:[indexPath row]];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kNLSettingsCellIdentifier];
            [[cell textLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
            [[cell textLabel] setTextColor:[UIColor nl_applicationRedColor]];
            
            [[cell detailTextLabel] setMinimumScaleFactor:0.9];
            [[cell detailTextLabel] setLineBreakMode:NSLineBreakByTruncatingTail];
            [[cell detailTextLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:17.0f]];
            [[cell detailTextLabel] setTextColor:[UIColor lightGrayColor]];
            
        }
        
        [[cell textLabel] setText:[dataDictionary valueForKey:@"name"]];
        [[cell detailTextLabel] setText:[dataDictionary valueForKey:@"value"]];
        
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deleteAccount"];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"deleteAccount"];
            [[cell textLabel] setTextColor:[UIColor whiteColor]];
            [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
            cell.textLabel.text = NSLocalizedString(@"deleteAccount", nil);
            cell.backgroundColor = [UIColor nl_applicationRedColor];
        }
        return cell;
        
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        switch ([indexPath row])
        {
            case 0:
            {
                NLSettingDetailViewController *detail = [[NLSettingDetailViewController alloc] initWithSettingDetail:NameSettingDetail];
                [[self navigationController] pushViewController:detail animated:YES];
                break;
            }
            case 1:
            {
                NLSettingDetailViewController *detail = [[NLSettingDetailViewController alloc] initWithSettingDetail:BioSettingDetail];
                [[self navigationController] pushViewController:detail animated:YES];
                break;
            }
            case 2:
            {
                NLSettingDetailViewController *detail = [[NLSettingDetailViewController alloc] initWithSettingDetail:WebSettingDetail];
                [[self navigationController] pushViewController:detail animated:YES];
                break;
            }
            case 3:
            {
                NLSettingDetailViewController *detail = [[NLSettingDetailViewController alloc] initWithSettingDetail:EmailSettingDetail];
                [[self navigationController] pushViewController:detail animated:YES];
                break;
            }
            case 4:
            {
                NLSettingDetailViewController *detail = [[NLSettingDetailViewController alloc] initWithSettingDetail:UsernameDetail];
                [[self navigationController] pushViewController:detail animated:YES];
                break;
            }
            default:
                break;
        }
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"deleteAccount?", nil) message:NSLocalizedString(@"deleteAccountMessage", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *borrar = [UIAlertAction actionWithTitle:NSLocalizedString(@"deleteAccountButton", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action)
                                 {
                                     
                                     [[NLSession currentUser] destroyWithCompletionBlock:^(NSError *error) {
                                         if (!error) {
                                             
                                             [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:kNLLogoutNotification object:nil];
                                             }];
                                         }
                                     }];
                                 }];
        UIAlertAction *cancelar = [UIAlertAction actionWithTitle:NSLocalizedString(@"dish_option_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[tableView cellForRowAtIndexPath:indexPath] setSelected:false animated:true];
        }];
        [alert addAction:borrar];
        [alert addAction:cancelar];
        [self presentViewController:alert animated:true completion:nil];
        
    }
    
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGFloat dimensions = rootView.frame.size.width;
    switch (buttonIndex) {
        case 0:
        {
            DBCameraLibraryViewController *libraryVC = [[DBCameraLibraryViewController alloc] init];
            [libraryVC setDelegate:self];
            [libraryVC setUseCameraSegue:YES];
            
            if ([self currentMode] == PhotoSelectionModeProfile)
            {
                [libraryVC setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions, dimensions);
                 }];
            }else
            {
                [libraryVC setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions, dimensions*0.625);
                     
                 }];
            }
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:libraryVC];
            [navController setNavigationBarHidden:YES];
            [self presentViewController:navController animated:YES completion:nil];
        }
            break;
        case 1:
        {
            DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
            
            DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self cameraSettingsBlock:^(DBCameraView *cameraView, id container) {
                cameraView.flashButton.hidden = YES;
                cameraView.gridButton.hidden = YES;
                cameraView.photoLibraryButton.hidden = YES;
            }];
            
            
            if ([self currentMode] == PhotoSelectionModeProfile)
            {
                [cameraController setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions, dimensions);
                     segue.filtersView = nil;
                 }];
                [cameraController setForceQuadCrop:YES];
                
            }else
            {
                [cameraController setUseCameraSegue:YES];
                [cameraController setCameraSegueConfigureBlock:^( DBCameraSegueViewController *segue )
                 {
                     segue.cropMode = YES;
                     segue.cropRect = CGRectMake(0, 0, dimensions , dimensions*0.625);
                 }];
            }
            [container setCameraViewController:cameraController];
            [container setFullScreenMode];
            
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:container];
            [nav setNavigationBarHidden:YES];
            [self presentViewController:nav animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
}

#pragma mark - DBCamera Delegates

- (void)dismissCamera:(id)cameraViewController
{
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    if (self.currentMode == PhotoSelectionModeProfile)
    {
        self.profileImage = image;
        [self.profilePicButton setImage:self.profileImage forState:UIControlStateNormal];
        self.avatarChanged = YES;
    }else
    {
        self.coverPhoto = image;
        [self.coverPicButton setImage:self.coverPhoto forState:UIControlStateNormal];
        self.coverChanged = YES;
    }
    
    self.navigationItem.rightBarButtonItem.customView.hidden = NO;
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
