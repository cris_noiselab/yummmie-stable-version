//
//  NLFollowerInteractionViewCell.h
//  Yummmie
//
//  Created by bot on 8/22/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>

@protocol NLFollowerInteractionViewCellDelegate;


@interface NLFollowerInteractionViewCell : UITableViewCell<TTTAttributedLabelDelegate>


@property (weak, nonatomic) id<NLFollowerInteractionViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *userImageButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

- (IBAction)openUser:(id)sender;

- (void)setUser:(NSString *)user;

@end


@protocol NLFollowerInteractionViewCellDelegate <NSObject>

@required

- (void)openFollowOwnerWithTag:(NSInteger)tag;

@end