//
//  NLDiscoverUserSwipeCellView.h
//  Yummmie
//
//  Created by bot on 2/26/15.
//  Copyright (c) 2015 Noiselab Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, NLDiscoverUserSwipeCellDirection) {
    NLDiscoverUserSwipeCellDirectionLeft = 0,
    NLDiscoverUserSwipeCellDirectionCenter,
    NLDiscoverUserSwipeCellDirectionRight
};

@protocol NLDiscoverUserSwipeCellViewDelegate;


@interface NLDiscoverUserSwipeCellView : UITableViewCell

@property (nonatomic, readwrite, weak) id<NLDiscoverUserSwipeCellViewDelegate>delegate;

@property (nonatomic, strong) UIButton *dishButton;
@property (nonatomic, strong) UIImageView *firstDish;
@property (nonatomic, strong) UIImageView *secondDish;
@property (nonatomic, strong) UIImageView *thirdDish;
@property (weak, nonatomic) IBOutlet UIButton *userPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *usernameButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;

@end

#pragma mark - NLDiscoverUserSwipeCellViewDelegate

@protocol NLDiscoverUserSwipeCellViewDelegate <NSObject>


@required

- (void)openDetailWithTag:(NSInteger)tag;
- (void)openDishWithTag:(NSInteger)tag;
@end