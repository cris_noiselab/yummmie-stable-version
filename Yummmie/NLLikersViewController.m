//
//  NLLikersViewController.m
//  Yummmie
//
//  Created by bot on 8/13/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import "NLLikersViewController.h"
#import "NLLikersViewCell.h"
#import "NLYum.h"
#import "NLUser.h"
#import "NLProfileViewController.h"
#import "NLSession.h"

static NSString * const kLikersviewCellIdentifier = @"kNLLikersviewCellIdentifier";

@interface NLLikersViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) NLYum *yum;
@property (weak, nonatomic) IBOutlet UITableView *userList;
@property (nonatomic) NSInteger totalUsers;
@property (nonatomic, strong) NSArray *users;

@end

@implementation NLLikersViewController
{
	NSInteger option;
	NSInteger currentPage;
}

- (id)initWithYum:(NLYum *)yum andOption:(NSInteger)opt
{
    if (self = [super initWithNibName:NSStringFromClass([NLLikersViewController class]) bundle:nil])
    {
        _yum = yum;
        self.edgesForExtendedLayout = UIRectEdgeNone;
        _users = [NSArray array];
			option = opt;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	currentPage = 1;
    [self showLoadingSpinner];
    
    [[self userList] registerNib:[UINib nibWithNibName:NSStringFromClass([NLLikersViewCell class]) bundle:nil] forCellReuseIdentifier:kLikersviewCellIdentifier];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    [footerView setBackgroundColor:[UIColor greenColor]];
    [self.userList setTableFooterView:footerView];
    
    [[self userList] setDelegate:self];
    [[self userList] setDataSource:self];
    
    
    __weak NLLikersViewController *weakSelf = self;
	
	NSInteger yumId = self.yum.yumId;
	if (self.yum.repost)
	{
		yumId = [[self.yum.repost objectForKey:@"original_yum_id"] integerValue];
	}
	switch (option)
	{
		case 1: //likers
		{
			self.title = NSLocalizedString(@"likers_view_name", nil);

			[NLYum getLikersForYum:yumId page:currentPage withCompletionBlock:^(NSError *error, NSArray *users, NSInteger count) {
				
				if (!error)
				{
					[weakSelf setUsers:users];
					dispatch_async(dispatch_get_main_queue(), ^{
						
						if ([[weakSelf users] count] > 0 )
						{
							[[weakSelf userList] reloadData];
						}else
						{
							//[weakSelf showErrorMessage:NSLocalizedString(@"no_likers_text", nil) withDuration:2.0f];
						}
					});
				}else
				{
					[weakSelf showErrorMessage:NSLocalizedString(@"connection_error", nil) withDuration:2.0f];
				}
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf hideLoadingSpinner];
				});
				
			}];
		}
			break;
			
		case 2: //favers
		{
			self.title = NSLocalizedString(@"favers_view_name", nil);

			[NLYum getFaversForYum:yumId page:1 withCompletionBlock:^(NSError *error, NSArray *users, NSInteger count) {
				
				if (!error)
				{
					[weakSelf setUsers:users];
					dispatch_async(dispatch_get_main_queue(), ^{
						
						if ([[weakSelf users] count] > 0 )
						{
							[[weakSelf userList] reloadData];
						}else
						{
							//[weakSelf showErrorMessage:NSLocalizedString(@"no_likers_text", nil) withDuration:2.0f];
						}
					});
				}else
				{
					[weakSelf showErrorMessage:NSLocalizedString(@"connection_error", nil) withDuration:2.0f];
				}
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf hideLoadingSpinner];
				});
				
			}];
		}
			break;
		case 3: //reposters
		{
			self.title = NSLocalizedString(@"reposters_view_name", nil);

			[NLYum getRepostersForYum:yumId page:1 withCompletionBlock:^(NSError *error, NSArray *users, NSInteger count) {
				
				if (!error)
				{
					[weakSelf setUsers:users];
					dispatch_async(dispatch_get_main_queue(), ^{
						
						if ([[weakSelf users] count] > 0 )
						{
							[[weakSelf userList] reloadData];
						}else
						{
							switch (option)
							{
								case 1:
									//[weakSelf showErrorMessage:NSLocalizedString(@"no_likers_text", nil) withDuration:2.0f];

									break;
								case 2:
									//[weakSelf showErrorMessage:NSLocalizedString(@"no_likers_text", nil) withDuration:2.0f];

									break;
								case 3:
									//[weakSelf showErrorMessage:NSLocalizedString(@"no_likers_text", nil) withDuration:2.0f];

									break;
								default:
									break;
							}
						}
					});
				}else
				{
					[weakSelf showErrorMessage:NSLocalizedString(@"connection_error", nil) withDuration:2.0f];
				}
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf hideLoadingSpinner];
				});
				
			}];
		}
			break;
			
  default:
			break;
	}
	
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGRect frame = self.navigationController.navigationBar.frame;
    frame.origin.y = 20.0f;
    [self.navigationController.navigationBar setFrame:frame];
    self.navigationItem.titleView.alpha  = 1.0;
        //Google Analytics
        NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self userList] deselectRowAtIndexPath:[[self userList] indexPathForSelectedRow] animated:YES];
    
	/* NSDictionary *analyticsDict  = @{
                                     kNLAnalyticsSectionNameKey:kNLAnalyticsLikesSection,
                                     kNLAnalyticsYumId:[NSString stringWithFormat:@"%ld",(long)[[self yum] yumId]],
                                     kNLAnalyticsYumOwnerKey:([[[self yum] user] username])?[[[self yum] user] username]:@"null",
                                     kNLAnalyticsUsernameKey:[[NLSession currentUser] username]
                                     };
	 */
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self users] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLLikersViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kLikersviewCellIdentifier forIndexPath:indexPath];
    NLUser *user = [[self users] objectAtIndex:indexPath.row];
    
    [[cell userNameLabel] setText:[NSString stringWithFormat:@"@%@",user.username]];
    [[cell nameLabel] setText:[user name]];
    [[cell userPhoto] sd_setImageWithURL:[NSURL URLWithString:[user thumbAvatar]] placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 57.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NLUser *user = [[self users] objectAtIndex:indexPath.row];
    
    NLProfileViewController *profileVC = [[NLProfileViewController alloc] initWithUser:user isProfileUser:NO];
    [profileVC setHidesBottomBarWhenPushed:YES];
    [[self navigationController] pushViewController:profileVC animated:YES];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(void)viewDidLayoutSubviews
{
    if ([self.userList respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.userList setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.userList respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.userList setLayoutMargins:UIEdgeInsetsZero];
    }
}
@end
