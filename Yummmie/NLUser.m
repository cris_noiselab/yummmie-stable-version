//
//  NLUser.m
//  Yummmie
//
//  Created by bot on 7/10/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <SDWebImage/SDImageCache.h>

#import "NLUser.h"
#import "NLConstants.h"
#import "NLSession.h"
#import "NLVenue.h"
#import "NLAuthentication.h"
#import "NLSuggestedUser.h"
#import "NLYum.h"
#import "NLNotifications.h"

#define NSUINT_BIT (CHAR_BIT * sizeof(NSUInteger))
#define NSUINTROTATE(val, howmuch) ((((NSUInteger)val) << howmuch) | (((NSUInteger)val) >> (NSUINT_BIT - howmuch)))

@interface AFHTTPSessionManager (PATCH_MULTIPART)

- (NSURLSessionDataTask *)PATCH:(NSString *)URLString
										 parameters:(id)parameters
			constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
												success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
												failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
@end

static NSString * const kUserAferKey = @"after_id";
static NSString * const kUserBeforeKey = @"before_id";
static NSString * const kUserQueryKey = @"username";
static NSString * const kUserTermsKey = @"terms";

@implementation AFHTTPSessionManager (PATCH_MULTIPART)

-(NSURLSessionDataTask *)PATCH:(NSString *)URLString parameters:(id)parameters constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure
{
	NSMutableURLRequest *request = [self.requestSerializer multipartFormRequestWithMethod:@"PATCH" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters constructingBodyWithBlock:block error:nil];
	
	__block NSURLSessionDataTask *task = [self uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
		if (error) {
			if (failure) {
				failure(task, error);
			}
		} else {
			if (success) {
				success(task, responseObject);
			}
		}
	}];
	
	[task resume];
	
	return task;
}

@end

static NSString * const kUserKey = @"user";
static NSString * const kUserEmailKey = @"user[email]";
static NSString * const kUserUsernameKey = @"user[username]";
static NSString * const kUserNameKey = @"user[name]";
static NSString * const kUserPasswordKey = @"user[password]";
static NSString * const kUserPasswordConfirmationKey = @"user[password_confirmation]";
static NSString * const kUserAvatarPhotoKey = @"user[avatar]";
static NSString * const kUserCoverPhotoKey = @"user[cover]";
static NSString * const kUserRelationshipKey = @"relationship[followed_id]";
static NSString * const kUserWebsiteKey = @"user[website]";
static NSString * const kUserBioKey = @"user[bio]";
static NSString * const kUserActivityKey = @"user[venue_user]";

@implementation NLUser
- (void) encodeWithCoder:(NSCoder *)encoder {
	
	[encoder encodeObject:_username forKey:@"username"];
	[encoder encodeInteger:_userID forKey:@"id"];
	[encoder encodeObject:_name forKey:@"name"];
	[encoder encodeObject:_thumbAvatar forKey:@"thumbA"];
	[encoder encodeObject:_avatar forKey:@"avatar"];

}

- (id)initWithCoder:(NSCoder *)decoder
{
	//NSString *username = [decoder decodeObjectForKey:@"username"];
	//NSString *username = [decoder decodeObjectForKey:@"username"];
	
	NSDictionary *dictionary = @{@"username": [decoder decodeObjectForKey:@"username"], @"id": [NSNumber numberWithInteger:[decoder decodeIntegerForKey:@"id"]], @"name": [decoder decodeObjectForKey:@"name"], @"avatar": [decoder decodeObjectForKey:@"avatar"]};
	
	return [self initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	if (self = [super init])
	{
        NSLog(@"%d", [[dictionary valueForKey:@"venue_user"] boolValue]);
		_name = [dictionary valueForKey:@"name"];
		_username = [dictionary valueForKey:@"username"];
        _verified = [[dictionary objectForKey:@"verified"] boolValue];
		_email = [dictionary valueForKey:@"email"];
		_bio = ([NSNull null]==[dictionary valueForKey:@"bio"])?nil:[dictionary valueForKey:@"bio"];
		_avatar = [dictionary valueForKey:@"avatar"];
		_thumbAvatar = [dictionary valueForKey:@"avatar"];
		_bigAvatar = [dictionary valueForKey:@"avatar"];
		_cover = [dictionary valueForKey:@"cover"];
		_website = ([NSNull null]==[dictionary valueForKey:@"website"])?nil:[dictionary valueForKey:@"website"];
		_authToken = [[dictionary objectForKey:@"profile"] valueForKey:@"auth_token"];
		_userID = [[dictionary valueForKey:@"id"] integerValue];
		
		
		//Profile_count
		NSDictionary *profileCounts = [dictionary objectForKey:@"profile_counts"];
		
		_yummmies = [[profileCounts valueForKey:@"yums_count"] intValue];
		
		_following = [[profileCounts valueForKey:@"following_count"] intValue];
		_followers = [[profileCounts valueForKey:@"followers_count"] intValue];
		
		
		_follow = [[dictionary valueForKey:@"follow"] boolValue];
		_followBack = [[dictionary valueForKey:@"follow_back"] boolValue];
		
		id authenticationList = [dictionary valueForKey:@"authentications"];
		NSMutableArray *authentications = [NSMutableArray array];
		if ([authenticationList isKindOfClass:[NSArray class]])
		{
			if ([authenticationList count] > 0)
			{
				for (id authDict in authenticationList)
				{
					if ([authDict isKindOfClass:[NSDictionary class]])
					{
						NLAuthentication *authentication = [[NLAuthentication alloc] initWithDictionary:authDict];
						[authentications addObject:authentication];
					}
				}
			}
			
		}
		_authentications = authentications;
	}
	
	return self;
}

- (BOOL)isEqual:(id)object
{
	if ([object isKindOfClass:[NLUser class]])
	{
		return [[self username] isEqualToString:[object username]];
	}else
	{
		return NO;
	}
}

- (NSUInteger)hash
{
	return NSUINTROTATE([_username hash], NSUINT_BIT / 2) ^ [_email hash];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"user: %@, name: %@, id:%ld, token: %@",self.username,self.name,(long)self.userID,self.authToken];
}

+ (NSURLSessionDataTask *)registerUserWithEmail:(NSString *)email name:(NSString *)name username:(NSString *)username activity:(BOOL)activity password:(NSString *)password coverImage:(UIImage *)coverImage profileImage:(UIImage *)profileImage withCompletionBlock:(void (^)(NSError * error, NLUser *user))block;
{
	NSDictionary *params = @
    {
        kUserEmailKey:email,
        kUserUsernameKey:username,
        kUserNameKey:name,
        kUserPasswordKey:password,
        kUserPasswordConfirmationKey:password,
        kUserActivityKey: [NSNumber numberWithBool:activity]
    };
	
	return [[NLYummieApiClient sharedClient] POST:@"signup" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
					{
						if (coverImage)
						{
							[formData appendPartWithFileData:UIImageJPEGRepresentation(coverImage, 0.5) name:kUserCoverPhotoKey fileName:@"cover.png" mimeType:kPNGMimeType];
						}
						if (profileImage)
						{
							[formData appendPartWithFileData:UIImageJPEGRepresentation(profileImage, 0.5) name:kUserAvatarPhotoKey fileName:@"avatar.png" mimeType:kPNGMimeType];
						}
						
					} success:^(NSURLSessionDataTask *task, id responseObject)
					{
						NSDictionary *userDict = [responseObject objectForKey:kUserKey];
						
						if (block)
						{
							if (userDict)
							{
								NLUser *user = [[NLUser alloc] initWithDictionary:userDict];
								block(nil,user);
							}else
							{
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error) {
						NSLog(@"%@",error);
						if (block)
						{
							block(error, nil);
						}
					}];
	
}
+ (NSURLSessionDataTask *)registerUserWithFacebookToken:(NSString *)token email:(NSString *)email activity:(BOOL)activity withCompletionBlock:(void (^)(NSError * error, NLUser *user))block
{
	NSDictionary *params = @{
													 kUserEmailKey:email,
													 @"user[facebook_access_token]": token
													 };

	if ([email isEqualToString:@""])
	{
		 params = @{
			@"user[facebook_access_token]": token,
            kUserActivityKey: [NSNumber numberWithBool:activity]
		};

	}
	
	return [[NLYummieApiClient sharedClient] POST:@"fbsignup" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
	{
		NSDictionary *userDict = [responseObject objectForKey:kUserKey];
		if (block)
		{
			if (userDict)
			{
				NLUser *user = [[NLUser alloc] initWithDictionary:userDict];
				block(nil,user);
			}else
			{
				block([NSError new], nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",error.localizedDescription);
		if (block)
		{
			block(error, nil);
		}
	}];
	
	
}

+ (NSURLSessionDataTask *)loginWithFacebookToken:(NSString *)facebookToken withCompletionBlock:(void (^)(NSError *error,NLUser *user))block
{
	NSDictionary *params = @{
													 @"facebook_access_token":facebookToken													 };
	
	return [[NLYummieApiClient sharedClient] POST:@"fblogin" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (responseObject)
						{
							NSDictionary *userDict = [responseObject objectForKey:kUserKey];
							
							if (block)
							{
								if (userDict)
								{
									NLUser *loggedUser = [[NLUser alloc] initWithDictionary:userDict];
									block(nil,loggedUser);
								}else
								{
									block([NSError new],nil);
								}
							}
						}else
						{
							block([NSError new],nil);
						}
						
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
						NSLog(@"\n");
						NSLog(@"%@",[[NSString alloc] initWithData:[[task originalRequest] HTTPBody] encoding:NSUTF8StringEncoding]);
						NSLog(@"\n");
						block(error,nil);
					}];
}

+ (NSURLSessionDataTask *)loginWithEmail:(NSString *)email password:(NSString *)password withCompletionBlock:(void (^)(NSError * error, NLUser *user))block
{
	NSDictionary *params = @{
													 @"email":email,
													 @"password":password
													 };
	return [[NLYummieApiClient sharedClient] POST:@"login" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if ([[responseObject objectForKey:@"success"] boolValue])
						{
							NSDictionary *userDict = [responseObject objectForKey:kUserKey];
							
							if (block)
							{
								if (userDict)
								{
									NLUser *loggedUser = [[NLUser alloc] initWithDictionary:userDict];
									block(nil,loggedUser);
								}else
								{
									block([NSError new],nil);
								}
							}
						}else
						{
							NSLog(@"login\n%@", responseObject);

							block([NSError new],nil);
						}
						
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
						NSLog(@"\n");
						NSLog(@"%@",[[NSString alloc] initWithData:[[task originalRequest] HTTPBody] encoding:NSUTF8StringEncoding]);
						NSLog(@"\n");
						block(error,nil);
					}];
}

+ (NSURLSessionDataTask *)getUser:(NSInteger)userID withCompletionBlock:(void (^)(NSError *, NLUser *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSString *apiCallPath = [NSString stringWithFormat:@"users/%ld",(long)userID];
	return [[NLYummieApiClient sharedClient] GET:apiCallPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
		if (responseObject)
		{
			NSDictionary *userDict = [responseObject valueForKey:kUserKey];
			if (block)
			{
				if (userDict)
				{
					NLUser *loggedUser = [[NLUser alloc] initWithDictionary:userDict];
					block(nil,loggedUser);
				}else
				{
					block([NSError new],nil);
				}
			}
		}else
		{
			block([NSError new],nil);
			
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		block(error,nil);
	}];
}

+ (NSURLSessionDataTask *)getUserWithUsername:(NSString *)username withCompletionBlock:(void (^)(NSError *, NLUser *))block
{
 
 [[NLYummieApiClient sharedClient] requestSerializer];
 [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
 
 NSString *apiCallPath = [[NSString stringWithFormat:@"users/%@",username] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 return [[NLYummieApiClient sharedClient] GET:apiCallPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
	 if (block)
	 {
		 if (responseObject)
		 {
			 NSDictionary *userDict = [responseObject valueForKey:kUserKey];
			 if (userDict)
			 {
				 NLUser *loggedUser = [[NLUser alloc] initWithDictionary:[responseObject valueForKey:kUserKey]];
				 block(nil,loggedUser);
			 }else
			 {
				 block([NSError new],nil);
			 }
		 }else
		 {
			 block([NSError new],nil);
		 }
	 }
 } failure:^(NSURLSessionDataTask *task, NSError *error) {
	 block(error,nil);
 }];
}

+ (NSURLSessionDataTask *)getFollowersFromUser:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *yums, NSInteger currentPage, NSInteger totalPages))block
{
	
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/followers",(long)userId];
	
	NSDictionary *params = @{@"page": [NSNumber numberWithInteger:page]};
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"users"] count]>0)
								{
									NSMutableArray *users = [NSMutableArray array];
									for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
									{
										NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
										[users addObject:user];
									}
									
									block(nil,users, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"total_pages"] integerValue]);
								}else
								{
									block([NSError new],nil, 1, 10);
								}
							}else
							{
								NSLog(@"%@\n%@", apiPath, responseObject);
								block([NSError new],nil, 1, 10);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						if (block)
						{
							block(error,nil, 1, 10);
						}
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
					}];
}

+ (NSURLSessionDataTask *)getFollowings:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *followings, NSInteger currentPage, NSInteger totalPages))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/following",(long)userId];
	NSDictionary *params = @{@"page": [NSNumber numberWithInteger:page]};

	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"users"] count]>0)
								{
									NSMutableArray *users = [NSMutableArray array];
									for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
									{
										NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
										[users addObject:user];
									}
									
									block(nil,users, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"total_pages"] integerValue]);
								}else
								{
									block([NSError new],nil, 1, 10);
								}
							}else
							{
								NSLog(@"%@\n%@", apiPath, responseObject);
								block([NSError new],nil, 1, 10);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						if (block)
						{
							block(error,nil, 1, 10);
						}
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
					}];
}
+ (NSURLSessionDataTask *)getFavsBeforeId:(NSInteger)beforeId withCompletionBlock:(void (^)(NSError *error, NSArray *yums))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSLog(@"%ld \n %@", [NLSession getUserID], [NLSession accessToken]);
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/favs",(long)[NLSession getUserID]];
	NSDictionary *params;
	if (beforeId != 0)
	{
		 params = @{@"before_id": [NSNumber numberWithInteger:beforeId]};

	}
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"mini_yums"] count]>0)
								{
									NSMutableArray *yums = [NSMutableArray array];
									for (NSDictionary *yumDict in [responseObject objectForKey:@"mini_yums"])
									{
										NLYum *yum = [[NLYum alloc] initWithDictionary:yumDict];
										[yums addObject:yum];
									}
									
									block(nil, yums);
								}else
								{
									block([NSError new],nil);
								}
							}else
							{
								NSLog(@"%@\n%@", apiPath, responseObject);
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);

						if (block)
						{
							block(error,nil);
						}
					}];
}
+ (NSURLSessionDataTask *)getFavsAfterId:(NSInteger)afterId withCompletionBlock:(void (^)(NSError *error, NSArray *yums))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	//NSLog(@"%ld \n %@", [NLSession getUserID], [NLSession accessToken]);
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/favs",(long)[NLSession getUserID]];
	NSDictionary *params;
	if (afterId != 0)
	{
		params = @{@"after_id": [NSNumber numberWithInteger:afterId]};
		
	}
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"mini_yums"] count]>0)
								{
									NSMutableArray *yums = [NSMutableArray array];
									for (NSDictionary *yumDict in [responseObject objectForKey:@"mini_yums"])
									{
										NLYum *yum = [[NLYum alloc] initWithDictionary:yumDict];
										[yums addObject:yum];
									}
									
									block(nil, yums);
								}else
								{
									block([NSError new],nil);
								}
							}else
							{
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
						
						if (block)
						{
							block(error,nil);
						}
					}];
}
+ (NSURLSessionDataTask *)unfollowUser:(NSInteger)userId withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/follow_user"];
	NSDictionary *params = @{@"follower_id": [NSNumber numberWithInteger:userId], @"follow": @"false"};
	return [[NLYummieApiClient sharedClient] POST:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			[[NSNotificationCenter defaultCenter] postNotificationName:@"ShouldReloadAll" object:nil];

			block(nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
		if(block)
		{
			block(error);
		}
	}];
}

+ (NSURLSessionDataTask *)followUser:(NSInteger)userId withCompletionBlock:(void (^)(NSError *))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSDictionary *params = @{@"follower_id": [NSNumber numberWithInteger:userId], @"follow": @"true"};
	
	return [[NLYummieApiClient sharedClient] POST:@"users/follow_user" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			[[NSNotificationCenter defaultCenter] postNotificationName:@"ShouldReloadAll" object:nil];

			block(nil);
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		if (block)
		{
			block(error);
		}
	}];
}

+ (NSURLSessionDataTask *)updateUserWithUserId:(NSInteger)userId name:(NSString *)name website:(NSString *)website bio:(NSString *)bio coverPhoto:(UIImage *)coverImage avatarPhoto:(UIImage *)avatarImage email:(NSString *)email username:(NSString *)username withCompletionBlock:(void (^)(NSError *error, NLUser *user))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld",(long)userId];
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	
	if (name)
	{
		[params setObject:name forKey:kUserNameKey];
	}
	
	if (website)
	{
		[params setObject:website forKey:kUserWebsiteKey];
	}
	
	if (bio)
	{
		[params setObject:bio forKey:kUserBioKey];
	}
	
	if (email)
	{
		[params setObject:email forKeyedSubscript:kUserEmailKey];
	}
	
	if (username)
	{
		[params setObject:username forKeyedSubscript:kUserUsernameKey];
	}
	
	BOOL updateCoverImage = (coverImage)?YES:NO;
	BOOL updateAvatarImage = (avatarImage)?YES:NO;
	
	return [[NLYummieApiClient sharedClient] PATCH:apiPath parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
		
		if (updateCoverImage)
			
		{
			
			[formData appendPartWithFileData:UIImageJPEGRepresentation(coverImage, 0.5) name:kUserCoverPhotoKey fileName:@"cover.png" mimeType:kPNGMimeType];
		}
		if (updateAvatarImage)
		{
			[formData appendPartWithFileData:UIImageJPEGRepresentation(avatarImage, 0.5) name:kUserAvatarPhotoKey fileName:@"avatar.png" mimeType:kPNGMimeType];
		}
		
	} success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if (responseObject)
							{
								NSDictionary *userDict = [responseObject valueForKey:kUserKey];
								if (userDict)
								{
									NLUser *loggedUser = [[NLUser alloc] initWithDictionary:[responseObject valueForKey:kUserKey]];
									if (updateCoverImage) {
										[[SDImageCache sharedImageCache] removeImageForKey:loggedUser.cover fromDisk:YES];
									}
									
									if (updateAvatarImage) {
										[[SDImageCache sharedImageCache] removeImageForKey:loggedUser.avatar fromDisk:YES];
										[[SDImageCache sharedImageCache] removeImageForKey:loggedUser.thumbAvatar fromDisk:YES];
										[[SDImageCache sharedImageCache] removeImageForKey:loggedUser.bigAvatar fromDisk:YES];
									}
									
									block(nil,loggedUser);
								}else
								{
									block([NSError new], nil);
								}
							}else
							{
								block([NSError new], nil);
							}
						}
						
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
						if (block)
						{
							block(error,nil);
						}
					}];
}

+ (NSURLSessionDataTask *)searchUserWithString:(NSString *)string afterUserId:(NSInteger)userId withCompletionBlock:(void (^)(NSError *, NSArray *))block
{
	NSString *apiPath = @"users/search";
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	
	if (userId != 0)
	{
		[params setObject:@(userId) forKey:kUserAferKey];
	}
	[params setObject:string forKey:kUserQueryKey];
	
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				NSMutableArray *users = [NSMutableArray array];
				for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
				{
					NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
					[users addObject:user];
				}
				
				block(nil, [NSArray arrayWithArray:users]);
			}
			else
			{
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];
}

+ (NSURLSessionDataTask *)getFollowersFromUser:(NSInteger)userId beforeUserId:(NSInteger)followerId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block
{
	
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/followers",(long)userId];
	NSDictionary *params = nil;
	if(followerId > 0)
	{
		params = @{kUserBeforeKey:[NSNumber numberWithInteger:followerId]};
	}
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"users"] count]>0)
								{
									NSMutableArray *users = [NSMutableArray array];
									for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
									{
										NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
										[users addObject:user];
									}
									
									block(nil,users);
								}
								else
								{
									block([NSError new],nil);
								}
							}else
							{
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						if (block)
						{
							block(error,nil);
						}
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
					}];
}
+ (NSURLSessionDataTask *)getFollowersFromUser:(NSInteger)userId afterUserId:(NSInteger)followerId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/followers",(long)userId];
	NSDictionary *params = nil;
	if (followerId != 0) {
		params = @{kUserAferKey:[NSNumber numberWithInteger:followerId]};
	}
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"users"] count]>0)
								{
									NSMutableArray *users = [NSMutableArray array];
									for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
									{
										NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
										[users addObject:user];
									}
									
									block(nil,users);
								}else
								{
									block([NSError new],nil);
								}
							}
							else
							{
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						if (block)
						{
							block(error,nil);
						}
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
					}];
}

+ (NSURLSessionDataTask *)getFollowingsFromUser:(NSInteger)userId page:(NSInteger)page withCompletionBlock:(void (^)(NSError *error, NSArray *users, NSInteger currentPage, NSInteger totalPages))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/following",(long)userId];
	NSDictionary *params = @{@"page":[NSNumber numberWithInteger:page]};
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([[responseObject objectForKey:@"users"] count]>0)
				{
					NSMutableArray *users = [NSMutableArray array];
					for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
					{
						NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
						[users addObject:user];
					}
					
					block(nil,users, [[responseObject objectForKey:@"current_page"] integerValue], [[responseObject objectForKey:@"total_pages"] integerValue]);
				}
				else
				{
					block([NSError new],nil, 1, 10);
				}
			}else
			{
				block([NSError new],nil, 1, 10);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		if (block)
		{
			block(error,nil, 1, 10);
		}
		NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
	}];
}
+ (NSURLSessionDataTask *)findFriends:(NSString *)provider withCompletionBlock:(void (^)(NSError *error, NSArray *users))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/friends"];
	
	NSDictionary *params = @{@"provider": provider};
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
	{
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([[responseObject objectForKey:@"users"] count]>0)
				{
					NSMutableArray *users = [NSMutableArray array];
					for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
					{
						NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
						[users addObject:user];
					}
					
					block(nil,users);
				}else
				{
					block([NSError new],nil);
				}
			}
			else
			{
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		if (block)
		{
			block(error,nil);
		}
		NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
	}];
}
+ (NSURLSessionDataTask *)getFollowingsFromUser:(NSInteger)userId afterUserId:(NSInteger)followingId withCompletionBlock:(void (^)(NSError *error, NSArray *users))block
{
	
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/following",(long)userId];
	
	NSDictionary *params = nil;
	if (followingId > 0) {
		params = @{kUserAferKey:[NSNumber numberWithInteger:followingId]};
	}
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
		
		if (block)
		{
			if ([[responseObject objectForKey:@"success"] boolValue] == true)
			{
				if ([[responseObject objectForKey:@"users"] count]>0)
				{
					NSMutableArray *users = [NSMutableArray array];
					for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
					{
						NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
						[users addObject:user];
					}
					
					block(nil,users);
				}else
				{
					block([NSError new],nil);
				}
			}
			else
			{
				NSLog(@"%@\n%@", apiPath, responseObject);
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		if (block)
		{
			block(error,nil);
		}
		NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
	}];
}

+ (NSURLSessionDataTask *)resetPasswordWithEmail:(NSString *)email andCompletionBlock:(void (^)(NSError *, BOOL))block
{
	NSString *path = @"password_resets";
	NSDictionary *params = @{@"email":email};
	
	return [[NLYummieApiClient sharedClient] POST:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
	{
		if ([[responseObject objectForKey:@"success"] boolValue])
		{
			if ([[responseObject objectForKey:@"message"] isEqualToString:@"USER NOT FOUND"])
			{
				if (block)
				{
					block(nil, NO);
				}
			}
			else
			{
				if (block)
				{
					block(nil, YES);
				}
			}
		}
		else
		{
			if (block)
			{
				block([NSError new],NO);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						if (block)
						{
							block(error,NO);
						}
					}];
}

+ (NSURLSessionDataTask *)randomUsersWithCompletionBlock:(void (^)(NSError * error, NSArray *users))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = @"users/random";
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
	{
		if([[responseObject objectForKey:@"success"] boolValue] == true)
		{
			if (block)
			{
				NSMutableArray *suggestedUsers = [NSMutableArray array];
				
				for (NSDictionary *userData in [responseObject objectForKey:@"users"])
				{
					
					NLSuggestedUser *suggested = [[NLSuggestedUser alloc] init];
					
					NLUser *user = [[NLUser alloc] initWithDictionary:[userData objectForKey:@"user"]];
					
					NSArray *topYums = [[userData objectForKey:@"user"] objectForKey:@"mini_yums"];
					[suggested setUser:user];
					
					if ([topYums count]>0)
					{
						NSMutableArray *yums = [NSMutableArray array];
						
						for (NSDictionary *yumDict in topYums)
						{
							NLYum *yum = [[NLYum alloc] initWithDictionary:yumDict];
							[yums addObject:yum];
						}
						[suggested setYummmies:yums];
					}
					else
					{
						[suggested setYummmies:nil];
					}
					
					[suggestedUsers addObject:suggested];
				}
				block(nil, [NSArray arrayWithArray:suggestedUsers]);
			}
			
		}
		else
		{
			NSLog(@"%@\n%@", apiPath, responseObject);
			if (block)
			{
				block([NSError new],nil);
			}
		}
	} failure:^(NSURLSessionDataTask *task, NSError *error) {
		NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
		if (block)
		{
			block(error,nil);
		}
	}];
	
}

+ (NSURLSessionDataTask *)getSuggestedFollowersFromUser:(NSInteger)userId andQuery:(NSString *)query withCompletionBlock:(void (^)(NSError *error, NSArray *followings))block
{
	[[NLYummieApiClient sharedClient] requestSerializer];
	[NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
	
	NSString *apiPath = [NSString stringWithFormat:@"users/%ld/following",(long)userId];
	
	NSDictionary *params;
	
	if (query) {
		params = @{
							 kUserTermsKey:query
							 };
	}
	
	return [[NLYummieApiClient sharedClient] GET:apiPath parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
					{
						if (block)
						{
							if ([[responseObject objectForKey:@"success"] boolValue] == true)
							{
								if ([[responseObject objectForKey:@"users"] count]>0)
								{
									NSMutableArray *users = [NSMutableArray array];
									for (NSDictionary *userDict in [responseObject objectForKey:@"users"])
									{
										NLUser *user = [[NLUser alloc] initWithDictionary:[userDict objectForKey:@"user"]];
										[users addObject:user];
									}
									
									block(nil,users);
								}else
								{
									block(nil,nil);
								}
							}else
							{
								NSLog(@"%@\n%@", apiPath, responseObject);
								block([NSError new],nil);
							}
						}
					} failure:^(NSURLSessionDataTask *task, NSError *error)
					{
						if (block)
						{
							block(error,nil);
						}
						NSLog(@"%@",[error.userInfo objectForKey:JSONResponseSerializerWithDataKey]);
					}];
	
}
- (NSURLSessionDataTask *)destroyWithCompletionBlock:(void (^)(NSError *))block
{
    [[NLYummieApiClient sharedClient] requestSerializer];
    [NLYummieApiClient setValue:[NSString stringWithFormat:@"Token token=%@",[NLSession accessToken]] forHeaderField:@"Authorization"];
    
    NSString *apiPath = [NSString stringWithFormat:@"users/%ld",(long)self.userID];
    return [[NLYummieApiClient sharedClient] DELETE:apiPath parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
            {
                if (block)
                {
                    block(nil);
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                NSLog(@"%@",[error.userInfo objectForKeyedSubscript:JSONResponseSerializerWithDataKey]);
                if (block)
                {
                    block(error);
                }
            }];
}
@end
