//
//  AppDelegate.m
//  Yummmie
//
//  Created by bot on 5/30/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import <Google/Analytics.h>

#import "AppDelegate.h"
#import "NLGetStartedViewController.h"
#import "UIColor+NLColor.h"
#import "UIButton+NLButton.h"
#import "NLNotifications.h"
#import "NLTimeLineViewController.h"
#import "NLNavigationViewController.h"
#import "NLProfileViewController.h"
#import "NLTabBarController.h"
#import "NLSession.h"
#import "NLUser.h"
#import "Foursquare2.h"
#import "NLInteractionViewContoroller.h"
#import "NLDashboardViewController.h"
#import "Flurry.h"
#import "NLUser.h"
#import "NLDiscoverCenterViewController.h"
#import "NLPushNotificationConstants.h"
#import "NLPhotoDetailViewController.h"
#import "NLCommentsViewController.h"
#import "NLPreferences.h"
#import "NLTemporalViewController.h"
#import "NLInteraction.h"
#import "NLVenueViewController.h"
#import "NLVenue.h"
#import "DCTReporter.h"
#import "ACTReporter.h"
#import "NLBlurredRateView.h"

#import "NLYum.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>


@interface AppDelegate ()

@property (nonatomic, strong) NLTabBarController *tabBarController;
@property (nonatomic, strong) NLProfileViewController *profileViewController;

- (void)userLogedIn:(NSNotification *)notification;
- (void)logOut;
- (void)showInteractions;
- (void)handleNotificationsWithActionType:(NLPushNotificationActionType)actionType andEntityId:(NSInteger)entityId appIsLaunching:(BOOL)appIsLaunching;
- (void)openVenue:(NSString *)venueId;

@end

@implementation AppDelegate
{
    NLBlurredRateView *blurView;

}
typedef enum ShortcutIdentifier : NSUInteger
{
    Photo
} ShortcutIdentifier;


- (BOOL)iRateShouldPromptForRating
{
    if (!blurView)
    {
        blurView = [[NLBlurredRateView alloc] initWithFrame:self.tabBarController.view.bounds];
        [blurView configureToRate:self];
        [self.tabBarController.view addSubview:blurView];
    }
    return NO;
}
-(void)rateNow
{
    [iRate sharedInstance].ratedThisVersion = YES;
    
    //launch app store
    [blurView removeFromSuperview];
    blurView = nil;
    [[iRate sharedInstance] openRatingsPageInAppStore];
   
    
}
-(void)rateLater
{
    [iRate sharedInstance].lastReminded = [NSDate date];
    [iRate sharedInstance].eventCount = 0;
    [blurView removeFromSuperview];
    blurView = nil;
    
}
-(void)noRate
{
    [iRate sharedInstance].declinedThisVersion = YES;
    [blurView removeFromSuperview];
    blurView = nil;
    
}
+ (void)initialize
{
    //set the bundle ID. normally you wouldn't need to do this
    //as it is picked up automatically from your Info.plist file
    //but we want to test with an app that's actually on the store
    [iRate sharedInstance].applicationBundleID = @"com.apps.noiselab.Yummmie";
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    //enable preview mode
    [iRate sharedInstance].previewMode = NO;
}

-(void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    if (shortcutItem)
    {
        
			NSString *string = [NSString stringWithFormat:@"%@", [[[shortcutItem type] componentsSeparatedByString:@"."] lastObject]];
			NSLog(@"%@", string);

        if ([string isEqualToString:@"Foto"])
        {
            [self.tabBarController setupCamera];
        }
        if ([string isEqualToString:@"News"])
        {
            [self.tabBarController setSelectedIndex:3];
        }
        if ([string isEqualToString:@"Buscar"])
        {
            [self.tabBarController setSelectedIndex:1];
            NLNavigationViewController *navVc = self.tabBarController.selectedViewController;
            NLDiscoverCenterViewController *searchVC  = [[navVc viewControllers] objectAtIndex:0];
            searchVC.selectAction = 1;
            
        }
        if ([string isEqualToString:@"Cerca"])
        {
            [self.tabBarController setSelectedIndex:4];
            //NLNavigationViewController *navVc = self.tabBarController.selectedViewController;
            //NLDiscoverCenterViewController *searchVC  = [[navVc viewControllers] objectAtIndex:0];
            //searchVC.selectAction = 2;
            
        }
        
    }

}

-(BOOL) application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([[Twitter sharedInstance] application:app openURL:url options:options]) {
        return true;
    }
    
    NSString *sourceApplication = options[UIApplicationOpenURLOptionsSourceApplicationKey];
    return [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url sourceApplication:sourceApplication annotation:nil];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //Twitter.sharedInstance().start(withConsumerKey: "psIvZ1mNFPILiDrePsa6Q87sx", consumerSecret: "8xLrcygq1lWJcbxNiIQTQp05DKkh72vd4zhtsJB1OprI33wxmH");
   
    [[Twitter sharedInstance] startWithConsumerKey:@"psIvZ1mNFPILiDrePsa6Q87sx" consumerSecret:@"8xLrcygq1lWJcbxNiIQTQp05DKkh72vd4zhtsJB1OprI33wxmH"];
    
    
	NSString* currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	NSString* versionOfLastRun = [def objectForKey:@"VersionOfLastRun"];
	
	if (versionOfLastRun == nil)
	{
		/*[def setObject:nil forKey:@"NLSearchScopeUser"];
		[def setObject:nil forKey:@"NLSearchScopeHashtag"];
		[def setObject:nil forKey:@"NLSearchScopeVenue"];
		[def setObject:nil forKey:@"NLSearchScopeDish"];
		 */
		NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
		[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
		// First start after installing the app
	} else if (![versionOfLastRun isEqual:currentVersion])
	{
		//NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
		//[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
		// App was updated since last run
	} else {
		// nothing changed
	}
	[[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:@"VersionOfLastRun"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	
	/*self.oneSignal = [[OneSignal alloc] initWithLaunchOptions:launchOptions
																											appId:@"bbe45e6e-c911-4942-a8ec-feea42a57000"
																				 handleNotification:nil];
	*/
	
	[OneSignal initWithLaunchOptions:launchOptions appId:@"bbe45e6e-c911-4942-a8ec-feea42a57000" handleNotificationAction:^(OSNotificationOpenedResult *result) {
		OSNotificationPayload* payload = result.notification.payload;
		
		NSString *messageTitle = @"";
		
		if (payload.additionalData)
		{
			
			if(payload.title)
				messageTitle = payload.title;
			
			NSDictionary* additionalData = payload.additionalData;
			if (additionalData)
			{
				NSLog(@"additionalData: %@", additionalData);
				NLPushNotificationActionType action = [[additionalData objectForKey:@"action_id"] integerValue];
				NSUInteger entity = [[additionalData objectForKey:@"entity_id"] integerValue];
				NSLog(@"%@", [additionalData objectForKey:@"entity_id"]);
				dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
					[self handleOneSignalNotifWithActionType:action andEntityId:entity appIsLaunching:true];
					
				});
				//[self handleOneSignalNotifWithActionType:action andEntityId:entity appIsLaunching:!isActive];
				
				// Check for and read any custom values you added to the notification
				// This done with the "Additonal Data" section the dashbaord.
				// OR setting the 'data' field on our REST API.
				NSString *customKey = additionalData[@"customKey"];
				if (customKey)
					NSLog(@"customKey: %@", customKey);
			}

		}
		
	}];

    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    //gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    
    [ACTConversionReporter reportWithConversionID:@"935274017" label:@"nNNpCNPNjWEQocz8vQM" value:@"0.00" isRepeatable:NO];
    
    [DCTConversionReporter reportWithConversionID:@"36799" value:nil /* or price of app if desired */ isRepeatable:NO];
    [DCTActivityReporter reportWithAdUnitID:@"/72776159/DFPAudiencePixel" publisherProvidedID:nil customParameters:@{@"dc_seg": @"116324770"}];

    
    //    [[NSNotificationCenter defaultCenter] addObserverForName:nil object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
//        NSLog(@"********************");
//        NSLog(@"Notification name : %@ ",note.name);
//        NSLog(@"Send by object : %@",note.object);
//        NSLog(@"====================");
////        NSLog(@"Info : %@",note.userInfo);
//    }];
	
	
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:50 * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    
    NSInteger entityId = -1;
    NLPushNotificationActionType actionType = NLPushNotificationActionNone;
    
    NSDictionary *remoteNotificationDict = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];

    if (remoteNotificationDict)
    {
			/*
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"DE arranque" message:[NSString stringWithFormat:@"%@",launchOptions] preferredStyle:UIAlertControllerStyleAlert];
        [controller addAction:[UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleCancel handler:nil]];
        [self.tabBarController presentViewController:controller animated:YES completion:nil];
     */
        entityId = [[remoteNotificationDict valueForKey:kNLPushNotificationEntityKey] integerValue];
        actionType = [[remoteNotificationDict valueForKey:kNLPushNotificationActionIdKey] integerValue];
    }
    
    [GMSServices provideAPIKey:@"AIzaSyDt1ADhc6Wl1OVs7Wq9q5X01BmafLHazsA"];
	//[Foursquare2 setupFoursquareWithClientId:@"ZEOTWNKQY5LUR5V15CD50SSEKHGB1C0GUTSNVPJKGQPUFQK1" secret:@"P4VBDJFX0002GLWUB1NVOPRHZKDFGSGI4PI0JPA1DZFSRGZX" callbackURL:@"yummmie://foursquare"];
    
    [NLSession sharedSession];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogedIn:) name:kNLLoginNotification object:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.tabBarController = [[NLTabBarController alloc] init];

    
    NLTimeLineViewController *timeLineController = [[NLTimeLineViewController alloc] initWithScrollableBar:YES];
    
    timeLineController.tabBarItem.image = [[UIImage imageNamed:@"tab_timeline_off_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    timeLineController.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_timeline_on_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    timeLineController.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    NLNavigationViewController *timeLineNav = [[NLNavigationViewController alloc] initWithRootViewController:timeLineController];
    
    
    
    
    
    
    
    //Set the camera View Controller
    UIViewController *cameraVC = [[UIViewController alloc] init];
    [[cameraVC view] setBackgroundColor:[UIColor greenColor]];
    cameraVC.tabBarItem.image = [[UIImage imageNamed:@"tab_camara_off_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    cameraVC.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    //popuar tabs
//    NLTemporalViewController *tempVC = [[NLTemporalViewController alloc] init];
    NLDiscoverCenterViewController *discoverVC = [[NLDiscoverCenterViewController alloc] init];
    discoverVC.tabBarItem.image = [[UIImage imageNamed:@"tab_popular_off_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    discoverVC.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_popular_on_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    discoverVC.tabBarItem.imageInsets = UIEdgeInsetsMake(7, -5, -7, 5);
    
    NLNavigationViewController *popularNav = [[NLNavigationViewController alloc] initWithRootViewController:discoverVC];


    //Interaction Tabs
    NLInteractionViewContoroller *interactionsVC = [[NLInteractionViewContoroller alloc] init];
    interactionsVC.tabBarItem.image = [[UIImage imageNamed:@"tab_news_off_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    interactionsVC.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_news_on_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    interactionsVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    NLNavigationViewController *interactionsNav = [[NLNavigationViewController alloc] initWithRootViewController:interactionsVC];
    
    
    //Set the Profile View Controller
    self.profileViewController = [[NLProfileViewController alloc] initWithUser:nil isProfileUser:YES];
    self.profileViewController.tabBarItem.image = [[UIImage imageNamed:@"tab_dashboard_off_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.profileViewController.tabBarItem.selectedImage = [[UIImage imageNamed:@"tab_dashboard_on_general"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.profileViewController.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);

    NLNavigationViewController *profileNav = [[NLNavigationViewController alloc] initWithRootViewController:self.profileViewController];
    
    self.tabBarController.viewControllers = @[timeLineNav,popularNav,cameraVC,interactionsNav,profileNav];
    
    [self.window setRootViewController:self.tabBarController];
    [self.window makeKeyAndVisible];

    if (![NLSession accessToken])
    {
        [self logOut];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNLLogoutNotification object:nil];
    }else
    {
        //Load the user
        __weak AppDelegate *weakSelf = self;
        
        [NLUser getUser:[NLSession getUserID] withCompletionBlock:^(NSError *error, NLUser *user)
        {
            
            if (!error)
            {
                [NLSession updateUser:user];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLLoadTimeLineNotification object:nil];
                [weakSelf.profileViewController setUser:[NLSession currentUser]];
                
                //Ask for push notification permissions
                
                // Register for Push Notitications, if running iOS 8
                if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
                    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                                    UIUserNotificationTypeBadge |
                                                                    UIUserNotificationTypeSound);
                    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                             categories:nil];
                    [application registerUserNotificationSettings:settings];
                    [application registerForRemoteNotifications];
                } else {
                    // Register for Push Notifications before iOS 8
                    [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                                     UIRemoteNotificationTypeAlert |
                                                                     UIRemoteNotificationTypeSound)];
                }
                [weakSelf handleNotificationsWithActionType:actionType andEntityId:entityId appIsLaunching:YES];
            }else
            {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kNLLoadTimeLineErrorNotification object:nil];
                // esto solía estar comentado pero si se borra un usuario de la base no hacía logout
                [self logOut];
            }
        }];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOut) name:kNLLogoutNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showInteractions) name:kNLShowActivityFeed object:nil];
	if ([FBSDKAccessToken currentAccessToken])
	{
		
	}
    
        

	//[Foursquare2 setupFoursquareWithClientId:@"ZEOTWNKQY5LUR5V15CD50SSEKHGB1C0GUTSNVPJKGQPUFQK1" secret:@"P4VBDJFX0002GLWUB1NVOPRHZKDFGSGI4PI0JPA1DZFSRGZX" callbackURL:@"yummmie://foursquare"];
    
    
    
    UIApplicationShortcutItem *shortCut = [launchOptions valueForKey:UIApplicationLaunchOptionsShortcutItemKey];
    NSLog(@"%@", shortCut);
    if (shortCut)
    {
        NSLog(@"%@", [shortCut type]);
        if ([[shortCut type] isEqualToString:@".Foto"])
        {
            [self.tabBarController setupCamera];
        }
        if ([[shortCut type] isEqualToString:@".News"])
        {
            [self.tabBarController setSelectedIndex:0];
        }
        if ([[shortCut type] isEqualToString:@".Buscar"])
        {
            [self.tabBarController setSelectedIndex:1];
            NLNavigationViewController *navVc = self.tabBarController.selectedViewController;
            NLDiscoverCenterViewController *searchVC  = [[navVc viewControllers] objectAtIndex:0];
            searchVC.selectAction = 1;
            
        }
        if ([[shortCut type] isEqualToString:@".Cerca"])
        {
            [self.tabBarController setSelectedIndex:1];
            NLNavigationViewController *navVc = self.tabBarController.selectedViewController;
            NLDiscoverCenterViewController *searchVC  = [[navVc viewControllers] objectAtIndex:0];
            searchVC.selectAction = 2;

        }

    
    }
	[OneSignal IdsAvailable:^(NSString* userId, NSString* pushToken)
	 {
		 NSLog(@"Player id:%@", userId);
		 NSLog(@"%d, %@", [NLSession containsAuthentication:NLAuthenticationTypePushNotification], pushToken);
		 if (pushToken != nil && ![NLSession containsAuthentication:NLAuthenticationTypePushNotification])
		 {
			 [NLAuthentication addAuthenticationWithType:NLAuthenticationTypePushNotification userID:[NLSession getUserID] token: userId tokenSecret:nil withCompletionBlock:^(NSError *error, NLAuthentication *authentication)
			 {
				 if(error)
				 {
					 //NSLog(@"error %@", error);
				 }
				 else
				 {
					 NSLog(@"%@", authentication);
				 }
			 }];
		 }
		 
	 }];
	[[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    NSMutableArray *viewedIds = [[NSUserDefaults standardUserDefaults] objectForKey:@"viewed"];
    if (viewedIds)
    {
        NSString *stringIds = [viewedIds componentsJoinedByString:@","];
        [NLYum sendViewedYums:stringIds withCompletionBlock:^(NSError *error) {
            if (!error)
            {
                NSMutableArray *viewedIds = [NSMutableArray array];
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:viewedIds forKey:@"viewed"];
                [def synchronize];
            }
        }];
        
               
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	[NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes)
	 {
		 
		 NSDictionary *userInfo  = @{
																 @"newComments":comments,
																 @"newFollowers":followers,
																 @"newLikes":likes
																 };
		 [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification object:nil userInfo:userInfo];
	 }];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
//    if ([NLSession currentUser] && [[NLSession sharedSession] showActivity])
//    {
//        [self showInteractions];
//        [[NLSession sharedSession] setShowActivity:NO];
//    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([url.scheme isEqualToString:@"yummmie"])
    {
        return [Foursquare2 handleURL:url];
    }else if([url.scheme isEqualToString:@"yummmieExtensionApp"])
    {
        //controller/venue?id=%@
//        NSLog(@"%@",url);
        NSString *actionType = [url host];
        if ([actionType isEqualToString:@"controller"] && [url query]) {
            if ([url pathComponents].count > 1) {
                NSString *controller = [[url pathComponents] lastObject];
                
                if ([controller isEqualToString:@"venue"]) {
                    NSString *venueId = [[[url query] componentsSeparatedByString:@"="] lastObject];
                    if (venueId) {
                        [self openVenue:venueId];
                    }
                }
            }
        }
    }else
    {
			return [[FBSDKApplicationDelegate sharedInstance] application:application
																										 openURL:url
																					 sourceApplication:sourceApplication
																									annotation:annotation];
    }
    return NO;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.

}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
	
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
	NSLog(@"Notificacion: %@", userInfo);
	NSDictionary *data = [[userInfo objectForKey:@"custom"] objectForKey:@"a"];
	NLPushNotificationActionType actionType = [[data objectForKey:@"action_id"] integerValue];
	NSInteger entityId = [[data objectForKey:@"entity_id"] integerValue];
	//UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%ld", (long)application.applicationState] message:[NSString stringWithFormat:@"%@", data] preferredStyle:UIAlertControllerStyleAlert];
	

	//[self.tabBarController presentViewController:alert animated:true completion:nil];
	/*if (application.applicationState == UIApplicationStateBackground ||application.applicationState == UIApplicationStateInactive)
	{
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			[self handleOneSignalNotifWithActionType:actionType andEntityId:entityId appIsLaunching:true];

		});
		//[self.tabBarController setSelectedIndex:1];

	}else*/
	if (application.applicationState == UIApplicationStateActive)
	{
		[NLInteraction getResumeWithCompletionBlock:^(NSError *error, NSNumber *followers, NSNumber *comments, NSNumber *likes)
		 {
			 
			 NSDictionary *userInfo  = @{
																	 @"newComments":comments,
																	 @"newFollowers":followers,
																	 @"newLikes":likes
																	 };
			 [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowInteractionPopUpNotification object:nil userInfo:userInfo];
		 }];
	}
}
/*- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
	NSLog(@"Notificacion: %@", userInfo);
    if (application.applicationState == UIApplicationStateInactive)
    {
        NSInteger entityId = [[userInfo valueForKey:kNLPushNotificationEntityKey] integerValue];
        NLPushNotificationActionType actionType = [[userInfo valueForKey:kNLPushNotificationActionIdKey] integerValue];
			[self handleOneSignalNotifWithActionType:actionType andEntityId:entityId appIsLaunching:true];
    }else if (application.applicationState == UIApplicationStateActive)
    {
        NLPushNotificationActionType actionType = [[userInfo valueForKey:kNLPushNotificationActionIdKey] integerValue];
        
        if (actionType == NLPushNotificationActionNone) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNLLoadLatestInteractions object:nil];
        }
    }
}*/

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
	NSLog(@"actividad %@",userActivity.activityType);
	if ([[[userActivity webpageURL] absoluteString] containsString:@"/y/"])
	{
		NSLog(@"el yumid : %@",[[userActivity webpageURL] lastPathComponent]);
		
		NSString *yumId = [[userActivity webpageURL] lastPathComponent];
		
		
		NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithStringYumId:yumId andDismissButton:YES];
		
		NLNavigationViewController *navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
		__weak AppDelegate *weakSelf = self;
		
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			[weakSelf.tabBarController presentViewController:navController animated:YES completion:nil];
		});
		return YES;
	}
    else if ([[[userActivity webpageURL] absoluteString] containsString:@"/v/"])
    {
        NSString *venueId = [[userActivity webpageURL] lastPathComponent];
        [NLVenue getVenueWithId:venueId withCompletionBlock:^(NSError *error, NLVenue *venue) {
            NLVenueViewController *venueVC = [[NLVenueViewController alloc] initWithVenue:venue sharePhoto:false andCloseButton:true];
            //        NLVenueDetailViewController *detailVC = [[NLVenueDetailViewController alloc] initWithVenue:selectedVenue];
            [venueVC setHidesBottomBarWhenPushed:YES];
            NLNavigationViewController *navController = [[NLNavigationViewController alloc] initWithRootViewController:venueVC];
            __weak AppDelegate *weakSelf = self;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.tabBarController presentViewController:navController animated:YES completion:nil];
            });
        }];
       
        return YES;
    }
	return false;
}
#pragma mark - NSNotifications

- (void)userLogedIn:(NSNotification *)notification
{
    [self.tabBarController dismissViewControllerAnimated:YES completion:nil];
    [self.profileViewController setUser:[NLSession currentUser]];

    
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else {
        // Register for Push Notifications before iOS 8
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
                                                         UIRemoteNotificationTypeSound)];
    }
    //MVP Crashlytics
    //[Crashlytics setUserName:[[NLSession currentUser] username]];
    //[Crashlytics setUserEmail:[[NLSession currentUser] email]];
    
}

- (void)logOut
{
    UINavigationController *getStartedFlow = [[UINavigationController alloc] init];
    NLGetStartedViewController *getStartedVC = [[NLGetStartedViewController alloc] initWithNibName:NSStringFromClass([NLGetStartedViewController class]) bundle:nil];
    [getStartedFlow setViewControllers:@[getStartedVC]];
    [self.tabBarController presentViewController:getStartedFlow animated:NO completion:nil];
    [self.tabBarController setSelectedIndex:0];
	[NLAuthentication removeAuthenticationWithType:NLAuthenticationTypePushNotification withCompletionBlock:^(NSError *error) {
		if (error) {
			
		}
	}];
	if ([FBSDKAccessToken currentAccessToken])
	{
		FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
		[login logOut];
	}
	
    
    [NLSession endSession];
}

- (void)showInteractions
{
#warning interactions
    [self.tabBarController setSelectedIndex:3];
}

#pragma mark - Open Venue

- (void)openVenue:(NSString *)venueId
{

//    NLNavigationViewController *navController = [[NLNavigationViewController alloc] initWithRootViewController:venueVC];
//    [[self tabBarController] presentViewController:navController animated:YES completion:nil];
    __weak AppDelegate *weakSelf = self;
    
    [NLVenue getVenueWithId:venueId withCompletionBlock:^(NSError *error, NLVenue *venue) {
        
        if (!error) {
            NLVenueViewController *venueVC = [[NLVenueViewController alloc] initWithVenue:venue sharePhoto:YES andCloseButton:YES];
            NLNavigationViewController *navController = [[NLNavigationViewController alloc] initWithRootViewController:venueVC];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[weakSelf tabBarController] presentViewController:navController animated:YES completion:nil];
            });
        }
    }];
}

#pragma mark - Handle Notifications

-(void)handleOneSignalNotifWithActionType:(NLPushNotificationActionType)actionType andEntityId:(NSInteger)entityId appIsLaunching:(BOOL)appIsLaunching
{
	__weak AppDelegate *weakSelf = self;
	int a = 0;
	int b = 0;
	int c = 0;
	if ([NLSession currentUser] && actionType != NLPushNotificationActionNone)
	{
		a = 1;
		if (appIsLaunching)
		{
			b = 1;
			[self showInteractions];

			//[[NSNotificationCenter defaultCenter] postNotificationName:kNLShowActivityFeed object:nil];
			NLNavigationViewController *navController;
			switch (actionType)
			{
				case NLPushNotificationLikeType:
				{
					//Create the Navigation Controller present from tab bar controller
					NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithYumId:entityId andDismissButton:YES];
					navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
					break;
				}
				case NLPushNotificationCommentType:
				{
					NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithYumId:entityId andDismissButton:YES];
					navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
					break;
				}
				case NLPushNotificationFollowType:
				{
					NLProfileViewController *profileViewController = [[NLProfileViewController alloc] initWithUserId:entityId andCloseButton:YES];
					navController = [[NLNavigationViewController alloc] initWithRootViewController:profileViewController];
					break;
				}
				case NLPushNotificationMentionType:
				{
					NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithYumId:entityId andDismissButton:YES];
					navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
					break;
				}
				case NLPushNotificationRepostType:
				{
					NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithYumId:entityId andDismissButton:YES];
					navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
					break;
				}
				default:
					break;
			}
			if (navController)
			{
				c = 1;
				[weakSelf.tabBarController presentViewController:navController animated:YES completion:nil];

				dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
				{
				});
			}
			

		}
	

	}
	/*
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"data" message:[NSString stringWithFormat:@" %@, %d, %d, %d, %ld, %ld", [NLSession currentUser], a, b, c, (long)actionType, (long)entityId] preferredStyle:UIAlertControllerStyleAlert];
	UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"wtf" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
		[alert dismissViewControllerAnimated:true completion:nil];
	}];
	[alert addAction:aceptar];
																																																																									 //[self.tabBarController presentViewController:alert animated:true completion:nil];
	 */
	

}


- (void)handleNotificationsWithActionType:(NLPushNotificationActionType)actionType andEntityId:(NSInteger)entityId appIsLaunching:(BOOL)appIsLaunching
{
	/*
    __weak AppDelegate *weakSelf = self;
	
    if ([NLSession currentUser] && actionType != NLPushNotificationActionNone)
    {
        [self showInteractions];
			
        if (!appIsLaunching)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNLShowActivityFeed object:nil];
        }
			
        NLNavigationViewController *navController;
        switch (actionType)
        {
            case NLPushNotificationLikeType:
                {
                    //Create the Navigation Controller present from tab bar controller
                    NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithYumId:entityId andDismissButton:YES];
                    navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
                    break;
                }
            case NLPushNotificationCommentType:
                {
                    NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithYumId:entityId andDismissButton:YES];
                    navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
                    break;
                }
            case NLPushNotificationFollowType:
                {
                    NLProfileViewController *profileViewController = [[NLProfileViewController alloc] initWithUserId:entityId andCloseButton:YES];
                    navController = [[NLNavigationViewController alloc] initWithRootViewController:profileViewController];
                    break;
                }
            case NLPushNotificationMentionType:
                {
                    NLPhotoDetailViewController *detailViewController = [[NLPhotoDetailViewController alloc] initWithYumId:entityId andDismissButton:YES];
                    navController = [[NLNavigationViewController alloc] initWithRootViewController:detailViewController];
                    break;
                }
            default:
                break;
        }
        
        if (navController)
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
            {
                [weakSelf.tabBarController presentViewController:navController animated:YES completion:nil];
            });
        }
    }
	 */
}

@end
