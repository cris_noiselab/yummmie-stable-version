//
//  NLSearchFriendsViewController.h
//  Yummmie
//
//  Created by bot on 12/18/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLSearchFriendsViewController : NLBaseViewController

- (instancetype)initWithSkipButton:(BOOL)addSkipButton;

@end
