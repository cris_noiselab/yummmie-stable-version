//
//  NLTimeLineViewCell.m
//  Yummmie
//
//  Created by bot on 6/16/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLTimeLineViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "UIColor+NLColor.h"
#import "NLConstants.h"
#import "NLComment.h"
#import "NLUser.h"
#import "NLYum.h"
#import "NLVenue.h"

static const CGFloat kCommentLabelX = 30.0f;
static const CGFloat kCommentLabelYOffset = 15.0f;
static const CGFloat kLikeFaceFadeInAnimationTime = 0.40f;
static const CGFloat kLikeFaceFadeOutAnimationTime = 0.25f;
static const CGFloat kLikeFaceWidth = 104.0f;
static const CGFloat kLikeFaceHeight = 104.0f;

static const CGFloat kRecommendHeight = 36.0f;

//static const CGFloat kLikeFaceFadeInAnimationTime = 4.40f;
//static const CGFloat kLikeFaceFadeOutAnimationTime = 4.25f;

static NSRegularExpression *mentionExpression;
static NSRegularExpression *hashtagExpression;

//static NSArray *_imageColors;

@interface NLTimeLineViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *usernameButton;
@property (weak, nonatomic) IBOutlet UIButton *dishButton;
@property (weak, nonatomic) IBOutlet UIButton *venueButton;
@property (weak, nonatomic) IBOutlet UIButton *likesButton;
@property (strong, nonatomic) TTTAttributedLabel *commentLabel;
@property (strong, nonatomic) TTTAttributedLabel *hastagLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIImageView *likeFace;

@property (strong, nonatomic) UIButton *moreCommentsButton;
@property (strong, nonatomic) UIView *firstCommentView;
@property (strong, nonatomic) UIButton *firstCommentUserPhoto;
@property (strong, nonatomic) UIButton *firstCommentUserNameButton;
@property (strong, nonatomic) TTTAttributedLabel *firstCommentLabel;


@property (strong, nonatomic) UIView *secondCommentView;
@property (strong, nonatomic) UIButton *secondCommentUserPhoto;
@property (strong, nonatomic) UIButton *secondCommentUserNameButton;
@property (strong, nonatomic) TTTAttributedLabel *secondCommentLabel;

@property (nonatomic,getter = isAnimating) BOOL animating;

@property (nonatomic, strong) CALayer *moreCommentsStripe;
@property (nonatomic, strong) CALayer *firstCommentStripe;
@property (nonatomic, strong) CALayer *secondCommentStripe;
@property (weak, nonatomic) IBOutlet UIButton *markFav;


- (IBAction)profileThumbAction:(id)sender;
- (IBAction)usernameAction:(id)sender;
- (IBAction)dishAction:(id)sender;
- (IBAction)venueAction:(id)sender;
- (IBAction)likeAction:(id)sender;
- (IBAction)favAction:(UIButton *)sender;

- (IBAction)commentAction:(id)sender;
- (IBAction)moreAction:(id)sender;
- (IBAction)openLikers:(id)sender;

- (void)changeLikeValue:(BOOL)newValue;
- (void)changeCommentsNumber:(NSInteger)newValue;
- (void)changeLikesNumber:(NSInteger)newValue;

- (void)doubleTapHandler:(UITapGestureRecognizer *)gesture;
- (void)tapHandler:(UITapGestureRecognizer *)gesture;

- (void)moreCommentsButtonAction:(id)sender;
- (void)firstCommentUsernameButtonTouched:(id)sender;
- (void)secondCommentUsernameButtonTouched:(id)sender;
- (void)firstCommentUserPhotoTouched:(id)sender;
- (void)secondCommentUserPhotoTouched:(id)sender;

@end

@implementation NLTimeLineViewCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        _liked = NO;
			_fav = false;
			[self.markFav setSelected:self.fav];

        _numberOfComments = 0;
        _numberOfLikes = 0;
        _animating = NO;
        [self addObserver:self forKeyPath:@"numberOfComments" options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:@"liked" options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:@"numberOfLikes" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)awakeFromNib
{
	[self.markFav setSelected:self.fav];
	self.recommendConstraint.constant = 0;
	self.recommendView.alpha = 0;
    // Initialization code
    [self.dishButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    [self.dishButton.titleLabel setNumberOfLines:1];
    [self.dishButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.dishButton.titleLabel setMinimumScaleFactor:0.6f];
    
    [self.dishButton.titleLabel setNumberOfLines:0];
    [self.dishButton.titleLabel setLineBreakMode:UILineBreakModeWordWrap];
    
    
    [self.venueButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    [self.venueButton.titleLabel setNumberOfLines:1];
    [self.venueButton.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.venueButton.titleLabel setMinimumScaleFactor:0.6f];
    
    [self.profileThumbButton setClipsToBounds:YES];
    
    
    //setup comment label
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    //self.commentLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(kCommentLabelX, 0, 265.0f, 35.0f)];
    self.commentLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(kCommentLabelX, 0,rootView.frame.size.width-kCommentLabelX, 35.0f)];
    
    UIColor *commentColor = [UIColor nl_colorWith255Red:89.0f green:92.0f blue:105.0f andAlpha:255.0f];
    
    [self.commentLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.commentLabel setMinimumScaleFactor:0.5f];
    [self.commentLabel setNumberOfLines:0];
    [self.commentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.commentLabel setTextColor:commentColor];
    [self.commentLabel adjustsFontSizeToFitWidth];
    [self.commentLabel setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    
    NSDictionary *linkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self commentLabel] setLinkAttributes:linkAttributes];

    NSDictionary *activeLinkAttributes = @{(id)kCTForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    [[self commentLabel] setActiveLinkAttributes:activeLinkAttributes];
    NSDictionary *inactiveLinkAttributes = @{(id)kCTForegroundColorAttributeName:[UIColor blackColor]};
    [[self commentLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    
    [[self commentLabel] setDelegate:self];
    [self.commentContent addSubview:self.commentLabel];
    
    //----
    self.hastagLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(kCommentLabelX, 0,rootView.frame.size.width-kCommentLabelX, 35.0f)];
    [self.hastagLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.hastagLabel setMinimumScaleFactor:0.5f];
    [self.hastagLabel setNumberOfLines:0];
    [self.hastagLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    [self.hastagLabel setTextColor:commentColor];
    [self.hastagLabel adjustsFontSizeToFitWidth];
    [self.hastagLabel setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    [[self hastagLabel] setLinkAttributes:linkAttributes];
    [[self hastagLabel] setActiveLinkAttributes:activeLinkAttributes];
    [[self hastagLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    
    [[self hastagLabel] setDelegate:self];
    [self.hashtagContent addSubview:self.hastagLabel];
    //----
    
    self.moreCommentsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.commentLabel.frame), rootView.frame.size.width, kTimeLineCellMoreCommentsButtonHeight)];
    [self.moreCommentsButton setTitle:@"21 Comments" forState:UIControlStateNormal];
    [self.moreCommentsButton setTitleColor:[UIColor nl_applicationLightGrayColor] forState:UIControlStateNormal];
    [self.moreCommentsButton setTitleColor:[UIColor nl_applicationGrayColor] forState:UIControlStateHighlighted];
    [[self.moreCommentsButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.moreCommentsButton setImage:[UIImage imageNamed:@"moreComments"] forState:UIControlStateNormal];
    [self.moreCommentsButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self.moreCommentsButton setContentEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
    [self.moreCommentsButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [self.moreCommentsButton addTarget:self action:@selector(moreCommentsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	
    self.moreCommentsStripe = [CALayer layer];
    [self.moreCommentsStripe setFrame:CGRectMake(0, 0, rootView.frame.size.width, 0.5)];
    [self.moreCommentsStripe setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1].CGColor];
    [self.moreCommentsButton.layer addSublayer:self.moreCommentsStripe];
		 
    
//    [self.moreCommentsButton setBackgroundColor:[UIColor greenColor]];
    [self.contentView addSubview:self.moreCommentsButton];
    
    self.firstCommentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.moreCommentsButton.frame.origin.y+CGRectGetHeight(self.moreCommentsButton.bounds), rootView.frame.size.width, kTImeLineCellCommentHeight)];
    
    self.firstCommentUserPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.firstCommentUserPhoto setFrame:CGRectMake(10, 10, 35, 35)];
    self.firstCommentUserPhoto.layer.cornerRadius = self.firstCommentUserPhoto.bounds.size.height/2.0f;
    self.firstCommentUserPhoto.layer.masksToBounds = YES;
    self.firstCommentUserPhoto.clipsToBounds = YES;
    [self.firstCommentUserPhoto addTarget:self action:@selector(firstCommentUserPhotoTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    self.firstCommentUserNameButton = [[UIButton alloc] initWithFrame:CGRectMake(53, 5, 150, 20)];
    [self.firstCommentUserNameButton setTitle:@"@primero" forState:UIControlStateNormal];
    [[self.firstCommentUserNameButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
//    [self.firstCommentUserNameButton setBackgroundColor:[UIColor redColor]];
    [self.firstCommentUserNameButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.firstCommentUserNameButton setTitleColor:[UIColor nl_applicationGrayColor] forState:UIControlStateHighlighted];
    [self.firstCommentUserNameButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self.firstCommentUserNameButton addTarget:self action:@selector(firstCommentUsernameButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    self.firstCommentLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(self.firstCommentUserNameButton.frame.origin.x,self.firstCommentUserNameButton.frame.origin.y+self.firstCommentUserNameButton.frame.size.height, 260, kTimeLineCellMaxCommentLabelHeight)];
    
    [self.firstCommentLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [self.firstCommentLabel setTextColor:[UIColor nl_colorWith255Red:89.0f green:92.0f blue:105.0f andAlpha:255.0f]];
    [self.firstCommentLabel setNumberOfLines:2];

    [[self firstCommentLabel] setLinkAttributes:linkAttributes];
    [[self firstCommentLabel] setActiveLinkAttributes:activeLinkAttributes];
    [[self firstCommentLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    [[self firstCommentLabel] setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    [[self firstCommentLabel] setDelegate:self];
    [self.firstCommentLabel setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
//    [self.firstCommentLabel setBackgroundColor:[UIColor greenColor]];
    
    [self.firstCommentView addSubview:self.firstCommentLabel];
    
    [self.firstCommentView addSubview:self.firstCommentUserPhoto];
    [self.firstCommentView addSubview:self.firstCommentUserNameButton];
    
    self.firstCommentStripe = [CALayer layer];
    [self.firstCommentStripe setFrame:self.moreCommentsStripe.frame];
    [self.firstCommentStripe setBackgroundColor:[UIColor colorWithRed:0.827 green:0.827 blue:0.827 alpha:1].CGColor];
    [self.firstCommentView.layer addSublayer:self.firstCommentStripe];
    
    [self.firstCommentView setClipsToBounds:YES];
    [self.firstCommentView setOpaque:YES];
    
    [self.contentView addSubview:self.firstCommentView];
//    [[self firstCommentLabel] setBackgroundColor:[UIColor redColor]];
    
    
    CGRect secondCommentFrame = self.firstCommentView.frame;
    secondCommentFrame.origin.y += CGRectGetHeight(self.firstCommentView.bounds)+10;
    self.secondCommentView = [[UIView alloc] initWithFrame:secondCommentFrame];

    
    self.secondCommentUserPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.secondCommentUserPhoto setFrame:self.firstCommentUserPhoto.frame];
    self.secondCommentUserPhoto.layer.cornerRadius = self.secondCommentUserPhoto.bounds.size.height/2.0f;
    self.secondCommentUserPhoto.layer.masksToBounds = YES;
    self.secondCommentUserPhoto.clipsToBounds = YES;
    [self.secondCommentUserPhoto addTarget:self action:@selector(secondCommentUserPhotoTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.secondCommentUserNameButton = [[UIButton alloc] initWithFrame:self.firstCommentUserNameButton.frame];
    [self.secondCommentUserNameButton setTitle:@"@segundo" forState:UIControlStateNormal];
    [[self.secondCommentUserNameButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    [self.secondCommentUserNameButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.secondCommentUserNameButton setTitleColor:[UIColor nl_applicationGrayColor] forState:UIControlStateHighlighted];
    [self.secondCommentView addSubview:self.secondCommentUserPhoto];
    [self.secondCommentView addSubview:self.secondCommentUserNameButton];
    [self.secondCommentUserNameButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self.secondCommentUserNameButton addTarget:self action:@selector(secondCommentUsernameButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    self.secondCommentLabel = [[TTTAttributedLabel alloc] initWithFrame:self.firstCommentLabel.frame];
    [self.secondCommentView addSubview:self.secondCommentLabel];
    [self.secondCommentLabel setTextColor:[UIColor nl_colorWith255Red:89.0f green:92.0f blue:105.0f andAlpha:255.0f]];
    [self.secondCommentLabel setNumberOfLines:2];
    [self.secondCommentLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
//    [self.secondCommentLabel setBackgroundColor:[UIColor yellowColor]];
    
    self.secondCommentStripe = [CALayer layer];
    [self.secondCommentStripe setFrame:self.moreCommentsStripe.frame];
    [self.secondCommentStripe setBackgroundColor:[UIColor colorWithRed:0.827 green:0.827 blue:0.827 alpha:1].CGColor];
    [self.secondCommentView.layer addSublayer:self.secondCommentStripe];
    
    [[self secondCommentLabel] setLinkAttributes:linkAttributes];
    [[self secondCommentLabel] setActiveLinkAttributes:activeLinkAttributes];
    [[self secondCommentLabel] setInactiveLinkAttributes:inactiveLinkAttributes];
    [[self secondCommentLabel] setEnabledTextCheckingTypes:NSTextCheckingTypeLink];
    [[self secondCommentLabel] setDelegate:self];
    
    
    [self.secondCommentView setClipsToBounds:YES];
    [self.secondCommentView setOpaque:YES];
    
    [self.contentView addSubview:self.secondCommentView];
    
    [self.likesButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, 0.0f)];
    [self.commentsButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, 0.0f)];

    [[self.commentsButton titleLabel] setAdjustsFontSizeToFitWidth:YES];
    [self.moreButton setTitleEdgeInsets:UIEdgeInsetsMake(-6.0f, 2.0f, 0.0f, 0.0f)];
    
    
    self.likeFace.image = [UIImage imageNamed:@"caralike"];
    self.likeFace.center = CGPointMake(CGRectGetMidX(self.dishPhoto.bounds), CGRectGetMidY(self.dishPhoto.bounds));
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    [singleTap setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapHandler:)];
    [doubleTap setNumberOfTapsRequired:2];

    
    [[self dishPhoto] addGestureRecognizer:singleTap];
    [[self dishPhoto] addGestureRecognizer:doubleTap];
    
    [singleTap requireGestureRecognizerToFail:doubleTap];
    [singleTap setDelaysTouchesBegan:NO];
    
    //setup transforms
    [self.profileThumbButton setFrame:CGRectMake(self.profileThumbButton.frame.origin.x, self.profileThumbButton.frame.origin.y, 30.0f, 30.0f)];
    CGFloat width = rootView.frame.size.width;
    self.profileThumbButton.layer.cornerRadius =  self.profileThumbButton.frame.size.height/2.0f;
    
    
    self.profileThumbButton.layer.masksToBounds = YES;
    
//    self.likeFace.image = [[UIImage imageNamed:@"caralike"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    self.likeFace.tintColor = [UIColor redColor];
    
    self.indicator = [[NLProgressIndicator alloc] init];
    self.indicator.center = CGPointMake(width/2,width/2);
    [self.indicator setProgress:0.0f];
    [self.dishPhoto insertSubview:self.indicator atIndex:[[self subviews] count]];
    
    [self.moreCommentsButton setHidden:YES];
    [self.firstCommentView setHidden:YES];
    [self.secondCommentView setHidden:YES];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)prepareForReuse
{
	
	self.yumId = 0;
	self.recommendView.alpha = 0;
	self.recommendConstraint.constant = 0;
	[super prepareForReuse];
	self.animating = NO;
	[self.indicator setProgress:0.0001f];
	[[self dishPhoto] setImage:nil];
	[[self indicator] setAlpha:0.0f];
	[[self moreCommentsButton] setHidden:YES];
	[[self firstCommentView] setHidden:YES];
	[[self secondCommentView] setHidden:YES];
	[self setUserInteractionEnabled:true];
	
	[self setNumberOfComments:0];
	[self setLiked:0];
	[self setNumberOfLikes:0];
	[self setNumberOfFavs:0];
	[self.repostButton setSelected:0];
	[self setNumberOfReposts:0];
	[self.numberOfFavsButton setTitle:@"" forState:UIControlStateNormal];
	[self.numberOfRepostsButton setTitle:@"" forState:UIControlStateNormal];
	
	[self.numbersOfViews setSelected:0];
	[self setViewCount:0];
	[self.numbersOfViews setTitle:@"" forState:UIControlStateNormal];

	
	[self.numberOfLikesButton setTitle:@"" forState:UIControlStateNormal];

	
	[self.numberOfFavsButton setTitle:@"" forState:UIControlStateSelected];
	[self.numberOfRepostsButton setTitle:@"" forState:UIControlStateSelected];
	[self.numberOfLikesButton setTitle:@"" forState:UIControlStateSelected];
	

	
	[self setUsername:@""];
	[self setVenueName:@""];
	
	[self setDishName:@""];
	
	
}
-(void)fillCellFromYum:(NLYum *)yum
{
	/*
	[self setYumId:yum.yumId];
	
	
	if (yum.venue.venueType == NLPrivateVenue)
	{
		[self setVenueTextColor:[UIColor colorWithRed:0.890 green:0.506 blue:0.537 alpha:1]];
		
	}else
	{
		[self setVenueTextColor:[UIColor nl_colorWith255Red:219 green:19 blue:36 andAlpha:255]];
	}
	[[self profileThumbButton] sd_setBackgroundImageWithURL:[NSURL URLWithString:[[yum user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
	
	
	[self setNumberOfComments:[yum numberOfComments]];
	[self setLiked:[yum userLikes]];
	[self setNumberOfLikes:[yum likes]];
	[self setNumberOfFavs:[yum favs]];
	[self.repostButton setSelected:yum.isReposted];
	[self setNumberOfReposts:[yum reposts]];
	[self.numberOfFavsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum favs]] forState:UIControlStateNormal];
	//[self setFav:yum.isFav];
	[self.numberOfRepostsButton setTitle:[NSString stringWithFormat:@"%ld", (long)[yum reposts]] forState:UIControlStateNormal];
	
	[self setUsername:[NSString stringWithFormat:@"@%@",[[yum user] username]]];
	[self setVenueName:[[yum venue] name]];

	[self setDishName:(!yum.name)?@"Default Name":yum.name];
	if (yum.repost)
	{
		[self setRecommend:yum.repost];
		
	}
	[self setPublishedTime:[yum createdDict]];
	
	[self changeFavsNumber:yum.favs selfFav:yum.isFav];
*/
}
-(void)setFavourite:(BOOL)fav
{
	//self.fav = fav;
	//[self.numberOfFavsButton setTitle: [NSString stringWithFormat:@"%d", self.numberOfFavs +  1] forState:UIControlStateNormal];
	//[self.markFav setSelected:fav];
}
- (void)setRecommend:(NSDictionary *)repost
{
	self.recommendConstraint.constant = kRecommendHeight;
	self.recommendView.alpha = 1;
	self.recommendUser.text = [NSString stringWithFormat:@"@%@", [[repost objectForKey:@"user"] objectForKey:@"username"]];
}
- (void)setPublishedTime:(NSDictionary *)created
{
	if ([[created objectForKey:@"is_month"] boolValue] == true)
	{
		self.dateView.alpha = 1;
		//self.diffTimeView.alpha = 0;
		self.createdMonth.text = NSLocalizedString([[created objectForKey:@"month"] uppercaseString], nil);
		self.createdYear.text = [NSString stringWithFormat:@"%ld", [[created objectForKey:@"year"] integerValue]];

	}
	else
	{
		self.dateView.alpha = 0;
		//self.diffTimeView.alpha = 1;
		NSInteger value = [[created objectForKey:@"value"] integerValue];
		
		NSString *unidad = NSLocalizedString([created objectForKey:@"units"], nil);

		if (value > 1 && ![[created objectForKey:@"units"] isEqualToString:@"minutes"] && ![[created objectForKey:@"units"] isEqualToString:@"seconds"])
		{
			unidad = [NSString stringWithFormat:@"%@S",NSLocalizedString([created objectForKey:@"units"], nil)];

		}
		NSString *text = [NSString stringWithFormat:@"%ld %@", (long)value, unidad];
		NSMutableAttributedString *timeText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld %@", (long)value, unidad] attributes:@{}];
		
		NSRange range = [text rangeOfString:unidad];
		[timeText addAttribute:NSFontAttributeName  value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:10] range:NSMakeRange(0, text.length - 1)];
		
		[timeText addAttribute:NSFontAttributeName  value:[UIFont fontWithName:@"HelveticaNeue" size:9] range:range];

		//[timeText addAttribute:NSFontAttributeName  value:[UIFont fontWithName:@"Helvetica-Medium" size:10.0] range:range];
		
		self.diffTimeUnityLbl.attributedText = timeText;
		if (![[created objectForKey:@"units"] isEqualToString:@"minutes"] && ![[created objectForKey:@"units"] isEqualToString:@"seconds"])
		{
			
		}
		else
		{
			
		}
		
		[self.diffTimeUnityLbl adjustsFontSizeToFitWidth];

	}
}

- (void)setDishName:(NSString *)name
{
    [[self dishButton] setTitle:name forState:UIControlStateNormal];
}
- (void)setDishName:(NSString *)dishName withSize:(CGSize)size
{
    
    [self.dishButton setTitle:dishName forState:UIControlStateNormal];
    
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    
    
    
    CGFloat topInset = 0.0f;
    if (size.height<20){
        topInset=0.0f;
    }
    if (size.height>37){
        topInset=-18.0f;
    }
    if (size.height>58){
        topInset=-36.0f;
    }
    
    
     [self.dishButton  setImageEdgeInsets:UIEdgeInsetsMake(topInset, 0, 0, 0)];
    /*
    CGRect newCommentFrame = CGRectZero;
    //newCommentFrame.origin.x = kCommentLabelX;
    newCommentFrame.origin.y = 4;
    newCommentFrame.size = size;
    NSLog(@"Dish: %@, x:%f y:%f  width:%f  height:%f",dishName,newCommentFrame.origin.x,newCommentFrame.origin.y,newCommentFrame.size.width,newCommentFrame.size.height);
    //newCommentFrame.size.width = self.commentLabel.frame.size.width-5;
    newCommentFrame.size.width = rootView.frame.size.width-30;
    [self.dishNameLeftConstraint setConstant:10];
    [self.dishButton setFrame:newCommentFrame];
     */
}


-(void)setDishConstraint:(CGFloat)constant{
    [self.dishNameConstraint setConstant:constant];
}

- (void)setVenueName:(NSString *)name
{
    if ([name containsString:@"Home"])
    {
        [[self venueButton] setTitle:@"Home" forState:UIControlStateNormal];
        [[self venueButton] setTintColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1]];
        [[self venueButton] setTitleColor:[UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:0.5] forState:UIControlStateNormal];
			
        [self.venueButton setUserInteractionEnabled:false];
    }
    else
    {
        [[self venueButton] setTitle:name forState:UIControlStateNormal];
        [self.venueButton setUserInteractionEnabled:true];


    }
}

- (void)setUsername:(NSString *)username
{
    [[self usernameButton] setTitle:username forState:UIControlStateNormal];
}

- (void)setVenueTextColor:(UIColor *)color
{
    [[self venueButton] setTitleColor:color forState:UIControlStateNormal];
    [[self venueButton] setTintColor:color];
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"numberOfComments" context:nil];
    [self removeObserver:self forKeyPath:@"liked" context:nil];
    [self removeObserver:self forKeyPath:@"numberOfLikes" context:nil];
}


#pragma mark - Actions

- (IBAction)profileThumbAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openProfileWithTag:)])
    {
        [[self delegate] openProfileWithTag:[self tag]];
			[self setUserInteractionEnabled:false];

    }
}

- (IBAction)usernameAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openProfileWithTag:)])
    {
        [[self delegate] openProfileWithTag:[self tag]];
			[self setUserInteractionEnabled:false];

    }
}

- (IBAction)dishAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openDishWithTag:)])
    {
        [[self delegate] openDishWithTag:[self tag]];
			[self setUserInteractionEnabled:false];

    }
}

- (IBAction)venueAction:(UIButton *)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openVenueWithTag:)])
    {
        [[self delegate] openVenueWithTag:[self tag]];
			[self setUserInteractionEnabled:false];
    }
}

- (IBAction)likeAction:(id)sender
{
	NSInteger numberOfLikes = self.numberOfLikes;
	if ([self liked])
	{
		self.numberOfLikes--;
	}
	else
	{
		self.numberOfLikes++;
	}
	[self setLiked:![self liked]];
	
	if ([self delegate] && [[self delegate] respondsToSelector:@selector(updateLikeWithTag:value:andNumberOfLikes:)])
	{
		//self.likesButton.userInteractionEnabled = false;
		[[self delegate] updateLikeWithTag:[self tag] value:[self liked] andNumberOfLikes:numberOfLikes];
	}
}

- (IBAction)favAction:(id)sender
{
	//[self setFavourite:![self fav]];

	if ([self delegate] && [[self delegate] respondsToSelector:@selector(updateFavWithTag:)])
	{


		[[self delegate] updateFavWithTag:[self tag]];
	}
}
- (IBAction)commentAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openCommentsWithTag:)])
    {
        [[self delegate] openCommentsWithTag:[self tag]];
    }
}
- (IBAction)moreAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openOptionsWithTag:)])
    {
        [[self delegate] openOptionsWithTag:[self tag]];
    }
}

- (IBAction)openLikers:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openLikers:)])
    {
        [[self delegate] openLikers:[self tag]];

    }
}
- (IBAction)openFavers:(id)sender
{
	if ([self delegate] && [[self delegate] respondsToSelector:@selector(openLikers:)])
	{
		[[self delegate] openFavers:[self tag]];
		
	}
}
- (IBAction)repost:(id)sender
{
	if ([self delegate] && [[self delegate] respondsToSelector:@selector(updateRepostWithTag:andNumberOfLikes:)])
	{

		[[self delegate] updateRepostWithTag:[self tag] andNumberOfLikes:[self numberOfLikes]];
	}
}

- (IBAction)openReposters:(id)sender
{
	if ([self delegate] && [[self delegate] respondsToSelector:@selector(openLikers:)])
	{
		[[self delegate] openReposters:[self tag]];
		
	}
}
- (IBAction)openRecommender:(id)sender
{
	[[self delegate] openRecommender:[self tag]];
}

#pragma mark - UIGestures Handlers

- (void)doubleTapHandler:(UITapGestureRecognizer *)gesture
{
    if (![self isAnimating])
    {
        self.animating = YES;
        self.likeFace.center = CGPointMake(CGRectGetMidX(self.dishPhoto.bounds), CGRectGetMidY(self.dishPhoto.bounds)+self.dishPhoto.frame.origin.y);
        
        [UIView animateWithDuration:kLikeFaceFadeInAnimationTime delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGRect faceRect = self.likeFace.frame;
            faceRect.size = CGSizeMake(kLikeFaceWidth*1.1, kLikeFaceHeight*1.1);
            faceRect.origin = CGPointMake((self.dishPhoto.bounds.size.width-faceRect.size.width)/2-0, (self.dishPhoto.bounds.size.height-faceRect.size.height)/2.0f+self.dishPhoto.frame.origin.y);
            self.likeFace.frame = faceRect;
            [self.likeFace setAlpha:1.0f];
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:kLikeFaceFadeOutAnimationTime delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.likeFace setFrame:CGRectMake((self.dishPhoto.bounds.size.width-kLikeFaceWidth)/2.0f, (self.dishPhoto.bounds.size.height-kLikeFaceHeight)/2.0f+self.dishPhoto.frame.origin.y, kLikeFaceWidth, kLikeFaceHeight)];
                [self.likeFace setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self setAnimating:NO];
            }];
        }];
        if (![self liked])
        {
            [self setLiked:YES];
            [self setNumberOfLikes:([self numberOfLikes]+1)];
            //notify that we like something
            if ([self delegate] && [[self delegate] respondsToSelector:@selector(updateLikeWithTag:value:andNumberOfLikes:)])
            {
                [[self delegate] updateLikeWithTag:[self tag] value:[self liked] andNumberOfLikes:[self numberOfLikes]];
            }
        }
        
    }
}

- (void)tapHandler:(UITapGestureRecognizer *)gesture
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openPhotoWithTag:)])
    {
        [[self delegate] openPhotoWithTag:[self tag]];
    }
}

#pragma mark - Properties Changed Methods

- (void)changeLikeValue:(BOOL)newValue
{
    if (![self liked])
    {
        //turn it black
        [[self likesButton] setTintColor:[UIColor blackColor]];
    }else
    {
        //turn it red
        [[self likesButton] setTintColor:[UIColor nl_colorWith255Red:219.0f green:19.0f blue:36.0f andAlpha:255.0f]];
    }
}

- (void)changeCommentsNumber:(NSInteger)newValue
{
    [self.commentsButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateNormal];
}

- (void)changeLikesNumber:(NSInteger)newValue
{
    //[self.likesButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateNormal];
	
	[self.numberOfLikesButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateNormal];
	[self.numberOfLikesButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateSelected];

}
- (void)changeFavsNumber:(NSInteger)newValue selfFav:(BOOL)selfFav
{
	//[self.likesButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateNormal];
	[self.numberOfFavsButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateNormal];
	[self.numberOfFavsButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateSelected];
	[self.markFav setSelected:selfFav];
}

- (void)changeRepostsNumber:(NSInteger)newValue
{
	//[self.likesButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateNormal];
	[self.numberOfRepostsButton setTitle:[NSString stringWithFormat:@"%ld",(long)newValue] forState:UIControlStateNormal];
}

#pragma mark - set comment

- (void)setComment:(NSString *)comment withSize:(CGSize)size
{
    self.commentLabel.text = comment;
    
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        mentionExpression = [NSRegularExpression regularExpressionWithPattern:@"@{1}([-A-Za-z0-9_]{2,})" options:NO error:nil];
        hashtagExpression = [NSRegularExpression regularExpressionWithPattern:@"[##]+([A-Za-z0-9-_ñNáÁÉéíÍóÓúÓüÜ]+)" options:NO error:nil];
    });
    
    NSArray *matches;
    NSRange range;
    
    @try
    {
        range = NSMakeRange(0, [comment length]);
        matches = [mentionExpression matchesInString:comment
                                                      options:0
                                                        range:range];
    }
    @catch (NSException *exception)
    {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
    }
    @finally {
        
    }


    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [comment substringWithRange:matchRange];
        NSString* user = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
        [self.commentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }

    
    @try
    {
        matches = [hashtagExpression matchesInString:comment
                                             options:0
                                               range:NSMakeRange(0, [comment length])];
    }
    @catch (NSException *exception) {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
    }
    @finally {
        
    }

    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [comment substringWithRange:matchRange];
        NSString* hashtag = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self.commentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGRect newCommentFrame = CGRectZero;
    newCommentFrame.origin.x = kCommentLabelX;
    newCommentFrame.origin.y = self.venueButton.frame.origin.y+self.venueButton.frame.size.height+kCommentLabelYOffset;
    newCommentFrame.size = size;
    //newCommentFrame.size.width = self.commentLabel.frame.size.width-5;
    newCommentFrame.size.width = rootView.frame.size.width-kCommentLabelX-10;
    [self.commentLabel setFrame:newCommentFrame];
    
    //update comment button
    
    CGRect buttonFrame = self.moreCommentsButton.frame;
    
    buttonFrame.origin.y = self.commentLabel.frame.origin.y+self.commentLabel.frame.size.height+kTimeLineCellMoreButtonTopOffset;
    
    [self.moreCommentsButton setFrame:buttonFrame];
    
    //update first comment View
    
    CGRect firstCommentViewFrame = self.firstCommentView.frame;
    
    firstCommentViewFrame.origin.y = self.moreCommentsButton.frame.origin.y+CGRectGetHeight(self.moreCommentsButton.bounds)+kTimeLineCellCommentSeparation;
    
    [self.firstCommentView setFrame:firstCommentViewFrame];
    
    //update second commetn view
    
    CGRect secondCommentViewFrame = self.secondCommentView.frame;
    
    secondCommentViewFrame.origin.y = self.firstCommentView.frame.origin.y+CGRectGetHeight(self.firstCommentView.bounds)+kTimeLineCellCommentSeparation;
    [self.secondCommentView setFrame:secondCommentViewFrame];
}

- (void)setComment:(NSString *)comment withSize:(CGSize)size andComments:(NSArray *)comments
{
    
    __block NSString *comment1, *comment2;
    
    if (comments.count == 1)
    {
        comment1 = [[comments lastObject] content];
    }else if (comments.count > 1)
    {
        comment2 = [[comments lastObject] content];
        comment1 = [[comments objectAtIndex:([comments count]-2)] content];
    }
    
    self.commentLabel.text = comment;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        mentionExpression = [NSRegularExpression regularExpressionWithPattern:@"@{1}([-A-Za-z0-9_]{2,})" options:NO error:nil];
        hashtagExpression = [NSRegularExpression regularExpressionWithPattern:@"[##]+([A-Za-z0-9-_ñNáÁÉéíÍóÓúÓüÜ]+)" options:NO error:nil];
    });
    
    NSArray *matches;
    NSRange range;
    
    @try
    {
        range = NSMakeRange(0, [comment length]);
        matches = [mentionExpression matchesInString:comment
                                             options:0
                                               range:range];
    }
    @catch (NSException *exception)
    {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
    }
    @finally {
        
    }
    
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [comment substringWithRange:matchRange];
        NSString* user = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
        [self.commentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    
    @try
    {
        matches = [hashtagExpression matchesInString:comment
                                             options:0
                                               range:NSMakeRange(0, [comment length])];
    }
    @catch (NSException *exception) {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
    }
    @finally {
        
    }
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [comment substringWithRange:matchRange];
        NSString* hashtag = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self.commentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGRect newCommentFrame = CGRectZero;
    newCommentFrame.origin.x = kCommentLabelX;
    newCommentFrame.origin.y = 4;
    newCommentFrame.size = size;
    //newCommentFrame.size.width = self.commentLabel.frame.size.width-5;
    newCommentFrame.size.width = rootView.frame.size.width-kCommentLabelX-10;
	self.commentConstraint.constant = size.height + 8;
    [self.commentLabel setFrame:newCommentFrame];
    
    if (comments.count > 0)
    {
        //update comment button
        if (comments.count == 1)
        {
            BOOL bigComment = (comment1.length > 42);
            
            [self.moreCommentsButton setHidden:YES];
            [self.secondCommentView setHidden:YES];
            [self.firstCommentView setHidden:NO];
            
            CGRect firstCommentViewFrame = self.firstCommentView.frame;
            CGRect firstCommentLabelFrame = self.firstCommentLabel.frame;
            
            firstCommentViewFrame.origin.y = self.commentLabel.frame.origin.y+self.commentLabel.frame.size.height+kTimeLineCellMoreButtonTopOffset;
            
            NLComment *comment = [comments firstObject];
            [self.firstCommentUserPhoto sd_setBackgroundImageWithURL:[NSURL URLWithString:[[comment user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [self.firstCommentUserNameButton setTitle:[NSString stringWithFormat:@"@%@",comment.user.username] forState:UIControlStateNormal];
            [self.firstCommentLabel setText:comment.content];

            if (!bigComment)
            {
                firstCommentViewFrame.size.height=kTImeLineCellMinimumCommentHeight;
                firstCommentLabelFrame.size.height = kTimeLineCellMinCommentLabelHeight;
                firstCommentLabelFrame.origin.y = self.firstCommentUserNameButton.frame.origin.y+self.firstCommentUserNameButton.frame.size.height;
            }else
            {
                firstCommentViewFrame.size.height = kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2;
                firstCommentLabelFrame.size.height = kTimeLineCellMaxCommentLabelHeight;
                firstCommentLabelFrame.origin.y = self.firstCommentUserNameButton.frame.origin.y+self.firstCommentUserNameButton.frame.size.height+kTimelineCellBigCommentOffsetY;
            }
            
            [self.firstCommentView setFrame:firstCommentViewFrame];
            [self.firstCommentLabel setFrame:firstCommentLabelFrame];
            
            @try
            {
                range = NSMakeRange(0, [comment.content length]);
                matches = [mentionExpression matchesInString:comment.content
                                                     options:0
                                                       range:range];
            }
            @catch (NSException *exception)
            {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [comment.content substringWithRange:matchRange];
                NSString* user = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
                [self.firstCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            @try
            {
                matches = [hashtagExpression matchesInString:comment.content
                                                     options:0
                                                       range:NSMakeRange(0, [comment.content length])];
            }
            @catch (NSException *exception) {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [comment.content substringWithRange:matchRange];
                NSString* hashtag = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                [self.firstCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            
        }else if(comments.count == 2)
        {
            [self.moreCommentsButton setHidden:YES];
            [self.firstCommentView setHidden:NO];
            [self.secondCommentView setHidden:NO];

            BOOL firstCommentBig = (comment1.length > 42);
            BOOL secondCommentBig = (comment2.length > 42);
            
            //update first comment View
            
            CGRect firstCommentViewFrame = self.firstCommentView.frame;
            CGRect firstCommentLabelFrame = self.firstCommentLabel.frame;
            
            firstCommentViewFrame.origin.y = self.commentLabel.frame.origin.y+self.commentLabel.frame.size.height+kTimeLineCellMoreButtonTopOffset;
            
            if (firstCommentBig)
            {
                //arreglar el frame
                firstCommentViewFrame.size.height = kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2;
                firstCommentLabelFrame.size.height = kTimeLineCellMaxCommentLabelHeight;
                firstCommentLabelFrame.origin.y = self.firstCommentUserNameButton.frame.origin.y+self.firstCommentUserNameButton.frame.size.height+kTimelineCellBigCommentOffsetY;
            }else
            {
                //setear el frame
                firstCommentLabelFrame.size.height = kTimeLineCellMinCommentLabelHeight;
                firstCommentViewFrame.size.height = kTImeLineCellMinimumCommentHeight;
                firstCommentLabelFrame.origin.y = self.firstCommentUserNameButton.frame.origin.y+self.firstCommentUserNameButton.frame.size.height;
            }
            
            [self.firstCommentView setFrame:firstCommentViewFrame];
            [self.firstCommentLabel setFrame:firstCommentLabelFrame];
            
            CGRect secondCommentViewFrame = self.secondCommentView.frame;
            CGRect secondCommentLabelFrame = self.secondCommentLabel.frame;
            
            secondCommentViewFrame.origin.y = self.firstCommentView.frame.origin.y+CGRectGetHeight(self.firstCommentView.bounds)+kTimeLineCellCommentSeparation;
            
            if (secondCommentBig)
            {
                secondCommentViewFrame.size.height = kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2;
                secondCommentLabelFrame.size.height = kTimeLineCellMaxCommentLabelHeight;
                secondCommentLabelFrame.origin.y = self.secondCommentUserNameButton.frame.origin.y+self.secondCommentUserNameButton.frame.size.height+kTimelineCellBigCommentOffsetY;
            }else
            {
                secondCommentViewFrame.size.height = kTImeLineCellMinimumCommentHeight;
                secondCommentLabelFrame.size.height = kTimeLineCellMinCommentLabelHeight;
                secondCommentLabelFrame.origin.y = self.secondCommentUserNameButton.frame.origin.y+self.secondCommentUserNameButton.frame.size.height;
            }
            
            [self.secondCommentView setFrame:secondCommentViewFrame];
            [self.secondCommentLabel setFrame:secondCommentLabelFrame];
            
            NLComment *firstComment = [comments firstObject];
            [self.firstCommentUserPhoto sd_setBackgroundImageWithURL:[NSURL URLWithString:[[firstComment user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [self.firstCommentUserNameButton setTitle:[NSString stringWithFormat:@"@%@",firstComment.user.username] forState:UIControlStateNormal];
            [self.firstCommentLabel setText:firstComment.content];
            
            NLComment *secondComment = [comments lastObject];
            [self.secondCommentUserPhoto sd_setBackgroundImageWithURL:[NSURL URLWithString:[[secondComment user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [self.secondCommentUserNameButton setTitle:[NSString stringWithFormat:@"@%@",secondComment.user.username] forState:UIControlStateNormal];
            [self.secondCommentLabel setText:secondComment.content];
            
            @try
            {
                range = NSMakeRange(0, [firstComment.content length]);
                matches = [mentionExpression matchesInString:firstComment.content
                                                     options:0
                                                       range:range];
            }
            @catch (NSException *exception)
            {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",firstComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [firstComment.content substringWithRange:matchRange];
                NSString* user = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
                [self.firstCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            @try
            {
                matches = [hashtagExpression matchesInString:firstComment.content
                                                     options:0
                                                       range:NSMakeRange(0, [firstComment.content length])];
            }
            @catch (NSException *exception) {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",firstComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [firstComment.content substringWithRange:matchRange];
                NSString* hashtag = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                [self.firstCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            @try
            {
                range = NSMakeRange(0, [secondComment.content length]);
                matches = [mentionExpression matchesInString:secondComment.content
                                                     options:0
                                                       range:range];
            }
            @catch (NSException *exception)
            {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",secondComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [secondComment.content substringWithRange:matchRange];
                NSString* user = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
                [self.secondCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            @try
            {
                matches = [hashtagExpression matchesInString:secondComment.content
                                                     options:0
                                                       range:NSMakeRange(0, [secondComment.content length])];
            }
            @catch (NSException *exception) {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",secondComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [secondComment.content substringWithRange:matchRange];
                NSString* hashtag = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                [self.secondCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
        }else
        {
            
            BOOL firstCommentBig = (comment1.length > 42);
            BOOL secondCommentBig = (comment2.length > 42);
            
            NSInteger total = comments.count-2;
            if (total == 1)
            {
                [[self moreCommentsButton] setTitle:@"1 more comment" forState:UIControlStateNormal];
            }else
            {
                [[self moreCommentsButton] setTitle:[NSString stringWithFormat:@"%ld more comments",(long)total] forState:UIControlStateNormal];
            }
            
            [self.moreCommentsButton setHidden:NO];
            [self.firstCommentView setHidden:NO];
            [self.secondCommentView setHidden:NO];
            
            CGRect buttonFrame = self.moreCommentsButton.frame;
            
            buttonFrame.origin.y = self.commentLabel.frame.origin.y+self.commentLabel.frame.size.height+kTimeLineCellMoreButtonTopOffset;
            
            [self.moreCommentsButton setFrame:buttonFrame];
            //update first comment View
            
            CGRect firstCommentViewFrame = self.firstCommentView.frame;
            CGRect firstCommentLabelFrame = self.firstCommentLabel.frame;
            
            firstCommentViewFrame.origin.y = self.moreCommentsButton.frame.origin.y+CGRectGetHeight(self.moreCommentsButton.bounds);
            
            if (firstCommentBig)
            {
                //arreglar el frame
                firstCommentViewFrame.size.height = kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2;
                firstCommentLabelFrame.size.height = kTimeLineCellMaxCommentLabelHeight;
                firstCommentLabelFrame.origin.y = self.firstCommentUserNameButton.frame.origin.y+self.firstCommentUserNameButton.frame.size.height+kTimelineCellBigCommentOffsetY;
            }else
            {
                //setear el frame
                firstCommentLabelFrame.size.height = kTimeLineCellMinCommentLabelHeight;
                firstCommentViewFrame.size.height = kTImeLineCellMinimumCommentHeight;
                firstCommentLabelFrame.origin.y = self.firstCommentUserNameButton.frame.origin.y+self.firstCommentUserNameButton.frame.size.height;
            }
            
            [self.firstCommentView setFrame:firstCommentViewFrame];
            [self.firstCommentLabel setFrame:firstCommentLabelFrame];
            
            CGRect secondCommentViewFrame = self.secondCommentView.frame;
            CGRect secondCommentLabelFrame = self.secondCommentLabel.frame;
            
            secondCommentViewFrame.origin.y = self.firstCommentView.frame.origin.y+CGRectGetHeight(self.firstCommentView.bounds)+kTimeLineCellCommentSeparation;
            
            if (secondCommentBig)
            {
                secondCommentViewFrame.size.height = kTImeLineCellCommentHeight+kTimelineCellBigCommentOffsetY*2;
                secondCommentLabelFrame.size.height = kTimeLineCellMaxCommentLabelHeight;
                secondCommentLabelFrame.origin.y = self.secondCommentUserNameButton.frame.origin.y+self.secondCommentUserNameButton.frame.size.height+kTimelineCellBigCommentOffsetY;
            }else
            {
                secondCommentViewFrame.size.height = kTImeLineCellMinimumCommentHeight;
                secondCommentLabelFrame.size.height = kTimeLineCellMinCommentLabelHeight;
                secondCommentLabelFrame.origin.y = self.secondCommentUserNameButton.frame.origin.y+self.secondCommentUserNameButton.frame.size.height;
            }
//            [self.secondCommentView setBackgroundColor:[UIColor purpleColor]];
            [self.secondCommentView setFrame:secondCommentViewFrame];
            [self.secondCommentLabel setFrame:secondCommentLabelFrame];
            
            NLComment *firstComment = [comments objectAtIndex:comments.count-2];
            [self.firstCommentUserPhoto sd_setBackgroundImageWithURL:[NSURL URLWithString:[[firstComment user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [self.firstCommentUserNameButton setTitle:[NSString stringWithFormat:@"@%@",firstComment.user.username] forState:UIControlStateNormal];
            [self.firstCommentLabel setText:firstComment.content];
            
            NLComment *secondComment = [comments lastObject];
            [self.secondCommentUserPhoto sd_setBackgroundImageWithURL:[NSURL URLWithString:[[secondComment user] thumbAvatar]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_lista"]];
            [self.secondCommentUserNameButton setTitle:[NSString stringWithFormat:@"@%@",secondComment.user.username] forState:UIControlStateNormal];
            [self.secondCommentLabel setText:secondComment.content];
            
            @try
            {
                range = NSMakeRange(0, [firstComment.content length]);
                matches = [mentionExpression matchesInString:firstComment.content
                                                     options:0
                                                       range:range];
            }
            @catch (NSException *exception)
            {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",firstComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [firstComment.content substringWithRange:matchRange];
                NSString* user = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
                [self.firstCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            @try
            {
                matches = [hashtagExpression matchesInString:firstComment.content
                                                     options:0
                                                       range:NSMakeRange(0, [firstComment.content length])];
            }
            @catch (NSException *exception) {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",firstComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [firstComment.content substringWithRange:matchRange];
                NSString* hashtag = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                [self.firstCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            @try
            {
                range = NSMakeRange(0, [secondComment.content length]);
                matches = [mentionExpression matchesInString:secondComment.content
                                                     options:0
                                                       range:range];
            }
            @catch (NSException *exception)
            {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",secondComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [secondComment.content substringWithRange:matchRange];
                NSString* user = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
                [self.secondCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
            
            
            @try
            {
                matches = [hashtagExpression matchesInString:secondComment.content
                                                     options:0
                                                       range:NSMakeRange(0, [secondComment.content length])];
            }
            @catch (NSException *exception) {
                //MVP Crashlytics
                //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",secondComment.content,NSStringFromRange(range));
            }
            @finally {
                
            }
            
            for (NSTextCheckingResult *match in matches)
            {
                NSRange matchRange = [match rangeAtIndex:0];
                NSString *mentionString = [secondComment.content substringWithRange:matchRange];
                NSString* hashtag = [mentionString substringFromIndex:1];
                NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                [self.secondCommentLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
            }
        }

    }else
    {
        [self.moreCommentsButton setHidden:YES];
        [self.firstCommentView setHidden:YES];
        [self.secondCommentView setHidden:YES];
    }
}

- (void)setHashTags:(NSString *)region withSubRegion:(NSString *)subregion andFoodType:(NSString *)foodtype{
    
    NSString *hashTags = [[NSString alloc] initWithFormat:@"#%@ #%@ #%@",region,subregion,foodtype];
    [self.hastagLabel setText:hashTags];
    
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        mentionExpression = [NSRegularExpression regularExpressionWithPattern:@"@{1}([-A-Za-z0-9_]{2,})" options:NO error:nil];
        hashtagExpression = [NSRegularExpression regularExpressionWithPattern:@"[##]+([A-Za-z0-9-_ñNáÁÉéíÍóÓúÓüÜ]+)" options:NO error:nil];
    });
    
    NSArray *matches;
    NSRange range;
    
    @try
    {
        range = NSMakeRange(0, [hashTags length]);
        matches = [mentionExpression matchesInString:hashTags
                                             options:0
                                               range:range];
    }
    @catch (NSException *exception)
    {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting user regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
    }
    @finally {
        
    }
    
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [hashTags substringWithRange:matchRange];
        NSString* user = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kUsernameScheme,user];
        [self.hastagLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    
    
    @try
    {
        matches = [hashtagExpression matchesInString:hashTags
                                             options:0
                                               range:NSMakeRange(0, [hashTags length])];
    }
    @catch (NSException *exception) {
        //MVP Crashlytics
        //CLSLog(@"Crashes getting hashtags regex with comment :%@ and range : %@",comment,NSStringFromRange(range));
    }
    @finally {
        
    }
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        NSString *mentionString = [hashTags substringWithRange:matchRange];
        NSString* hashtag = [mentionString substringFromIndex:1];
        NSString* linkURLString = [NSString stringWithFormat:@"%@:%@",kHashtagScheme,[hashtag stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [self.hastagLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:matchRange];
    }
    UIView *rootView = [[[UIApplication sharedApplication] keyWindow]rootViewController].view;
    CGRect newCommentFrame = CGRectZero;
    newCommentFrame.origin.x = kCommentLabelX;
    newCommentFrame.origin.y = self.venueButton.frame.origin.y+self.venueButton.frame.size.height+kCommentLabelYOffset;
    //newCommentFrame.size.width = self.commentLabel.frame.size.width-5;
    newCommentFrame.size.width = rootView.frame.size.width-kCommentLabelX-10;
    //[self.hastagLabel setFrame:newCommentFrame];
    
    //update comment button
    
    CGRect buttonFrame = self.moreCommentsButton.frame;
    
    buttonFrame.origin.y = self.hastagLabel.frame.origin.y+self.hastagLabel.frame.size.height+kTimeLineCellMoreButtonTopOffset;
    
    [self.moreCommentsButton setFrame:buttonFrame];
    
    //update first comment View
    
    CGRect firstCommentViewFrame = self.firstCommentView.frame;
    
    firstCommentViewFrame.origin.y = self.moreCommentsButton.frame.origin.y+CGRectGetHeight(self.moreCommentsButton.bounds)+kTimeLineCellCommentSeparation;
    
    [self.firstCommentView setFrame:firstCommentViewFrame];
    
    //update second commetn view
    
    CGRect secondCommentViewFrame = self.secondCommentView.frame;
    
    secondCommentViewFrame.origin.y = self.firstCommentView.frame.origin.y+CGRectGetHeight(self.firstCommentView.bounds)+kTimeLineCellCommentSeparation;
    [self.secondCommentView setFrame:secondCommentViewFrame];

}
#pragma mark - comments methods

- (void)moreCommentsButtonAction:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openCommentsWithTag:)])
    {
        [[self delegate] openCommentsWithTag:[self tag]];
    }
}

- (void)firstCommentUsernameButtonTouched:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openFirstCommentUser:)])
    {
        [[self delegate] openFirstCommentUser:[self tag]];
    }
}
- (void)secondCommentUsernameButtonTouched:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openSecondCommentUser:)])
    {
        [[self delegate] openSecondCommentUser:[self tag]];
    }
}

- (void)firstCommentUserPhotoTouched:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openFirstCommentUser:)])
    {
        [[self delegate] openFirstCommentUser:[self tag]];
    }
}

- (void)secondCommentUserPhotoTouched:(id)sender
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(openSecondCommentUser:)])
    {
        [[self delegate] openSecondCommentUser:[self tag]];
    }
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"liked"])
    {
			[self changeLikeValue:[[change objectForKey:@"new"] boolValue]];
    }
	else if ([keyPath isEqualToString:@"faved"])
	{
		//[self changeFavValue:[[change objectForKey:@"new"] boolValue]];
	}
		else if ([keyPath isEqualToString:@"numberOfComments"])
    {
        [self changeCommentsNumber:[[change objectForKey:@"new"] integerValue]];
    }
		else if([keyPath isEqualToString:@"numberOfLikes"])
    {
        [self changeLikesNumber:[[change objectForKey:@"new"] integerValue]];
    }
		else if([keyPath isEqualToString:@"numberOfFavs"])
		{
			//[self changeFavsNumber:[[change objectForKey:@"new"] integerValue]];
		}

		else if([keyPath isEqualToString:@"numberOfReposts"])
		{
			[self changeRepostsNumber:[[change objectForKey:@"new"] integerValue]];
		}

}

#pragma mark - TTTAtributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:kUsernameScheme])
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(openUser:)])
        {
            [[self delegate] openUser:[url resourceSpecifier]];
        }
    }else if ([[url scheme] isEqualToString:kHashtagScheme])
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(openHashTag:withTag:)])
        {
            if (label == self.firstCommentLabel) {
                if ([[self delegate] respondsToSelector:@selector(openFirstCommentHashTag:andHashtag:)]) {
                    [[self delegate] openFirstCommentHashTag:[self tag] andHashtag:[[url resourceSpecifier] stringByRemovingPercentEncoding]];
                }
            }else if (label == self.secondCommentLabel){
                if ([[self delegate] respondsToSelector:@selector(openSecondCommentHashTag:andHashtag:)]) {
                    [[self delegate] openSecondCommentHashTag:[self tag] andHashtag:[[url resourceSpecifier] stringByRemovingPercentEncoding]];
                }
            }else{
                [[self delegate] openHashTag:[[url resourceSpecifier] stringByRemovingPercentEncoding] withTag:[self tag]];
            }
        }
    }
}

@end
