//
//  NLDishesViewController.h
//  Yummmie
//
//  Created by bot on 7/3/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"

@interface NLDishesCollectionViewController : NLBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *dishesCollection;

- (id)initWithDishes:(NSArray *)dishes;

@end
