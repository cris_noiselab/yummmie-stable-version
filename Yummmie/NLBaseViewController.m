//
//  NLBaseViewController.m
//  Yummmie
//
//  Created by bot on 6/2/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NLBaseViewController.h"
#import "AppDelegate.h"
#import "NLErrorMessageView.h"
#import "UIColor+NLColor.h"
#import "NLConstants.h"
#import "NLUser.h"
#import "NLSession.h"

@interface NLBaseViewController ()<UIAlertViewDelegate>

@property (nonatomic, strong, readwrite) NLErrorMessageView *errorMessageView;
@property (nonatomic, strong) UIView *loadingHud;
@property (nonatomic, strong) UIImageView *animationImage;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@end

@implementation NLBaseViewController
{
	UIBarButtonItem *loadingSpinner;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //setup the Navigation Bar
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    UIColor *barColor = [UIColor nl_colorWith255Red:205.0f green:36.0f blue:49.0f andAlpha:255.0f];
	[[[self navigationController] navigationBar] setBarTintColor:barColor];
    [[[self navigationController] navigationBar] setBarStyle:UIBarStyleBlack];
    //setup the Error Message View
    _errorMessageView = [[NLErrorMessageView alloc] init];
    [self.navigationController.view addSubview:self.errorMessageView];
    //[self showErrorMessage:@"Probando en el baseViewController linea 40" withDuration:1.0];
    //NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(prueba) userInfo:nil repeats:true];
    //timer;
    /*UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    logoView.image = [UIImage imageNamed:@"logo_navbar"];
    logoView.backgroundColor = [UIColor greenColor];
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    if (self.title == nil)
    {
        self.navigationItem.titleView = logoView;

    }*/
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
}
-(void)prueba
{
    [self showErrorMessage:@"Probando en el baseViewController linea 40" withDuration:1.0];
}
/*- (void)viewWillAppear:(BOOL)animated
{
    //Google Analytics
    [super viewWillAppear:animated];
    NSString *name = [NSString stringWithFormat:@"%@ View", self.title];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}*/
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    /*
    CGRect viewBounds = self.view.bounds;
    CGRect newSpinnerFrame = self.loadingHud.frame;
    newSpinnerFrame.origin = CGPointMake((viewBounds.size.width-newSpinnerFrame.size.width)/2, (viewBounds.size.height-newSpinnerFrame.size.height)/2);
    self.loadingHud.frame = newSpinnerFrame;
		 */
    
}

#pragma mark - Helpers

- (void)addRedBackground
{
    [[self view] setBackgroundColor:[UIColor nl_colorWith255Red:205.0f green:36.0f blue:49.0f andAlpha:255.0f]];
}

- (void)showLoadingSpinner
{
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.spinner startAnimating];
		loadingSpinner = [[UIBarButtonItem alloc] initWithCustomView:self.spinner];
	if (!self.navigationItem.rightBarButtonItem)
	{
		self.navigationItem.rightBarButtonItem = loadingSpinner;

	}
}

- (void)hideLoadingSpinner
{
    [UIView animateWithDuration:0.25 animations:^{
			
        self.spinner.alpha = 0.0f;
        [self.spinner stopAnimating];
    } completion:nil];

}

#pragma mark - Error Message Methods

-(void)showErrorMessage:(NSString *)message withDuration:(float)seconds
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self errorMessageView] showWithMessage:message andDuration:seconds];        
    });
}

- (void)showErrorMessage:(NSString *)message popViewController:(BOOL)pop withDuration:(float)seconds
{
    [[self errorMessageView] showWithMessage:message andDuration:seconds];
    
    __weak NLBaseViewController *weakSelf = self;
    
    if (self.navigationController && pop)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((seconds+1) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
        });
    }
}
#pragma mark - Errors Handling

- (NSString *)errorToString:(NSError *)error
{
    id message = [error.userInfo objectForKey:JSONResponseSerializerWithDataKey];
    
    NSString *errorMessage;
    
    if (message)
    {
			NSDictionary *errors = [message objectForKey:@"errors"];
			if (errors)
			{
				if ([errors objectForKey:kJSONUserEmailKey])
				{
					errorMessage = NSLocalizedString(@"email_error_key", nil);
				}
				else if ([errors objectForKey:kJSONUserUsernameKey])
				{
					errorMessage = NSLocalizedString(@"username_error_key", nil);
				}
				else
				{
					errorMessage = NSLocalizedString(@"field_error", nil);
				}
			}
			else
			{
				id statusError = [error.userInfo objectForKey:@"status"];
				if (statusError)
				{
					if ([statusError integerValue] >= 500)
					{
						errorMessage = NSLocalizedString(@"connection_error", nil);
					}else
					{
						errorMessage = NSLocalizedString(@"unprocesable_entity_error", nil);
					}
				}
			}
        /*if (!jsonError)
        {
            id statusError = [jsonResult objectForKey:@"status"];
            if (statusError)
            {
                if ([statusError integerValue] >= 500)
                {
                    errorMessage = NSLocalizedString(@"connection_error", nil);
                }else
                {
                    errorMessage = NSLocalizedString(@"unprocesable_entity_error", nil);
                }
            }else
            {
                if ([jsonResult objectForKey:kJSONUserEmailKey])
                {
                    errorMessage = NSLocalizedString(@"email_error_key", nil);
                }else if ([jsonResult objectForKey:kJSONUserUsernameKey])
                {
                    errorMessage = NSLocalizedString(@"username_error_key", nil);
                }else
                {
                    errorMessage = NSLocalizedString(@"field_error", nil);
                }
            }
        }else
        {
            //something unexpected just happened, so we tell the user it's a connection error
            errorMessage = NSLocalizedString(@"connection_error", nil);
        }*/
    }
		else
    {
        errorMessage = NSLocalizedString(@"connection_error", nil);
    }
    return errorMessage;
}



#pragma mark - Scroll To Top

- (void)scrollToTop
{
    CGRect frame = self.navigationController.navigationBar.frame;
    frame.origin.y = 20.0f;
    [self.navigationController.navigationBar setFrame:frame];
    self.navigationItem.titleView.alpha  = 1.0;
    
	
}
@end
