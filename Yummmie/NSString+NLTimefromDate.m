//
//  NSString+NLTimefromDate.m
//  Yummmie
//
//  Created by bot on 8/27/14.
//  Copyright (c) 2014 Noiselab Apps. All rights reserved.
//

#import "NSString+NLTimefromDate.h"

@implementation NSString (NLTimefromDate)
/*
+ (NSString *)nl_elapsedTimeSinceDate:(NSDate *)aDate
{

    NSDate *now = [NSDate date];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitMinute|NSCalendarUnitHour|NSCalendarUnitSecond fromDate:aDate toDate:now options:0];
    
    NSInteger days = [components day];
    NSInteger months = [components month];
    NSInteger hours = [components hour];
    NSInteger minutes = [components minute];
    NSInteger seconds = ([components second]<0)?[components second]*-1:[components second];
    
    NSString *timeSince;
    if (months > 0)
    {
        timeSince = [NSString stringWithFormat:@"%ldmo",(long)months];
    }else if(days > 0)
    {
        timeSince = [NSString stringWithFormat:@"%ldd",(long)days];
    }else if (hours > 0)
    {
        timeSince = [NSString stringWithFormat:@"%ldh",(long)hours];
    }else if (minutes > 0)
    {
        timeSince = [NSString stringWithFormat:@"%ldm",(long)minutes];
    }else if (seconds)
    {
        timeSince = [NSString stringWithFormat:@"%lds",(long)seconds];
    }else
    {
        timeSince = @"2s";
    }
    
    return timeSince;
	 
}*/
@end
